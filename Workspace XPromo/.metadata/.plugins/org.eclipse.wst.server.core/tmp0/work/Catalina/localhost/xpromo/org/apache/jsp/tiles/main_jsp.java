package org.apache.jsp.tiles;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class main_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(4);
    _jspx_dependants.add("/WEB-INF/struts-bean.tld");
    _jspx_dependants.add("/WEB-INF/struts-html.tld");
    _jspx_dependants.add("/WEB-INF/struts-logic.tld");
    _jspx_dependants.add("/WEB-INF/struts-tiles.tld");
  }

  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005flogic_005fpresent_0026_005fscope_005fname;
  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fc_005fif_0026_005ftest;
  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005flogic_005fredirect_0026_005fforward_005fnobody;
  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fhtml_005fhtml_0026_005flocale;
  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005ftiles_005fuseAttribute_0026_005fname_005fid_005fclassname_005fnobody;
  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fbean_005fwrite_0026_005fname_005fnobody;
  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005ftiles_005finsert_0026_005fattribute_005fnobody;
  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005ftiles_005finsert_0026_005fignore_005fattribute_005fnobody;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005flogic_005fpresent_0026_005fscope_005fname = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fc_005fif_0026_005ftest = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005flogic_005fredirect_0026_005fforward_005fnobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fhtml_005fhtml_0026_005flocale = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005ftiles_005fuseAttribute_0026_005fname_005fid_005fclassname_005fnobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fbean_005fwrite_0026_005fname_005fnobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005ftiles_005finsert_0026_005fattribute_005fnobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005ftiles_005finsert_0026_005fignore_005fattribute_005fnobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005flogic_005fpresent_0026_005fscope_005fname.release();
    _005fjspx_005ftagPool_005fc_005fif_0026_005ftest.release();
    _005fjspx_005ftagPool_005flogic_005fredirect_0026_005fforward_005fnobody.release();
    _005fjspx_005ftagPool_005fhtml_005fhtml_0026_005flocale.release();
    _005fjspx_005ftagPool_005ftiles_005fuseAttribute_0026_005fname_005fid_005fclassname_005fnobody.release();
    _005fjspx_005ftagPool_005fbean_005fwrite_0026_005fname_005fnobody.release();
    _005fjspx_005ftagPool_005ftiles_005finsert_0026_005fattribute_005fnobody.release();
    _005fjspx_005ftagPool_005ftiles_005finsert_0026_005fignore_005fattribute_005fnobody.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write(" \r\n");
      if (_jspx_meth_logic_005fpresent_005f0(_jspx_page_context))
        return;
      out.write('\r');
      out.write('\n');
      out.write(' ');
      //  html:html
      org.apache.struts.taglib.html.HtmlTag _jspx_th_html_005fhtml_005f0 = (org.apache.struts.taglib.html.HtmlTag) _005fjspx_005ftagPool_005fhtml_005fhtml_0026_005flocale.get(org.apache.struts.taglib.html.HtmlTag.class);
      _jspx_th_html_005fhtml_005f0.setPageContext(_jspx_page_context);
      _jspx_th_html_005fhtml_005f0.setParent(null);
      // /tiles/main.jsp(12,1) name = locale type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
      _jspx_th_html_005fhtml_005f0.setLocale(true);
      int _jspx_eval_html_005fhtml_005f0 = _jspx_th_html_005fhtml_005f0.doStartTag();
      if (_jspx_eval_html_005fhtml_005f0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write('\r');
          out.write('\n');
          //  tiles:useAttribute
          org.apache.struts.taglib.tiles.UseAttributeTag _jspx_th_tiles_005fuseAttribute_005f0 = (org.apache.struts.taglib.tiles.UseAttributeTag) _005fjspx_005ftagPool_005ftiles_005fuseAttribute_0026_005fname_005fid_005fclassname_005fnobody.get(org.apache.struts.taglib.tiles.UseAttributeTag.class);
          _jspx_th_tiles_005fuseAttribute_005f0.setPageContext(_jspx_page_context);
          _jspx_th_tiles_005fuseAttribute_005f0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_html_005fhtml_005f0);
          // /tiles/main.jsp(13,0) name = id type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
          _jspx_th_tiles_005fuseAttribute_005f0.setId("title");
          // /tiles/main.jsp(13,0) name = name type = null reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
          _jspx_th_tiles_005fuseAttribute_005f0.setName("title");
          // /tiles/main.jsp(13,0) name = classname type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
          _jspx_th_tiles_005fuseAttribute_005f0.setClassname("java.lang.String");
          int _jspx_eval_tiles_005fuseAttribute_005f0 = _jspx_th_tiles_005fuseAttribute_005f0.doStartTag();
          if (_jspx_th_tiles_005fuseAttribute_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
            _005fjspx_005ftagPool_005ftiles_005fuseAttribute_0026_005fname_005fid_005fclassname_005fnobody.reuse(_jspx_th_tiles_005fuseAttribute_005f0);
            return;
          }
          _005fjspx_005ftagPool_005ftiles_005fuseAttribute_0026_005fname_005fid_005fclassname_005fnobody.reuse(_jspx_th_tiles_005fuseAttribute_005f0);
          java.lang.String title = null;
          title = (java.lang.String) _jspx_page_context.findAttribute("title");
          out.write("\r\n");
          out.write("\r\n");
          out.write("<head>\r\n");
          out.write("<title>");
          if (_jspx_meth_bean_005fwrite_005f0(_jspx_th_html_005fhtml_005f0, _jspx_page_context))
            return;
          out.write("</title>\r\n");
          out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\r\n");
          out.write("<meta http-equiv=\"Pragma\" content=\"no-cache\">\r\n");
          out.write("<!-- Pragma content set to no-cache tells the browser not to cache the page\r\n");
          out.write("This may or may not work in IE --> \r\n");
          out.write("<meta http-equiv=\"expires\" content=\"0\">\r\n");
          out.write("<!-- Setting the page to expire at 0 means the page is immediately expired\r\n");
          out.write("Any vales less then one will set the page to expire some time in past and\r\n");
          out.write("not be cached. This may not work with Navigator -->\r\n");
          out.write("<link href=\"common/estilos_6.css\" rel=\"stylesheet\" type=\"text/css\">\r\n");
          out.write("<script language=\"JavaScript\">\r\n");
          out.write("\r\n");
          out.write("     function getPromos(control) {\r\n");
          out.write("        form = control.form;\r\n");
          out.write("        form.action = \"ListPromoXSucursal.do?sucursal=\";\r\n");
          out.write("        form.action += control.value;\r\n");
          out.write("        form.submit();\r\n");
          out.write("     }\r\n");
          out.write("     function getSendPromos(control) {\r\n");
          out.write("        form = control.form;\r\n");
          out.write("        form.action = \"SendPromo.do?methodToCall=changeSucursal\";\r\n");
          out.write("        //form.action += control.value;\r\n");
          out.write("        form.submit();\r\n");
          out.write("     }\r\n");
          out.write("     function openNewWindow(htmlElement){\r\n");
          out.write("     \twindow.open(\"\",\"Hola\",\"\");\r\n");
          out.write("     \tform = htmlElement.form;\r\n");
          out.write("     \tform.action=\"SendPromo.do?methodToCall=Analizar Competencias\";\r\n");
          out.write("     \tform.submit();\r\n");
          out.write("     }\r\n");
          out.write("     \r\n");
          out.write("     function confirmation(control) {\r\n");
          out.write("        form = control.form;\r\n");
          out.write("        var answer = confirm(\"Esta seguro de eliminar la promoción?\")\r\n");
          out.write("        if (answer){\r\n");
          out.write("           form.action = \"ListPromo.do?command=Eliminar\";\r\n");
          out.write("        }\r\n");
          out.write("        else{\r\n");
          out.write("           form.action = \"ListPromoPage.do?\";\r\n");
          out.write("       }\r\n");
          out.write("     }\r\n");
          out.write("\r\n");
          out.write("\t/**************************************************************\r\n");
          out.write("\t****** Funciones JavaScript acción Configurar Tipo Promo ****** \r\n");
          out.write("\t**************************************************************/\r\n");
          out.write("\r\n");
          out.write("\t/** funcion q se ejecuta al hacer clic en un radio y habilita el botón editar **/\r\n");
          out.write("\tfunction enbledControl(){\r\n");
          out.write("\t\t\r\n");
          out.write("\t\tif (document.typePromoForm.Edit != null)\r\n");
          out.write("\t    \tdocument.typePromoForm.Edit.disabled = false;\r\n");
          out.write("\t    if (document.typePromoForm.Update != null)\r\n");
          out.write("\t\t    document.typePromoForm.Update.disabled = true; \r\n");
          out.write("\t    if (document.typePromoForm.typePromo != null)\r\n");
          out.write("\t\t    document.typePromoForm.typePromo.value = \"\";\r\n");
          out.write("\t    if (document.typePromoForm.description != null){\r\n");
          out.write("\t\t    document.typePromoForm.description.value = \"\";\r\n");
          out.write("\t\t    document.typePromoForm.description.disabled = true;\r\n");
          out.write("\t\t}\r\n");
          out.write("\t\t\r\n");
          out.write("\t}\r\n");
          out.write("\r\n");
          out.write("\t/** funcion q se ejecuta al modificar un tipo de promocion  **/\t\r\n");
          out.write("\tfunction updateTypePromo(){\r\n");
          out.write("\t\t\r\n");
          out.write("\t\tdocument.typePromoForm.command.value = \"update\";\r\n");
          out.write("\t\tdocument.typePromoForm.submit();\r\n");
          out.write("\t\t\t\t\r\n");
          out.write("\t}\t\t\r\n");
          out.write("\t\r\n");
          out.write("\t/** funcion q se ejecuta al editar un tipo de promocion  **/\r\n");
          out.write("\tfunction editTypePromo(control){\r\n");
          out.write("\t\r\n");
          out.write("\t\tvar id;\r\n");
          out.write("\t\tfor (i=0;i<document.typePromoForm.typePromoSelected.length;i++){\r\n");
          out.write("       \t\tif (document.typePromoForm.typePromoSelected[i].checked){\r\n");
          out.write("       \t\t\tid = document.typePromoForm.typePromoSelected[i].value\r\n");
          out.write("          \t\tbreak;\r\n");
          out.write("          \t}\r\n");
          out.write("    \t} \t\t\r\n");
          out.write("\t    document.typePromoForm.typePromoSel.value = id;\r\n");
          out.write("\t\tdocument.typePromoForm.command.value = \"selectTypeForm\";\r\n");
          out.write("\t\tdocument.typePromoForm.submit();\r\n");
          out.write("\t\t\r\n");
          out.write("\t}\t\t\r\n");
          out.write("\t\r\n");
          out.write("\t/**************************************************************\r\n");
          out.write("\t******* Funciones JavaScript acción Asociar Promociones ******* \r\n");
          out.write("\t**************************************************************/\r\n");
          out.write("\t\r\n");
          out.write("\t/** Funcion q se ejecuta al seleccionar una promoción **/\r\n");
          out.write("\tfunction getPromosByBranch() {\r\n");
          out.write("\t\tdocument.prosucForm.command.value = \"searchPromoByBranch\";\r\n");
          out.write(" \t   \tdocument.prosucForm.submit();\r\n");
          out.write("\r\n");
          out.write("\t}\r\n");
          out.write("\t\r\n");
          out.write("\t/** Funcion q se ejecuta al cargar el form o al seleccionar una promoción **/\r\n");
          out.write("\tfunction getBranchByPromo() {\r\n");
          out.write("\t\tdocument.prosucForm.command.value = \"searchBranchByPromo\";\r\n");
          out.write(" \t   \tdocument.prosucForm.submit();\r\n");
          out.write("\r\n");
          out.write("\t}\r\n");
          out.write("    \r\n");
          out.write("    /** Funcion q se ejecuta cuando hacemos clic en el boton aceptar para registrar las asociasiones **/\r\n");
          out.write("\tfunction asociatePromoBranch(){\r\n");
          out.write("\t \t\r\n");
          out.write("\t \tdocument.prosucForm.command.value = \"asociatePromoBranch\";\r\n");
          out.write("\t \tdocument.prosucForm.submit();\r\n");
          out.write("\t \r\n");
          out.write("\t}\r\n");
          out.write("\t\r\n");
          out.write("\t/**\tFuncion q selecciona o deselecciona los check de un formulario **/\r\n");
          out.write("\tfunction checkUncheckAll(theElement) {\r\n");
          out.write("\t\r\n");
          out.write("    var theForm = theElement.form, i = 0;\r\n");
          out.write("\t \tfor(i = 0; i < theForm.length; i++){\r\n");
          out.write("\t \t\t\t// alert(theForm[i].name);\r\n");
          out.write("\t \t\t\tif (theForm[i].name != 'button.ftp.yes.or.no'){\r\n");
          out.write("      \t\tif(theForm[i].type == 'checkbox' && theForm[i].name != 'checkall'){\r\n");
          out.write("\t  \t\t\t\ttheForm[i].checked = theElement.checked;\r\n");
          out.write("\t  \t\t\t}\r\n");
          out.write("\t  \t\t}\t\r\n");
          out.write("     \t}\r\n");
          out.write("    }\r\n");
          out.write("\t\t\t\t\r\n");
          out.write("</script>\r\n");
          out.write("\r\n");
          out.write("</head>\r\n");
          out.write("<body onload=\"validateChecks()\">\r\n");
          out.write("<div align=center>\r\n");
          out.write("<script type=\"text/javascript\" src=\"scripts/wz_tooltip.js\"></script>\r\n");
          out.write("<script type=\"text/javascript\" src=\"scripts/funciones.js\"></script>\r\n");
          out.write("<table class=\"marco\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"760\">\r\n");
          out.write("  <tr>\r\n");
          out.write("    <th colspan=\"5\">");
          if (_jspx_meth_tiles_005finsert_005f0(_jspx_th_html_005fhtml_005f0, _jspx_page_context))
            return;
          out.write("</th>\r\n");
          out.write("  </tr>\r\n");
          out.write("  <tr>\r\n");
          out.write("    <td valign=\"top\" class=\"fondo_menu\" width=\"200\">");
          if (_jspx_meth_tiles_005finsert_005f1(_jspx_th_html_005fhtml_005f0, _jspx_page_context))
            return;
          out.write("</td>\r\n");
          out.write("    <td align=\"center\" valign=\"top\" width=\"560\">");
          if (_jspx_meth_tiles_005finsert_005f2(_jspx_th_html_005fhtml_005f0, _jspx_page_context))
            return;
          out.write("\r\n");
          out.write("    </td>\r\n");
          out.write("  </tr>\r\n");
          out.write("  <tr>\r\n");
          out.write("    <th colspan=\"5\">");
          if (_jspx_meth_tiles_005finsert_005f3(_jspx_th_html_005fhtml_005f0, _jspx_page_context))
            return;
          out.write("</th>\r\n");
          out.write("  </tr>\r\n");
          out.write("</table>\r\n");
          out.write("<DIV ID=\"testdiv1\"\r\n");
          out.write("  STYLE=\"position:absolute;visibility:hidden;background-color:white;layer-background-color:white;\"></DIV>\r\n");
          out.write("  </div>\r\n");
          out.write("</body>\r\n");
          int evalDoAfterBody = _jspx_th_html_005fhtml_005f0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_html_005fhtml_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _005fjspx_005ftagPool_005fhtml_005fhtml_0026_005flocale.reuse(_jspx_th_html_005fhtml_005f0);
        return;
      }
      _005fjspx_005ftagPool_005fhtml_005fhtml_0026_005flocale.reuse(_jspx_th_html_005fhtml_005f0);
      out.write('\r');
      out.write('\n');
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_logic_005fpresent_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  logic:present
    org.apache.struts.taglib.logic.PresentTag _jspx_th_logic_005fpresent_005f0 = (org.apache.struts.taglib.logic.PresentTag) _005fjspx_005ftagPool_005flogic_005fpresent_0026_005fscope_005fname.get(org.apache.struts.taglib.logic.PresentTag.class);
    _jspx_th_logic_005fpresent_005f0.setPageContext(_jspx_page_context);
    _jspx_th_logic_005fpresent_005f0.setParent(null);
    // /tiles/main.jsp(7,0) name = scope type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_logic_005fpresent_005f0.setScope("session");
    // /tiles/main.jsp(7,0) name = name type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_logic_005fpresent_005f0.setName("user");
    int _jspx_eval_logic_005fpresent_005f0 = _jspx_th_logic_005fpresent_005f0.doStartTag();
    if (_jspx_eval_logic_005fpresent_005f0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write('\r');
        out.write('\n');
        out.write('	');
        if (_jspx_meth_c_005fif_005f0(_jspx_th_logic_005fpresent_005f0, _jspx_page_context))
          return true;
        out.write('\r');
        out.write('\n');
        out.write(' ');
        int evalDoAfterBody = _jspx_th_logic_005fpresent_005f0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_logic_005fpresent_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005flogic_005fpresent_0026_005fscope_005fname.reuse(_jspx_th_logic_005fpresent_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005flogic_005fpresent_0026_005fscope_005fname.reuse(_jspx_th_logic_005fpresent_005f0);
    return false;
  }

  private boolean _jspx_meth_c_005fif_005f0(javax.servlet.jsp.tagext.JspTag _jspx_th_logic_005fpresent_005f0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_005fif_005f0 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _005fjspx_005ftagPool_005fc_005fif_0026_005ftest.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_005fif_005f0.setPageContext(_jspx_page_context);
    _jspx_th_c_005fif_005f0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_logic_005fpresent_005f0);
    // /tiles/main.jsp(8,1) name = test type = boolean reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_c_005fif_005f0.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${user.expired}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null, false)).booleanValue());
    int _jspx_eval_c_005fif_005f0 = _jspx_th_c_005fif_005f0.doStartTag();
    if (_jspx_eval_c_005fif_005f0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("\t\t");
        if (_jspx_meth_logic_005fredirect_005f0(_jspx_th_c_005fif_005f0, _jspx_page_context))
          return true;
        out.write('\r');
        out.write('\n');
        out.write('	');
        int evalDoAfterBody = _jspx_th_c_005fif_005f0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_005fif_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fc_005fif_0026_005ftest.reuse(_jspx_th_c_005fif_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fc_005fif_0026_005ftest.reuse(_jspx_th_c_005fif_005f0);
    return false;
  }

  private boolean _jspx_meth_logic_005fredirect_005f0(javax.servlet.jsp.tagext.JspTag _jspx_th_c_005fif_005f0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  logic:redirect
    org.apache.struts.taglib.logic.RedirectTag _jspx_th_logic_005fredirect_005f0 = (org.apache.struts.taglib.logic.RedirectTag) _005fjspx_005ftagPool_005flogic_005fredirect_0026_005fforward_005fnobody.get(org.apache.struts.taglib.logic.RedirectTag.class);
    _jspx_th_logic_005fredirect_005f0.setPageContext(_jspx_page_context);
    _jspx_th_logic_005fredirect_005f0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_005fif_005f0);
    // /tiles/main.jsp(9,2) name = forward type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_logic_005fredirect_005f0.setForward("logout");
    int _jspx_eval_logic_005fredirect_005f0 = _jspx_th_logic_005fredirect_005f0.doStartTag();
    if (_jspx_th_logic_005fredirect_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005flogic_005fredirect_0026_005fforward_005fnobody.reuse(_jspx_th_logic_005fredirect_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005flogic_005fredirect_0026_005fforward_005fnobody.reuse(_jspx_th_logic_005fredirect_005f0);
    return false;
  }

  private boolean _jspx_meth_bean_005fwrite_005f0(javax.servlet.jsp.tagext.JspTag _jspx_th_html_005fhtml_005f0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  bean:write
    org.apache.struts.taglib.bean.WriteTag _jspx_th_bean_005fwrite_005f0 = (org.apache.struts.taglib.bean.WriteTag) _005fjspx_005ftagPool_005fbean_005fwrite_0026_005fname_005fnobody.get(org.apache.struts.taglib.bean.WriteTag.class);
    _jspx_th_bean_005fwrite_005f0.setPageContext(_jspx_page_context);
    _jspx_th_bean_005fwrite_005f0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_html_005fhtml_005f0);
    // /tiles/main.jsp(16,7) name = name type = null reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_bean_005fwrite_005f0.setName("title");
    int _jspx_eval_bean_005fwrite_005f0 = _jspx_th_bean_005fwrite_005f0.doStartTag();
    if (_jspx_th_bean_005fwrite_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fbean_005fwrite_0026_005fname_005fnobody.reuse(_jspx_th_bean_005fwrite_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fbean_005fwrite_0026_005fname_005fnobody.reuse(_jspx_th_bean_005fwrite_005f0);
    return false;
  }

  private boolean _jspx_meth_tiles_005finsert_005f0(javax.servlet.jsp.tagext.JspTag _jspx_th_html_005fhtml_005f0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  tiles:insert
    org.apache.struts.taglib.tiles.InsertTag _jspx_th_tiles_005finsert_005f0 = (org.apache.struts.taglib.tiles.InsertTag) _005fjspx_005ftagPool_005ftiles_005finsert_0026_005fattribute_005fnobody.get(org.apache.struts.taglib.tiles.InsertTag.class);
    _jspx_th_tiles_005finsert_005f0.setPageContext(_jspx_page_context);
    _jspx_th_tiles_005finsert_005f0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_html_005fhtml_005f0);
    // /tiles/main.jsp(151,20) name = attribute type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_tiles_005finsert_005f0.setAttribute("header");
    int _jspx_eval_tiles_005finsert_005f0 = _jspx_th_tiles_005finsert_005f0.doStartTag();
    if (_jspx_th_tiles_005finsert_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005ftiles_005finsert_0026_005fattribute_005fnobody.reuse(_jspx_th_tiles_005finsert_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005ftiles_005finsert_0026_005fattribute_005fnobody.reuse(_jspx_th_tiles_005finsert_005f0);
    return false;
  }

  private boolean _jspx_meth_tiles_005finsert_005f1(javax.servlet.jsp.tagext.JspTag _jspx_th_html_005fhtml_005f0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  tiles:insert
    org.apache.struts.taglib.tiles.InsertTag _jspx_th_tiles_005finsert_005f1 = (org.apache.struts.taglib.tiles.InsertTag) _005fjspx_005ftagPool_005ftiles_005finsert_0026_005fattribute_005fnobody.get(org.apache.struts.taglib.tiles.InsertTag.class);
    _jspx_th_tiles_005finsert_005f1.setPageContext(_jspx_page_context);
    _jspx_th_tiles_005finsert_005f1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_html_005fhtml_005f0);
    // /tiles/main.jsp(154,52) name = attribute type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_tiles_005finsert_005f1.setAttribute("menu");
    int _jspx_eval_tiles_005finsert_005f1 = _jspx_th_tiles_005finsert_005f1.doStartTag();
    if (_jspx_th_tiles_005finsert_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005ftiles_005finsert_0026_005fattribute_005fnobody.reuse(_jspx_th_tiles_005finsert_005f1);
      return true;
    }
    _005fjspx_005ftagPool_005ftiles_005finsert_0026_005fattribute_005fnobody.reuse(_jspx_th_tiles_005finsert_005f1);
    return false;
  }

  private boolean _jspx_meth_tiles_005finsert_005f2(javax.servlet.jsp.tagext.JspTag _jspx_th_html_005fhtml_005f0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  tiles:insert
    org.apache.struts.taglib.tiles.InsertTag _jspx_th_tiles_005finsert_005f2 = (org.apache.struts.taglib.tiles.InsertTag) _005fjspx_005ftagPool_005ftiles_005finsert_0026_005fattribute_005fnobody.get(org.apache.struts.taglib.tiles.InsertTag.class);
    _jspx_th_tiles_005finsert_005f2.setPageContext(_jspx_page_context);
    _jspx_th_tiles_005finsert_005f2.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_html_005fhtml_005f0);
    // /tiles/main.jsp(156,48) name = attribute type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_tiles_005finsert_005f2.setAttribute("body");
    int _jspx_eval_tiles_005finsert_005f2 = _jspx_th_tiles_005finsert_005f2.doStartTag();
    if (_jspx_th_tiles_005finsert_005f2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005ftiles_005finsert_0026_005fattribute_005fnobody.reuse(_jspx_th_tiles_005finsert_005f2);
      return true;
    }
    _005fjspx_005ftagPool_005ftiles_005finsert_0026_005fattribute_005fnobody.reuse(_jspx_th_tiles_005finsert_005f2);
    return false;
  }

  private boolean _jspx_meth_tiles_005finsert_005f3(javax.servlet.jsp.tagext.JspTag _jspx_th_html_005fhtml_005f0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  tiles:insert
    org.apache.struts.taglib.tiles.InsertTag _jspx_th_tiles_005finsert_005f3 = (org.apache.struts.taglib.tiles.InsertTag) _005fjspx_005ftagPool_005ftiles_005finsert_0026_005fignore_005fattribute_005fnobody.get(org.apache.struts.taglib.tiles.InsertTag.class);
    _jspx_th_tiles_005finsert_005f3.setPageContext(_jspx_page_context);
    _jspx_th_tiles_005finsert_005f3.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_html_005fhtml_005f0);
    // /tiles/main.jsp(160,20) name = attribute type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_tiles_005finsert_005f3.setAttribute("footer");
    // /tiles/main.jsp(160,20) name = ignore type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_tiles_005finsert_005f3.setIgnore(true);
    int _jspx_eval_tiles_005finsert_005f3 = _jspx_th_tiles_005finsert_005f3.doStartTag();
    if (_jspx_th_tiles_005finsert_005f3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005ftiles_005finsert_0026_005fignore_005fattribute_005fnobody.reuse(_jspx_th_tiles_005finsert_005f3);
      return true;
    }
    _005fjspx_005ftagPool_005ftiles_005finsert_0026_005fignore_005fattribute_005fnobody.reuse(_jspx_th_tiles_005finsert_005f3);
    return false;
  }
}
