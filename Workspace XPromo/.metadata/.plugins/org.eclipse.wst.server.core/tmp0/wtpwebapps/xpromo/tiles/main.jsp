<%@ page language="java"%>
<%@ taglib prefix="bean" uri="/WEB-INF/struts-bean.tld"%>
<%@ taglib prefix="html" uri="/WEB-INF/struts-html.tld"%>
<%@ taglib prefix="logic" uri="/WEB-INF/struts-logic.tld"%>
<%@ taglib prefix="tiles" uri="/WEB-INF/struts-tiles.tld"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<logic:present scope="session" name="user">
	<c:if test="${user.expired}">
		<logic:redirect forward="logout" />
	</c:if>
 </logic:present>
 <html:html locale="true">
<tiles:useAttribute id="title" name="title" classname="java.lang.String" />

<head>
<title><bean:write name="title" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Pragma" content="no-cache">
<!-- Pragma content set to no-cache tells the browser not to cache the page
This may or may not work in IE --> 
<meta http-equiv="expires" content="0">
<!-- Setting the page to expire at 0 means the page is immediately expired
Any vales less then one will set the page to expire some time in past and
not be cached. This may not work with Navigator -->
<link href="common/estilos_6.css" rel="stylesheet" type="text/css">
<script language="JavaScript">

     function getPromos(control) {
        form = control.form;
        form.action = "ListPromoXSucursal.do?sucursal=";
        form.action += control.value;
        form.submit();
     }
     function getSendPromos(control) {
        form = control.form;
        form.action = "SendPromo.do?methodToCall=changeSucursal";
        //form.action += control.value;
        form.submit();
     }
     function openNewWindow(htmlElement){
     	window.open("","Hola","");
     	form = htmlElement.form;
     	form.action="SendPromo.do?methodToCall=Analizar Competencias";
     	form.submit();
     }
     
     function confirmation(control) {
���   � form = control.form;
����    var answer = confirm("Esta seguro de eliminar la promoci�n?")
   ���� if (answer){
           form.action = "ListPromo.do?command=Eliminar";
���   � }
   ���� else{
           form.action = "ListPromoPage.do?";
����   }
     }

	/**************************************************************
	****** Funciones JavaScript acci�n Configurar Tipo Promo ****** 
	**************************************************************/

	/** funcion q se ejecuta al hacer clic en un radio y habilita el bot�n editar **/
	function enbledControl(){
		
		if (document.typePromoForm.Edit != null)
	    	document.typePromoForm.Edit.disabled = false;
	    if (document.typePromoForm.Update != null)
		    document.typePromoForm.Update.disabled = true; 
	    if (document.typePromoForm.typePromo != null)
		    document.typePromoForm.typePromo.value = "";
	    if (document.typePromoForm.description != null){
		    document.typePromoForm.description.value = "";
		    document.typePromoForm.description.disabled = true;
		}
		
	}

	/** funcion q se ejecuta al modificar un tipo de promocion  **/	
	function updateTypePromo(){
		
		document.typePromoForm.command.value = "update";
		document.typePromoForm.submit();
				
	}		
	
	/** funcion q se ejecuta al editar un tipo de promocion  **/
	function editTypePromo(control){
	
		var id;
		for (i=0;i<document.typePromoForm.typePromoSelected.length;i++){
       		if (document.typePromoForm.typePromoSelected[i].checked){
       			id = document.typePromoForm.typePromoSelected[i].value
          		break;
          	}
    	} 		
	    document.typePromoForm.typePromoSel.value = id;
		document.typePromoForm.command.value = "selectTypeForm";
		document.typePromoForm.submit();
		
	}		
	
	/**************************************************************
	******* Funciones JavaScript acci�n Asociar Promociones ******* 
	**************************************************************/
	
	/** Funcion q se ejecuta al seleccionar una promoci�n **/
	function getPromosByBranch() {
		document.prosucForm.command.value = "searchPromoByBranch";
 	   	document.prosucForm.submit();

	}
	
	/** Funcion q se ejecuta al cargar el form o al seleccionar una promoci�n **/
	function getBranchByPromo() {
		document.prosucForm.command.value = "searchBranchByPromo";
 	   	document.prosucForm.submit();

	}
    
    /** Funcion q se ejecuta cuando hacemos clic en el boton aceptar para registrar las asociasiones **/
	function asociatePromoBranch(){
	 	
	 	document.prosucForm.command.value = "asociatePromoBranch";
	 	document.prosucForm.submit();
	 
	}
	
	/**	Funcion q selecciona o deselecciona los check de un formulario **/
	function checkUncheckAll(theElement) {
	
    var theForm = theElement.form, i = 0;
	 	for(i = 0; i < theForm.length; i++){
	 			// alert(theForm[i].name);
	 			if (theForm[i].name != 'button.ftp.yes.or.no'){
      		if(theForm[i].type == 'checkbox' && theForm[i].name != 'checkall'){
	  				theForm[i].checked = theElement.checked;
	  			}
	  		}	
     	}
    }
				
</script>

</head>
<body onload="validateChecks()">
<div align=center>
<script type="text/javascript" src="scripts/wz_tooltip.js"></script>
<script type="text/javascript" src="scripts/funciones.js"></script>
<table class="marco" border="0" cellpadding="0" cellspacing="0" width="760">
  <tr>
    <th colspan="5"><tiles:insert attribute="header" /></th>
  </tr>
  <tr>
    <td valign="top" class="fondo_menu" width="200"><tiles:insert
      attribute="menu" /></td>
    <td align="center" valign="top" width="560"><tiles:insert attribute="body" />
    </td>
  </tr>
  <tr>
    <th colspan="5"><tiles:insert attribute="footer" ignore="true" /></th>
  </tr>
</table>
<DIV ID="testdiv1"
  STYLE="position:absolute;visibility:hidden;background-color:white;layer-background-color:white;"></DIV>
  </div>
</body>
</html:html>
