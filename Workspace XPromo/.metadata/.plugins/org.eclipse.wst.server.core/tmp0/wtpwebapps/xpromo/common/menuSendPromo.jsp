<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-html" prefix="html" %>

<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<table>
<tr>
<td valign="top">
<html:link forward="initmainpromo"><bean:message key="app.home"/></html:link>
</td>
</tr>
<tr>
<td valign="top">
<logic:present name="user">
    <html:link forward="logout"><bean:message key="app.logoutuser"/></html:link>
</logic:present>
<logic:notPresent name="user">
   <html:link forward="login"><bean:message key="app.loginuser"/></html:link>
</logic:notPresent>
</td>
</tr>

</table>
