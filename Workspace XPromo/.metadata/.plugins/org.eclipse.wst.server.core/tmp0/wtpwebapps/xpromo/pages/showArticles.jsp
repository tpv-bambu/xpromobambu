<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html" prefix="html"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript">
	
		/*
	 	* Funcion q selecciona o deselecciona los check de un formulario.
	 	* @param theElement Check seleccionar o deselccionar todos.
	 	* @param chkName Check de la tabla q se debe aplicar la seleccion o desseleccion
	 	*/
		function checkUncheckAll(theElement, chkName) {
			
     	var theForm = theElement.form, i = 0;
	 		for(i = 0; i < theForm.length; i++){
	 				if (theForm[i].name == chkName){	 				
      			if(theForm[i].type == 'checkbox' && theForm[i].name != 'checkall')
		  				theForm[i].checked = theElement.checked;
	  			}
     	}
     	
    }
		  
	</script>

<html:form action="ChooseArticle.do" method="POST"
	enctype="multipart/form-data">
	<DIV ALIGN=CENTER>

	<table cellpadding="0" cellspacing="0" border="1" align="center"
		width="900">
		<tr>
			<td class="fondomodulos" valign="top">
			<blockquote><span class="subtitulo"><bean:message
				key="label.select.articles" /></span></blockquote>
			<hr class="linea" color="#006600" noshade="noshade" size="1">

			</td>
		</tr>
	</table>
	<div class="textowarning">
	<ul>
		<html:messages id="msg" property="errors" message="true">
			<li><bean:write name="msg" /></li>
		</html:messages>
	</ul>
	</div>
	<div class="textoconfirm">
	<ul>
		<html:messages id="msg" property="confirms" message="true">
			<li><bean:write name="msg" /></li>
		</html:messages>
	</ul>
	</div>
	<!-- <ul class="textowarning">
		<html:messages message="true" id="msg">
			<li><bean:write name="msg" /></li>
		</html:messages>
	</ul> -->

	<TABLE cellpadding="0" cellspacing="0" border="0" align="center"
		width="860">
		<tr>
			<td>
			<blockquote /><span class="subtitulo"> <bean:message
				key="subtitle.import.articles" /> </span>
			</td>
		</tr>
		<tr>
			<td>
			<blockquote /><html:file property="articlesToImport"
				styleClass="login" /> <html:submit property="methodToCall"
				styleClass="form">
				<bean:message key="button.import.articles" />
			</html:submit>
			</td>
		</tr>
		<tr>
			<td>
			<blockquote />
			<hr class="linea" color="#006600" noshade="noshade" size="1" />
			</td>
		</tr>
	</TABLE>

	<TABLE cellpadding="0" cellspacing="0" border="0" align="center"
		width="860">

		<TR>
			<TD valign="top" align="left">
			<blockquote />
			<TABLE>
				<tr>
					<td colspan="3" align="left"><span class="subtitulo"> <bean:message
						key="subtitle.browse.articles" /> </span></td>
				</tr>
				<TR>
					<td align="right"><bean:message key="label.filtrar.por" /></td>
					<td>
					<div align="right"><html:select name="findArticleForm"
						property="filterfield" styleClass="form">
						<html:option value="codbarra">
							<bean:message key="label.option.codbarra" />
						</html:option>
						<html:option value="codint">
							<bean:message key="label.option.codinterno" />
						</html:option>
						<html:option value="nombre">
							<bean:message key="label.option.nombre" />
						</html:option>
						<html:option value="marca">
							<bean:message key="label.option.marca" />
						</html:option>
					</html:select></div>
					</td>
					<td><html:submit property="methodToCall" styleClass="form">
						<bean:message key="button.find" />
					</html:submit></td>
				</TR>
				<tr>
					<td><bean:message key="label.desde" /></td>
					<td><html:text name="findArticleForm" property="beginvalue"
						size="15" styleClass="form" /></td>
				</tr>
				<tr>
					<td><bean:message key="label.hasta" /></td>
					<td><html:text name="findArticleForm" property="endvalue"
						size="15" styleClass="form" /></td>
				</tr>
			</TABLE>
			</TD>
			<TD></TD>
		</TR>
		<TR>
			<TD valign="top" align="center">

			<table class="marco" cellpadding="3" cellspacing="0" width="430"
				border="0">
				<tr class="cabeceratabla" align="center">
					<TD width="10%"></TD>
					<TD valign="top" align="left" width="20%"><bean:message
						key="label.column.codbarra" /></TD>
					<TD valign="top" align="right" width="15%"><bean:message
						key="label.column.codinterno" /></TD>
					<TD valign="top" align="center" width="35%"><bean:message
						key="label.column.nombre" /></TD>
					<TD valign="top" align="rigth" width="20%"><bean:message
						key="label.column.marca" /></TD>
				</tr>
			</table>
			<table class="marco" width="50%">
				<tr align="center">
					<td>
					<div align="left" style="width: 430; height: 300; overflow: auto;">
					<table class="marco" cellpadding="3" cellspacing="0" width="414"
						border="0">
						<c:forEach var="articuloselected" items="${findArticleForm.search}"
							varStatus="status">
							
								<TR>
									<TD valign="top" class="fondotabla" width="5%"><input
										id="chkArt-Selected" type="checkbox" name="art-selected"
										value="${status.index}"></TD>
									<TD valign="top" class="fondotabla" width="22%"><bean:write
										name="articuloselected" property="codBarra" /></TD>
									<TD valign="top" class="fondotabla" align="right" width="13%">
									<bean:write name="articuloselected" format="000000"
										property="codInterno" /></TD>
									<TD valign="top" class="fondotabla" width="40%"><bean:write
										name="articuloselected" property="description" /></TD>
									<TD valign="top" class="fondobotonera" align="right"
										width="20%"><bean:write name="articuloselected"
										property="marca" /></TD>
								</TR>
							
						</c:forEach>
					</table>
					</div>
					</td>
				</tr>
				<TR>
					<td colspan="5" class="fondobotonera" align="center"><!-- check select/deselect -->
					<input id="idCheckall1" type="checkbox" name="checkall"
						onclick="checkUncheckAll(this, 'art-selected');"
						value="<bean:message key="button.selectall"/>" /> <bean:message
						key="button.selectall" /> <html:submit property="methodToCall"
						styleClass="form">
						<bean:message key="button.add" />
					</html:submit></td>
				</TR>
			</table>
			</TD>
			<TD align="center" valign="top">

			<table class="marco" cellpadding="3" cellspacing="0" width="430"
				border="0">
				<tr class="cabeceratabla" align="center">
					<TD width="10%"></TD>
					<TD valign="top" align="left" width="20%"><bean:message
						key="label.column.codbarra" /></TD>
					<TD valign="top" align="center" width="15%"><bean:message
						key="label.column.codinterno" /></TD>
					<TD valign="top" align="center" width="35%"><bean:message
						key="label.column.nombre" /></TD>
					<TD valign="top" align="rigth" width="20%"><bean:message
						key="label.column.marca" /></TD>
				</tr>
			</table>
			<table class="marco" width="50%">
				<tr align="center">
					<td>
					<div align="left" style="width: 430; height: 300; overflow: auto;">
					<table class="marco" cellpadding="3" cellspacing="0" width="414"
						border="0">
						<c:forEach var="articuloselected" items="${findArticleForm.cart}"
							varStatus="status">
							
								<TR>
									<TD valign="top" class="fondotabla" width="5%"><input
										id="chkArticleInCart" type="checkbox" name="articleincart"
										value="${status.index}" /></TD>
									<TD valign=top class="fondotabla" width="22%"><bean:write
										name="articuloselected" property="codBarra" /></TD>
									<TD valign=top class="fondotabla" align="right" width="13%">
									<bean:write name="articuloselected" format="000000"
										property="codInterno" /></TD>
									<TD valign=top class="fondotabla" width="40%"><bean:write
										name="articuloselected" property="description" /></TD>
									<TD valign=top class="fondobotonera" align="right" width="20%"><bean:write
										name="articuloselected" property="marca" /></TD>
								</TR>
							
						</c:forEach>
					</table>
					</div>
					</td>
				</tr>
				<TR>
					<td colspan="5" class="fondobotonera" align="center"><!-- check select/deselect -->
					<input id="idCheckall" type="checkbox" name="checkall"
						onclick="checkUncheckAll(this, 'articleincart');"
						value="<bean:message key="button.selectall"/>" /> <bean:message
						key="button.selectall" /> <html:submit property="methodToCall"
						styleClass="form">
						<bean:message key="button.delete" />
					</html:submit></td>
				</TR>
			</table>
			</TD>
		</TR>
	</TABLE>
	</DIV>
	<div>
	<table>
		<tr>
			<td><html:submit property="methodToCall" styleClass="form">
				<bean:message key="button.accept" />
			</html:submit></td>
			<td><html:submit property="methodToCall" styleClass="form">
				<bean:message key="button.cancel" />
			</html:submit></td>
		</tr>
	</table>
	</div>
</html:form>
