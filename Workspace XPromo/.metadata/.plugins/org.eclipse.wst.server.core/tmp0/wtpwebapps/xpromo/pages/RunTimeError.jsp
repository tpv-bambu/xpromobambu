<%@ taglib uri="/WEB-INF/struts-bean" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html" prefix="html"%>
<%@ page isErrorPage="true"%>
<table cellpadding="0" cellspacing="0" align="center" width="570">
  <tr>
    <td align="center" class="textowarning">
    <ul>
      <html:messages message="true" id="msg">
        <li><bean:write name="msg" /></li>
      </html:messages>
    </ul>
    </td>
  </tr>
  <tr>
    <td>
      <H4><bean:message key="errors.general"/></h4>
    </td>
  </tr>
  
  
</table>

