<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib prefix="html"  uri="/WEB-INF/struts-html.tld" %>
<%@ taglib prefix="bean"  uri="/WEB-INF/struts-bean.tld" %>
<div align="center"><html:img page="/images/cabecera_1.jpg" alt="Invel Promociones" width="760" height="84"/><br></div>
<div> La aplicación no puede ejecutarse. Revise la siguiente lista de errores: </div>

<UL>
 <html:messages message="true" id="msg">
 <LI><bean:write name="msg"/></LI>
 </html:messages>
</UL>

<html:link href="InitMainPromo.do?reload=true">Re-intentar</html:link>
