<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html" prefix="html"%>
<table cellpadding="0" cellspacing="0" border="0" align="center"
	width="570">
	<tr>
		<td class="fondomodulos" valign="top">
		<blockquote>
		<div align="left" class="subtitulo"><br>
		<span class="subtitulo"> <bean:message
			key="label.promos.x.sucursal" /> </span>
		<hr class="linea" color="#006600" noshade="noshade" size="1">
		</div>
		</blockquote>
		</td>
	</tr>
	<tr>
		<td align="center" valign="middle" class="textowarning">
		<ul>
			<html:messages id="msg" property="errors" message="true">
				<bean:write name="msg" />
				<br>
			</html:messages>
		</ul>
		</td>
	</tr>
</table>
<html:form action="AsociarPromo.do" method="POST">
	<table>
		<tr>
			<td></td>
			<td align="left"><!-- tabla columnas de la grilla de promociones -->
			<table class="marco" cellpadding="3" cellspacing="0" width="538">
				<tr class="cabeceratabla" align="center">
					<TD width="5%"></TD>
					<TD align="center" width="10%"><bean:message
						key="label.column.cod" /></TD>
					<TD align="center" width="85%"><bean:message
						key="label.column.descripcion" /></TD>
				</tr>
			</table>
			<!-- tabla filas de la grilla -->
			<table class="marco" width="500">
				<tr>
					<td>
					<div align="left" style="width: 530; height: 300; overflow: auto;">
					<table class="marco" cellpadding="3" cellspacing="0" width="500">
						<!-- recorro las collección del form de la accion de las promociones -->
						<logic:iterate id="promoselected" name="prosucForm"
							property="promociones">
							<TR>
								<!-- columna checked -->
								<TD valign="top" class="fondotabla" width="5%"><input
									type="checkbox" name="art-selected"
									value="<bean:write name="promoselected" property="internalId"/>"
									<logic:equal name="promoselected" property="selected" value="true">
					                      				checked="checked"
					                   	 			</logic:equal> />
								</TD>
								<!-- columna id -->
								<TD valign=top align="right" class="fondotabla" width="10%">
								<bean:write name="promoselected" property="internalId" /></TD>
								<!-- columna descripciòn -->
								<TD valign=top align=left class="fondobotonera" width="85%">
								<bean:write name="promoselected" property="description" /></TD>
							</TR>
						</logic:iterate>
					</table>
					</div>
					</td>
				</tr>
			</table>
			</td>
			<td></td>
		</tr>
	</table>
	<P><html:submit property="methodToCall" styleClass="form">
		<bean:message key="button.accept" />
	</html:submit> <html:submit property="methodToCall" styleClass="form">
		<bean:message key="button.back" />
	</html:submit></P>
</html:form>
