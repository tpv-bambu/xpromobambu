<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html" prefix="html"%>
<table width=570>
  <tr>
    <td>
    <div align="center" class="subtitle">&nbsp;<B><bean:message key="title.descripcion.promocion" />: </B> <logic:present name="promoform">
      <bean:write name="promoform" property="description" />
    </logic:present></div>
    <div align=center class="textowarning"><html:messages message="true" id="msg">
      <b><bean:write name="msg" /></b><br />
    </html:messages></div>

    </td>
  </tr>
  <tr>
    <td align="center"><logic:present name="promoform">
      <html:textarea name="promoform" property="xml" rows="20" cols="60" />
    </logic:present></td>
  </tr>
</table>
