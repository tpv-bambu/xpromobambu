<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html" prefix="html"%>

<html:form action="ChooseDepto.do" method="POST">
  <DIV ALIGN=CENTER>

  <table cellpadding="0" cellspacing="0" border="0" align="center" width="570">
    <tr>
      <td class="fondomodulos" valign="top">
      <blockquote>
      <span class="subtitulo"><bean:message key="label.select.departments" /></span>
      <hr class="linea" color="#006600" noshade="noshade" size="1">
      </blockquote>
      </td>
    </tr>
  </table>


  <blockquote>
  <table class="marco" cellpadding="3" cellspacing="0" width=400>
    <TR class="cabeceratabla">
      <TD></TD>
      <TD align="center"><bean:message key="label.column.cod" /></TD>
      <TD align="center"><bean:message key="label.column.descripcion" /></TD>
    </TR>
    <logic:iterate id="departamentoselected" name="findDepartmentForm" property="allDepartments">
      <TR>
        <TD valign="top" class="fondotabla">
          <input type="checkbox" name="art-selected"
            value="<bean:write name="departamentoselected" property="cod_depto"/>"
            <logic:equal name="departamentoselected" property="selected" value="true">
              checked="checked"
            </logic:equal>
          />
        </TD>
        <TD width="15%" valign=top align="right" class="fondotabla">
        <bean:write name="departamentoselected" property="cod_depto" /></TD>
        <TD width="80%" valign=top align=left class="fondobotonera"><bean:write name="departamentoselected" property="desc_depto" /></TD>
      </TR>
    </logic:iterate>
  </table>

  <P><html:submit property="command" styleClass="form">
    <bean:message key="button.accept" />
  </html:submit>
  <html:submit property="command" styleClass="form">
    <bean:message key="button.back" />
  </html:submit></P>
  </blockquote>
  </DIV>
</html:form>
