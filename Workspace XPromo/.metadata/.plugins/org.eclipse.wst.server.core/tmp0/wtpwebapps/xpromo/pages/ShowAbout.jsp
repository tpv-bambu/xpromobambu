<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html" prefix="html"%>



<table align="center" width="500">
  <tr>
    <td colspan="2">
    <div class="subtitulo" align="center">
    <bean:message key="subtitle.xpromo" />
    </div>
    </td>
    <td colspan="2">
    <div class="subtitulo" align="center">
    <bean:message key="subtitle.output.compiler.version" />

    </div>
    </td>
  <tr>
  </tr>
  <tr>

    <td colspan="2">
    <jsp:useBean id="about" scope="request" type="com.invel.promo.struts.form.AboutForm"/>
    <div align="center" class="textomodulos1">
    <jsp:getProperty name="about" property="XPromoVersion"/>
    </div>
    </td>

    <td colspan="2">
    <div align="center" class="textomodulos1">
    <jsp:getProperty name="about" property="compiladorVersion"/>
    </div>
    </td>
  </tr>

  <tr>
  <td>
 	<br/>
    <div align="center" > <span class="subtitulo"> 
    Plantillas</span><br/>
    <html:link action="/ShowPlantillas.do" target="_blank">
      Versi&oacute;n
    </html:link></div>
  </td>
  <td colspan="3">
  </td></tr><tr>
    <td colspan="4">
	<br/>
    <div align="right"><span class="vinculostextos"> <html:link action="/InitMainPromo.do">
      <bean:message key="label.back" />
    </html:link> </span></div>
    </td>
  </tr>
</table>

