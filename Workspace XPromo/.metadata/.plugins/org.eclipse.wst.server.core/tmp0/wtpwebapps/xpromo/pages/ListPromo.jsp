<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>


<table cellpadding="0" cellspacing="0" border="0" align="center" width="570">
  <tr>
    <td class="fondomodulos" valign="top">
    <blockquote>
    <div align="left" class="subtitulo"><br>
    <span class="subtitulo"><bean:message key="label.edit.promociones" /></span>
    <hr class="linea" color="#006600" noshade="noshade" size="1">
    </div>
    </blockquote>
    </td>
  </tr>
  <tr>
    <td align="center" class="textowarning">
    <ul>
      <html:messages message="true" id="msg">
        <li><bean:write name="msg" /></li>
      </html:messages>
    </ul>
    </td>
  </tr>
</table>
<html:form action="ListPromo.do" method="POST">
  <div align="center">
  <table width="500" border="0" cellspacing="5" cellpadding="0">
    <!-- opciones de ordenamiento -->  
  	<tr>
  		<td class="textomodulos1">
  			<bean:message key="label.list.promo.order" />
  		</td>
  	</tr>
    <tr class="cabeceratabla">
      <td>
      	<html:radio property="orderType" value="0" name="listpromoForm" />
      	<bean:message key="label.list.promo.order.codigo" /> 
 	      <html:radio property="orderType" value="4" name="listpromoForm" />
      	<bean:message key="label.list.promo.order.descripcion" />
      	<html:radio property="orderType" value="1" name="listpromoForm" />
      	<bean:message	key="label.list.promo.order.tipopromocion" /> 
      	<html:radio property="orderType" value="2" name="listpromoForm" />
      	<bean:message key="label.list.promo.order.fechainicio" />
      	<html:radio property="orderType" value="3" name="listpromoForm" />
      	<bean:message key="label.list.promo.order.fechafin" />
      </td>
    </tr>
    <!-- opciones de filtro -->  
  	<tr>
  		<td class="textomodulos1">
  			<bean:message key="label.list.Tipo.Promo.filter" />
  		</td>
  	</tr>
    <tr class="cabeceratabla">
      <td><html:radio property="condition" value="0" name="listpromoForm" /><bean:message key="label.list.Todas" /> <html:radio property="condition" value="1" name="listpromoForm" /><bean:message
        key="label.list.Habilitadas" /> <html:radio property="condition" value="2" name="listpromoForm" /><bean:message key="label.list.Deshabilitadas" /></td>
    </tr>
    <tr class="cabeceratabla">
      <td><bean:message key="label.list.Tipo.Promo" />: <html:select name="listpromoForm" property="typeselected" styleClass="form">
        <html:options collection="types" property="value" labelProperty="label" />
      </html:select> <html:submit property="command" styleClass="form">
        <bean:message key="button.filter" />
      </html:submit>
      <hr class="linea" color="#006600" noshade="noshade" size="1">
      </td>
    </tr>
  </table>
  <table width="500" border="0" cellspacing="5" cellpadding="0">
  	<tr>
	<th><bean:message key="label.list.massive"/></th>
	<th><bean:message key="label.list.one"/></th>
	</tr>
    <tr>
    	<td>
      <html:select name="listpromoForm" property="massive" styleClass="form">
        <html:options collection="massiveActions" property="value" labelProperty="label" />
      </html:select>
      <html:submit property="command" styleClass="form" onclick="datosTextos(this, massive.value)">
        <bean:message key="button.accept" />
      </html:submit>
      </td>
      <td align="right"><html:submit property="command" styleClass="form">
        <bean:message key="button.edit" />
      </html:submit> <html:submit property="command" styleClass="form">
        <bean:message key="button.copy" />
      </html:submit></td>
    </tr>
    <tr>
    </tr>
  </table>
  
  <table>
	<tr>
		<td></td>
		<td align="left">
			<table class="marco"  cellpadding="3" cellspacing="0" width="538">
				<tr class="cabeceratabla" align="center">
					<TD width="5%"></TD>
					<TD align="center" width="10%"><bean:message key="label.column.cod"/></TD>
					<TD align="center" width="85%"><bean:message key="label.column.descripcion"/></TD>
				</tr>
			</table>
			<table class="marco" width="500">
				<tr>
					<td>
					<div align="left" style="width: 530; height: 300; overflow: auto;">
						<table class="marco" id="TablaDatos" cellpadding="3" cellspacing="0" width="500">
							 <logic:iterate id="one_promo" name="listpromoForm" property="promos">
						      <logic:equal value="true" property="enabled" name="one_promo">
						        <tr>
						          <td class="enabled" width="5%">
						       	   	<input type="checkbox" name="promos_selected"
										value="<bean:write name="one_promo" property="internalId"/>"
						            <logic:equal name="one_promo" property="selected" value="true">
						               checked="checked"
						            </logic:equal>
									/>
						          </td>
						          <td class="enabled" align="right" width="10%"  onmouseover="Tip(formatdate('<bean:write name="one_promo" property="desde"/>','<bean:write name="one_promo" property="hasta"/>','<bean:write name="one_promo" property="descTypePromo"/>'),TITLE,'<bean:write name="one_promo" property="description"/>',WIDTH,300)">
						          	<bean:write name="one_promo" property="internalId" />
						          </td>
						          <td class="enabledlast" align="left" width="85%" onmouseover="Tip(formatdate('<bean:write name="one_promo" property="desde"/>','<bean:write name="one_promo" property="hasta"/>','<bean:write name="one_promo" property="descTypePromo"/>'),TITLE,'<bean:write name="one_promo" property="description"/>',WIDTH,300)">
						          	<bean:write name="one_promo" property="description" />
						          </td>
						        </tr>
						      </logic:equal>
						      <logic:equal value="false" property="enabled" name="one_promo">
						        <tr>
						          <td class="notenabled" width="5%">
						          	
						     	   	<input type="checkbox" name="promos_selected"
										value="<bean:write name="one_promo" property="internalId"/>"
						            <logic:equal name="one_promo" property="selected" value="true">
						               checked="checked"
						            </logic:equal>
									/>         
						          </td>
						          <td class="notenabled" align="right" width="10%"
						          onmouseover="Tip(formatdate('<bean:write name="one_promo" property="desde"/>','<bean:write name="one_promo" property="hasta"/>','<bean:write name="one_promo" property="descTypePromo"/>'),TITLE,'<bean:write name="one_promo" property="description"/>',WIDTH,300)">
						          	<bean:write name="one_promo" property="internalId" />
						          </td>
						          <td class="notenabledlast" align="left" width="85%"  onmouseover="Tip(formatdate('<bean:write name="one_promo" property="desde"/>','<bean:write name="one_promo" property="hasta"/>','<bean:write name="one_promo" property="descTypePromo"/>'),TITLE,'<bean:write name="one_promo" property="description"/>',WIDTH,300)">
						          	<bean:write name="one_promo" property="description" />
						          </td>
						        </tr>
						      </logic:equal>
						    </logic:iterate>
						</table>
					</div>
					</td>
				</tr>
			</table>
		</td>
		<td></td>
	</tr>
</table>
<br>
  <table align="center" cellpadding="3" cellspacing="0" width="500">
    <TR>
    	<TD align="left" class="textomodulos1">
  	  	<!-- check select/deselect -->
				<input id="idCheckall" type="checkbox" name="checkall" onclick="checkUncheckAll(this);"
					value="<bean:message key="button.selectall"/>"/>
				<bean:message key="button.selectall"/>
    	</TD>
    	<TD colspan="3"></TD>
      <TD>
      	<div align="right">
      		<span class="vinculostextos">
      			<a href="InitMainPromo.do?" />
      				<bean:message key="label.back" />
      			</a>
      		</span>
      	</div>
      </TD>
    </TR>
  </table>
  </div>
</html:form>

