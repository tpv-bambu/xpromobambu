<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html" prefix="html"%>


<table cellpadding="0" cellspacing="0" border="0" align="center" width="570">
  <tr>
    <td class="fondomodulos" valign="top">
    <blockquote>
    <div class="subtitulo" align="left"><logic:present name="showxmlform">
      <br>
      <span class="subtitulo"><bean:message key="label.type.promo" />: <bean:write name="showxmlform" property="stringTypePromo" /></span>
    </logic:present>
    <hr class="linea" color="#006600" noshade="noshade" size="1">
    </div>
    </blockquote>
    </td>
  </tr>
</table>
<div align=CENTER>
<ul>
  <html:messages message="true" id="msg">
    <li><bean:write name="msg" /></li>
  </html:messages>
</ul>
</div>
<table cellpadding="10" cellspacing="10" border="0" align="center" width="525">
  <tr>
    <td height="150" valign="bottom">
    <div align="right"><span class="vinculostextos"> <a href="ListPromoPage.do?" /><bean:message key="label.back" /></a> </span></div>
    </td>
  </tr>
</table>
