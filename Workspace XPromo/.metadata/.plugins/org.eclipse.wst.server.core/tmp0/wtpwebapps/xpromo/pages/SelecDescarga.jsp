<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html" prefix="html"%>

<html:form action="ChooseDescarga.do" method="POST">
  <DIV ALIGN=CENTER>

  <table cellpadding="0" cellspacing="0" border="0" align="center" width="570">
    <tr>
      <td class="fondomodulos" valign="top">
      <blockquote>
      <span class="subtitulo"><bean:message key="label.select.articles" /></span>
      <hr class="linea" color="#006600" noshade="noshade" size="1">
      </blockquote>
      </td>
    </tr>
  </table>
  <ul class="textowarning">
    <html:messages message="true" id="msg">
      <li><bean:write name="msg" /></li>
    </html:messages>
  </ul>
  <TABLE cellpadding="0" cellspacing="0" border="0" align="center" width="500">
    <TR>
      <TD valign="top" align="center">
      <TABLE>
        <TR>
          <td align="right"><bean:message key="label.filtrar.por" /></td>
          <td>
          <div align="right"><html:select name="itemDescargaForm" property="filterfield" styleClass="form">
            <html:option value="codbarra">
              <bean:message key="label.option.codbarra" />
            </html:option>
            <html:option value="codint">
              <bean:message key="label.option.codinterno" />
            </html:option>
            <html:option value="nombre">
              <bean:message key="label.option.nombre" />
            </html:option>
            <html:option value="marca">
              <bean:message key="label.option.marca" />
            </html:option>
          </html:select></div>
          </td>
          <td><html:submit property="methodToCall" styleClass="form">
            <bean:message key="button.find" />
          </html:submit></td>
        </TR>
        <tr>
          <td><bean:message key="label.desde" /></td>
          <td><html:text name="itemDescargaForm" property="beginvalue" size="15" styleClass="form" /></td>
        </tr>
        <tr>
          <td><bean:message key="label.hasta" /></td>
          <td><html:text name="itemDescargaForm" property="endvalue" size="15" styleClass="form" /></td>
        </tr>
      </TABLE>
      </TD>
    </TR>
    <TR>
      <TD valign="top" align="center">
      
	<table>
		<tr>
			<td></td>
			<td align="left">
				<table class="marco"  cellpadding="3" cellspacing="0" width="508">
					<tr class="cabeceratabla" align="center">
						<TD width="25"></TD>
						<TD align="center" width="100"><bean:message key="label.column.codbarra" /></TD>
				        <TD align="center" width="100"><bean:message key="label.column.codinterno" /></TD>
				        <TD align="center" width="175"><bean:message key="label.column.nombre" /></TD>
				        <TD align="center" width="100"><bean:message key="label.column.marca" /></TD>
					</tr>
				</table>
				<table class="marco" width="500">
					<tr>
						<td>
						<div align="left" style="width: 500; height: 300; overflow:auto;">
							<table class="marco" cellpadding="3" cellspacing="0" width="480">
								  <logic:notEmpty name="itemDescargaForm" property="articles">
						          <logic:iterate id="articuloselected" name="itemDescargaForm" property="articles" indexId="index">
						            <TR>
						              <TD valign="center" class="fondotabla" width="25">
						              <html:radio name="itemDescargaForm" property="itemdescarga" value="<%= String.valueOf(index) %>" styleClass="textoform" />
						              </TD>
						              <TD valign="center" class="fondotabla" width="100"><bean:write name="articuloselected" property="codBarra" /></TD>
						              <TD valign="center" class="fondotabla" width="100" align="right">
						              <bean:write name="articuloselected" property="codInterno" />
						              </TD>
						              <TD valign="center" class="fondotabla" width="175">
						              <bean:write name="articuloselected" property="description" /></TD>
						              <TD valign="center" class="fondobotonera" width="100" align="right"><bean:write name="articuloselected" property="marca" /></TD>
						            </TR>
						          </logic:iterate>
						        </logic:notEmpty>
							</table>
						</div>
						</td>
					</tr>
				</table>	
			</td>
			<td></td>
		</tr>
	</table>
    </TD>
    </TR>
    <TR>
          <td colspan="5" class="fondobotonera" align="center"><html:submit property="methodToCall" styleClass="form">
            <bean:message key="button.accept" />
          </html:submit> <html:submit property="methodToCall" styleClass="form">
            <bean:message key="button.cancel" />
          </html:submit></td>
   </TR>
  </TABLE>
  </DIV>
</html:form>
