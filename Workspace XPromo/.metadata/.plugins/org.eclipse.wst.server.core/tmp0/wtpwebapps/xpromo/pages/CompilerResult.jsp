<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html" prefix="html"%>

<html:form action="CompilerResult.do" method="POST">
  <table cellpadding="0" cellspacing="0" border="0" align="center" width="570">
    <tr>
      <td class="fondomodulos" valign="top">
      <blockquote>
      <span class="subtitulo"><bean:message key="subtitle.output.compiler.and.ftp" /> </span>
      <hr class="linea" color="#006600" noshade="noshade" size="1">
      </blockquote>
      </td>
    </tr>
  <tr>
    <td align="center" valign="middle">
    	<div class="textowarning">
    	<ul>
				<html:messages id="msg" property="errors" message="true">
          <li>
	          <bean:write name="msg"/>
					</li>
	      </html:messages>		
			</ul>
    	</div>
    	<div class="textoconfirm">
    	<ul>
      		<html:messages id="msg" property="confirms" message="true">
        		<li>
        			<bean:write name="msg"/>
        		</li>
      		</html:messages>
    	</ul>    	
    	</div>
    </td>
  </tr>
  </table>
  <table cellpadding="0" cellspacing="0" border="0" align="center" width="570">
    <tr><td><blockquote><hr class="linea" color="#006600" noshade="noshade" size="1"></blockquote></td></tr>
    <tr>
    <td valign="bottom" class="textomodulos1"><blockquote><bean:message key="subtitle.output.compiler"/></blockquote></td></tr>
    <tr>
      <td align="center" valign="top">
        <html:textarea name="compilerForm" property="result" rows="10" cols="58" />
      </td>
    </tr>
    <tr>
      <td align="center" height="45" valign="middle"><html:submit property="commandResult" styleClass="form" style="cursor:pointer;">
        <bean:message key="button.accept" />
      </html:submit></td>
    </tr>
  </table>
</html:form>

