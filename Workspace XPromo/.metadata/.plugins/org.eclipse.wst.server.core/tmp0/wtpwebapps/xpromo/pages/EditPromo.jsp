<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html" prefix="html"%>


<table cellpadding="0" cellspacing="0" border="0" align="center" width="570">
  <tr>
    <td class="fondomodulos" valign="top">
    <blockquote>
    <div align="left" class="subtitulo"><br>
    <span class="subtitulo"><bean:message key="label.edit.promociones" /></span>
    <hr class="linea" color="#006600" noshade="noshade" size="1">
    </div>
    </blockquote>
    </td>
  </tr>
  <tr>
    <td>
    <div align="center">
    <table>
      <logic:iterate id="promoselected" name="promos">
        <TR>
          <TD width="90%" valign=top><html:link forward="editpromo" paramId="promo-selected" paramName="promoselected" paramProperty="id">
            <bean:write name="promoselected" property="description" />
          </html:link></TD>
        </TR>
      </logic:iterate>
    </table>
    </div>
    </td>
  </tr>
</table>
