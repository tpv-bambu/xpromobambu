<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html" prefix="html"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<table cellpadding="0" cellspacing="0" border="0" align="center" width="570">
  <tr>
    <td class="fondomodulos" valign="top">
    	<blockquote>
    		<div align="left" class="subtitulo"><br>
    			<span class="subtitulo">
    				<bean:message key="subtitle.typepromo" />
    			</span>
    			<hr class="linea" color="#006600" noshade="noshade" size="1">
    		</div>
    	</blockquote>
    </td>
  </tr>
  <tr>
    <td align="center" valign="middle" class="textowarning">
    	<ul>
      		<html:messages message="true" id="msg">
        		<li>
        			<bean:write name="msg" />
        		</li>
      		</html:messages>
    	</ul>
    </td>
  </tr>
</table>
<html:form action="TypePromo.do" method="POST">
	<html:hidden property="command" value=""/>
	<html:hidden property="typePromoSel" value=""/>
	<div align="center">
		<table align="center" width="500" cellpadding="3" cellspacing="0">
    		<c:if test="${action != 'logout'}">
    		<tr>
      			<td align="left" class="textomodulos1">
      				<bean:message key="label.typepromo.id"/>:
				</td>
				<td>
					<html:text property="typePromo" disabled="true" style="width:80px;"></html:text>
				</td>
			</tr>
    		<tr>
      			<td align="left" class="textomodulos1">
      				<bean:message key="label.typepromo.description"/>:
				</td>
				<td>
					<!-- verifico como debo dibujar el text -->
					<c:if test="${action == 'load'}">
						<html:text property="description" style="width:400px;" disabled="true"></html:text>					
					</c:if>
					<c:if test="${action == 'select'}">
						<html:text property="description" style="width:400px;" disabled="false"></html:text>					
					</c:if>
					<c:if test="${action == 'update'}">
						<html:text property="description" style="width:400px;" disabled="true"></html:text>					
					</c:if>
				</td>
			</tr>
			<!-- button grabar -->
			<tr>
				<td align="left">
					<!-- verifico como debo dibujar el button -->
					<c:if test="${action == 'load'}">
						<html:button property="Update" styleClass="form" style="cursor:pointer;" disabled="true">
							<bean:message key="button.typepromo.submit" />
						</html:button>
					</c:if>
					<c:if test="${action == 'select'}">
						<html:button property="Update" styleClass="form" style="cursor:pointer;" disabled="false"
							onclick="updateTypePromo();">
							<bean:message key="button.typepromo.submit" />
						</html:button>
					</c:if>
					<c:if test="${action == 'update'}">
						<html:button property="Update" styleClass="form" style="cursor:pointer;" disabled="true">
							<bean:message key="button.typepromo.submit" />
						</html:button>
					</c:if>
				</td>
				<!-- button editar -->
				<td align="left">
					<!-- verifico como debo dibujar el button -->
					<c:if test="${action == 'load'}">
						<html:button property="Edit" styleClass="form" disabled="true"
							onclick="editTypePromo();" style="cursor:pointer;">
							<bean:message key="button.typepromo.edit" />
						</html:button>
					</c:if>
					<c:if test="${action == 'select'}">
						<html:submit property="Edit" styleClass="form" style="cursor:pointer;" disabled="true"
							onclick="editTypePromo(this);">
							<bean:message key="button.typepromo.edit" />
						</html:submit>
					</c:if>
					<c:if test="${action == 'update'}">
						<html:button property="Edit" styleClass="form" disabled="true"
							onclick="editTypePromo(this);" style="cursor:pointer;">
							<bean:message key="button.typepromo.edit" />
						</html:button>
					</c:if>
				</td>
			</tr>
			</c:if>    		
		</table>
   		<table>
			<tr>
				<td></td>
				<td align="left">
					<!-- tabla columnas de la grilla de promociones -->
					<table class="marco"  cellpadding="3" cellspacing="0" width="508">
						<tr class="cabeceratabla" align="center">
							<TD width="5%"></TD>
							<TD align="center" width="95%">
								<bean:message key="label.typepromo.column.description"/>
							</TD>
						</tr>
					</table>
					<table class="marco" width="500">
						<tr>
							<td>
								<div align="left" style="width: 500; height: 300; overflow: auto;">
									<table class="marco" cellpadding="3" cellspacing="0" width="500">
										<!-- verificamos sino esta vacia la collecciòn del form de la acciòn -->
										<logic:notEmpty name="typePromoForm" property="listTypePromo">
											<!-- iteramos la collecciòn para generar los datos de la grilla -->
						      				<logic:iterate id="typePromo" name="typePromoForm" property="listTypePromo">						      				
						        				<TR>
													<!-- columna checked -->
													<TD valign="top" class="fondotabla" width="5%">
											  			<input type="radio" name="typePromoSelected" value="<bean:write name="typePromo" property="id"/>"
											  				onclick="enbledControl();" style="cursor:pointer;"/>
													</TD>						        				
						          					<TD valign="top" class="fondobotonera">
						          						<bean:write name="typePromo" property="description"/>
						          					</TD>
						        				</TR>
						      				</logic:iterate>
						    			</logic:notEmpty>
									</table>
								</div>
							</td>
						</tr>
					</table>	
				</td>
				<td>
				</td>
			</tr>
		</table>
  		<br>
  		<table align="center" cellpadding="3" cellspacing="0" width="500">
    		<TR>
      			<TD>
      				<div align="right">
      					<span class="vinculostextos"><a href="InitMainPromo.do?" />
      						<bean:message key="label.back" /></a>
      					</span>
      				</div>
      			</TD>
    		</TR>
  		</table>		
	</div>
</html:form>