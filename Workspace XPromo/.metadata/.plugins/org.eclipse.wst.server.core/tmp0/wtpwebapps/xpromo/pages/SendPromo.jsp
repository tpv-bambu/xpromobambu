<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html" prefix="html"%>
<table cellpadding="0" cellspacing="0" border="0" align="center"
	width="570">
	<tr>
		<td class="fondomodulos" valign="top">
		<blockquote>
		<div align="left" class="subtitulo"><br>
		<span class="subtitulo"> <bean:message key="subtitle.compiler" />
		</span>
		<hr class="linea" color="#006600" noshade="noshade" size="1">
		</div>
		</blockquote>
		</td>
	</tr>
	<tr>
		<td align="center" valign="middle">
		<div class="textowarning">
		<ul>
			<html:messages id="msg"  message="true">
				<li><bean:write name="msg" /></li>
			</html:messages>
		</ul>
		</div>
		</td>
	</tr>
</table>
<html:form action="SendPromo.do" method="POST">
	<div align="center">
	<table align="center" cellpadding="3" cellspacing="0" width="500">
		<tr>
			<td align="left"><!-- boton compilar --> <html:submit
				property="methodToCall" styleClass="form"
				onclick="this.form.target='_self';" style="cursor:pointer;">
				<bean:message key="button.compilar" />
			</html:submit></td>
			<td align="left" class="textomodulos1"><!--  check FTP or not FTP -->
			<input id="idFTP" type="checkbox" name="button.ftp.yes.or.no" /> <bean:message
				key="button.ftp.yes.or.no" /></td>
			<td align="right" colspan="3" class="textomodulos1"><!-- check select/deselect -->
			<input id="idCheckall" type="checkbox" name="checkall"
				onclick="checkUncheckAll(this);"
				value="<bean:message key="button.selectall"/>" /> <bean:message
				key="button.selectall" /></td>
		</tr>
	</table>
	<table>
		<tr>
			<td></td>
			<td align="left"><!-- columnas de la grilla -->
			<table class="marco" cellpadding="3" cellspacing="0" width="488">
				<tr class="cabeceratabla" align="center">
					<TD width="5%"></TD>
					<TD align="center" width="10%"><bean:message
						key="label.column.cod" /></TD>
					<TD align="center" width="85%"><bean:message
						key="label.column.descripcion" /></TD>
				</tr>
			</table>
			<table class="marco" width="480">
				<tr>
					<td>
					<div align="left" style="width: 480; height: 300; overflow: auto;">
					<!-- registro de la grilla de sucursales -->
					<table class="marco" cellpadding="3" cellspacing="0" width="480">
						<logic:iterate id="suc" name="prosucForm" property="sucursales">
							<TR>
								<TD width="5%" valign="top" class="fondotabla"><input
									type="checkbox" name="suc-selected"
									value="<bean:write name="suc" property="cod_sucursal"/>"
									<logic:equal name="suc" property="selected" value="true">
						              					checked="checked"
						            			</logic:equal> />
								</TD>
								<TD width="10%" valign="center" class="fondotabla"><bean:write
									name="suc" property="cod_sucursal"/></TD>
								<TD width="85%" valign="center" class="fondobotonera"><bean:write
									name="suc" property="descripcion" /></TD>
							</TR>
						</logic:iterate>
					</table>
					</div>
					</td>
				</tr>
			</table>
			</td>
			<td></td>
		</tr>
	</table>
	<br>
	<!-- tabla link volver -->
	<table align="center" cellpadding="3" cellspacing="0" width="500">
		<TR>
			<TD>
			<div align="right"><span class="vinculostextos"> <a
				href="InitMainPromo.do?" /> <bean:message key="label.back" /> </a> </span></div>
			</TD>
		</TR>
	</table>
	</div>
</html:form>

