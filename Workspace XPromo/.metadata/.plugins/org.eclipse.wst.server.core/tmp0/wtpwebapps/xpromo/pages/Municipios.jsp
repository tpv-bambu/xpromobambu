<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html" prefix="html"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript">
	
		/*
	 	* Funcion q selecciona o deselecciona los check de un formulario.
	 	* @param theElement Check seleccionar o deselccionar todos.
	 	* @param chkName Check de la tabla q se debe aplicar la seleccion o desseleccion
	 	*/
		function checkUncheckAll(theElement, chkName) {
			
     	var theForm = theElement.form, i = 0;
	 		for(i = 0; i < theForm.length; i++){
	 				if (theForm[i].name == chkName){	 				
      			if(theForm[i].type == 'checkbox' && theForm[i].name != 'checkall')
		  				theForm[i].checked = theElement.checked;
	  			}
     	}
     	
    }
		  
	</script>

<html:form action="LoadMunicipios.do" method="POST"
	enctype="multipart/form-data">
	<DIV ALIGN="CENTER">

	<table cellpadding="0" cellspacing="0" border="1" align="center"
		width="900">
		<tr>
			<td class="fondomodulos" valign="top">
			<blockquote><span class="subtitulo"><bean:message
				key="subtitle.browse.municipios" /></span></blockquote>
			<hr class="linea" color="#006600" noshade="noshade" size="1">

			</td>
		</tr>
	</table>
	<div class="textowarning">
	<ul>
		<html:messages id="msg" property="errors" message="true">
			<li><bean:write name="msg" /></li>
		</html:messages>
	</ul>
	</div>
	<div class="textoconfirm">
	<ul>
		<html:messages id="msg" property="confirms" message="true">
			<li><bean:write name="msg" /></li>
		</html:messages>
	</ul>
	</div>
	<!-- <ul class="textowarning">
		<html:messages message="true" id="msg">
			<li><bean:write name="msg" /></li>
		</html:messages>
	</ul> -->


	<TABLE cellpadding="0" cellspacing="0" border="0" align="center"
		width="860">

		<TR>
			<TD valign="top" align="left">
			<blockquote /><!-- Tabla con el combo y botón -->
			<TABLE>
			
				<tr>
					<td colspan="3" align="left"><span class="subtitulo"> <bean:message
						key="label.municipio.choose" /> </span></td>
				</tr>
				<TR>

				</TR>
			</TABLE>
			</TD>
			<TD></TD>
		</TR>
		<TR>
			<TD valign="top" align="center">

			<table class="marco" cellpadding="3" cellspacing="0" width="430"
				border="0">
				<tr class="cabeceratabla" align="center">
					<TD width="10%"></TD>
					<TD width="90%" valign="top" align="left" width="20%"><bean:message
						key="label.municipio.descripcion" /></TD>
				</tr>
			</table>
			<table class="marco" width="50%">
				<tr align="center">
					<td>
					<div align="left" style="width: 430; height: 300; overflow: auto;">
					<table class="marco" cellpadding="3" cellspacing="0" width="414"
						border="0">
						<c:forEach var="muni"
							items="${muniForm.municipios}" varStatus="status">

							<TR>
								<TD valign="top" class="fondotabla" width="5%"><input
									id="chkMunicipiosNotInCart" type="checkbox" name="chkMunicipiosNotInCart"
									value="${status.index}"></TD>
								<TD valign="top" class="fondotabla" width="95%"><bean:write name="muni" property="descripcion" /></TD>
							
							</TR>

						</c:forEach>
					</table>
					</div>
					</td>
				</tr>
				<TR>
					<td colspan="5" class="fondobotonera" align="center"><!-- check select/deselect -->
					<input id="idCheckall1" type="checkbox" name="checkall"
						onclick="checkUncheckAll(this, 'chkMunicipiosNotInCart');"
						value="<bean:message key="button.selectall"/>" /> <bean:message
						key="button.selectall" /> <html:submit property="command"
						styleClass="form">
						<bean:message key="button.add" />
					</html:submit></td>
				</TR>
			</table>
			</TD>
			<TD align="center" valign="top">

			<table class="marco" cellpadding="3" cellspacing="0" width="433"
				border="0">
				<tr class="cabeceratabla" align="center">
					<TD width="5%"></TD>
					<TD valign="top" align="center" width="35%"><bean:message
						key="label.municipio.codigo" /></TD>					
					<TD valign="top" align="center" width="60%"><bean:message
						key="label.municipio.descripcion" /></TD>
				</tr>
			</table>
			<table class="marco" width="50%">
				<tr align="center">
					<td>
					<div align="left" style="width: 430; height: 300; overflow: auto;">
					<table class="marco" cellpadding="3" cellspacing="0" width="414"
						border="0">
						<c:forEach var="muniCart" items="${muniForm.cart}"
							varStatus="status">

							<TR>
								<TD valign="top" class="fondotabla" width="5%"><input
									id="chkMunicipalidadesInCart" type="checkbox" name="chkMunicipiosInCart"
									value="${status.index}" /></TD>
								<TD valign="top" class="fondotabla" width="6%"><bean:write
									name="muniCart" property="codMunicipio" /></TD>
								<TD valign="top" class="fondotabla" align="right" width="79%">
								<bean:write name="muniCart" property="descripcion" /></TD>
						
							</TR>

						</c:forEach>
					</table>
					</div>
					</td>
				</tr>
				<TR>
					<td colspan="5" class="fondobotonera" align="center"><!-- check select/deselect -->
					<input id="idCheckall" type="checkbox" name="checkall"
						onclick="checkUncheckAll(this, 'chkMunicipiosInCart');"
						value="<bean:message key="button.selectall"/>" /> <bean:message
						key="button.selectall" /> <html:submit property="command"
						styleClass="form">
						<bean:message key="button.delete" />
					</html:submit></td>
				</TR>
			</table>
			</TD>
		</TR>
	</TABLE>
	</DIV>
	<div>
	<table>
		<tr>
			<td><html:submit property="command" styleClass="form">
				<bean:message key="button.accept" />
			</html:submit></td>
			<td><html:cancel property="command" styleClass="form">
				<bean:message key="button.cancel" />
			</html:cancel></td>
		</tr>
	</table>
	</div>
</html:form>
