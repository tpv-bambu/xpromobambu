<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean" prefix="bean"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<table width="570">
  <tr>
    <td align="center" class="textowarning">
      <ul>
        <html:messages message="true" id="msg">
          <li><bean:write name="msg" /></li>
        </html:messages>
      </ul></td>
  </tr>
  <tr>
    <c:if test="${user==null || !user.impuesto }">
    <td colspan="3" rowspan="8" class="fondomodulos" valign="top">
      <blockquote>
        <br> <span class="subtitulo">Módulo de promociones </span>
        <hr class="linea" color="#006600" noshade="noshade" size="1">
        <ul>
          <li class="textomodulos1" type="square">Seleccione <span class="vinculostextos"><a href="InitPromo.do?" />nueva promoción</a> </span> para crear una nueva promoción desde cero.</li>
          <li class="textomodulos1" type="square"><span class="vinculostextos"><a href="ListPromoPage.do?" />Ver promociones</a> </span> le permite navegar entre ellas y seleccionarlas para edición.</li>
          <li class="textomodulos1" type="square">Con <span class="vinculostextos"><a href="ListPromoXSucursal.do?" />asociar a sucursales</a> </span> puede administrar qué promociones deben enviarse a cada sucursal.</li>
          <li class="textomodulos1" type="square"><span class="vinculostextos"><a href="ShowSendPromo.do?" />compilar promociones</a> </span> permite preparar el paquete completo que debe ser enviado a las cajas.</li>
          <li class="textomodulos1" type="square"><span class="vinculostextos"><a href="#" />Configuración</a> </span> permite modificar la descripción de una promoción.</li>
        </ul>
        <span class="subtitulo"> </span>
      </blockquote>
    </td>
    </c:if>
    
    
  </tr>
</table>
