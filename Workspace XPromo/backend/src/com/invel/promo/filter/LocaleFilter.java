package com.invel.promo.filter;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.invel.promo.dao.User;

/**
 * Para colocar el locale.
 * @author sebastian
 *
 */
public class LocaleFilter implements Filter {

    static Log localLog = LogFactory.getLog(LocaleFilter.class);
    
    @Override
    public void destroy() {
	// TODO Auto-generated method stub
	
    }

    /**
     * Para colocar el locale de impuestos, este filtro se ejecuta antes que todos los actions de negocio.
     */
    @Override
    public void doFilter(ServletRequest arg0, ServletResponse arg1,
	    FilterChain arg2) throws IOException, ServletException {
	HttpSession session = ((HttpServletRequest) arg0).getSession();
	
	User user = (User)session.getAttribute("user");
	if(user!=null && user.isImpuesto()){
	    session.setAttribute("org.apache.struts.action.LOCALE", new java.util.Locale("be"));
	    Config.set(session, Config.FMT_LOCALE,  new java.util.Locale("be"));
	    localLog.debug("Session touched");
	}else{
	    //si no hay usuario restauramos el locale a default.
	    session.setAttribute("org.apache.struts.action.LOCALE", Locale.US);
	    Config.set(session, Config.FMT_LOCALE,  Locale.US);
	    localLog.debug("Session restored");	    
	}
	
	
	//keep on the chain.
	localLog.debug("Filter executed");
	arg2.doFilter(arg0, arg1);

    }

    @Override
    public void init(FilterConfig arg0) throws ServletException {
	// TODO Auto-generated method stub
	
    }



}
