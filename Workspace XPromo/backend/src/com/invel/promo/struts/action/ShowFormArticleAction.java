/*
 * ShowFormArticleAction.java
 *
 * Created on 15 de octubre de 2005, 11:15
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.invel.promo.struts.action;

import invel.framework.xform.xelements.XForm;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

import com.invel.common.PromoCommons;
import com.invel.promo.struts.form.PromoForm;

public class ShowFormArticleAction extends Action {

    static Log log = LogFactory.getLog(ShowFormArticleAction.class);

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws IOException, ServletException, Exception {

	// form ---> request.getAttribute("promoform");
	PromoForm aPromoVo = (PromoForm) form;
	Document data = aPromoVo.getData();

	// TODO Manejo de exclusiones de articulos

	XForm xform = aPromoVo.getXform();
	String newform = xform.getHtmlForm(null, data, aPromoVo.getWhereToGo(),
		false, PromoCommons.htmlencoding, false);
	aPromoVo.setHtml(newform);

	request.setAttribute("promoform", aPromoVo);
	return (mapping.findForward("failure"));
    }

    /***********************************************************************
     * String newItemId = aPromoVo.getIditems(); String oponentItemId = null;
     * Collection<ArticuloSelected> newList = aPromoVo.getItems();
     * 
     * if (newItemId.equals("precios")) { oponentItemId = "descuentos"; } else
     * if (newItemId.equals("descuentos")) { oponentItemId = "precios"; }
     * 
     * if (oponentItemId != null) { Collection<ArticuloSelected> oldList =
     * LoadArticlesAction. fillItems(data, aPromoVo, oponentItemId);
     * 
     * Collection<ArticuloSelected> deleteList = new
     * ArrayList<ArticuloSelected>();
     * 
     * for (ArticuloSelected itemNew : newList) { for (ArticuloSelected itemOld
     * : oldList) { if (itemNew.equals(itemOld)) { deleteList.add(itemOld); } }
     * }
     * 
     * for (ArticuloSelected itemDel : deleteList) { oldList.remove(itemDel); }
     * deleteList.clear();
     * 
     * deleteContents(data, oponentItemId); fillArticles(data, oldList,
     * oponentItemId); }
     **********************************************************************/

}
