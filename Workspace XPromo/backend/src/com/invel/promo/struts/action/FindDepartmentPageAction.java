package com.invel.promo.struts.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.*;

import com.invel.common.PromoSystem;
import com.invel.promo.dao.Departamento;
import com.invel.promo.dao.DepartamentoSelected;
import com.invel.promo.dao.DepartamentoStore;
import com.invel.promo.struts.form.FindDepartmentForm;

public class FindDepartmentPageAction extends Action {

    static Log log = LogFactory.getLog(FindDepartmentPageAction.class);

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws IOException, ServletException, Exception {

	FindDepartmentForm findDepartment = (FindDepartmentForm) form;
	try {
	    ArrayList<DepartamentoSelected> deptosToShow = new ArrayList<DepartamentoSelected>();
	    Collection<Departamento> deptos = PromoSystem
		    .getDepartamentoStore().getAllDepartamentos(
			    DepartamentoStore.ORDER_CODE);
	    Collection<DepartamentoSelected> oldSelected = findDepartment
		    .getDepartments();

	    for (Departamento item : deptos) {
		DepartamentoSelected depto = new DepartamentoSelected(item);
		if ((oldSelected != null) && oldSelected.contains(depto)) {
		    depto.setSelected(true);
		}
		deptosToShow.add(depto);
	    }
	    // request.getSession().setAttribute("deptos", deptosToShow);
	    return (mapping.findForward("success"));
	} catch (Exception ex) {
	    log.error("Exception in FindArticlesPageAction.execute: " + ex);
	}
	return (mapping.findForward("success"));
    }

}
