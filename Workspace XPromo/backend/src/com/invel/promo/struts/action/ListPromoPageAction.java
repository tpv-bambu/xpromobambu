package com.invel.promo.struts.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.*;
import org.apache.struts.util.LabelValueBean;

import com.invel.common.PromoCommons;
import com.invel.common.PromoSystem;
import com.invel.promo.dao.*;
import com.invel.promo.struts.form.ListPromoForm;

public class ListPromoPageAction extends Action {

    static Log localLog = LogFactory.getLog(ListPromoPageAction.class);

    /* las acciones que se realizan sobre m�s de una promoci�n */
    private static final String[] masivas = { "label.option.deshabilitar",
	    "label.option.exportar.sql", "label.option.exportar.oracle",
	    "label.option.habilitar", "label.option.eliminar" };

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws IOException, ServletException, Exception {
	ActionMessages localMessages = new ActionMessages();
	ActionMessage message = null;
	User user = (User) request.getSession().getAttribute("user");
	if (user == null) {
	    message = new ActionMessage("message.user.not.logged");
	    localMessages.add(ActionMessages.GLOBAL_MESSAGE, message);
	    return (mapping.findForward("logout"));
	} else if (!user.getCanreadpromo()) {
	    message = new ActionMessage("xvalidate.cannotread");
	    localMessages.add(ActionMessages.GLOBAL_MESSAGE, message);
	    return (mapping.findForward("initmainpromo"));
	}
	ListPromoForm listPromo = (ListPromoForm) form;
	Collection<Promo> allPromos;
	try {
	    ArrayList<PromoSelected> promosToShow = new ArrayList<PromoSelected>();
	    if (listPromo.getTypeselected() != PromoCommons.DEFAULT_TYPE_SELECTED) {
		allPromos = PromoSystem.getPromoStore().getAllTypePromo(
			listPromo.getTypeselected(), listPromo.getOrderType());
	    } else {
		allPromos = PromoSystem.getPromoStore().getAllPromo(false, user.isImpuesto(),
			listPromo.getOrderType());
	    }

	    if (allPromos != null) {
		int condition = listPromo.getCondition();

		for (Promo promo : allPromos) {
		    PromoSelected selectedPromo = new PromoSelected(promo);
		    selectedPromo.setSelected(false);
		    if (condition == PromoCommons.ALL_PROMOS
			    || (condition == PromoCommons.PROMO_ENABLED && promo
				    .isEnabled())
			    || (condition == PromoCommons.PROMO_DISABLED && !promo
				    .isEnabled())) {
			// ok, esta promo debe ser renderizada en el cliente, la
			// agrego.
			promosToShow.add(selectedPromo);
			selectedPromo.setSelected(listPromo.isFlag());
		    }
		}
	    }
	    listPromo.setPromos(promosToShow);
	} catch (Exception e) {
	    localLog.error("Exception in ListPromoPageAction.execute: ", e);

	}
	try {
	    Collection<TypePromo> typePromo = PromoSystem.getTypePromoStore()
		    .getAllTypePromo(user.isImpuesto());
	    // esta coleccion termina en el jsp como el listado de tipos.
	    ArrayList<LabelValueBean> prod = new ArrayList<LabelValueBean>();
	    prod.add(new LabelValueBean("Todos", String
		    .valueOf(PromoCommons.DEFAULT_TYPE_SELECTED)));
	    for (TypePromo tpromo : typePromo) {
		prod.add(new LabelValueBean(tpromo.getDescription(), String
			.valueOf(tpromo.getId())));
	    }
	    request.setAttribute("types", prod);
	} catch (Exception e) {

	    localLog.error("Exception in ListPromoPageAction :", e);
	}
	loadMassiveActions(request);
	return (mapping.findForward("success"));
    }

    /**
     * Carga el arreglo con las opciones de acciones masivas.
     * 
     * @param request
     */
    @SuppressWarnings("unchecked")
    private void loadMassiveActions(HttpServletRequest request) {
	ArrayList<LabelValueBean> acciones = new ArrayList<LabelValueBean>();
	for (String value : masivas) {
	    String label = getResources(request).getMessage(value);
	    acciones.add(new LabelValueBean(label, value));
	}
	Collections.sort(acciones);
	request.setAttribute("massiveActions", acciones);
    }
}
