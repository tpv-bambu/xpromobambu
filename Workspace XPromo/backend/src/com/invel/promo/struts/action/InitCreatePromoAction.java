/*
 * InitCreatePromoAction.java
 *
 * Created on 16 de septiembre de 2005, 13:57
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.invel.promo.struts.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.LabelValueBean;

import com.invel.common.PromoCommons;
import com.invel.common.PromoSystem;
import com.invel.promo.agrupaciones.Agrupaciones2;
import com.invel.promo.agrupaciones.AgrupacionesType;
import com.invel.promo.dao.TypePromo;
import com.invel.promo.dao.User;
import com.invel.promo.struts.form.PromoForm;

/**
 * Acci�n q se ejecuta cuando vamos a crear una nueva promoci�n.
 * 
 * @author Administrador
 * @version 1.0
 */
public class InitCreatePromoAction extends Action {

    private static Log log = LogFactory.getLog(InitCreatePromoAction.class);

    @SuppressWarnings("unchecked")
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws IOException, ServletException, Exception {

	PromoForm promoForm = (PromoForm) form;
	String give = "failure";
	ActionMessages messages = new ActionMessages();
	User user = (User) request.getSession().getAttribute("user");
	// validamos las credenciales del usuario

	if (user == null || !user.getCancreatepromo()) {
	    ActionMessage message;
	    if (user == null) {
		message = new ActionMessage("message.user.not.logged");
	    } else {
		message = new ActionMessage("xvalidate.cannotcreate");
	    }
	    messages.add(ActionMessages.GLOBAL_MESSAGE, message);
	    saveMessages(request, messages);
	    return mapping.findForward(give);
	}
	// ok, usuario validado

	Agrupaciones2 agrupaciones;
	String grupos = "";
	ArrayList<LabelValueBean> prod = new ArrayList<LabelValueBean>();
	List<TypePromo> typepromoList = null;

	try {
	    typepromoList = (ArrayList<TypePromo>) PromoSystem
		    .getTypePromoStore().getAllTypePromo(user.isImpuesto());

	    agrupaciones = PromoCommons.getXmlAgrupaciones();
	    AgrupacionesType agrupacionesType = agrupaciones.agrupaciones3
		    .first();
	    Iterator iterator = agrupacionesType.agrupacion.iterator();
	    int i = 0;
	    boolean sel = true;

	    String[] selected = new String[1];
	    if (request.getParameterMap().get("description") != null) {
		selected = request.getParameterValues("description");
	    }

	    if (request.getParameterMap().get("description") == null
		    || selected[0].equalsIgnoreCase("-1")) {
		prod.add(new LabelValueBean("Todos", String
			.valueOf(PromoCommons.DEFAULT_TYPE_SELECTED)));
		while (iterator.hasNext()) {
		    grupos = agrupacionesType.agrupacion.at(i).name.first()
			    .getValue();
		    prod.add(new LabelValueBean(grupos, String.valueOf(i)));
		    i++;
		    iterator.next();
		}
		// ordeno, guardo el array en request y seteo la agrupacion por
		Collections.sort(prod);
		promoForm.setDescription("-1");
		request.setAttribute("xformtree", prod);
	    } else {
		i = Integer.parseInt(selected[0]);
		while (iterator.hasNext()) {
		    if (sel || (!sel && i != Integer.parseInt(selected[0]))) {
			grupos = agrupacionesType.agrupacion.at(i).name.first()
				.getValue();
			prod.add(new LabelValueBean(grupos, String.valueOf(i)));
		    }
		    if (sel) {
			prod.add(new LabelValueBean("Todos", String
				.valueOf(PromoCommons.DEFAULT_TYPE_SELECTED)));
			sel = false;
			i = 0;
		    } else {
			i++;
			iterator.next();
		    }
		    // ordeno, guardo el array en request (req 25)
		    Collections.sort(prod);
		    request.setAttribute("xformtree", prod);
		}

		// (+) Se utiliza lo siguiente para traer los Type_Promo
		// correspondientes al Grupo seleccionado
		iterator = agrupacionesType.agrupacion.at(Integer
			.parseInt(selected[0])).typepromo.at(0).id.iterator();
		i = 0;
		ArrayList<String> params = new ArrayList<String>();
		String strParam = "";
		int paramSel = Integer.parseInt(selected[0]);
		while (iterator.hasNext()) {
		    strParam = String.valueOf(agrupacionesType.agrupacion
			    .at(paramSel).typepromo.at(0).id.at(i).getValue());
		    params.add(i, strParam);
		    i++;
		    iterator.next();
		}
		typepromoList = (ArrayList<TypePromo>) PromoSystem
			.getTypePromoStore().getTypePromoGroup(params);
		// (-) Se utiliza lo siguiente para traer los Type_Promo
		// correspondientes al Grupo seleccionado
	    }

	} catch (Exception e) {
	    log.error("Exception in InitCreatePromoAction " + e);
	}

	if (typepromoList != null) {
	    request.getSession().setAttribute("xformlist", typepromoList);
	    give = "success";
	}
	return (mapping.findForward(give));
    }
}
