package com.invel.promo.struts.action;

import invel.framework.xform.DomUtils;
import invel.framework.xform.xelements.XForm;

import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

import com.invel.common.PromoCommons;
import com.invel.common.PromoSystem;
import com.invel.promo.dao.ProSuc;
import com.invel.promo.dao.Promo;
import com.invel.promo.dao.Sucursal;
import com.invel.promo.dao.TypePromo;
import com.invel.promo.struts.form.PromoForm;

public class PrepareFormAction extends Action {

    static Log localLog = LogFactory.getLog(PrepareFormAction.class);

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {

	PromoForm promoForm = (PromoForm) form;
	Promo promo = null;
	int idpromo = (Integer) request.getAttribute("cod_promo");

	try {
	    promo = PromoSystem.getPromoStore().getPromo(idpromo, true);
	} catch (Exception ex) {
	    localLog.error("No se encuentra promo para editar: " + ex);
	}

	if (promo != null) {
	    StringBuilder warning = new StringBuilder();
	    TypePromo typePromo = null;

	    try {
		int promoid = promo.getIntTypePromo();
		typePromo = PromoSystem.getTypePromoStore().getTypePromo(
			promoid);
	    } catch (Exception ex) {
		localLog.error("Error al obtener tipo de promocion: " + ex);
	    }

	    try {
		Collection<ProSuc> prosuc = PromoSystem.getProSucStore()
			.getAllSucursal(idpromo);
		if (!prosuc.isEmpty()) {
		    warning.append("Sucursales afectadas: ");
		    for (Iterator<ProSuc> it = prosuc.iterator(); it.hasNext();) {
			ProSuc aprosuc = it.next();
			try {
			    Sucursal suc = PromoSystem.getSucursalStore()
				    .getSucursal(aprosuc.getCod_sucursal());
			    warning.append(suc.getDescripcion());
			    if (it.hasNext()) {
				warning.append(", ");
			    } else {
				warning.append(".");
			    }
			} catch (Exception ex) {
			    localLog.error("Sucursal no encontrada: " + ex);
			}
		    }
		}
	    } catch (Exception ex) {
		localLog.error("No se encuentra promo asociada a sucursal: "
			+ ex);
	    }

	    if (typePromo != null) {
		Document sourceDoc = DomUtils.parse(promo.getStringXML(),
			PromoCommons.dbencoding, null, null);

		promoForm.myReset();
		promoForm.setErrormessage(warning.toString());
		promoForm.setCode(promo.getInternalId());
		promoForm.setTypePromo(promo.getIntTypePromo());
		promoForm.setStringTypePromo(typePromo.getDescription());
		promoForm.setEnabled(promo.isEnabled());
		promoForm.setData(sourceDoc);
		promoForm.setXform(typePromo.getXForm(), request.getLocale());
		promoForm.setDescription(promo.getDescription());
		promoForm.setWhereToGo("EditPromo.do");

		XForm xform = promoForm.getXform();

		String newform = xform.getHtmlForm(
			request,
			sourceDoc,
			"EditPromo.do?" + "typepromo="
				+ promoForm.getTypePromo(), false,
			PromoCommons.htmlencoding, false);
		promoForm.setHtml(newform);
		request.setAttribute("promoform", promoForm);
	    }
	}
	return mapping.findForward("success");
    }
}
