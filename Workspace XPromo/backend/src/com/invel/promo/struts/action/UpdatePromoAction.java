package com.invel.promo.struts.action;

import invel.framework.xform.DomUtils;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.invel.common.PromoCommons;
import com.invel.common.PromoSystem;
import com.invel.promo.dao.SimplePromo;
import com.invel.promo.dao.User;
import com.invel.promo.struts.form.PromoForm;

public class UpdatePromoAction extends Action {

    static Log localLog = LogFactory.getLog(UpdatePromoAction.class);

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws IOException, ServletException, Exception {

	String give = "success";
	PromoForm aPromoForm = (PromoForm) form;
	ActionMessages messages = new ActionMessages();
	User user = (User) request.getSession().getAttribute("user");

	if (user == null) {
	    ActionMessage message = new ActionMessage("message.user.not.logged");
	    messages.add(ActionMessages.GLOBAL_MESSAGE, message);
	} else {
	    if ((aPromoForm.getDescription() != null)
		    && aPromoForm.getDescription().length() > 0) {
		try {
		    String id = null, name = null, externalId = null;
		    id = request.getParameter(PromoCommons
			    .getCodeRequestParameter());
		    name = request.getParameter(PromoCommons
			    .getNameRequestParameter());
		    externalId = request.getParameter(PromoCommons
			    .getExternCodeRequestParameter());
		    if (id == null || name == null || externalId == null) {
			// en realidad nunca va a pasar esto. Supuestamente
			// estoy aca
			// porque el xform paso la validacion
			localLog.error("Error. No se encontro en el request algun id o nombre de la promo");
		    }

		    SimplePromo aSimplePromo = new SimplePromo();
		    aSimplePromo.setOldInternalId(aPromoForm.getCode());
		    aSimplePromo.setEnabled(aPromoForm.isEnabled());
		    aSimplePromo.setInternalId(Integer.parseInt(id));
		    aSimplePromo.setExternalId(externalId);
		    aSimplePromo.setDescription(name);
		    aSimplePromo.setStringXML(DomUtils.domToString(
			    aPromoForm.getData(), null, null, false));
		    aSimplePromo.setIntTypePromo(aPromoForm.getTypePromo());
		    aSimplePromo.setVigencia(request);
		    PromoSystem.getPromoStore().update(aSimplePromo);
		    PromoSystem.getProSucStore().updatePromo(aSimplePromo);
		    aPromoForm.setDescription(name);
		    ActionMessage message = new ActionMessage(
			    "message.insert.description.saved",
			    aPromoForm.getDescription());
		    messages.add(ActionMessages.GLOBAL_MESSAGE, message);
		    localLog.info("Usuario " + user.getUsername()
			    + " actualiz� la promoci�n "
			    + aSimplePromo.getDescription());
		} catch (Exception ex) {
		    localLog.error("Exception in UpdatePromoAction ", ex);
		    ActionMessage message = new ActionMessage(
			    "errors.login.error", ex.getMessage());
		    messages.add(ActionMessages.GLOBAL_MESSAGE, message);
		}

		// aShowXmlForm.myReset();
	    } else {
		ActionMessage message = new ActionMessage(
			"message.insert.description");
		messages.add(ActionMessages.GLOBAL_MESSAGE, message);
	    }
	}
	request.setAttribute("showxmlform", aPromoForm);
	if (!messages.isEmpty()) {
	    saveMessages(request, messages);
	}
	return (mapping.findForward(give));
    }

}
