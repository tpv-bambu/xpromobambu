/*
 * InitMainPromoAction.java
 *
 * Created on 16 de octubre de 2005, 9:36
 *
 * Action de entrada a la aplicaci�n. Desde aqu� mostramos la pantalla de 
 * inicio, por lo tanto desde aqu� validamos si la configuraci�n es la correcta.
 */

package com.invel.promo.struts.action;

import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.invel.common.PromoCommons;
import com.invel.common.PromoSystem;
import com.invel.promo.dao.User;
import com.invel.promo.struts.form.PromoForm;

public class InitMainPromoAction extends Action {
    static Log log = LogFactory.getLog(PrepareFormAction.class);

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {
	String give = "success";
	PromoForm aShowXmlForm = (PromoForm) form;
	aShowXmlForm.myReset();
	if (request.getParameter("reload") != null) {
	    log.debug("Se re-lee la configuracion");
	    Properties p = this.loadFile();
	    PromoCommons.init(p);
	    PromoSystem.init(p);
	}
	
	
	if (PromoCommons.getErrors().size() > 0
		|| PromoSystem.getErrors().size() > 0) {
	    log.debug("Ocurrieron errores en configuracion");
	    ActionMessages errors = new ActionMessages();
	    for (String s : PromoCommons.getErrors()) {
		errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage(
			"errors.init", s));
	    }
	    for (String s : PromoSystem.getErrors()) {
		errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage(
			"errors.init", s));
	    }
	    saveMessages(request, errors);
	    give = "error";
	}
	return mapping.findForward(give);

    }

    /**
     * Lee el archivo de configuraci�n.
     * 
     * @return Properties
     */
    private Properties loadFile() {
	ServletConfig servletConfig = super.getServlet().getServletConfig();
	ServletContext servletContext = servletConfig.getServletContext();

	String propertyName = "invel.promociones.properties.path";
	String path = servletConfig.getInitParameter(propertyName);
	if (path == null) {
	    log.error("Init parameter " + propertyName + " not specified.");
	    path = "/WEB-INF/promociones.properties";
	}
	log.info("Using " + path + " to load Promociones properties.");

	Properties p = new Properties();
	try {
	    p.load(servletContext.getResourceAsStream(path));
	} catch (Exception ex) {
	    p = null;
	}
	log.info("Using properties " + p);
	return p;
    }

}
