package com.invel.promo.struts.action;

import invel.framework.xform.FalseRequest;
import invel.framework.xform.FormException;
import invel.framework.xform.xelements.XForm;

import java.io.IOException;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.LookupDispatchAction;
import org.xml.sax.SAXException;

import com.invel.common.PromoCommons;
import com.invel.common.PromoSystem;
import com.invel.promo.dao.EstructuraComercialNivel;
import com.invel.promo.dao.TypePromo;
import com.invel.promo.dao.User;
import com.invel.promo.datahandler.FormDataHandler;
import com.invel.promo.struts.form.PromoForm;

public class ShowXmlPageAction extends LookupDispatchAction {

    static Log localLog = LogFactory.getLog(PromoCommons.class);

    @SuppressWarnings("unchecked")
    public ShowXmlPageAction() {
	this.keyMethodMap = new HashMap();
	this.keyMethodMap.put("xform.init", "init");
	this.keyMethodMap.put("xform.ok", "save");
	this.keyMethodMap.put("xform.cancel", "comeback");
	this.keyMethodMap.put("xform.article", "article");
	this.keyMethodMap.put("xform.articles", "articles");
	this.keyMethodMap.put("xform.department", "department");
	this.keyMethodMap.put("xform.add", "additem");
	this.keyMethodMap.put("xform.del", "delitem");

	this.keyMethodMap.put("xform.previos", "articles");
	this.keyMethodMap.put("xform.premios", "articles");
	this.keyMethodMap.put("xform.descarga", "article");
	this.keyMethodMap.put("xform.add.rango", "additem");
	this.keyMethodMap.put("xform.del.rango", "delitem");

	this.keyMethodMap.put("xform.add.medio", "additem");
	this.keyMethodMap.put("xform.del.medio", "delitem");

	this.keyMethodMap.put("xform.add.plan", "additem");
	this.keyMethodMap.put("xform.del.plan", "delitem");

	this.keyMethodMap.put("xform.add.line.latente", "additem");
	this.keyMethodMap.put("xform.del.line.latente", "delitem");
	this.keyMethodMap.put("xform.add.line.activa", "additem");
	this.keyMethodMap.put("xform.del.line.activa", "delitem");
	this.keyMethodMap.put("xform.add.line.ticket", "additem");
	this.keyMethodMap.put("xform.del.line.ticket", "delitem");
	this.keyMethodMap.put("xform.add.line.descripcion", "additem");
	this.keyMethodMap.put("xform.del.line.descripcion", "delitem");

	this.keyMethodMap.put("xform.estructura.comercial", "eComercial");
	this.keyMethodMap.put("xform.municipios", "municipios");
    }

    @Override
    protected Map getKeyMethodMap() {
	return this.keyMethodMap;
    }

    // Manager of xform.init
    public ActionForward init(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {

	PromoForm promoForm = (PromoForm) form;
	Enumeration attributeNames = request.getSession().getAttributeNames();
	Object attribute = request.getSession().getAttribute("org.apache.struts.action.MESSAGE");
	Object attribute2 = request.getSession().getAttribute("org.apache.struts.action.LOCALE");
	String give = "reload";
	String promoid = request.getParameter("typepromo");
	User user = (User) request.getSession().getAttribute("user");
	if (user == null) {
	    @SuppressWarnings("hiding")
	    ActionMessages messages = new ActionMessages();
	    ActionMessage message = new ActionMessage("message.user.not.logged");
	    messages.add(ActionMessages.GLOBAL_MESSAGE, message);
	    super.saveMessages(request, messages);
	    give = "back";
	} else if (promoid == null) {
	    // Request invalido
	    localLog.warn("Requerimiento sin tipo de promocion");
	    give = "RunTimeError";
	} else {

	    TypePromo typePromo = PromoSystem.getTypePromoStore().getTypePromo(
		    Integer.parseInt(promoid));

	    FalseRequest falseRequest = new FalseRequest(request);
	    falseRequest.addParameterValue(
		    PromoCommons.getCodeRequestParameter(),
		    String.valueOf(typePromo.getSuggestedCode()));
	    promoForm.myReset();
	    promoForm.setEnabled(true);// una promo nueva siempre estara
	    // habilitada.
	    promoForm.setTypePromo(typePromo.getId());
	    promoForm.setDescription(typePromo.getDescription());
	    promoForm.setStringTypePromo(typePromo.getDescription());
	    promoForm.setXform(typePromo.getXForm(), request.getLocale());
	    promoForm.setWhereToGo("ShowFormPromo.do");

	    XForm xform = promoForm.getXform();
	    String newform = xform.getHtmlForm(falseRequest, null,
		    promoForm.getWhereToGo(), false, PromoCommons.htmlencoding,
		    false);
	    promoForm.setHtml(newform);
	}

	request.setAttribute("promoform", promoForm);
	return (mapping.findForward(give));
    }

    // Manager of xform.ok
    public ActionForward save(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws IOException, SAXException {

	String give = "reload";
	PromoForm promoForm = (PromoForm) form;

	try {
	    XForm xform = promoForm.getXform();

	    String newform = xform.getHtmlForm(request, promoForm.getData(),
		    promoForm.getWhereToGo(), false, PromoCommons.htmlencoding,
		    true);
	    promoForm.updateData(request);
	    xform.getReporter().clear();
	    boolean ok = xform.validate(request);
	    if (ok) {

		promoForm.setDescription("");
		give = "success";
		promoForm.setErrormessage(null);
	    } else {
		if (PromoCommons.isXFormsErrorMessages()) {
		    promoForm.setErrormessage(xform.getReporter()
			    .getValidationErrors());
		}
		xform.getReporter().clear();
		promoForm.setHtml(newform);
	    }
	} catch (FormException ex) {
	    // TODO Auto-generated catch block
	    localLog.error("FormException: " + ex);
	}

	request.setAttribute("promoform", promoForm);
	return (mapping.findForward(give));
    }

    // Manager of xform.cancel
    public ActionForward comeback(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {

	PromoForm promoForm = (PromoForm) form;
	request.setAttribute("promoform", promoForm);

	return (mapping.findForward("back"));
    }

    // Manager of xform.article
    public ActionForward article(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws IOException, SAXException {

	PromoForm promoForm = (PromoForm) form;

	try {
	    promoForm.updateData(request);
	} catch (FormException ex) {
	    // TODO Auto-generated catch block
	    ex.printStackTrace();
	}

	request.setAttribute("promoform", promoForm);
	return (mapping.findForward("descarga"));
    }

    // Manager of xform.articles
    public ActionForward articles(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws IOException, SAXException {

	PromoForm promoForm = (PromoForm) form;

	try {
	    promoForm.updateData(request);
	} catch (FormException ex) {
	    // TODO Auto-generated catch block
	    ex.printStackTrace();
	}

	request.setAttribute("promoform", promoForm);
	return (mapping.findForward("loadarticle"));
    }

    // Manager of xform.department
    public ActionForward department(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws IOException, SAXException {

	PromoForm promoForm = (PromoForm) form;

	try {
	    promoForm.updateData(request);
	} catch (FormException ex) {
	    // TODO Auto-generated catch block
	    ex.printStackTrace();
	}

	request.setAttribute("promoform", promoForm);
	return (mapping.findForward("loaddepartment"));
    }

    // Manager of xform.add
    public ActionForward additem(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {

	PromoForm promoForm = (PromoForm) form;

	try {
	    XForm xform = promoForm.getXform();
	    promoForm.updateData(request);

	    FormDataHandler.addNew(promoForm.getData(), xform);

	    String newform = xform.getHtmlForm(null, promoForm.getData(),
		    promoForm.getWhereToGo(), false, PromoCommons.htmlencoding,
		    false);
	    promoForm.setHtml(newform);
	} catch (FormException ex) {
	    // TODO Auto-generated catch block
	    localLog.error("FormEx:", ex);
	}

	request.setAttribute("promoform", promoForm);
	return (mapping.findForward("reload"));
    }

    // Manager of xform.del
    public ActionForward delitem(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {

	PromoForm promoForm = (PromoForm) form;

	try {
	    XForm xform = promoForm.getXform();
	    promoForm.updateData(request);

	    FormDataHandler.delLast(promoForm.getData(), xform);

	    String newform = xform.getHtmlForm(null, promoForm.getData(),
		    promoForm.getWhereToGo(), false, PromoCommons.htmlencoding,
		    false);
	    promoForm.setHtml(newform);
	} catch (FormException ex) {
	    // TODO Auto-generated catch block
	    ex.printStackTrace();
	}

	request.setAttribute("promoform", promoForm);
	return (mapping.findForward("reload"));
    }

    // Manager of xform.estructura.comercial
    public ActionForward eComercial(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {
	PromoForm promoForm = (PromoForm) form;
	
	try {
	   
	    promoForm.updateData(request);

	} catch (Exception ex) {
	    // TODO Auto-generated catch block
	    localLog.error("estructura comercial",ex);
	}
	request.setAttribute("promoform", promoForm);
	return (mapping.findForward("loadEC"));
    }

    // Manager of xform.municipios
    public ActionForward municipios(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {
	PromoForm promoForm = (PromoForm) form;

	try {
	    promoForm.updateData(request);
	} catch (FormException ex) {
	    // TODO Auto-generated catch block
	    ex.printStackTrace();
	}
	request.setAttribute("promoform", promoForm);
	return (mapping.findForward("loadMunicipios"));
    }

}
