/*
 * LogoutAction.java
 *
 * Created on 28 de junio de 2005, 21:26
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.invel.promo.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.*;

import com.invel.promo.dao.User;

public class LogoutAction extends Action {

    private static Log myLog = LogFactory.getLog(InitCreatePromoAction.class);

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {
	String forward = "success";
	User user = (User) request.getSession().getAttribute("user");
	if (user != null) {
	    user.logout();
	    ActionMessages messages = new ActionMessages();
	    ActionMessage message = new ActionMessage(
		    "message.user.logged.out", user.getUsername());
	    messages.add(ActionMessages.GLOBAL_MESSAGE, message);
	    saveMessages(request, messages);
	    request.getSession(true).removeAttribute("user");
	    myLog.info("Usuario deslogueado: " + user.getUsername());
	    if(user.isImpuesto()){
		forward = "successTax";
	    }
	}
	return mapping.findForward(forward);

    }

}
