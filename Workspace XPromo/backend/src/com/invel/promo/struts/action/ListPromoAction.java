package com.invel.promo.struts.action;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.LookupDispatchAction;

import com.invel.common.PromoCommons;
import com.invel.common.PromoSystem;
import com.invel.promo.dao.Promo;
import com.invel.promo.dao.PromoSelected;
import com.invel.promo.dao.User;
import com.invel.promo.struts.form.ListPromoForm;

public class ListPromoAction extends LookupDispatchAction {

    static Log logger = LogFactory.getLog(ListPromoAction.class);

    /** Prefijo de todos los archivos a exportar */
    static private final String PREFIX = "XPromo.Exported.Promos_";

    /** Para Obtener el sufijo del nombre del archivo */
    private static final DateFormat DATEFORMAT = new SimpleDateFormat(
	    "yyyyMMdd_HHmmss", new Locale("es"));

    /**
     * Se suprimen los warnings porque depende de la libreria de struts
     */
    @SuppressWarnings("unchecked")
    public ListPromoAction() {
	this.keyMethodMap = new HashMap();
	this.keyMethodMap.put("button.copy", "copy");
	this.keyMethodMap.put("button.edit", "edit");
	// this.keyMethodMap.put("button.delete", "delete");
	this.keyMethodMap.put("button.filter", "filter");
	this.keyMethodMap.put("button.accept", "massive");
	this.keyMethodMap.put("button.selectall", "selectall");
    }

    @Override
    protected Map getKeyMethodMap() {
	return this.keyMethodMap;
    }

    /**
     * Metodo asociado al filtrado del listado de promociones, sencillamente se
     * refresca la p�gina y se vuelve a leer el filtrado.
     * 
     * No se validan las credenciales del usuario porque este metodo siempre
     * termina filtrando.
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForward
     */
    public ActionForward filter(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {

	String give = "reloadlist";
	return (mapping.findForward(give));
    }

    /**
     * M�todo asociado al bot�n de crear una nueva promoci�n, redirige al action
     * correspondiente siempre que tenga las credenciales suficientes.
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForward
     */
    public ActionForward anew(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {

	String give = "reloadlist";
	@SuppressWarnings("hiding")
	ActionMessages messages = new ActionMessages();
	User user = (User) request.getSession().getAttribute("user");
	// validamos las credenciales del usuario
	if (user == null || !user.getCancreatepromo()) {
	    ActionMessage message = new ActionMessage("xvalidate.cannotcreate");
	    messages.add(ActionMessages.GLOBAL_MESSAGE, message);
	    saveMessages(request, messages);
	    return mapping.findForward(give);
	}
	// ok, usuario validado
	give = "newpromo";

	return (mapping.findForward(give));

    }

    /**
     * M�todo asociado al boton de "copiar", donde se crea una nueva promoci�n a
     * partir de una existente (salvo el id de la promo).
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForward
     */
    public ActionForward copy(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {
	// validar que se puede borrar y borrar tanto la promo como la promo
	// por suc.
	String forward = "reloadlist";

	User user = (User) request.getSession().getAttribute("user");
	ActionMessages localmessages = new ActionMessages();
	String[] checkedPromos = request.getParameterValues("promos_selected");
	// TODO pensar el a donde ir
	if (user == null) {
	    ActionMessage message = new ActionMessage("message.user.not.logged");
	    localmessages.add(ActionMessages.GLOBAL_MESSAGE, message);
	}

	else if (!user.getCancreatepromo()) {
	    ActionMessage message = new ActionMessage("xvalidate.cannotcreate");
	    localmessages.add(ActionMessages.GLOBAL_MESSAGE, message);
	} else if (checkedPromos == null || checkedPromos.length == 0) {
	    ActionMessage message = new ActionMessage(
		    "message.should.select.promo.copy");
	    localmessages.add(ActionMessages.GLOBAL_MESSAGE, message);
	} else if (checkedPromos.length > 1) {
	    ActionMessage message = new ActionMessage("message.select.one");
	    localmessages.add(ActionMessages.GLOBAL_MESSAGE, message);
	} else {
	    // Ok, forwardeando al copy promo.
	    request.setAttribute("cod_promo", new Integer(checkedPromos[0]));
	    forward = "copypromo";
	}
	if (!localmessages.isEmpty()) {
	    saveMessages(request, localmessages);
	}
	return (mapping.findForward(forward));
    }

    /**
     * M�todo asociado al bot�n de edici�n.
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForward
     */
    public ActionForward edit(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {
	String forward = "reloadlist";
	User user = (User) request.getSession().getAttribute("user");
	ActionMessages localmessages = new ActionMessages();
	String[] checkedPromos = request.getParameterValues("promos_selected");

	if (user == null) {
	    ActionMessage message = new ActionMessage("message.user.not.logged");
	    localmessages.add(ActionMessages.GLOBAL_MESSAGE, message);
	} else if (!user.getCanupdatepromo()) {
	    ActionMessage message = new ActionMessage("xvalidate.cannotedit");
	    localmessages.add(ActionMessages.GLOBAL_MESSAGE, message);
	} else if (checkedPromos == null || checkedPromos.length == 0) {
	    ActionMessage message = new ActionMessage(
		    "message.should.select.promo.edit");
	    localmessages.add(ActionMessages.GLOBAL_MESSAGE, message);
	} else if (checkedPromos.length > 1) {
	    ActionMessage message = new ActionMessage("message.select.one");
	    localmessages.add(ActionMessages.GLOBAL_MESSAGE, message);
	} else {
	    // Ok, preparando el forwardeo para editar la promo
	    request.setAttribute("cod_promo", new Integer(checkedPromos[0]));
	    forward = "editpromo";
	}

	if (!localmessages.isEmpty()) {
	    saveMessages(request, localmessages);
	}
	return (mapping.findForward(forward));
    }

    /**
     * Metodo asociado al boton "delete". Aplica solo en una promoci�n
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForward
     */
    public ActionForward delete(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {

	// validar que se puede borrar y borrar tanto la promo como la promo
	// por suc.
	String forward = "reloadlist";

	User user = (User) request.getSession().getAttribute("user");
	ActionMessages localmessages = new ActionMessages();
	String[] checkedPromos = request.getParameterValues("promos_selected");
	// TODO pensar el a donde ir
	if (user == null) {
	    ActionMessage message = new ActionMessage("message.user.not.logged");
	    localmessages.add(ActionMessages.GLOBAL_MESSAGE, message);
	} else if (!user.getCandeletepromo()) {
	    ActionMessage message = new ActionMessage("xvalidate.cannotdelete");
	    localmessages.add(ActionMessages.GLOBAL_MESSAGE, message);
	} else if (checkedPromos == null || checkedPromos.length == 0) {
	    ActionMessage message = new ActionMessage(
		    "message.should.select.promo.delete");
	    localmessages.add(ActionMessages.GLOBAL_MESSAGE, message);
	} else if (checkedPromos.length > 1) {
	    ActionMessage message = new ActionMessage("message.select.one");
	    localmessages.add(ActionMessages.GLOBAL_MESSAGE, message);
	} else {
	    // Ok, borrando la promo
	    int id = Integer.parseInt(checkedPromos[0]);
	    try {
		// TODO �se podr� hacer un rollback?...
		PromoSystem.getPromoStore().delete(id);
		PromoSystem.getProSucStore().deleteCodPromo(id);
	    } catch (Exception ex) {
		logger.error("Error en delete promo. ", ex);
		ActionMessage message = new ActionMessage("errors.login.error",
			ex.getMessage());
		localmessages.add(ActionMessages.GLOBAL_MESSAGE, message);
	    }
	}
	if (!localmessages.isEmpty()) {
	    saveMessages(request, localmessages);
	}
	return (mapping.findForward(forward));
    }

    /**
     * M�todo asociado al bot�n de "select all", para seleccionar todas las
     * promociones (o deseleccionar todas las promociones).
     * 
     * Como este objeto no carga las promociones a renderizar, simplemente
     * cambio el valor del flag en el form asociado y el action encargado de
     * renderizar (ListPromoPageAction) seleccionar� todos o no seg�n el valor
     * del flag.
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForward
     */
    public ActionForward selectall(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {
	ListPromoForm element = (ListPromoForm) form;
	element.setFlag(!element.isFlag());
	String give = "reloadlist";
	return (mapping.findForward(give));
    }

    /**
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForward TODO urgente partir este monstruo.
     */
    public ActionForward massive(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {
	ActionForward forward = mapping.findForward("reloadlist");
	ListPromoForm listpromoForm = (ListPromoForm) form;
	User user = (User) request.getSession().getAttribute("user");
	ActionMessages localmessages = new ActionMessages();
	String[] checkedPromos = request.getParameterValues("promos_selected");
	// TODO pensar el a donde ir
	if (user == null) {
	    ActionMessage message = new ActionMessage("message.user.not.logged");
	    localmessages.add(ActionMessages.GLOBAL_MESSAGE, message);
	} else if (checkedPromos == null) {
	    ActionMessage message = new ActionMessage("message.at.least.one");
	    localmessages.add(ActionMessages.GLOBAL_MESSAGE, message);
	} else if (listpromoForm.getMassive().equals(
		"label.option.exportar.sql")
		|| listpromoForm.getMassive().equals(
			"label.option.exportar.oracle")) {
	    // Exportar promos si tiene permiso para exportar
	    if (user.getCanexportpromo()) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {// TODO refactorizar y crear funciones exportar, habilitar.
		    Collection<Promo> toExport = PromoSystem.getPromoStore()
			    .getAllPromo(true, checkedPromos);
		    if (toExport != null) {
			for (Promo promo : toExport) {
			    String exportedPromo;
			    if (listpromoForm.getMassive().equals(
				    "label.option.exportar.sql")) {
				exportedPromo = promo
					.export(Promo.EExportFormats.SQLSERVER);
			    } else {
				exportedPromo = promo
					.export(Promo.EExportFormats.ORACLE);
			    }
			    out.write(exportedPromo
				    .getBytes(PromoCommons.dbencoding));
			}
		    }
		    String fileName = PREFIX + DATEFORMAT.format(new Date())
			    + ".sql";
		    response.setContentType("text/plain");
		    response.setHeader("Content-disposition",
			    "attachment;filename=" + fileName);
		    response.setHeader("Content-Length",
			    String.valueOf(out.size()));
		    response.getOutputStream().write(out.toByteArray());
		    forward = null;
		} catch (Exception e) {
		    log.error("Exception in EditPromoPageAction.execute: ", e);
		}
	    } else {
		// No tiene permiso para exportar
		ActionMessage message = new ActionMessage(
			"xvalidate.cannotexport");
		localmessages.add(ActionMessages.GLOBAL_MESSAGE, message);
	    }
	} else if (listpromoForm.getMassive().equals("label.option.habilitar")) {
	    // Habilitar la promoci�n solo si tiene permisos para actualizar.
	    if (user.getCanupdatepromo()) {
		for (PromoSelected promoSelected : listpromoForm.getPromos()) {
		    promoSelected.setSelected(false);
		    for (String checkedPromo : checkedPromos) {
			if (promoSelected.getInternalId() == Integer
				.parseInt(checkedPromo)) {
			    promoSelected.setSelected(true);
			    try {
				if (!promoSelected.isEnabled()) {
				    PromoSystem.getPromoStore().enable(
					    promoSelected);
				}
			    } catch (Exception ex) {
				log.error("Error en habilitar promo.", ex);
				ActionMessage message = new ActionMessage(
					"errors.login.error", ex.getMessage());
				localmessages.add(
					ActionMessages.GLOBAL_MESSAGE, message);
			    }
			    break;
			}
		    }
		}
	    } else {
		// No tiene permiso para modificar
		ActionMessage message = new ActionMessage(
			"xvalidate.cannotedit");
		localmessages.add(ActionMessages.GLOBAL_MESSAGE, message);
	    }
	} else if (listpromoForm.getMassive().equals(
		"label.option.deshabilitar")) {
	    if (user.getCanupdatepromo()) {
		for (PromoSelected promoSelected : listpromoForm.getPromos()) {
		    // log.debug("promo-selected :" + id);
		    promoSelected.setSelected(false);
		    for (String checkedPromo : checkedPromos) {
			if (promoSelected.getInternalId() == Integer
				.parseInt(checkedPromo)) {
			    promoSelected.setSelected(true);
			    try {
				if (promoSelected.isEnabled()) {
				    PromoSystem.getPromoStore().disable(
					    promoSelected);
				}
			    } catch (Exception ex) {
				log.error("Error en habilitar promo.", ex);
				ActionMessage message = new ActionMessage(
					"errors.login.error", ex.getMessage());
				localmessages.add(
					ActionMessages.GLOBAL_MESSAGE, message);
			    }
			    break;
			}
		    }
		}
	    } else {
		// No tiene permiso para modificar
		ActionMessage message = new ActionMessage(
			"xvalidate.cannotedit");
		localmessages.add(ActionMessages.GLOBAL_MESSAGE, message);
	    }
	}
	// (+) eliminar
	else if (listpromoForm.getMassive().equals("label.option.eliminar")) {
	    if (user.getCandeletepromo()) {
		for (PromoSelected promoSelected : listpromoForm.getPromos()) {
		    promoSelected.setSelected(false);
		    for (String checkedPromo : checkedPromos) {
			if (promoSelected.getInternalId() == Integer
				.parseInt(checkedPromo)) {
			    promoSelected.setSelected(true);
			    try {

				int id = 0;
				for (int i = 0; i < checkedPromos.length; i++) {
				    id = Integer.parseInt(checkedPromos[i]);

				    PromoSystem.getPromoStore().delete(id);
				    PromoSystem.getProSucStore()
					    .deleteCodPromo(id);

				}
			    } catch (Exception ex) {
				log.error("Error en delete promo.", ex);
				ActionMessage message = new ActionMessage(
					"errors.login.error", ex.getMessage());
				localmessages.add(
					ActionMessages.GLOBAL_MESSAGE, message);
			    }
			    break;
			}
		    }
		}
	    } else {
		// No tiene permiso para eliminar
		ActionMessage message = new ActionMessage(
			"xvalidate.cannotdelete");
		localmessages.add(ActionMessages.GLOBAL_MESSAGE, message);
	    }
	}
	// /(-)

	// Grabando errores en el bean, si ocurrieron.
	if (!localmessages.isEmpty()) {
	    saveMessages(request, localmessages);
	}
	return forward;
    }

}
