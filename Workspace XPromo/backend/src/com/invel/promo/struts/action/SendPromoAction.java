/*
 * SendPromoAction.java
 *
 * Created on 16 de octubre de 2005, 10:53
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.invel.promo.struts.action;

import invel.framework.xform.DomUtils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.LookupDispatchAction;
import org.apache.struts.util.MessageResources;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.invel.common.InvelFtp;
import com.invel.common.PromoCommons;
import com.invel.common.PromoSystem;
import com.invel.promo.dao.ProSuc;
import com.invel.promo.dao.Promo;
import com.invel.promo.dao.PromoSelected;
import com.invel.promo.dao.SimpleSucursal;
import com.invel.promo.dao.Sucursal;
import com.invel.promo.dao.User;
import com.invel.promo.struts.form.AsociarProSucForm;

public class SendPromoAction extends LookupDispatchAction {

    protected static Log localLog = LogFactory.getLog(SendPromoAction.class);

    protected static MessageResources messagesRes = MessageResources
	    .getMessageResources("MessageResources");

    /**
     * Se necesita suprimir los warnings porque el campo keyMethodMap no esta
     * declarado adecuadamente en la clase padre.
     */
    @SuppressWarnings("unchecked")
    public SendPromoAction() {
	this.keyMethodMap = new HashMap<String, String>();
	this.keyMethodMap.put("button.compilar", "accept");
	this.keyMethodMap.put("button.prior", "prior");
	this.keyMethodMap.put("button.next", "next");
	this.keyMethodMap.put("button.back", "back");
	this.keyMethodMap.put("button.selectall", "selectall");
	this.keyMethodMap.put("button.changesuc", "changesucursal");
	this.keyMethodMap.put("button.analysis", "analysis");

    }

    @Override
    protected Map getKeyMethodMap() {
	return this.keyMethodMap;
    }

    public ActionForward back(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {

	return (mapping.findForward("back"));

    }

    /**
     * M�todo asociado al boton de analisis de competencias, llama a la
     * plantilla de transformaci�n y la aplica a las promociones seleccionadas.
     * 
     * Este metodo no tiene ning�n boton asociado porque tomaba demasiado tiempo
     * la generaci�n.
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForward
     */
    public ActionForward analysis(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {
	User user = (User) request.getSession().getAttribute("user");

	cleanTemp(new File(PromoCommons.tempPath));
	ArrayList<PromoSelected> promos = new ArrayList<PromoSelected>();
	FilterItems(form, request, promos);
	// permiso de leer para analizar las competencias.
	if (user.getCanreadpromo()) {
	    try {

		ByteArrayOutputStream result = new ByteArrayOutputStream(1024);
		StreamResult out = new StreamResult(result);
		StreamSource stylesheet = new StreamSource(
			PromoCommons.listadoXsl);
		StreamSource source = new StreamSource(PromoCommons.progXml);
		PrepareBigForm(promos, false, null);
		// Create transformer
		Transformer xf = TransformerFactory.newInstance()
			.newTransformer(stylesheet);
		// execute transformation
		xf.transform(source, out);
		response.getWriter().write(new String(result.toByteArray()));

	    } catch (Exception ex) {
		localLog.error("Error al generar analisis de competencias", ex);
	    }
	}
	return null;
    }

    /**
     * Asociado al combo de selecci�n de sucursal, y activado por un js.
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForward
     */
    public ActionForward changesucursal(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {
	@SuppressWarnings("hiding")
	ActionMessages messages = new ActionMessages();
	User user = (User) request.getSession().getAttribute("user");

	if (user == null) {

	    ActionMessage message = new ActionMessage("xvalidate.cannotread");
	    messages.add(ActionMessages.GLOBAL_MESSAGE, message);
	    saveMessages(request, messages);
	    return mapping.findForward("failure");
	}

	AsociarProSucForm prosucForm = (AsociarProSucForm) form;
	ArrayList<Promo> allpromos = new ArrayList<Promo>();

	// TODO solucionar esto, debe quedar todo en el bean
	request.setAttribute("sucursales", prosucForm.getSucursales());
	try {
	    int cod_sucursal = Integer.parseInt(prosucForm.getSucursal());
	    Collection<ProSuc> prosuc = PromoSystem.getProSucStore()
		    .getAllPromo(cod_sucursal,user.isImpuesto());

	    for (ProSuc promo : prosuc) {
		Promo apromo = null;
		try {
		    apromo = PromoSystem.getPromoStore().getPromo(
			    promo.getCod_promo(), false);
		} catch (com.invel.promo.dao.PromoStore.PromoNotFoundException ex) {
		    localLog.warn("Promoci�n invalida en tabla sucursales <-> promos");
		    PromoSystem.getProSucStore().deleteCodPromo(
			    promo.getCod_promo());
		}

		catch (Exception ex) {
		    localLog.error("Exception in SendPromoAction 1:" + ex);
		}
		if (apromo != null && apromo.isEnabled() == true) {
		    allpromos.add(apromo);
		}
	    }
	} catch (Exception ex) {
	    localLog.error("Exception in SendPromoAction 2:" + ex);
	}

	ArrayList<PromoSelected> promosToShow = new ArrayList<PromoSelected>();
	for (Promo promo : allpromos) {
	    PromoSelected promosele = new PromoSelected(promo);
	    promosele.setSelected(true);
	    promosToShow.add(promosele);
	}

	prosucForm.setPromociones(promosToShow);
	request.getSession().setAttribute("promos", promosToShow);
	return (mapping.findForward("failure"));
    }

    public ActionForward next(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {

	return (mapping.findForward("failure"));
    }

    public ActionForward prior(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {

	return (mapping.findForward("failure"));
    }

    @Deprecated
    public ActionForward selectall(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {

	AsociarProSucForm prosucForm = (AsociarProSucForm) form;
	Collection<PromoSelected> promosele = prosucForm.getPromociones();

	request.setAttribute("sucursales", prosucForm.getSucursales());

	for (PromoSelected art : promosele) {
	    art.setSelected(!art.isSelected());
	}
	// request.getSession().setAttribute("promos", promosele);
	return (mapping.findForward("failure"));
    }

    /**
     * M�todo asociado al bot�n de compilar, este termina generando los archivos
     * a mandar a las cajas.
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForward
     * @throws IOException
     * @throws ServletException
     * @throws Exception
     */
    public ActionForward accept(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws IOException, ServletException, Exception {

	String give = "failure";

	ActionMessages localMessages = new ActionMessages();
	ActionMessage message = null;

	// verificamos si el usuario esta logeuado y si tiene permiso para
	// compilar
	User user = (User) request.getSession().getAttribute("user");
	if (user == null) {
	    message = new ActionMessage("message.user.not.logged");
	    localMessages.add(ActionMessages.GLOBAL_MESSAGE, message);
	} else if (!user.getCancompilepromo()) {
	    message = new ActionMessage("xvalidate.cannotcompile");
	    localMessages.add(ActionMessages.GLOBAL_MESSAGE, message);
	} else {
	    cleanTemp(new File(PromoCommons.tempPath));// limpio el directorio
	    // creo el array de sucursales
	    ArrayList<SimpleSucursal> sucursales = new ArrayList<SimpleSucursal>();

	    // filtro las sucursales seleccionadas
	    filterSucursales(form, request, sucursales);
	    // creo el array de las pormos para las diferentes sucursales
	    ArrayList<PromoSelected> promos = null;

	    // limpio la variable de session de resultado de compilacion
	    if (request.getSession().getAttribute("compilerResult") != null) {
		request.getSession().setAttribute("compilerResult", null);
	    }
	    String pathSucursal = "";
	    // recorro las sucursales
	    for (SimpleSucursal simpleSucursal : sucursales) {

		pathSucursal = simpleSucursal.getPath();
		promos = new ArrayList<PromoSelected>();
		// filtra las promociones de la sucursal
		filterPromo(simpleSucursal.getCod_sucursal(), promos,user);
		if (!promos.isEmpty()) {
		    try {

			// genera los xml de las promos
			PrepareBigForm(promos, true, pathSucursal);
			// realiza las transformaciones
			getSourceCode(true, pathSucursal);
			getSQLCode(pathSucursal);

			// compila la promo
			Sucursal sucursal = PromoSystem.getSucursalStore()
				.getSucursal(simpleSucursal.getCod_sucursal());
			int compresult = getByteCode(pathSucursal, request);
			if (compresult == 0
				&& request.getParameter("button.ftp.yes.or.no") == null) {
			    // obtengo el objeto sucursal por id
			    Map<String, String> files = new HashMap<String, String>();
			    files.put(pathSucursal + PromoCommons.xpromoCod,
				    "promo.cod");
			    files.put(pathSucursal + PromoCommons.xpromoSQL,
				    "promo.sql");
			    // creo el objeto ftp de la sucursal
			    InvelFtp aftp = new InvelFtp(true, true,
				    sucursal.getIpFtp(), sucursal.getUserFtp(),
				    sucursal.getPasswordFtp(), files);
			    if (aftp.execute() == InvelFtp.ALL_RIGHT) {
				localLog.info(messagesRes
					.getMessage("message.ftp.successful"));
				try {
				    PromoSystem.getSucursalStore().sendScript(
					    sucursal);
				    localLog.info(messagesRes
					    .getMessage("message.script.successful"));
				    message = new ActionMessage(
					    "message.file.promo.sent",
					    sucursal.getDescripcion());
				    localMessages.add("confirms", message);
				} catch (Exception ex) {
				    localLog.error(
					    messagesRes
						    .getMessage("message.script.unsuccessful"),
					    ex);

				    message = new ActionMessage(
					    "message.file.promo.sent.unsuccessful",
					    sucursal.getDescripcion());
				    localMessages.add("errors", message);
				}
			    } else {
				message = new ActionMessage(
					"message.file.promo.sent.unsuccessful",
					sucursal.getDescripcion());
				localMessages.add("errors", message);
				localLog.error(messagesRes
					.getMessage("message.ftp.unsuccessful"));
			    }
			} else if (compresult == 0) {
			    message = new ActionMessage(
				    "message.compiler.successful",
				    sucursal.getDescripcion());
			    localMessages.add("confirms", message);
			} else {
			    message = new ActionMessage(
				    "message.compiler.unsuccessful",
				    sucursal.getDescripcion());
			    localMessages.add("errors", message);
			    localLog.error(messagesRes
				    .getMessage("message.compiler.unsuccessful"));
			}
		    } catch (Exception ex) {
			localLog.error("Error en plantillas de transf.", ex);
		    }
		    give = "success";
		} else {

		    message = new ActionMessage("message.sucursal.vacia");
		    localMessages.add(ActionMessages.GLOBAL_MESSAGE, message);
		}
	    }
	}

	if (!localMessages.isEmpty()) {
	    saveMessages(request.getSession(), localMessages);
	}

	return (mapping.findForward(give));

    }

    /**
     * Elimina todo el contenido del directorio temp antes de comenzar
     * 
     */
    private void cleanTemp(File pathTemp) {
	if (pathTemp.exists()) {
	    File[] files = pathTemp.listFiles();
	    for (File file : files) {
		if (file.isDirectory()) {
		    cleanTemp(file);
		} else {

		    file.delete();
		}
	    }
	}
	pathTemp.delete();
    }

    /**
     * Me devuelve en promos las promociones seleccionadas por el usuario.
     * 
     * @TODO hacer que el método devuelva un resultado, no que modifique
     *       parámetros.
     * @param form
     * @param request
     * @param promos
     */
    private void FilterItems(ActionForm form, HttpServletRequest request,
	    ArrayList<PromoSelected> promos) {

	AsociarProSucForm aprosucForm = (AsociarProSucForm) form;
	Collection<PromoSelected> promosele = aprosucForm.getPromociones();

	String[] paramValues = request.getParameterValues("promo-selected");
	if (paramValues != null) {
	    for (String id : paramValues) {
		// log.debug("promo-selected :" + id);
		for (PromoSelected promoSelected : promosele) {
		    if (promoSelected.getInternalId() == Integer.parseInt(id)) {
			promoSelected.setSelected(!promoSelected.isSelected());
			promos.add(promoSelected);
			break;
		    }
		}
	    }
	}
    }

    synchronized private int getByteCode(String pathSucursal,
	    HttpServletRequest request) throws Exception {

	int exitVal = 1;
	// modificar path del archivo
	File temp = new File(pathSucursal + PromoCommons.promoPro);
	if (!temp.exists()) {
	    throw new IOException("Imposible compilar. No existe archivo .pro");
	}
	// modificar el path del archivo
	FileOutputStream fos = new FileOutputStream(pathSucursal
		+ PromoCommons.xpromoTxt);
	PrintWriter pw = new PrintWriter(fos);

	/*
	 * ProcessBuilder pb = new ProcessBuilder("cmd", "/C",
	 * "\""+PromoCommons.compiler+"\"", PromoCommons.promoPro,"-o",
	 * PromoCommons.xpromoCod);
	 */
	ProcessBuilder pb = new ProcessBuilder(PromoCommons.compiler,
		pathSucursal + PromoCommons.promoPro, "-o", pathSucursal
			+ PromoCommons.xpromoCod);
	pb.directory(new File(PromoCommons.getWorkpath()));
	localLog.debug(pb.command());
	localLog.debug(pb.directory());
	pb.redirectErrorStream(true);
	Process proc = pb.start();
	/*
	 * Process proc = Runtime.getRuntime().exec(PromoCommons.compiler, null,
	 * new File(PromoCommons.workpath));
	 */

	BufferedReader br = new BufferedReader(new InputStreamReader(
		proc.getInputStream()));
	String line = null;

	// verifico q existe la variable de session para cargarle
	// el resultado de la compilacion
	StringBuilder compilerResult;
	if (request.getSession().getAttribute("compilerResult") == null)
	    compilerResult = new StringBuilder();
	else
	    compilerResult = (StringBuilder) request.getSession().getAttribute(
		    "compilerResult");

	// antes de escribir en el objeto
	compilerResult.append("\r");
	while ((line = br.readLine()) != null) {
	    pw.println(line);
	    log.info("Compiller> " + line);
	    // cargo el contenido del resultado de la compilaci�n en variable
	    // de session para mostrarla como resultado
	    compilerResult.append(line);
	    compilerResult.append("\r");
	}

	// cargo el texto del resultado de la compilacion en la variable de
	// session
	request.getSession().setAttribute("compilerResult", compilerResult);

	// any error???
	exitVal = proc.waitFor();

	localLog.info("ExitValue: " + exitVal);
	pw.flush();
	pw.close();
	fos.flush();
	fos.close();

	return exitVal;
    }

    /**
     * Ejecuta la transformaci�n consultas.xsl, que genera el archivo sql con
     * los distintos bolsones de art�culos para la caja.
     * 
     * @param file
     * @param pathSucursal
     */
    synchronized private void getSQLCode(String pathSucursal) throws Exception {
	/*
	 * Default system property for Xalan processor * /
	 * System.setProperty("javax.xml.transform.TransformerFactory",
	 * "org.apache.xalan.processor.TransformerFactoryImpl"); /*
	 * -------------------------------------------
	 */
	StreamResult out;
	StreamSource stylesheet = new StreamSource(
		new File(PromoCommons.sqlXsl));
	StreamSource source = new StreamSource(new File(pathSucursal
		+ PromoCommons.progXml));

	// Create transformer
	TransformerFactory factory = TransformerFactory.newInstance();
	Transformer xf;
	xf = factory.newTransformer(stylesheet);
	out = new StreamResult(pathSucursal + PromoCommons.xpromoSQL);

	xf.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

	// Perform the transformation
	xf.transform(source, out);

    }

    synchronized private void getSourceCode(boolean file, String pathSucursal)
	    throws Exception {

	/*
	 * Default system property for Xalan processor * /
	 * System.setProperty("javax.xml.transform.TransformerFactory",
	 * "org.apache.xalan.processor.TransformerFactoryImpl"); /*
	 * -------------------------------------------
	 */
	StreamResult out;
	StreamSource stylesheet = new StreamSource(new File(
		PromoCommons.progXsl));
	StreamSource source = new StreamSource(new File(PromoCommons.treeXml));

	// Create transformer
	TransformerFactory factory = TransformerFactory.newInstance();
	Transformer xf;
	xf = factory.newTransformer(stylesheet);
	xf.setParameter("pathProgXml", pathSucursal + PromoCommons.progXml);
	// Output
	if (file) {
	    // modificar path del archivo
	    out = new StreamResult(pathSucursal + PromoCommons.promoPro);
	} else {
	    out = new StreamResult(System.out);
	}

	// Set output encoding property
	// xf.setOutputProperty(OutputKeys.METHOD, "text");
	// xf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
	xf.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

	// Perform the transformation
	xf.transform(source, out);

    }

    synchronized private void PrepareBigForm(Collection<PromoSelected> promos,
	    boolean header, String sucursalPath) throws Exception {

	FileOutputStream file = null;
	String aform = null;
	StringBuilder strbuff = new StringBuilder("Promociones Elegidas --> ");
	DocumentBuilder docBuilder = DomUtils.getDocBuilder();

	Document progDoc = docBuilder.newDocument();
	Element progRoot = progDoc.createElement("prog");
	progDoc.appendChild(progRoot);
	if (header) {
	    appendHeader(docBuilder, progDoc, progRoot);
	}

	for (PromoSelected promo : promos) {
	    Promo promoxml = PromoSystem.getPromoStore().getPromo(
		    promo.getInternalId(), true);
	    Document promoDoc = docBuilder.parse(new ByteArrayInputStream(
		    promoxml.getStringXML().getBytes("UTF-8")));
	    Element promoRoot = promoDoc.getDocumentElement();
	    Node progPromo = progDoc.importNode(promoRoot, true);
	    // Node progPromo = progDoc.adoptNode(promoRoot);
	    progRoot.appendChild(progPromo);
	    strbuff.append("id=" + promo.getInternalId() + " ");
	}

	aform = DomUtils.domToString(progDoc, null, PromoCommons.dbencoding,
		false);

	localLog.info(strbuff.toString());

	try {
	    // modificar el path de la sucursal
	    file = new FileOutputStream(sucursalPath + PromoCommons.progXml);
	    if (aform != null) {
		file.write(aform.getBytes(PromoCommons.dbencoding));
	    }
	} finally {
	    if (file != null) {
		file.close();
	    }
	}
    }

    /**
     * Agrega el bloque unico de configuraciones globales que afectan
     * directamente al motor de promociones
     * 
     * @param docBuilder
     * @param progDoc
     * @param progRoot
     */
    private static void appendHeader(DocumentBuilder docBuilder,
	    Document progDoc, Element progRoot) {

	Element promo = progDoc.createElement("promo");
	Element cupon_error = progDoc.createElement("cupon_error");
	Element cupon_error_vigencia = progDoc
		.createElement("cupon_error_vigencia");
	Element cupon_error_limite = progDoc
		.createElement("cupon_error_limite");
	Element consultas = progDoc.createElement("consultas");
	Element validar_afinidad = progDoc.createElement("validar_afinidad");
	validar_afinidad.setTextContent(PromoCommons.validar_afinidad);
	validar_afinidad.setAttribute("datatype", "bool");

	Element validar_vigencia = progDoc.createElement("validar_vigencia");
	validar_vigencia.setTextContent(PromoCommons.validar_vigencia);
	validar_vigencia.setAttribute("datatype", "bool");

	Element validar_rangofecha = progDoc
		.createElement("validar_rangofecha");
	validar_rangofecha.setTextContent(PromoCommons.validar_rangofecha);
	validar_rangofecha.setAttribute("datatype", "bool");

	Element validar_rangohora = progDoc.createElement("validar_rangohora");
	validar_rangohora.setTextContent(PromoCommons.validar_rangohora);
	validar_rangohora.setAttribute("datatype", "bool");

	Element validar_rangocaja = progDoc.createElement("validar_rangocaja");
	validar_rangocaja.setTextContent(PromoCommons.validar_rangocaja);
	validar_rangocaja.setAttribute("datatype", "bool");

	Element validar_perfil = progDoc.createElement("validar_perfil");
	validar_perfil.setTextContent(PromoCommons.validar_perfil);
	validar_perfil.setAttribute("datatype", "bool");

	Element validar_rangotarjeta = progDoc
		.createElement("validar_rangotarjeta");
	validar_rangotarjeta.setTextContent(PromoCommons.validar_rangotarjeta);
	validar_rangotarjeta.setAttribute("datatype", "bool");

	consultas.appendChild(validar_afinidad);
	consultas.appendChild(validar_vigencia);
	consultas.appendChild(validar_rangofecha);
	consultas.appendChild(validar_rangohora);
	consultas.appendChild(validar_rangocaja);
	consultas.appendChild(validar_perfil);
	consultas.appendChild(validar_rangotarjeta);
	promo.appendChild(consultas);

	promo.setAttribute("promotype", "global_config");
	promo.setAttribute("block", "0");
	promo.setAttribute("priority", "0");

	// Numero de validacion del cupon a emitirse en caso
	// que fallen las asignaciones a monedero electronico
	cupon_error.setTextContent(String.valueOf(PromoCommons.cuponerror));
	// Cantidad de dias por los que el cupon estara vigente
	cupon_error_vigencia.setTextContent(String
		.valueOf(PromoCommons.cuponerrorvigencia));
	// Fecha limite hasta la que estara vigente el cupon
	cupon_error_limite.setTextContent(PromoCommons.cuponerrorlimite);

	promo.appendChild(cupon_error);
	promo.appendChild(cupon_error_vigencia);
	promo.appendChild(cupon_error_limite);

	Node progPromo = progDoc.importNode(promo, true);
	progRoot.appendChild(progPromo);
    }

    /**
     * Metodo q arma el array con las sucursales seleccionadas ppara compilar.
     * 
     * @param form
     *            Objeto ActionForm
     * @param request
     *            Objeto HttpServletRequest
     * @param sucursales
     *            Array al q se la van agregar los objetos sucursales
     *            seleccionados
     */
    private void filterSucursales(ActionForm form, HttpServletRequest request,
	    ArrayList<SimpleSucursal> sucursales) {

	AsociarProSucForm asociarProSucForm = (AsociarProSucForm) form;
	Collection<SimpleSucursal> sucursalesSel = asociarProSucForm
		.getSucursales();

	String[] paramValues = request.getParameterValues("suc-selected");
	if (paramValues != null) {
	    for (String id : paramValues) {
		for (SimpleSucursal sucursal : sucursalesSel) {
		    if (sucursal.getCod_sucursal() == Integer.valueOf(id)) {
			sucursal.setSelected(!sucursal.isSelected());
			sucursales.add(sucursal);
		    }
		}
	    }
	}

    }

    /**
     * Metodo q crea una lista con las promociones de la sucursal seleccionada
     * para compilar.
     * 
     * @param cod_sucursal
     *            C{odigo de la sucursal
     * @param promos
     *            Objeto Array al q se le van a cargar las promociones
     */
    private void filterPromo(Integer cod_sucursal,
	    ArrayList<PromoSelected> promos,User user) {
	
	Collection<ProSuc> collProSuc = null;
	try {
	    collProSuc = PromoSystem.getProSucStore().getAllPromo(cod_sucursal,user.isImpuesto());
	} catch (Exception ex) {
	    log.error("Error en las busqueda de Promociones por Sucursal: "
		    + cod_sucursal);
	    log.error("Error: " + ex.getMessage());
	}

	PromoSelected promoSelected;
	if (collProSuc != null) {
	    for (ProSuc proSuc : collProSuc) {
		Promo promo = null;
		try {
		    promo = PromoSystem.getPromoStore().getPromo(
			    proSuc.getCod_promo(), true);
		} catch (Exception ex) {
		    log.error("Error en las busqueda de Promoci�n por id: "
			    + proSuc.getCod_promo());
		    log.error("Error: " + ex.getMessage());
		}
		if (promo != null) {
		    promoSelected = new PromoSelected(promo);
		    promos.add(promoSelected);
		}
	    }
	}
    }

}
