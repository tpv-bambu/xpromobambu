/*
 * FindArticlesAction.java
 *
 * Created on 1 de octubre de 2005, 10:31
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.invel.promo.struts.action;

import java.io.*;
import java.sql.SQLException;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.*;
import org.apache.struts.actions.LookupDispatchAction;
import org.apache.struts.upload.FormFile;

import com.invel.common.PromoCommons;
import com.invel.common.PromoSystem;
import com.invel.promo.dao.*;
import com.invel.promo.struts.form.FindArticleForm;

public class FindArticlesAction extends LookupDispatchAction {

    // Local Forwards
    public static final String FORWARD_showpage = "showpage";

    public static final String FORWARD_loadArticle = "loadArticle";

    private static final Log LOG = LogFactory.getLog(PrepareFormAction.class);

    @SuppressWarnings("unchecked")
    public FindArticlesAction() {
	this.keyMethodMap = new HashMap();
	this.keyMethodMap.put("button.accept", "accept");
	this.keyMethodMap.put("button.invertir", "invertirsel");
	this.keyMethodMap.put("button.add", "add");
	this.keyMethodMap.put("button.selectall", "selecall");
	this.keyMethodMap.put("button.delete", "delete");
	this.keyMethodMap.put("button.find", "find");
	this.keyMethodMap.put("button.cancel", "cancel");
	this.keyMethodMap.put("button.import.articles", "importar");
    }

    @Override
    protected Map getKeyMethodMap() {
	return this.keyMethodMap;
    }

    public ActionForward cancel(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {

	FindArticleForm element = (FindArticleForm) form;
	element.setAction("cancela");
	return (mapping.findForward(FORWARD_loadArticle));
    }

    public ActionForward accept(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {

	FindArticleForm element = (FindArticleForm) form;

	element.setAction("acepta");
	return (mapping.findForward(FORWARD_loadArticle));
    }

    /**
     * Agrega articulos a la promoci�n, leo los valores comunicados por el
     * cliente y marco estos elementos como pertenecientes a la promocion.
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForward
     */
    public ActionForward add(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {

	FindArticleForm element = (FindArticleForm) form;

	List<Articulo> listArticles = element.getSearch();
	List<Articulo> cart = element.getCart();
	if (cart == null) {
	    element.setCart(new ArrayList<Articulo>());
	}
	String[] paramValues = request.getParameterValues("art-selected");
	// verifico si no esta inicializada
	if (paramValues != null) {
	    for (String sIndex : paramValues) {
		int index = Integer.valueOf(sIndex);
		Articulo art = listArticles.get(index);
		if (cart == null || !cart.contains(art)) {
		    element.getCart().add(art);
		}

	    }
	}

	// request.setAttribute("articulos", element.getArticles());
	return (mapping.findForward(FORWARD_showpage));
    }

    /**
     * Elimina articulos de la promo, es sacarlos de la coleccion.
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForward
     */
    public ActionForward delete(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {

	FindArticleForm element = (FindArticleForm) form;
	List<Articulo> cart = element.getCart();
	List<Articulo> newCart = new ArrayList<Articulo>();

	String[] paramValues = request.getParameterValues("articleincart");
	// verifico si no esta inicializada
	if (paramValues != null) {
	    for (String sIndex : paramValues) {
		int index = Integer.valueOf(sIndex);
		cart.set(index, null);
	    }
	    for (Articulo art : cart) {
		if (art != null) {
		    newCart.add(art);
		}
	    }
	}
	element.setCart(newCart);
	return (mapping.findForward(FORWARD_showpage));
    }

    /**
     * Asociado a la b�squeda en la base de datos, agrega los articulos en la
     * colecci�n (eliminando antes todos los que no est�n en la promoci�n).
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForward
     * @throws IOException
     * @throws ServletException
     * @throws Exception
     */
    public ActionForward find(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws IOException, ServletException, Exception {

	FindArticleForm element = (FindArticleForm) form;

	String field = element.getFilterfield();
	String begin = element.getBeginvalue();
	String end = element.getEndvalue();

	List<Articulo> articlesDB = null;
	@SuppressWarnings("hiding")
	ActionMessages messages = new ActionMessages();
	ActionMessage message = null;

	if (begin.length() == 0 && end.length() == 0) {
	    message = new ActionMessage("message.fields.from.and.to.empty");
	    messages.add(ActionMessages.GLOBAL_MESSAGE, message);
	} else if (begin.length() == 0) {
	    message = new ActionMessage("message.field.from.empty");
	    messages.add(ActionMessages.GLOBAL_MESSAGE, message);
	} else if (end.length() == 0) {
	    message = new ActionMessage("message.field.to.empty");
	    messages.add(ActionMessages.GLOBAL_MESSAGE, message);
	} else if ("codbarra".equals(field) || "codint2".equals(field)
		|| "codint".equals(field)) {
	    long nBegin = 0, nEnd = 0;
	    try {
		// Se cambia el parseo a Long para numeros grandes DS 13/09/2007
		nBegin = Long.parseLong(element.getBeginvalue().trim());
		nEnd = Long.parseLong(element.getEndvalue().trim());
		if (nBegin > nEnd) {
		    // Se cambia el mensaje que muestra si: nBegin > nEnd DS
		    // 13/09/2007
		    // message = new
		    // ActionMessage("message.begin.must.be.bigger.than.end");
		    message = new ActionMessage(
			    "message.begin.must.be.smaller.than.end");
		} else if (nBegin < 0 || nEnd < 0) {
		    // Se cambia el mensaje que muestra si es negativo DS
		    // 13/09/2007
		    // message = new ActionMessage("no negativo");
		    message = new ActionMessage(
			    "message.begin.or.end.is.negative");
		}
	    } catch (NumberFormatException ex) {
		// Se cambia el mensaje que muestra si no es numero DS
		// 13/09/2007
		// message = new ActionMessage("no numero");
		message = new ActionMessage(
			"message.begin.or.end.is.not.a.number");
	    }
	    if (message != null) {
		messages.add(ActionMessages.GLOBAL_MESSAGE, message);
	    } else {
		// OK
		int width = 0;
		String order = null, where = null;
		if ("codbarra".equals(field)) {
		    width = PromoCommons.getLengthBarCode();
		    element.setBeginvalue(String.format("%0" + width + "d",
			    nBegin));
		    element.setEndvalue(String.format("%0" + width + "d", nEnd));
		    order = ArticuloStore.ORDER_BARRA;
		    where = "codigo_barra";
		} else if ("codint2".equals(field)) {
		    width = PromoCommons.getLengthInnerCode();
		    element.setBeginvalue(String.format("%0" + width + "d",
			    nBegin));
		    element.setEndvalue(String.format("%0" + width + "d", nEnd));
		    order = ArticuloStore.ORDER_ALFA;
		    where = "cod_interno2";
		} else {
		    order = ArticuloStore.ORDER_CODE;
		    where = "cod_interno";
		}
		articlesDB = PromoSystem.getArticuloStore().getAllArticulo(
			order, where, element.getBeginvalue(),
			element.getEndvalue());
	    }
	}

	else if ("nombre".equals(field)) {
	    if (begin.compareTo(end) > 0) {
		message = new ActionMessage(
			"message.begin.must.be.bigger.than.end");
		messages.add(ActionMessages.GLOBAL_MESSAGE, message);
	    } else {
		articlesDB = PromoSystem.getArticuloStore().getAllArticulo(
			ArticuloStore.ORDER_NAME, "nombre", begin, end);
	    }
	} else if ("marca".equals(field)) {
	    if (begin.compareTo(end) > 0) {
		message = new ActionMessage(
			"message.begin.must.be.bigger.than.end");
		messages.add(ActionMessages.GLOBAL_MESSAGE, message);
	    } else {
		articlesDB = PromoSystem.getArticuloStore().getAllArticulo(
			ArticuloStore.ORDER_MARCA, "marca", begin, end);
	    }
	}
	if (articlesDB != null) {

	    element.setSearch(articlesDB);

	}

	if (!messages.isEmpty()) {
	    saveMessages(request, messages);
	}

	// request.setAttribute("articulos", element.getArticles());
	return (mapping.findForward(FORWARD_showpage));
    }

    /**
     * M�todo asociado a la importaci�n de art�culos, ya sea desde un archivo
     * excel o un archivo de texto plano.
     * 
     * La importacion elimina todos los articulos en la promo y deja solo
     * aquellos a importar.
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForward
     */
    public synchronized ActionForward importar(ActionMapping mapping,
	    ActionForm form, HttpServletRequest request,
	    HttpServletResponse response) {

	String give = FORWARD_showpage;
	ActionMessages localmessages = new ActionMessages();
	ActionMessage message = null;
	FindArticleForm element = (FindArticleForm) form;
	FormFile file = element.getArticlesToImport();
	BufferedReader reader = null;
	String countArtImport = "0";
	try {
	    if (file == null || file.getFileSize() == 0) {
		throw new IOException("Archivo a importar null");
	    }
	    reader = new BufferedReader(new InputStreamReader(
		    file.getInputStream()));
	} catch (IOException ex) {
	    LOG.error("Error al leer archivo de articulos a importar", ex);
	    message = new ActionMessage("message.articles.file",
		    ex.getMessage());
	}
	if (reader != null && file != null) {
	    try {
		String filestring = file.toString().trim();
		int lengthfile = filestring.length();
		int position = lengthfile - 3;
		String extension = filestring.substring(position, lengthfile);

		// me aseguro que no haya articulos repetidos con un set.
		HashSet<String> set = new HashSet<String>();

		// verifico q tipo de archivo estoy importando
		if (extension.equalsIgnoreCase("txt")) {
		    String line = reader.readLine();
		    while (line != null) {
			set.add(line.trim());
			line = reader.readLine();
		    }
		    // una vez usados los objetos los destruyo
		    try {
			file.destroy();
			reader.close();
		    } catch (IOException ex) {
			LOG.error(ex.getMessage(), ex);
		    }
		}// fin txt
		else if (extension.equalsIgnoreCase("xls")) {
		    // excel file
		    try {
			Workbook workbook = Workbook.getWorkbook(file
				.getInputStream());
			Sheet sheet = workbook.getSheet(0);

			for (int i = 0; i < sheet.getRows() - 1; i++) {
			    String string_col = getcelldata(i, sheet);
			    String insert_str = string_col.split("\\.")[0];
			    // insertar todo
			    set.add(insert_str);
			}
			workbook.close();
		    } catch (Exception ex) {
			LOG.error("Error al leer archivo excel.", ex);
			message = new ActionMessage("message.articles.file",
				ex.getMessage());
			localmessages.add("errors", message);
		    }
		} else {
		    LOG.error("Archivo no soportado.");
		    message = new ActionMessage(
			    "message.articles.unsupported.file");
		    localmessages.add("errors", message);
		}

		// obtengo los articulos y seteo la lista del form
		List<Articulo> imported = PromoSystem.getArticuloStore()
			.importArticulo(set);
		element.setCart(imported);
		// obtengo la cantidad de articulos procesados
		countArtImport = Integer.valueOf(imported.size()).toString();

		// obtengo la lista de articulos lo encontrados en la
		// importacion
		Collection<String> missing = PromoSystem.getArticuloStore()
			.getImportMissing();
		// los logueo a los articulo no encontrados, pero permito la
		// importacion.
		for (String articulo : missing) {
		    LOG.warn("Articulo no encontrado en BD (importar): "
			    + articulo);

		    message = new ActionMessage("message.article.not.found",
			    articulo);
		    localmessages.add("errors", message);
		}
		// fin comun
		set.clear();
		missing.clear();
		// imported.clear();
	    } catch (IOException ex) {
		LOG.error("Error al leer archivo de articulos a importar", ex);
		message = new ActionMessage("message.articles.file",
			ex.getMessage());
		localmessages.add("errors", message);
	    } catch (SQLException ex) {
		LOG.error("Error en base de datos al importar archivo", ex);
		message = new ActionMessage("errors.login.error",
			ex.getMessage());
		localmessages.add("errors", message);
	    }
	}
	// genero el mensaje de cantidad de articulos importados
	message = new ActionMessage("message.articles.inpromo", countArtImport);
	localmessages.add("confirms", message);
	// registro los mensajes para ser visualizados
	if (!localmessages.isEmpty()) {
	    saveMessages(request, localmessages);
	}
	// request.setAttribute("articulos", element.getArticles());
	return mapping.findForward(give);
    }

    /**
     * Obtiene el valor de cada celda del excel par�metros: i= n� fila, sheet=
     * n� de hoja
     * 
     * @return String
     */

    private static String getcelldata(int i, Sheet sheet) {
	String string_col = "";
	double number_cell = 0;
	Cell col_a = sheet.getCell(0, i);

	if (col_a.getType() == CellType.LABEL) {
	    LabelCell lc = (LabelCell) col_a;
	    string_col = lc.getString();
	}

	if (col_a.getType() == CellType.NUMBER) {
	    NumberCell lc = (NumberCell) col_a;
	    number_cell = lc.getValue();
	    string_col = String.valueOf(number_cell);
	    if (string_col.length() >= 11) {
		String[] temp1 = string_col.split("E");
		string_col = temp1[0];
		String[] temp2 = string_col.split("\\.");
		string_col = temp2[0] + temp2[1];
	    }

	}
	if (col_a.getType() == CellType.EMPTY) {
	    string_col = "";
	}
	return string_col;

    }

    /**
     * Limpia de la coleccion de articulos todos aquellos que no estan en la
     * promo.
     * 
     * @param articulos
     * 
     *            private static final void
     *            cleanArticulos(List<ArticuloSelected> articulos){ for(int
     *            i=0;i<articulos.size();i++){ ArticuloSelected temp =
     *            articulos.get(i); if(!temp.isInPromo()){ articulos.remove(i);
     *            } } }
     */

}
