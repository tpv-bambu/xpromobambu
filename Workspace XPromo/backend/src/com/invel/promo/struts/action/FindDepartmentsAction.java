package com.invel.promo.struts.action;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.LookupDispatchAction;

import com.invel.promo.dao.DepartamentoSelected;
import com.invel.promo.struts.form.FindDepartmentForm;

public class FindDepartmentsAction extends LookupDispatchAction {

    public static final String FORWARD_loadDepartment = "loadDepartment";

    @SuppressWarnings("unchecked")
    public FindDepartmentsAction() {
	this.keyMethodMap = new HashMap();
	this.keyMethodMap.put("button.accept", "accept");
	this.keyMethodMap.put("button.prior", "prior");
	this.keyMethodMap.put("button.next", "next");
	this.keyMethodMap.put("button.back", "back");
    }

    @Override
    protected Map getKeyMethodMap() {
	return this.keyMethodMap;
    }

    private void FilterItems(HttpServletRequest request,
	    Collection<DepartamentoSelected> deptos,
	    Collection<DepartamentoSelected> deptosele) {

	if (deptos != null) {
	    for (DepartamentoSelected dptoSelected : deptos) {
		dptoSelected.setSelected(false);
	    }

	    String[] paramValues = request.getParameterValues("art-selected");
	    if (paramValues != null) {
		for (String id : paramValues) {
		    for (DepartamentoSelected dptoSelected : deptos) {
			if (dptoSelected.getCod_depto() == Long.parseLong(id)) {
			    dptoSelected
				    .setSelected(!dptoSelected.isSelected());
			    if (!deptosele.contains(dptoSelected)) {
				deptosele.add(dptoSelected);
			    }
			}
		    }
		}
	    }

	    for (Iterator<DepartamentoSelected> itemsIt = deptosele.iterator(); itemsIt
		    .hasNext();) {
		DepartamentoSelected anMarked = itemsIt.next();
		if (deptos.contains(anMarked)) {
		    for (DepartamentoSelected item : deptos) {
			if (item.equals(anMarked) && !item.isSelected()) {
			    itemsIt.remove();
			}
		    }
		}
	    }
	}
    }

    public ActionForward accept(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {

	FindDepartmentForm element = (FindDepartmentForm) form;
	FilterItems(request, element.getAllDepartments(),
		element.getDepartments());
	element.setAction("acepta");
	return (mapping.findForward(FORWARD_loadDepartment));
    }

    public ActionForward back(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {

	FindDepartmentForm element = (FindDepartmentForm) form;
	element.setAction("acepta");
	return (mapping.findForward(FORWARD_loadDepartment));

    }
}
