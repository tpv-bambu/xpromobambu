package com.invel.promo.struts.action;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ExceptionHandler;
import org.apache.struts.config.ExceptionConfig;

import com.invel.common.PromoSystem;

public class InvelExceptionHandler extends ExceptionHandler {

    private static Log myLog = LogFactory.getLog(PromoSystem.class);

    /**
   * 
   */
    @Override
    public ActionForward execute(java.lang.Exception ex, ExceptionConfig ae,
	    ActionMapping mapping, ActionForm formInstance,
	    javax.servlet.http.HttpServletRequest request,
	    javax.servlet.http.HttpServletResponse response) {
	String redirect = "RunTimeError";
	if (ex instanceof javax.servlet.ServletException) {
	    redirect = "initmainpromo";
	}
	myLog.error("Excepcion no capturada: ", ex);
	return mapping.findForward(redirect);
    }

}
