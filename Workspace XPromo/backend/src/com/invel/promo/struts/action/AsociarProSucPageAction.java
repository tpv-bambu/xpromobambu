package com.invel.promo.struts.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;

import com.invel.common.PromoSystem;
import com.invel.promo.dao.ProSuc;
import com.invel.promo.dao.Promo;
import com.invel.promo.dao.PromoSelected;
import com.invel.promo.dao.SimpleProSuc;
import com.invel.promo.dao.SimpleSucursal;
import com.invel.promo.dao.User;
import com.invel.promo.struts.form.AsociarProSucForm;

/**
 * Clase action q visualiza y registra la asociaci�n entre promociones y
 * sucursales.
 * 
 * @author
 * @version 1.0
 */
public class AsociarProSucPageAction extends DispatchAction {

    static Log localLog = LogFactory.getLog(AsociarProSucPageAction.class);

    /**
     * Carga todas las promociones de una sucursal dada.
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForward
     * @throws Exception
     */
    public ActionForward searchPromoByBranch(ActionMapping mapping,
	    ActionForm form, HttpServletRequest request,
	    HttpServletResponse response) throws Exception {

	AsociarProSucForm prosucForm = (AsociarProSucForm) form;
	@SuppressWarnings("hiding")
	ActionMessages messages = new ActionMessages();

	String rta = "success";

	// verificamos si el usuario est� en la session
	User user = (User) request.getSession().getAttribute("user");
	if (user == null || !user.getCanassociatepromo()) {
	    saveError(request, messages, "xvalidate.cannotassociate");
	    prosucForm.setFilter(0);
	    return mapping.findForward(rta);
	}

	// creamos los objetos array y collection para las promos y sucursales
	Collection<SimpleSucursal> sucursales = new ArrayList<SimpleSucursal>();
	try {
	    // obtenemos las sucursales
	    sucursales = PromoSystem.getSucursalStore().getAllSucursal();
	    // seteamos las colleccion de sucursales a la propiedad del form
	    prosucForm.setSucursales(sucursales);
	    // ponemos en request las sucursales
	    request.setAttribute("sucursales", sucursales);
	} catch (Exception e) {
	    localLog.error("Exception in AsociarProSucPageAction 1: " + e);
	}

	// obtenemos el id de la sucursal
	String sucursal = prosucForm.getSucursal();
	int cod_sucursal = 1;
	if (sucursal != null) {
	    try {
		// parseamos el codigo de sucursal a Interger
		cod_sucursal = Integer.parseInt(sucursal);
	    } catch (Exception e) {
		localLog.error("Excepcion en AsociarProSucPageAction 2: " + e);
	    }
	} else {
	    cod_sucursal = sucursales.iterator().next().getCod_sucursal();
	    prosucForm.setSucursal(String.valueOf(cod_sucursal));
	}

	try {
	    // creo el array de las promos q voy a mostrar en el jsp
	    ArrayList<PromoSelected> promosToShow = new ArrayList<PromoSelected>();
	    // obtengo en una collecci�n todas las promos
	    Collection<Promo> allpromos = PromoSystem.getPromoStore()
		    .getAllPromo(false,user.isImpuesto());
	    if (allpromos != null) {
		// itero las promociones de la collecci�n
		for (Iterator<Promo> it = allpromos.iterator(); it.hasNext();) {
		    Promo apromo = it.next();
		    // verifico si esta habilitada
		    if (apromo.isEnabled() == true) {
			PromoSelected promo = new PromoSelected(apromo);
			try {
			    // busco la promocion - sucursal
			    ProSuc prosuc = PromoSystem.getProSucStore()
				    .getProSuc(cod_sucursal,
					    promo.getInternalId());
			    // si la promo es de la sucursal la seleccionamos
			    if (prosuc != null) {
				promo.setSelected(true);
			    }
			} catch (Exception e) {
			    promo.setSelected(false);
			}
			// agregamos a la collecci�n la promo
			promosToShow.add(promo);
		    }
		}
	    }
	    // setemos en la collecci�n del form de la acci�n la collecci�n
	    // obtenida
	    prosucForm.setPromociones(promosToShow);
	    // seteamos el filtro q estamos ejecutando
	    prosucForm.setFilter(0);

	} catch (Exception e) {
	    localLog.error("Exception in AsociarPromoPageAction.searchPromoByBranch: "
		    + e);
	}

	return mapping.findForward(rta);

    }

    /**
     * Acci�n q carga las sucursales asociadas a una promocion dada.
     * promociones.
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForward
     * @throws Exception
     */
    public ActionForward searchBranchByPromo(ActionMapping mapping,
	    ActionForm form, HttpServletRequest request,
	    HttpServletResponse response) throws Exception {

	AsociarProSucForm prosucForm = (AsociarProSucForm) form;
	ActionMessages localMessages = new ActionMessages();

	String rta = "success";

	// verificamos si el usuario est� en la session
	User user = (User) request.getSession().getAttribute("user");
	// validamos las credenciales del usuario
	if (user == null || !user.getCanassociatepromo()) {
	    saveError(request, localMessages, "xvalidate.cannotassociate");
	    prosucForm.setFilter(1);
	    return mapping.findForward(rta);
	}

	try {

	    // obtengo las promociones habilitadas
	    Collection<PromoSelected> allPromoEnabled = PromoSystem
		    .getPromoStore().getAllPromoEnabled(user.isImpuesto());
	    if (allPromoEnabled != null) {
		request.setAttribute("promosEnabled", allPromoEnabled);

		// obtenemos el codigo de la promocion
		String cod_promocion = prosucForm.getPromotion();
		if (cod_promocion == null) {
		    cod_promocion = Integer.valueOf(
			    allPromoEnabled.iterator().next().getInternalId())
			    .toString();
		}

		// obtengo todas las sucursales
		Collection<SimpleSucursal> sucursales = new ArrayList<SimpleSucursal>();
		sucursales = PromoSystem.getSucursalStore().getAllSucursal();

		// creo el array para agregarle las sucursales
		ArrayList<SimpleSucursal> sucursalToShow = new ArrayList<SimpleSucursal>();
		for (SimpleSucursal simpleSucursal : sucursales) {

		    try {
			// busco la promocion - sucursal
			ProSuc prosuc = PromoSystem.getProSucStore().getProSuc(
				simpleSucursal.getCod_sucursal(),
				Integer.valueOf(cod_promocion).intValue());
			// si la promo es de la sucursal la seleccionamos
			if (prosuc != null)
			    simpleSucursal.setSelected(true);
		    } catch (Exception e) {
			simpleSucursal.setSelected(false);
		    }
		    sucursalToShow.add(simpleSucursal);
		}
		// seteamos las colleccion de sucursales a la propiedad del form
		prosucForm.setSucursales(sucursalToShow);
	    }

	} catch (Exception ex) {
	    localLog.error("Exception in AsociarPromoPageAction.searchBranchByPromo: "
		    + ex);
	}

	// seteo el filtro q estoy usando
	prosucForm.setFilter(1);

	return mapping.findForward(rta);

    }

    /**
     * Acci�n q asocia promociones a sucursales y sucursales a promociones.
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForward
     * @throws Exception
     */
    public ActionForward asociatePromoBranch(ActionMapping mapping,
	    ActionForm form, HttpServletRequest request,
	    HttpServletResponse response) throws Exception {

	AsociarProSucForm prosucForm = (AsociarProSucForm) form;
	@SuppressWarnings("hiding")
	ActionMessages messages = new ActionMessages();

	String rta = "success";

	// verificamos si el usuario est� en la session
	User user = (User) request.getSession().getAttribute("user");
	// validamos las credenciales del usuario
	if (user == null || !user.getCanassociatepromo()) {
	    // estilo de mensaje a visualizar
	    saveError(request, messages, "xvalidate.cannotassociate");
	    return mapping.findForward(rta);
	}

	// verifico si el usuario se encuentra en request
	User usuario = (User) request.getSession().getAttribute("user");
	if (usuario != null) {
	    try {
		StringBuilder toLog = new StringBuilder(32);

		// verifico segun la variable de request filter q estoy
		// ejecutando
		// en el form
		if (prosucForm.getFilter().equals(0)) {
		    // obtengo las promociones seleccionadas
		    String[] promoValues = request
			    .getParameterValues("promo_selected");
		    Integer cod_sucursal = null;
		    if (prosucForm.getSucursal() != null)
			cod_sucursal = Integer
				.valueOf(prosucForm.getSucursal());
		    // elimino las promociones de la sucursal
		    try {
			PromoSystem.getProSucStore().deleteCodSuc(cod_sucursal,user.isImpuesto());
		    } catch (Exception e) {
			localLog.error("Exception in Delete from Prosuc " + e);
		    }
		    if (promoValues != null) {
			if (cod_sucursal != null) {
			    for (String id : promoValues) {
				toLog.append(id);
				toLog.append(", ");
				// registro la promocion en la sucursal
				SimpleProSuc aprosuc = new SimpleProSuc(
					cod_sucursal, Integer.parseInt(id));
				PromoSystem.getProSucStore().create(aprosuc);
			    }
			    localLog.info(" - El usuario "
				    + user.getUsername()
				    + " actualiz� la asociacion de promociones para la sucursal: "
				    + cod_sucursal);
			    localLog.info("Promociones asociadas: "
				    + toLog.toString());
			} else {
			    saveError(request, messages,
				    "Debe seleccionar una sucursal");
			}
		    }
		    rta = "success_associate_promos";
		    saveMessage(request, messages,
			    "message.associate.branch.sucess");
		} else {
		    if (prosucForm.getFilter().equals(1)) {
			// obtengo las sucursales seleccionadas
			String[] sucValues = request
				.getParameterValues("suc_selected");
			Integer cod_promo = null;
			if (prosucForm.getPromotion() != null)
			    cod_promo = Integer.valueOf(prosucForm
				    .getPromotion());
			// elimino las promociones de la sucursal
			try {
			    PromoSystem.getProSucStore().deleteCodPromo(
				    cod_promo);
			} catch (Exception e) {
			    localLog.error("Exception in Delete from Prosuc "
				    + e);
			}
			if (sucValues != null) {
			    for (String id : sucValues) {
				toLog.append(id);
				toLog.append(", ");
				ProSuc proSuc = null;
				try {
				    // busco si la promocion ya se encuentra
				    // asociada a la
				    // sucursal
				    proSuc = PromoSystem.getProSucStore()
					    .getProSuc(Integer.parseInt(id),
						    cod_promo);
				} catch (Exception ex) {
				    localLog.error("Promo - Branch not found "
					    + ex);
				}
				if (proSuc == null) {
				    // registro la asociacion entre la promocion
				    // y la
				    // sucursal
				    SimpleProSuc aprosuc = new SimpleProSuc(
					    Integer.parseInt(id), cod_promo);
				    PromoSystem.getProSucStore()
					    .create(aprosuc);
				}
			    }
			}
			rta = "success_associate_suc";
			localLog.info(" - El usuario "
				+ user.getUsername()
				+ " actualiz� la asociacion de sucursales para la promocion: "
				+ cod_promo);
			localLog.info("Sucursales asociadas "
				+ toLog.toString());
			saveMessage(request, messages,
				"message.associate.promo.sucess");
		    }
		}
	    } catch (Exception e) {
		localLog.error("Exception in Association " + e);
	    }
	} else {
	    saveError(request, messages, "message.user.not.logged");
	    rta = "success";
	}

	return mapping.findForward(rta);

    }

    /***********************************************************************
     * Meotodo q registra mensaje de confirmaci�n pra ser visualizado en el jsp.
     * 
     * @param request
     *            Objeto HttpServletRequest
     * @param messages
     *            Objeto ActionMessages
     * @param message
     *            Mensaje de confirmaci�n
     */
    private void saveMessage(HttpServletRequest request,
	    @SuppressWarnings("hiding") ActionMessages messages, String message) {

	ActionMessage confirm = new ActionMessage(message);
	messages.add("confirms", confirm);
	saveMessages(request, messages);

    }

    /***********************************************************************
     * Meotodo q registra un mensaje de error para ser visualizado en el jsp.
     * 
     * @param request
     *            Objeto HttpServletRequest
     * @param messages
     *            Objeto ActionMessages
     * @param messageErro
     *            Mensaje de error
     */
    private void saveError(HttpServletRequest request, ActionMessages errors,
	    String messageError) {

	ActionMessage error = new ActionMessage(messageError);
	errors.add("errors", error);
	saveMessages(request, errors);

    }

}
