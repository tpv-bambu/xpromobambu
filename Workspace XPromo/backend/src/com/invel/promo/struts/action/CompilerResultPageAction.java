package com.invel.promo.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.invel.promo.struts.form.CompilerForm;

public class CompilerResultPageAction extends Action {

    static Log log = LogFactory.getLog(CompilerResultPageAction.class);

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {
	CompilerForm compilerForm = (CompilerForm) form;

	String res;
	StringBuilder sb = new StringBuilder();
	try {

	    // obtengo la variable de session q contiene el resultado de la/s
	    // compilaciones
	    if (request.getSession().getAttribute("compilerResult") != null)
		sb = (StringBuilder) request.getSession().getAttribute(
			"compilerResult");

	    res = sb.toString();

	} catch (Exception ex) {
	    log.error(ex);
	    res = "error";
	}
	compilerForm.setResult(res);
	System.gc();
	return mapping.findForward("success");
    }
}
