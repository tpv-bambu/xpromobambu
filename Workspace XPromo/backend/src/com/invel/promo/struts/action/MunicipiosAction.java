package com.invel.promo.struts.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.LookupDispatchAction;

import com.invel.common.PromoSystem;
import com.invel.promo.dao.Municipios;
import com.invel.promo.datahandler.FormDataHandler;
import com.invel.promo.struts.form.MunicipiosForm;
import com.invel.promo.struts.form.PromoForm;

/**
 * Action entre la promocion y la pantalla de seleccion de Municipios, guarda el
 * form de la promo y redirige al jsp. El jsp luego devuelve el control a este
 * action donde se actualiza el xml de la promo y se vuelve a la pantalla de la
 * promo.
 * 
 * @author szeballos
 * 
 */
public class MunicipiosAction extends LookupDispatchAction {

    // mensajes al cliente, errores o confirmaciones.
    private ActionMessages localMessages = new ActionMessages();

    private PromoForm aPromoVo = null;

    private final static String JSP_FORWARD = "jsp";
    private final static String BACK_FORWARD = "back";

    static Log localLog = LogFactory.getLog(EditPromoPageAction.class);

    @SuppressWarnings("unchecked")
    public MunicipiosAction() {
	this.keyMethodMap = new HashMap();
	this.keyMethodMap.put("button.accept", "accept");

	this.keyMethodMap.put("button.cancel", "cancelled");

	this.keyMethodMap.put("button.add", "add");
	this.keyMethodMap.put("button.delete", "delete");
	this.keyMethodMap.put("xform.init", "unspecified");
	// this.keyMethodMap.put("", arg1)
    }

    @SuppressWarnings("unchecked")
    @Override
    protected Map getKeyMethodMap() {
	return this.keyMethodMap;
    }

    /**
     * Cuando no llega nada en el parametro "command" se ejecuta este metodo,
     * ocurre cuando reci�n llegamos desde el action de la promo hasta ac�.
     */
    @SuppressWarnings("unchecked")
    @Override
    public ActionForward unspecified(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws IOException, ServletException, Exception {

	MunicipiosForm localForm = (MunicipiosForm) form;
	this.aPromoVo = (PromoForm) request.getAttribute("promoform");
	this.localMessages.clear();

	localForm.setMunicipios(PromoSystem.getMunicipiosStore()
		.getAllMunicipios());

	localForm.setCart((ArrayList<Municipios>) (FormDataHandler.exportData(
		this.aPromoVo.getData(), this.aPromoVo.getXform())));
	Collection<Municipios> municipios = localForm.getMunicipios();
	municipios.removeAll(localForm.getCart());
	/* request.setAttribute("nodos", localForm.getNodos()); */
	// registro los mensajes para ser visualizados
	if (!this.localMessages.isEmpty()) {
	    saveMessages(request, this.localMessages);
	}
	localLog.debug("Municipios en form:" + localForm.getMunicipios().size());
	return (mapping.findForward(JSP_FORWARD));
    }

    /**
     * M�todo asociado al boton de aceptar, es decir leer el cart y cargar todos
     * los nodos seleccionados para incorporar en la promo e incorporarlos.
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForward
     */
    public ActionForward accept(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {

	ActionForward result = null;
	MunicipiosForm localForm = (MunicipiosForm) form;
	this.localMessages.clear();

	try {
	    FormDataHandler.importData(this.aPromoVo.getData(),
		    this.aPromoVo.getXform(), localForm.getCart());
	    request.setAttribute("promoform", this.aPromoVo);
	    // localLog.info(invel.framework.xform.DomUtils.domToString(this.aPromoVo.getData(),
	    // null, null, 0, 0, true));
	    result = mapping.findForward(BACK_FORWARD);
	} catch (Exception ex) {
	    // nada, pero pintar
	    localLog.error(ex);
	    ActionMessage message = new ActionMessage("errors.detail",
		    ex.getMessage());
	    this.localMessages.add("errors", message);

	    /* request.setAttribute("nodos", localForm.getNodos()); */
	    result = mapping.findForward(JSP_FORWARD);
	}
	// registro los mensajes para ser visualizados
	if (!this.localMessages.isEmpty()) {
	    saveMessages(request, this.localMessages);
	}

	return result;

    }

    /**
     * Agrega al cart los municipios seleccionados y los saca de los
     * disponibles.
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForward
     */
    public ActionForward add(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {
	MunicipiosForm localForm = (MunicipiosForm) form;
	String[] addToCart = request
		.getParameterValues("chkMunicipiosNotInCart");
	ArrayList<Municipios> cart = localForm.getCart();
	ArrayList<Municipios> toAdd = new ArrayList<Municipios>();
	this.localMessages.clear();

	if (addToCart != null) {
	    for (String index : addToCart) {
		Municipios muni = localForm.getMunicipios().get(
			Integer.parseInt(index));
		if (!cart.contains(muni)) {
		    toAdd.add(muni);

		}
	    }
	}
	cart.addAll(toAdd);
	// localForm.getMunicipios().removeAll(toAdd);
	// request.setAttribute("nivelesLabels", localForm.getNivelesLabels());
	// request.setAttribute("nodos", localForm.getNodos());
	return mapping.findForward(JSP_FORWARD);
    }

    /**
     * Quita los municipios seleccionados de la promo, no los pone
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForward
     */
    public ActionForward delete(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {
	MunicipiosForm localForm = (MunicipiosForm) form;
	String[] deleteFromCart = request
		.getParameterValues("chkMunicipiosInCart");
	ArrayList<Municipios> cart = localForm.getCart();
	this.localMessages.clear();

	if (deleteFromCart != null) {
	    ArrayList<Municipios> newCart = new ArrayList<Municipios>();
	    for (String index : deleteFromCart) {
		cart.set(Integer.parseInt(index), null);
	    }
	    for (Municipios nodo : cart) {
		if (nodo != null) {
		    newCart.add(nodo);
		}
	    }
	    localForm.setCart(newCart);
	}
	// leer los checkboxes y actualizar el cart segun lo
	// mandado por los checkboxes. El nivel y los nodos siguen iguales.

	/* request.setAttribute("nodos", localForm.getNodos()); */
	return mapping.findForward(JSP_FORWARD);
    }

    /**
     * actualiza el cart con los checboxs, elimina lo que hay que sacar del cart
     * y carga lo que hay que agregar.
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForward
     */
    /*
     * public ActionForward register(ActionMapping mapping, ActionForm form,
     * HttpServletRequest request, HttpServletResponse response) { ECForm
     * localForm = (ECForm) form; //comunican el index del arreglo String[]
     * deleteFromCart = request.getParameterValues("chkEstructurasInCart");
     * //comunican el codigo del nodo String[] addToCart =
     * request.getParameterValues("chkEstructurasNotInCart");
     * this.localMessages.clear();
     * 
     * 
     * ArrayList<EstructuraComercialNodo> cart = localForm.getCart();
     * if(deleteFromCart!=null){ ArrayList<EstructuraComercialNodo> newCart =
     * new ArrayList<EstructuraComercialNodo> (); for(String
     * index:deleteFromCart){ cart.set(Integer.parseInt(index), null); }
     * for(EstructuraComercialNodo nodo: cart){ if(nodo!=null){
     * newCart.add(nodo); } } localForm.setCart(newCart); }
     * 
     * if(addToCart!=null){ for(String index:addToCart){ cart =
     * localForm.getCart(); EstructuraComercialNodo
     * nodo=localForm.getNodos().get(Integer.parseInt(index)); cart.add(nodo); }
     * }
     * 
     * 
     * //leer los checkboxes y actualizar el cart segun lo //mandado por los
     * checkboxes. El nivel y los nodos siguen iguales.
     * request.setAttribute("nivelesLabels", localForm.getNivelesLabels());
     * /*request.setAttribute("nodos", localForm.getNodos());
     */
    // return mapping.findForward(JSP_FORWARD);
    // }

    /**
     * Metodo asociado al boton de cancelar.
     */
    @Override
    public ActionForward cancelled(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {
	request.setAttribute("promoform", this.aPromoVo);
	this.localMessages.clear();
	return (mapping.findForward(BACK_FORWARD));
    }
}
