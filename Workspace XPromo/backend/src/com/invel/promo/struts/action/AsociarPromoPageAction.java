package com.invel.promo.struts.action;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.*;

import com.invel.common.PromoSystem;
import com.invel.promo.dao.*;
import com.invel.promo.struts.form.AsociarProSucForm;

/**
 * Clase action q se ejecuta antes de cargar 'AsociarPromo.jsp', llenando las
 * collecciones de datos que se visualizaran en el jsp.
 * 
 * @author
 * @version 1.0
 */
public class AsociarPromoPageAction extends Action {
    static Log log = LogFactory.getLog(AsociarPromoPageAction.class);

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {

	ActionMessages messages = new ActionMessages();
	String rta = "success";
	// verificamos si el usuario est� en la session
	User user = (User) request.getSession().getAttribute("user");
	// validamos las credenciales del usuario
	if (user == null || !user.getCanassociatepromo()) {
	    ActionMessage message = new ActionMessage(
		    "xvalidate.cannotassociate");
	    messages.add(ActionMessages.GLOBAL_MESSAGE, message);
	    saveMessages(request, messages);
	    return mapping.findForward(rta);
	}
	// ok, usuario validado
	// obtengo el c�digo de sucursal del form
	String cod_sucursal = (String) PropertyUtils.getProperty(form,
		"sucursal");
	// si es nulo lo obtengo del request
	if (cod_sucursal == null) {
	    cod_sucursal = request.getParameter("cod-sucursal");
	}
	// obtenemos el command ejecutado
	String command = request.getParameter("methodToCall");
	if (command != null) {
	    if ("Aceptar".equals(command)) {
		User usuario = (User) request.getSession().getAttribute("user");
		if (usuario != null) {
		    try {
			// elimino las promociones de la sucursal
			// PromoSystem.getProSucStore().deleteCodSuc(Integer.parseInt(cod_sucursal));
		    } catch (Exception e) {
			log.error("Exception in Delete from Prosuc " + e);
		    }
		    try {
			// obtengo los articulos seleccionados
			String[] paramValues = request
				.getParameterValues("art-selected");
			if (paramValues != null) {
			    StringBuilder toLog = new StringBuilder(32);

			    for (String id : paramValues) {
				toLog.append(id);
				toLog.append(", ");
				// registro la promocion en la sucursal
				SimpleProSuc aprosuc = new SimpleProSuc(
					Integer.parseInt(cod_sucursal),
					Integer.parseInt(id));
				PromoSystem.getProSucStore().create(aprosuc);

			    }
			    log.info(" - El usuario "
				    + user.getUsername()
				    + "actualizó la asociación DE promociones para la sucursal: "
				    + cod_sucursal);
			    log.info("Promociones asociadas: "
				    + toLog.toString());
			}
		    } catch (Exception e) {
			log.error("Exception in Add to Prosuc " + e);
		    }
		    rta = "back";
		} else {
		    ActionMessage message = new ActionMessage(
			    "message.user.not.logged");
		    messages.add(ActionMessages.GLOBAL_MESSAGE, message);
		    saveMessages(request, messages);
		    rta = "success";
		}
	    } else {
		rta = "back";
	    }
	} else {
	    // obtengo la instancia del form de la acci�n
	    AsociarProSucForm prosucForm = (AsociarProSucForm) form;
	    try {
		ArrayList<PromoSelected> promosToShow = new ArrayList<PromoSelected>();
		Collection<Promo> allpromos = PromoSystem.getPromoStore()
			.getAllPromo(false,user.isImpuesto());
		if (allpromos != null) {
		    // itero las promociones de la collecci�n
		    for (Iterator<Promo> it = allpromos.iterator(); it
			    .hasNext();) {
			Promo apromo = it.next();
			// verifico si esta habilitada
			if (apromo.isEnabled() == true) {
			    PromoSelected promo = new PromoSelected(apromo);
			    try {
				ProSuc prosuc = PromoSystem.getProSucStore()
					.getProSuc(
						Integer.parseInt(cod_sucursal),
						promo.getInternalId());
				// si la promo es de la sucursal la
				// seleccionamos
				if (prosuc != null) {
				    promo.setSelected(true);
				}else{
				    log.debug("La promo: " + promo.getInternalId()+" no esta asociada a la suc.");
				}
			    } catch (Exception e) {
				promo.setSelected(false);
			    }
			    // agregamos a la collecci�n la promo
			    promosToShow.add(promo);
			}
		    }
		}
		// setemos en la collecci�n del form de la acci�n la collecci�n
		// obtenida
		prosucForm.setPromociones(promosToShow);
	    } catch (Exception e) {
		log.error("Exception in AsociarPromoPageAction.execute: " + e);
	    }
	}
	return mapping.findForward(rta);
    }

}
