package com.invel.promo.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.actions.DispatchAction;

import com.invel.common.PromoSystem;
import com.invel.promo.dao.User;

public class LoginAction extends DispatchAction {

    static Log localLog = LogFactory.getLog(LoginAction.class);

    public ActionForward login(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {

	DynaActionForm loginForm = (DynaActionForm) form;
	String username = (String) loginForm.get("username");
	String password = (String) loginForm.get("password");
	Boolean isTax = Boolean.parseBoolean(loginForm.get("impuesto").toString());
	if(isTax){
	   request.getSession().setAttribute("org.apache.struts.action.LOCALE", new java.util.Locale("be"));
	}
	
	ActionMessages messages = new ActionMessages();
	String forward = "RunTimeError";

	try {
	    User user = PromoSystem.getUserStore().getUser(username, password);
	    user.setImpuesto(isTax);
	    request.getSession(true).setAttribute("user", user);
	    String redirect = (String) loginForm.get("redirect");
	    localLog.info("Logueado usuario: "+user.getUsername());
	    localLog.info("Modo Impuesto?"+ user.isImpuesto());
	    if (redirect != null) {
		return new ActionForward(redirect, true);
	    }

	    forward = "success";
	} catch (com.invel.promo.dao.User.InvalidUserException ex) {
	    localLog.info("Fallo validacion de usuario: " + ex.getMessage());
	    ActionMessage message = new ActionMessage("errors.invalid.user",ex.getMessage());
	    messages.add(ActionMessages.GLOBAL_MESSAGE, message);
	    forward = "init";
	} catch (Exception ex) {
	    ActionMessage message = new ActionMessage("errors.login.error",
		    ex.getMessage());
	    messages.add(ActionMessages.GLOBAL_MESSAGE, message);
	    localLog.error("Error al loguear usuario", ex);
	}

	if (!messages.isEmpty()) {
	    saveMessages(request, messages);
	}
	return mapping.findForward(forward);
    }
}
