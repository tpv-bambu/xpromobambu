package com.invel.promo.struts.action;

import invel.framework.xform.DomUtils;
import invel.framework.xform.xelements.XForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

import com.invel.common.PromoCommons;
import com.invel.common.PromoSystem;
import com.invel.promo.dao.Promo;
import com.invel.promo.dao.TypePromo;
import com.invel.promo.struts.form.PromoForm;

public class PrepareFormCopy extends org.apache.struts.action.Action {

    // Local Forwards
    public static final String FORWARD_success = "success";

    static Log log = LogFactory.getLog(PrepareFormCopy.class);

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {
	PromoForm promoForm = (PromoForm) form;

	Promo promo = null;

	int idpromo = (Integer) request.getAttribute("cod_promo");

	try {
	    promo = PromoSystem.getPromoStore().getPromo(idpromo, true);
	} catch (Exception ex) {
	    log.error("Error seleccionando promo: " + ex);
	}

	if (promo != null) {
	    int promoid = promo.getIntTypePromo();

	    TypePromo typePromo = null;
	    // FalseRequest falseRequest =null;
	    try {
		typePromo = PromoSystem.getTypePromoStore().getTypePromo(
			promoid);
		String newID = String.valueOf(typePromo.getSuggestedCode());
		promo.setXMLid(newID);
	    } catch (Exception ex) {
		log.error("Error seleccionando tipo de promo: " + ex);
	    }

	    if (typePromo != null) {
		Document sourceDoc = DomUtils.parse(promo.getStringXML(),
			PromoCommons.dbencoding, null, null);

		promoForm.myReset();
		promoForm.setEnabled(true);// una promo nueva siempre estar�
		// habilitada.
		promoForm.setCode(promo.getInternalId());
		promoForm.setErrormessage("");
		promoForm.setTypePromo(promo.getIntTypePromo());
		promoForm.setStringTypePromo(typePromo.getDescription());
		promoForm.setData(sourceDoc);
		promoForm.setXform(typePromo.getXForm(), request.getLocale());
		promoForm.setDescription(promo.getDescription());
		promoForm.setWhereToGo("CopyPromo.do");

		XForm xform = promoForm.getXform();
		String newform = xform.getHtmlForm(request,
			promoForm.getData(), "CopyPromo.do?" + "typepromo="
				+ promoForm.getTypePromo(), false,
			PromoCommons.htmlencoding, false);
		promoForm.setHtml(newform);
		request.setAttribute("promoform", promoForm);
	    }
	}

	return mapping.findForward("success");
    }

}
