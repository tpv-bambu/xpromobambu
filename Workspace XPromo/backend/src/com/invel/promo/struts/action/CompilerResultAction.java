package com.invel.promo.struts.action;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.LookupDispatchAction;

public class CompilerResultAction extends LookupDispatchAction {

    @SuppressWarnings("unchecked")
    public CompilerResultAction() {
	this.keyMethodMap = new HashMap();
	this.keyMethodMap.put("button.accept", "accept");
	this.keyMethodMap.put("button.back", "back");
    }

    @Override
    protected Map getKeyMethodMap() {
	return this.keyMethodMap;
    }

    public ActionForward accept(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {

	return (mapping.findForward("success"));
    }

    public ActionForward back(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {

	return (mapping.findForward("back"));
    }
}
