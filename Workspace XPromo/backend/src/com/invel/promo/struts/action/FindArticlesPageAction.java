/*
 * FindArticlesPageAction.java
 *
 * Created on 4 de octubre de 2005, 18:50
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.invel.promo.struts.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.*;

import com.invel.promo.dao.Articulo;
import com.invel.promo.struts.form.FindArticleForm;

public class FindArticlesPageAction extends Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws IOException, ServletException, Exception {

	FindArticleForm findArticleForm = (FindArticleForm) form;
	ArrayList<Articulo> articlesToShow = new ArrayList<Articulo>();
	ArrayList<Articulo> cart = new ArrayList<Articulo>();
	findArticleForm.setCart(cart);
	findArticleForm.setSearch(articlesToShow);
	return (mapping.findForward("success"));
    }
}
