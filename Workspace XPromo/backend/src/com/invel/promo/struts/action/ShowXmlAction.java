/*
 * ShowXmlAction.java
 *
 * Created on 20 de septiembre de 2005, 23:20
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.invel.promo.struts.action;

import invel.framework.xform.DomUtils;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.invel.common.PromoCommons;
import com.invel.common.PromoSystem;
import com.invel.promo.dao.SimplePromo;
import com.invel.promo.dao.User;
import com.invel.promo.struts.form.PromoForm;

public class ShowXmlAction extends Action {

    // private PromoForm localPromoVo=null;
    static Log localLog = LogFactory.getLog(ShowXmlAction.class);

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws IOException, ServletException, Exception {

	String give = "failure", id = null, name = null, externalId = null;
	PromoForm aShowXmlForm = (PromoForm) form;
	// log.debug(aShowXmlForm);
	ActionMessages messages = new ActionMessages();
	ActionMessage message = null;

	String command = request.getParameter("command");
	if ("Regresar".equals(command)) {
	    // TODO, �no deber�a comparar con "comeback"..?
	    aShowXmlForm.setDescription("");
	    request.setAttribute("promoform", aShowXmlForm);
	    return (mapping.findForward("backtoform"));
	}

	// modificacion titulo
	{

	    id = request.getParameter(PromoCommons.getCodeRequestParameter());
	    name = request.getParameter(PromoCommons.getNameRequestParameter());
	    externalId = request.getParameter(PromoCommons
		    .getExternCodeRequestParameter());
	    if (id == null || name == null || externalId == null) {
		localLog.error("Error. No se encontro en el request alg�n id o nombre de la promo (Ignorar bloque)");
	    }
	    aShowXmlForm.setCode(Integer.parseInt(id));
	    aShowXmlForm.setDescription(name);
	}
	// fin modificacion titulo

	User user = (User) request.getSession().getAttribute("user");
	if (user != null) {
	    if ((aShowXmlForm.getDescription() != null)
		    && (aShowXmlForm.getDescription().length() > 0)) {
		try {
		    SimplePromo aSimplePromo = new SimplePromo();
		    aSimplePromo.setEnabled(aShowXmlForm.isEnabled());
		    aSimplePromo.setInternalId(aShowXmlForm.getCode());
		    aSimplePromo.setStringXML(DomUtils.domToString(
			    aShowXmlForm.getData(), null,
			    PromoCommons.dbencoding, false));
		    aSimplePromo.setIntTypePromo(aShowXmlForm.getTypePromo());
		    aSimplePromo.setDescription(aShowXmlForm.getDescription());
		    aSimplePromo.setExternalId(externalId);
		    aSimplePromo.setVigencia(request);
		    PromoSystem.getPromoStore().create(aSimplePromo);
		    message = new ActionMessage(
			    "message.insert.description.saved",
			    aShowXmlForm.getDescription());

		    localLog.info("Usuario " + user.getUsername()
			    + " cre� la promoci�n: "
			    + aSimplePromo.getDescription());
		    give = "success";

		} catch (Exception ex) {
		    localLog.trace("Exception in ShowXmlAction.execute ", ex);
		    message = new ActionMessage("errors.login.error",
			    ex.getMessage());
		}
	    } else {
		// Nunca deberia entrar por aca.
		localLog.error("ShowXMLAction. Error linea 103");

	    }
	} else {
	    message = new ActionMessage("message.user.not.logged");
	}
	request.setAttribute("showxmlform", aShowXmlForm);
	messages.add(ActionMessages.GLOBAL_MESSAGE, message);
	if (!messages.isEmpty()) {
	    saveMessages(request, messages);
	}
	return (mapping.findForward(give));
    }
}
