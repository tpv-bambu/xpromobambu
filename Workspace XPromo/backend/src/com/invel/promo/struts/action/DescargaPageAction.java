package com.invel.promo.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.invel.promo.dao.Articulo;
import com.invel.promo.datahandler.FormDataHandler;
import com.invel.promo.struts.form.ItemDescargaForm;
import com.invel.promo.struts.form.PromoForm;

public class DescargaPageAction extends org.apache.struts.action.Action {

    // Global Forwards
    public static final String GLOBAL_FORWARD_initmainpromo = "initmainpromo";

    public static final String GLOBAL_FORWARD_listallpromos = "listallpromos";

    public static final String GLOBAL_FORWARD_listpromoxsucursal = "listpromoxsucursal";

    public static final String GLOBAL_FORWARD_loadarticle = "loadarticle";

    public static final String GLOBAL_FORWARD_loaddepartment = "loaddepartment";

    public static final String GLOBAL_FORWARD_login = "login";

    public static final String GLOBAL_FORWARD_logout = "logout";

    public static final String GLOBAL_FORWARD_showabout = "showabout";

    public static final String GLOBAL_FORWARD_shownewformpromo = "shownewformpromo";

    public static final String GLOBAL_FORWARD_showpromotosend = "showpromotosend";

    public static final String GLOBAL_FORWARD_showsendpromo = "showsendpromo";

    public static final String GLOBAL_FORWARD_welcome = "welcome";

    // Local Forwards
    public static final String FORWARD_showarticle = "showarticle";

    private PromoForm aPromoForm;

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {

	ItemDescargaForm itemDescargaForm = (ItemDescargaForm) form;
	String action = itemDescargaForm.getAction();
	String give;

	if ("acepta".equals(action)) {
	    int index = itemDescargaForm.getItemdescarga();
	    if ((index >= 0) && (index < itemDescargaForm.getArticles().size())) {
		Articulo descarga = itemDescargaForm.getArticles().get(index);
		FormDataHandler.setValue(this.aPromoForm.getData(),
			this.aPromoForm.getXform(), descarga);
	    }
	    request.setAttribute("promoform", this.aPromoForm);
	    give = "showform";
	} else if ("cancela".equals(action)) {
	    request.setAttribute("promoform", this.aPromoForm);
	    give = "showform";
	} else {
	    // Primera vez que entra, antes de mostrar pantalla
	    this.aPromoForm = (PromoForm) request.getAttribute("promoform");
	    give = "choosedescarga";
	}
	itemDescargaForm.myReset(mapping, request);
	return (mapping.findForward(give));
    }

}
