package com.invel.promo.struts.action;

import invel.framework.xform.FormException;
import invel.framework.xform.xelements.XForm;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.LookupDispatchAction;
import org.xml.sax.SAXException;

import com.invel.common.PromoCommons;
import com.invel.promo.datahandler.FormDataHandler;
import com.invel.promo.struts.form.PromoForm;

public class EditPromoAction extends LookupDispatchAction {

    @SuppressWarnings("unchecked")
    public EditPromoAction() {
	this.keyMethodMap = new HashMap();
	this.keyMethodMap.put("xform.ok", "save");
	this.keyMethodMap.put("xform.cancel", "comeback");
	this.keyMethodMap.put("xform.article", "article");
	this.keyMethodMap.put("xform.articles", "articles");
	this.keyMethodMap.put("xform.department", "department");
	this.keyMethodMap.put("xform.add", "additem");
	this.keyMethodMap.put("xform.del", "delitem");

	this.keyMethodMap.put("xform.previos", "articles");
	this.keyMethodMap.put("xform.premios", "articles");
	this.keyMethodMap.put("xform.add.rango", "additem");
	this.keyMethodMap.put("xform.del.rango", "delitem");
	this.keyMethodMap.put("xform.descarga", "article");

	this.keyMethodMap.put("xform.add.medio", "additem");
	this.keyMethodMap.put("xform.del.medio", "delitem");
	this.keyMethodMap.put("xform.add.plan", "additem");
	this.keyMethodMap.put("xform.del.plan", "delitem");
	this.keyMethodMap.put("xform.add.line.latente", "additem");
	this.keyMethodMap.put("xform.del.line.latente", "delitem");
	this.keyMethodMap.put("xform.add.line.activa", "additem");
	this.keyMethodMap.put("xform.del.line.activa", "delitem");
	this.keyMethodMap.put("xform.add.line.ticket", "additem");
	this.keyMethodMap.put("xform.del.line.ticket", "delitem");
	this.keyMethodMap.put("xform.add.line.descripcion", "additem");
	this.keyMethodMap.put("xform.del.line.descripcion", "delitem");

	this.keyMethodMap.put("xform.estructura.comercial", "eComercial");
    }

    @Override
    protected Map getKeyMethodMap() {
	return this.keyMethodMap;
    }

    // Manager of xform.ok
    public ActionForward save(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws IOException, SAXException {

	String give = "failure";
	PromoForm promoForm = (PromoForm) form;

	try {
	    XForm xform = promoForm.getXform();
	    promoForm.updateData(request);
	    String newform = xform.getHtmlForm(request, promoForm.getData(),
		    promoForm.getWhereToGo(), false, PromoCommons.htmlencoding,
		    true);
	    xform.getReporter().clear();
	    boolean ok = xform.validate(request);

	    if (ok) {
		// Si se anula el titulo, cree que es una nueva promocion y pide
		// nombre
		// promoForm.setDescription(request.).setTitle("");
		give = "success";
	    } else {

		if (PromoCommons.isXFormsErrorMessages()) {
		    promoForm.setErrormessage(xform.getReporter()
			    .getValidationErrors());
		}
		promoForm.setHtml(newform);
	    }
	} catch (FormException ex) {
	    // TODO Auto-generated catch block
	    ex.printStackTrace();
	}

	request.setAttribute("promoform", promoForm);
	return (mapping.findForward(give));
    }

    // Manager of xform.cancel
    public ActionForward comeback(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {

	PromoForm promoForm = (PromoForm) form;
	request.setAttribute("promoform", promoForm);

	return (mapping.findForward("back"));
    }

    // Manager of xform.article
    public ActionForward article(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws IOException, SAXException {

	PromoForm promoForm = (PromoForm) form;

	try {
	    promoForm.updateData(request);
	} catch (FormException ex) {
	    // TODO Auto-generated catch block
	    ex.printStackTrace();
	}

	request.setAttribute("promoform", promoForm);
	return (mapping.findForward("artdescarga"));
    }

    // Manager of xform.articles
    public ActionForward articles(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws IOException, SAXException {

	PromoForm promoForm = (PromoForm) form;

	try {
	    promoForm.updateData(request);
	} catch (FormException ex) {
	    // TODO Auto-generated catch block
	    ex.printStackTrace();
	}

	request.setAttribute("promoform", promoForm);
	return (mapping.findForward("loadarticle"));
    }

    // Manager of xform.department
    public ActionForward department(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws IOException, SAXException {

	PromoForm promoForm = (PromoForm) form;

	try {
	    promoForm.updateData(request);
	} catch (FormException ex) {
	    // TODO Auto-generated catch block
	    ex.printStackTrace();
	}

	request.setAttribute("promoform", promoForm);
	return (mapping.findForward("loaddepartment"));
    }

    // Manager of xform.add
    public ActionForward additem(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {

	PromoForm promoForm = (PromoForm) form;

	try {
	    XForm xform = promoForm.getXform();
	    promoForm.updateData(request);

	    FormDataHandler.addNew(promoForm.getData(), xform);

	    String newform = xform.getHtmlForm(null, promoForm.getData(),
		    promoForm.getWhereToGo(), false, PromoCommons.htmlencoding,
		    false);
	    promoForm.setHtml(newform);
	} catch (FormException ex) {
	    // TODO Auto-generated catch block
	    ex.printStackTrace();
	}

	request.setAttribute("promoform", promoForm);
	return (mapping.findForward("failure"));
    }

    // Manager of xform.del
    public ActionForward delitem(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {

	PromoForm promoForm = (PromoForm) form;

	try {
	    XForm xform = promoForm.getXform();
	    promoForm.updateData(request);

	    FormDataHandler.delLast(promoForm.getData(), xform);

	    String newform = xform.getHtmlForm(null, promoForm.getData(),
		    promoForm.getWhereToGo(), false, PromoCommons.htmlencoding,
		    false);
	    promoForm.setHtml(newform);
	} catch (FormException ex) {
	    // TODO Auto-generated catch block
	    ex.printStackTrace();
	}

	request.setAttribute("promoform", promoForm);
	return (mapping.findForward("failure"));
    }

    // Manager of xform.estructura.comercial
    public ActionForward eComercial(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) {
	PromoForm promoForm = (PromoForm) form;

	try {
	    promoForm.updateData(request);
	} catch (FormException ex) {
	    // TODO Auto-generated catch block
	    ex.printStackTrace();
	}
	request.setAttribute("promoform", promoForm);
	return (mapping.findForward("loadEC"));
    }

}
