/*
 * SendPromoPageAction.java
 *
 * Created on 16 de octubre de 2005, 10:53
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.invel.promo.struts.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.*;

import com.invel.common.PromoSystem;
import com.invel.promo.dao.*;
import com.invel.promo.struts.form.AsociarProSucForm;

public class SendPromoPageAction extends Action {

    static Log log = LogFactory.getLog(SendPromoAction.class);

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws IOException, ServletException, Exception {

	AsociarProSucForm prosucForm = (AsociarProSucForm) form;

	Collection<SimpleSucursal> sucursales = new ArrayList<SimpleSucursal>();
	try {
	    // obtengo las sucursales q tienen promociones habilitadas asociadas
	    sucursales = PromoSystem.getSucursalStore()
		    .getAllSucIncludeEnabledPromo();
	    prosucForm.setSucursales(sucursales);
	    request.setAttribute("sucursales", sucursales);
	} catch (Exception e) {
	    log.error("Exception in SendPromoPageAction 1:" + e);
	}

	return (mapping.findForward("success"));

    }
}
