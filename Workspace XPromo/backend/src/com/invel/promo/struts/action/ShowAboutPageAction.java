package com.invel.promo.struts.action;

import java.io.*;

import java.io.InputStreamReader;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.*;

import com.invel.common.PromoCommons;
import com.invel.promo.struts.form.AboutForm;

public class ShowAboutPageAction extends Action {

    static Log log = LogFactory.getLog(ShowAboutPageAction.class);

    public static final String FORWARD_success = "success";

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {

	// ByteArrayOutputStream version = new ByteArrayOutputStream(150);
	// PrintWriter pw = new PrintWriter(version);

	ProcessBuilder pb = new ProcessBuilder("cmd", "/C",
		PromoCommons.compiler, "-v");
	pb.directory(new File(PromoCommons.getWorkpath()));
	pb.redirectErrorStream(true);
	Process proc = pb.start();
	/*
	 * Process proc = Runtime.getRuntime().exec(PromoCommons.compiler, null,
	 * new File(PromoCommons.workpath));
	 */

	BufferedReader br = new BufferedReader(new InputStreamReader(
		proc.getInputStream()));
	String line = null;
	line = br.readLine();

	AboutForm about = new AboutForm();
	about.setCompiladorVersion(line);
	request.setAttribute("about", about);
	return mapping.findForward(FORWARD_success);

    }

}
