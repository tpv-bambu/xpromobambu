package com.invel.promo.struts.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.*;

import com.invel.common.PromoSystem;
import com.invel.promo.dao.Departamento;
import com.invel.promo.dao.DepartamentoSelected;
import com.invel.promo.dao.DepartamentoStore;
import com.invel.promo.datahandler.FormDataHandler;
import com.invel.promo.struts.form.FindDepartmentForm;
import com.invel.promo.struts.form.PromoForm;

public class LoadDepartmentsAction extends Action {

    // static Log log = LogFactory.getLog(LoadDepartmentsAction.class);

    private PromoForm aPromoForm;

    @SuppressWarnings("unchecked")
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws IOException, ServletException, Exception {

	FindDepartmentForm finDepartmentForm = (FindDepartmentForm) form;
	String action = finDepartmentForm.getAction();
	String give;

	if ("acepta".equals(action)) {
	    Collection<DepartamentoSelected> deptos = finDepartmentForm
		    .getDepartments();

	    FormDataHandler.importData(this.aPromoForm.getData(),
		    this.aPromoForm.getXform(), deptos);

	    request.setAttribute("promoform", this.aPromoForm);
	    finDepartmentForm.myReset(mapping, request);
	    give = "showform";
	} else if ("acepta".equals(action)) {
	    request.setAttribute("promoform", this.aPromoForm);
	    finDepartmentForm.myReset(mapping, request);
	    give = "showform";
	} else {
	    // Entra por aca la primera vez antes de mostrar pantalla
	    Collection<DepartamentoSelected> newList = null;
	    this.aPromoForm = (PromoForm) request.getAttribute("promoform");

	    try {
		newList = (Collection<DepartamentoSelected>) FormDataHandler
			.exportData(this.aPromoForm.getData(),
				this.aPromoForm.getXform());
	    } catch (Exception ex) {
		// TODO: handle exception
		ex.printStackTrace();
	    }

	    finDepartmentForm.setDepartments(newList);
	    finDepartmentForm.setAction("loaddepartment");

	    ArrayList<DepartamentoSelected> deptosToShow = new ArrayList<DepartamentoSelected>();
	    Collection<Departamento> deptos = PromoSystem
		    .getDepartamentoStore().getAllDepartamentos(
			    DepartamentoStore.ORDER_CODE);
	    Collection<DepartamentoSelected> oldSelected = finDepartmentForm
		    .getDepartments();

	    for (Departamento item : deptos) {
		DepartamentoSelected depto = new DepartamentoSelected(item);
		if (oldSelected != null && oldSelected.contains(depto)) {
		    depto.setSelected(true);
		}
		deptosToShow.add(depto);
	    }

	    finDepartmentForm.setAllDepartments(deptosToShow);
	    give = "finddepartment";
	}

	return (mapping.findForward(give));
    }

}
