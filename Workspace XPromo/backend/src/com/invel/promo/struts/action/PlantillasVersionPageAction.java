package com.invel.promo.struts.action;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.ErrorListener;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.invel.common.PromoCommons;

public class PlantillasVersionPageAction extends Action implements
	ErrorListener {

    static Log promoLog = LogFactory.getLog(ListPromoPageAction.class);

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws IOException {

	response.getWriter().write(getHTML());
	return null;

    }

    private String getHTML() {

	/*
	 * Default system property for Xalan processor * /
	 * System.setProperty("javax.xml.transform.TransformerFactory",
	 * "org.apache.xalan.processor.TransformerFactoryImpl"); /*
	 * -------------------------------------------
	 */

	ByteArrayOutputStream result = new ByteArrayOutputStream(4096);
	StreamResult out;
	StreamSource stylesheet = new StreamSource(PromoCommons.progXsl);
	StreamSource source = new StreamSource(PromoCommons.getVersionXML());

	// Create transformer
	TransformerFactory factory = TransformerFactory.newInstance();
	Transformer xf;
	try {
	    xf = factory.newTransformer(stylesheet);
	    xf.setErrorListener(this);
	    out = new StreamResult(result);

	    // xf.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
	    // Perform the transformation
	    xf.transform(source, out);
	} catch (TransformerConfigurationException ex) {
	    // TODO Auto-generated catch block
	    promoLog.error("Error al procesar plantillas", ex);

	} catch (TransformerException ex) {
	    // TODO Auto-generated catch block
	    promoLog.error("Error al procesar plantillas", ex);

	}
	return new String(result.toByteArray());
    }

    public void error(TransformerException arg0) throws TransformerException {
	promoLog.error("Error en plantillas. ", arg0);

    }

    public void fatalError(TransformerException arg0)
	    throws TransformerException {
	promoLog.fatal("Error Fatal en plantillas. ", arg0);

    }

    public void warning(TransformerException arg0) throws TransformerException {
	promoLog.warn("Warning en plantillas. ", arg0);

    }

}
