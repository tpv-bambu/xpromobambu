/*
 * ShowFormToSend.java
 *
 * Created on 18 de octubre de 2005, 16:02
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.invel.promo.struts.action;

import invel.framework.xform.DomUtils;
import invel.framework.xform.xelements.XForm;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.w3c.dom.Document;

import com.invel.common.PromoCommons;
import com.invel.common.PromoSystem;
import com.invel.promo.dao.Promo;
import com.invel.promo.dao.TypePromo;
import com.invel.promo.struts.form.PromoForm;

public class ShowFormToSend extends Action {

    // private PromoForm localPromoVo=null;
    static Log log = LogFactory.getLog(ShowFormToSend.class);

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws IOException, ServletException, Exception {

	PromoForm promoForm = (PromoForm) form;
	ActionMessages messages = new ActionMessages();
	String idpromo = request.getParameter("promo-selected");
	Promo promo = null;

	if (idpromo != null) {
	    try {
		promo = PromoSystem.getPromoStore().getPromo(
			Integer.parseInt(idpromo), true);
	    } catch (Exception ex) {
		log.error("Exception in ShowFormToSend.execute " + ex);
	    }
	    if (promo != null) {
		int promoid = promo.getIntTypePromo();
		if (promoid != 0) {
		    TypePromo typePromo = PromoSystem.getTypePromoStore()
			    .getTypePromo(promoid);
		    promoForm.myReset();
		    promoForm.setTypePromo(typePromo.getId());
		    promoForm.setStringTypePromo(typePromo.getDescription());
		    promoForm.setDescription(promo.getDescription());
		    promoForm.setXform(typePromo.getXForm(),
			    request.getLocale());
		    promoForm.setWhereToGo("ShowFormPromo.do");
		    promoForm.setEnabled(promo.isEnabled());

		    Document sourceDoc = DomUtils.parse(promo.getStringXML(),
			    PromoCommons.dbencoding, null, null);

		    XForm xform = promoForm.getXform();
		    String newform = xform.getHtmlForm(request, sourceDoc,
			    promoForm.getWhereToGo(), false,
			    PromoCommons.htmlencoding, false);

		    promoForm.setHtml(newform);
		}
		request.setAttribute("promoform", promoForm);
	    } else {
		ActionMessage message = new ActionMessage(
			"message.showformtosend.dbstore");
		messages.add(ActionMessages.GLOBAL_MESSAGE, message);
	    }
	} else {
	    ActionMessage message = new ActionMessage(
		    "message.showformtosend.noparam");
	    messages.add(ActionMessages.GLOBAL_MESSAGE, message);
	}
	if (!messages.isEmpty()) {
	    saveMessages(request, messages);
	}
	return (mapping.findForward("success"));
    }
}
