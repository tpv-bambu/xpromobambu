package com.invel.promo.struts.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.*;

import com.invel.common.PromoSystem;
import com.invel.promo.dao.Promo;
import com.invel.promo.dao.User;

public class EditPromoPageAction extends Action {

    static Log log = LogFactory.getLog(EditPromoPageAction.class);

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws IOException, ServletException, Exception {
	ActionMessages messages = new ActionMessages();
	try {
		// verificamos si el usuario est� en la session
		User user = (User) request.getSession().getAttribute("user");
		// validamos las credenciales del usuario
		if (user == null || !user.getCanassociatepromo()) {
		    ActionMessage message = new ActionMessage(
			    "xvalidate.cannotassociate");
		    messages.add(ActionMessages.GLOBAL_MESSAGE, message);
		    saveMessages(request, messages);
		    return mapping.findForward("success");
		}	    
	    ArrayList<Promo> promosToShow = new ArrayList<Promo>();
	    ArrayList<Promo> allpromos = (ArrayList<Promo>) PromoSystem
		    .getPromoStore().getAllPromo(false,user.isImpuesto());
	    if (allpromos != null) {
		Iterator<Promo> it = allpromos.iterator();
		while (it.hasNext()) {
		    Promo art = it.next();
		    // PromoSelected art = (PromoSelected)it.next();
		    promosToShow.add(art);
		}
	    }
	    request.getSession().setAttribute("promos", promosToShow);
	} catch (Exception e) {
	    log.error("Exception in EditPromoPageAction.execute: " + e);
	}
	return (mapping.findForward("success"));
    }
}
