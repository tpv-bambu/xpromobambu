package com.invel.promo.struts.action;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.*;

import com.invel.common.PromoCommons;
import com.invel.common.PromoSystem;
import com.invel.promo.dao.Articulo;

import com.invel.promo.dao.ArticuloStore;
import com.invel.promo.struts.form.ItemDescargaForm;

public class DescargaAction extends org.apache.struts.action.Action {

    // Global Forwards
    public static final String GLOBAL_FORWARD_initmainpromo = "initmainpromo";

    public static final String GLOBAL_FORWARD_listallpromos = "listallpromos";

    public static final String GLOBAL_FORWARD_listpromoxsucursal = "listpromoxsucursal";

    public static final String GLOBAL_FORWARD_loadarticle = "loadarticle";

    public static final String GLOBAL_FORWARD_loaddepartment = "loaddepartment";

    public static final String GLOBAL_FORWARD_login = "login";

    public static final String GLOBAL_FORWARD_logout = "logout";

    public static final String GLOBAL_FORWARD_showabout = "showabout";

    public static final String GLOBAL_FORWARD_shownewformpromo = "shownewformpromo";

    public static final String GLOBAL_FORWARD_showpromotosend = "showpromotosend";

    public static final String GLOBAL_FORWARD_showsendpromo = "showsendpromo";

    public static final String GLOBAL_FORWARD_welcome = "welcome";

    // Local Forwards
    public static final String FORWARD_descargaPageAction = "descargaPageAction";

    public static final String FORWARD_showitems = "showitems";

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {

	ActionMessages messages = new ActionMessages();
	ActionMessage message;
	String give = FORWARD_showitems;

	ItemDescargaForm itemDescargaForm = (ItemDescargaForm) form;
	String comando = request.getParameter("methodToCall");

	if ("Buscar".equals(comando)) {
	    String field = itemDescargaForm.getFilterfield();
	    String begin = itemDescargaForm.getBeginvalue();
	    String end = itemDescargaForm.getEndvalue();
	    if ((begin.length() > 0) && (end.length() > 0)) {
		// ArrayList<Articulo> articlesToShow = new
		// ArrayList<Articulo>();
		List<Articulo> articles = null;

		if ("codbarra".equals(field)) {
		    String ini = "";
		    String fin = "";
		    int c = begin.length();
		    int longBarCode = PromoCommons.getLengthBarCode();
		    if (c < longBarCode) {
			int cant = longBarCode - c;
			for (int i = 0; i < cant; i++) {
			    ini += "0";
			}
			ini += begin;
		    } else {
			ini = begin;
		    }
		    c = end.length();
		    if (c < longBarCode) {
			int cant = longBarCode - c;
			for (int i = 0; i < cant; i++) {
			    fin += "0";
			}
			fin += end;
		    } else {
			fin = end;
		    }
		    itemDescargaForm.setBeginvalue(ini);
		    itemDescargaForm.setEndvalue(fin);
		    if (ini.compareTo(fin) > 0) {
			message = new ActionMessage(
				"message.begin.must.be.bigger.than.end");
			messages.add(ActionMessages.GLOBAL_MESSAGE, message);
		    }
		    articles = PromoSystem.getArticuloStore()
			    .getAllArticulo(ArticuloStore.ORDER_BARRA,
				    "codigo_barra", ini, fin);
		} else if ("codint2".equals(field)) {
		    String ini = "";
		    String fin = "";
		    int c = begin.length();
		    int longInnerCode = PromoCommons.getLengthInnerCode();
		    if (c < longInnerCode) {
			int cant = longInnerCode - c;
			for (int i = 0; i < cant; i++) {
			    ini += "0";
			}
			ini += begin;
		    } else {
			ini = begin;
		    }
		    c = end.length();
		    if (c < longInnerCode) {
			int cant = longInnerCode - c;
			for (int i = 0; i < cant; i++) {
			    fin += "0";
			}
			fin += end;
		    } else {
			fin = end;
		    }
		    itemDescargaForm.setBeginvalue(ini);
		    itemDescargaForm.setEndvalue(fin);
		    if (ini.compareTo(fin) > 0) {
			message = new ActionMessage(
				"message.begin.must.be.bigger.than.end");
			messages.add(ActionMessages.GLOBAL_MESSAGE, message);
		    }
		    articles = PromoSystem.getArticuloStore().getAllArticulo(
			    ArticuloStore.ORDER_ALFA, "cod_interno2", ini, fin);
		} else if ("codint".equals(field)) {
		    long ini = Long.parseLong(begin);
		    long fin = Long.parseLong(end);
		    if (ini > fin) {
			message = new ActionMessage(
				"message.begin.must.be.bigger.than.end");
			messages.add(ActionMessages.GLOBAL_MESSAGE, message);
		    }
		    articles = PromoSystem.getArticuloStore().getAllArticulo(
			    ArticuloStore.ORDER_CODE, "cod_interno", ini, fin);
		} else if ("nombre".equals(field)) {
		    if (begin.compareTo(end) > 0) {
			message = new ActionMessage(
				"message.begin.must.be.bigger.than.end");
			messages.add(ActionMessages.GLOBAL_MESSAGE, message);
		    }
		    articles = PromoSystem.getArticuloStore().getAllArticulo(
			    ArticuloStore.ORDER_NAME, "nombre", begin, end);
		} else if ("marca".equals(field)) {
		    if (begin.compareTo(end) > 0) {
			message = new ActionMessage(
				"message.begin.must.be.bigger.than.end");
			messages.add(ActionMessages.GLOBAL_MESSAGE, message);
		    }
		    articles = PromoSystem.getArticuloStore().getAllArticulo(
			    ArticuloStore.ORDER_MARCA, "marca", begin, end);
		}
		if (articles != null) {
		    itemDescargaForm.setArticles(articles);
		}

	    } else {
		if ((begin.length() == 0) && (end.length() == 0)) {
		    message = new ActionMessage(
			    "message.fields.from.and.to.empty");
		    messages.add(ActionMessages.GLOBAL_MESSAGE, message);
		} else if (begin.length() == 0) {
		    message = new ActionMessage("message.field.from.empty");
		    messages.add(ActionMessages.GLOBAL_MESSAGE, message);
		} else {
		    message = new ActionMessage("message.field.to.empty");
		    messages.add(ActionMessages.GLOBAL_MESSAGE, message);
		}
	    }
	}
	// Boton Aceptar
	else if ("Aceptar".equals(comando)) {
	    int index = itemDescargaForm.getItemdescarga();
	    if ((index >= 0) && (index < itemDescargaForm.getArticles().size())) {
		itemDescargaForm.setAction("acepta");
		give = FORWARD_descargaPageAction;
	    }
	}
	// Boton cancelar
	else if ("Cancelar".equals(comando)) {
	    itemDescargaForm.setAction("cancela");
	    give = FORWARD_descargaPageAction;
	}
	if (!messages.isEmpty()) {
	    saveMessages(request, messages);
	}

	return (mapping.findForward(give));
    }
}
