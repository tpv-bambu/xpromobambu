/*
 * LoadArticlesAction.java
 *
 * Created on 3 de octubre de 2005, 17:42
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.invel.promo.struts.action;

import java.io.IOException;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.*;

import com.invel.promo.dao.Articulo;
import com.invel.promo.datahandler.FormDataHandler;
import com.invel.promo.struts.form.FindArticleForm;
import com.invel.promo.struts.form.PromoForm;

public class LoadArticlesAction extends Action {

    static Log localLog = LogFactory.getLog(LoadArticlesAction.class);

    private PromoForm aPromoVo;

    @SuppressWarnings("unchecked")
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws IOException, ServletException, Exception {

	FindArticleForm finArticleForm = (FindArticleForm) form;
	String action = finArticleForm.getAction();

	String give;

	if ("acepta".equals(action)) {
	    try {
		FormDataHandler.importData(this.aPromoVo.getData(),
			this.aPromoVo.getXform(), finArticleForm.getCart());
	    } catch (Exception ex) {
		// TODO: handle exception
		localLog.error("Error al generar xml", ex);
	    }

	    request.setAttribute("promoform", this.aPromoVo);
	    finArticleForm.myReset(mapping, request);
	    give = "showform";
	} else if ("cancela".equals(action)) {
	    request.setAttribute("promoform", this.aPromoVo);
	    finArticleForm.myReset(mapping, request);
	    give = "showform";
	} else {
	    // Entra por aca la primera vez antes de mostrar pantalla

	    this.aPromoVo = (PromoForm) request.getAttribute("promoform");
	    // request.getSession().setAttribute("listArticles", null);
	    // limpio la lista cart
	    List<Articulo> artInPromo = (List<Articulo>) FormDataHandler
		    .exportData(this.aPromoVo.getData(),
			    this.aPromoVo.getXform());

	    finArticleForm.setCart(artInPromo);
	    /*
	     * else{ // genero el mensaje de cantidad de articulos importados
	     * message = new ActionMessage("message.articles.import", 456);
	     * localmessages.add("confirms", message); saveMessages(request,
	     * localmessages); }
	     */

	    // request.setAttribute("articulos", finArticleForm.getArticles());

	    give = "chooseart";
	}
	return (mapping.findForward(give));
    }

}
