package com.invel.promo.struts;

/*
 * ShowFormServlet.java
 *
 * Created on 2 de septiembre de 2005, 17:00
 */

import invel.framework.xform.DomUtils;
import invel.framework.xform.xelements.XForm;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.invel.common.PromoCommons;
import com.invel.promo.dao.TypePromo;

public class ShowFormServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws IOException {
	this.processRequest(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws IOException {
	this.processRequest(request, response);
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     * 
     * @param request
     *            servlet request
     * @param response
     *            servlet response
     * @exception java.io.IOException
     */
    protected void processRequest(HttpServletRequest request,
	    HttpServletResponse response) throws IOException {

	String formDebug = null;
	PrintWriter out = response.getWriter();
	response.setContentType("text/html");

	try {
	    TypePromo typePromoVo = (TypePromo) request.getAttribute("xform");

	    String recordString = "";
	    String outputString = "";
	    if (typePromoVo != null) {
		XForm form = typePromoVo.getXForm();
		int id = typePromoVo.getId();

		String actionString = "ShowFormPromo.do?idForm=" + id;

		outputString = form.getHtmlForm(request, null, actionString,
			false, PromoCommons.htmlencoding, false);

		recordString = DomUtils.previewDocument(
			form.getDocument(request, null),
			PromoCommons.htmlencoding, true);
		// TODO colocar informacion debug, cuando sepa donde carga el
		// xform
		formDebug = form.getReporter().getDebugInfo();
	    }

	    // form title
	    out.println("<table width=600 cellpadding=3 cellspacing=0>");
	    out.println("<tr><td bgcolor=#ddddcc>");
	    out.println("<font face=\"arial,helvetica,sans-serif\">");
	    out.println("&nbsp;<B>Promociones</B> - Elija una promocion de prueba");
	    out.println("</font>");
	    out.println("</td></tr>");
	    out.println("</table>");

	    // form content
	    out.println("<table cellpadding=10 cellspacing=5>");
	    out.println("<tr><td>");
	    out.println("<font face=\"arial,helvetica,sans-serif\">");
	    out.println(outputString);
	    out.println("</font>");
	    out.println("</td></tr>");
	    out.println("</table>");

	    String m = request.getParameter("example1/submit");
	    if ("Submit Form".equals(m)) {
		// record title
		out.println("<table width=600 cellpadding=3 cellspacing=0>");
		out.println("<tr><td bgcolor=#ddddee>");
		out.println("<font face=\"arial,helvetica,sans-serif\">");
		out.println("&nbsp;<B>Record</B> - data user has entered as of last submission");
		out.println("</font>");
		out.println("</td></tr>");
		out.println("</table>");

		// output record
		out.println("<table cellpadding=10 cellspacing=5>");
		out.println("<tr><td>");
		out.println("<PRE>");
		out.println(recordString);
		out.println("</PRE>");
		out.println("</td></tr>");
		out.println("</table>");
	    }
	    if (formDebug != null) {
		out.println("<h3>Debug info: </h3>");
		out.println(formDebug);
	    }
	} catch (Exception ex) {
	    out.println("<H2>Error</H2>");
	    out.println(ex.toString());
	    out.println("<br>");
	    out.println("<br>");
	    ex.printStackTrace();
	}
    }

}
