package com.invel.promo.struts.form;

import org.apache.struts.action.ActionForm;

public class AboutForm extends ActionForm {

    // Parametros de version del xpromo

    private static transient final long serialVersionUID = 1;
    private static final String VERSION_XPROMO = "01";
    private static final String SUBVERSION_XPROMO = "001";
    private static final String REVISION_XPROMO = "05";
    private static final String BUILD_XPROMO = "03";
    private static final String TEXT_VERSION_XPROMO = "Versión: "
	    + VERSION_XPROMO + "." + SUBVERSION_XPROMO + "." + REVISION_XPROMO
	    + "." + BUILD_XPROMO;

    // Parametros de version del gestor de impuestos

    private static final String VERSION_IMPUESTOS = "01";
    private static final String SUBVERSION_IMPUESTOS = "001";
    private static final String REVISION_IMPUESTOS = "04";

    private static final String BUILD_IMPUESTOS = "01";

    private static final String TEXT_VERSION_IMPUESTOS = "Versión: "
	    + VERSION_IMPUESTOS + "." + SUBVERSION_IMPUESTOS + "."
	    + REVISION_IMPUESTOS + "." + BUILD_IMPUESTOS;

    /** Atributo donde se salvara la versión del compilador */
    private String promoCompVersion;
    /** Atributo donde se salvara la versión del compilador de impuestos */ 
    private String impuCompVersion;
    

    public String getImpuCompVersion() {
        return impuCompVersion;
    }

    public void setImpuCompVersion(String impuCompVersion) {
        this.impuCompVersion = impuCompVersion;
    }

    public String getPromoCompVersion() {
        return promoCompVersion;
    }

    public void setPromoCompVersion(String promoCompVersion) {
        this.promoCompVersion = promoCompVersion;
    }

    /**
     * Getter bean para la versión del xpromo. Notar que carece de un setter.
     * 
     * @return String
     */
    public String getXPromoVersion() {
	return TEXT_VERSION_XPROMO;
    }

    /**
     * Getter bean para la versión del gestor de impuestos. Notar que carece de
     * un setter.
     * 
     * @return String
     */
    public String getImpuestosVersion() {
	return TEXT_VERSION_IMPUESTOS;
    }

    /**
     * Getter bean para la versi�n del compilador.
     * 
     * @return String
     */
    public String getCompiladorVersion() {
	return this.promoCompVersion;
    }

    /**
     * Setter bean para la versión del compilador
     * 
     * @param pPromoCompVersion
     */
    public void setCompiladorVersion(String pPromoCompVersion) {
	this.promoCompVersion = pPromoCompVersion;
    }

}
