package com.invel.promo.struts.form;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.invel.promo.dao.Articulo;

public class ItemDescargaForm extends ActionForm {

    private static final long serialVersionUID = 1L;

    private String action;

    private String beginvalue;

    private String endvalue;

    private String filterfield;

    private int itemdescarga;

    transient private List<Articulo> articles;

    public void myReset(ActionMapping arg0, HttpServletRequest arg1) {
	super.reset(arg0, arg1);
	this.action = "";
	this.beginvalue = "";
	this.endvalue = "";
	this.action = "";
	this.itemdescarga = -1;
    }

    public String getAction() {
	return this.action;
    }

    public String getBeginvalue() {
	return this.beginvalue;
    }

    public String getEndvalue() {
	return this.endvalue;
    }

    public String getFilterfield() {
	return this.filterfield;
    }

    public int getItemdescarga() {
	return this.itemdescarga;
    }

    public List<Articulo> getArticles() {
	return this.articles;
    }

    public void setAction(String pAction) {
	this.action = pAction;
    }

    public void setBeginvalue(String pBeginValue) {
	this.beginvalue = pBeginValue;
    }

    public void setEndvalue(String pEndValue) {
	this.endvalue = pEndValue;
    }

    public void setFilterfield(String pFilterField) {
	this.filterfield = pFilterField;
    }

    public void setItemdescarga(int pItemDescarga) {
	this.itemdescarga = pItemDescarga;
    }

    public void setArticles(List<Articulo> pArticles) {
	this.articles = pArticles;
    }

}
