package com.invel.promo.struts.form;

import java.util.Collection;

import org.apache.struts.action.ActionForm;

import com.invel.promo.dao.PromoSelected;
import com.invel.promo.dao.SimpleSucursal;

public class AsociarProSucForm extends ActionForm {

    private static final long serialVersionUID = 1L;

    String sucursal;

    transient private Collection<PromoSelected> promociones;

    transient private Collection<SimpleSucursal> sucursales;

    private String promotion;

    private Integer localFilter;

    public Integer getFilter() {
	return this.localFilter;
    }

    public void setFilter(Integer filter) {
	this.localFilter = filter;
    }

    public Collection<PromoSelected> getPromociones() {
	return this.promociones;
    }

    public String getSucursal() {
	return this.sucursal;
    }

    public Collection<SimpleSucursal> getSucursales() {
	return this.sucursales;
    }

    public void setPromociones(Collection<PromoSelected> pPromociones) {
	this.promociones = pPromociones;
    }

    public void setSucursal(String pSucursal) {
	this.sucursal = pSucursal;
    }

    public void setSucursales(Collection<SimpleSucursal> pSucursales) {
	this.sucursales = pSucursales;
    }

    public String getPromotion() {
	return this.promotion;
    }

    public void setPromotion(String pPromotion) {
	this.promotion = pPromotion;
    }

}
