package com.invel.promo.struts.form;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.invel.common.PromoCommons;
import com.invel.promo.dao.Promo;
import com.invel.promo.dao.PromoSelected;

public class ListPromoForm extends ActionForm {

    private static final long serialVersionUID = 1L;

    /*
     * Filtro asociado al radio button, que indica si se filtran las promociones
     * habilitadas, las deshabilitadas o todas.
     */
    private int condition;

    /* C�digo de la promo seleccionada, carece de sentido ahora */
    private int promoSelected;

    /* Guarda el �ltimo tipo de promoci�n elegido por el usuario */
    private int typeselected = PromoCommons.DEFAULT_TYPE_SELECTED;
    /* guarda el orden en q deben mostrarse las promociones */
    private int orderType;

    // Todas las promociones (filtradas de corresponder)

    /* Acci�n masiva a realizar (exportar, habilitar, deshabilitar) */
    private String massive;

    transient private Collection<PromoSelected> promos;

    // Promociones seleccionadas (multicheckboxes)
    transient private Collection<Promo> selectedPromos;

    // Simple flag para alternar los "clicks" en el boton de select all.
    transient private boolean flag = false;

    /**
     * Getter de promos seleccionadas
     * 
     * @return Collection<Promo>
     */
    public Collection<Promo> getSelectedPromos() {
	return this.selectedPromos;
    }

    /**
     * Setter de promos seleccionadas
     * 
     * @param selectedPromos
     */
    public void setSelectedPromos(Collection<Promo> pSelectedPromos) {
	this.selectedPromos = pSelectedPromos;
    }

    public int getCondition() {
	return this.condition;
    }

    public Collection<PromoSelected> getPromos() {
	return this.promos;
    }

    public int getPromoSelected() {
	return this.promoSelected;
    }

    public int getTypeselected() {
	return this.typeselected;
    }

    @Override
    public void reset(ActionMapping mapping, HttpServletRequest request) {
	this.promoSelected = PromoCommons.DEFAULT_TYPE_SELECTED;
    }

    public void setCondition(int pCondition) {
	this.condition = pCondition;
    }

    public void setPromos(Collection<PromoSelected> pPromos) {
	this.promos = pPromos;
    }

    public void setPromoSelected(int pPromoSelected) {
	this.promoSelected = pPromoSelected;
    }

    public void setTypeselected(int pType) {
	this.typeselected = pType;
    }

    /**
     * Getter de <code>massive</code>
     * 
     * @return String
     */
    public String getMassive() {
	return this.massive;
    }

    /**
     * Setter de <code>massive</code>
     * 
     * @param massive
     */
    public void setMassive(String pMassive) {
	this.massive = pMassive;
    }

    public boolean isFlag() {
	return this.flag;
    }

    public void setFlag(boolean pFlag) {
	this.flag = pFlag;
    }

    public int getOrderType() {
	return this.orderType;
    }

    public void setOrderType(int pOrderType) {
	this.orderType = pOrderType;
    }
}
