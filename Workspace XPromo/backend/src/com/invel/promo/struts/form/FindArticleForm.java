/*
 * FindArticleForm.java
 *
 * Created on 1 de octubre de 2005, 10:36
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.invel.promo.struts.form;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import com.invel.promo.dao.Articulo;

public class FindArticleForm extends ActionForm {

    private static final long serialVersionUID = 1L;

    private String action;

    private String beginvalue;

    private String endvalue;

    private String filterfield;

    transient private FormFile articlesToImport;

    transient private List<Articulo> search;

    transient private List<Articulo> cart;

    public void myReset(ActionMapping arg0, HttpServletRequest arg1) {
	// super.reset(arg0, arg1);
	this.action = "";
	this.beginvalue = "";
	this.endvalue = "";
	this.search = null;
	this.cart = null;
	this.articlesToImport = null;
    }

    public String getAction() {
	return this.action;
    }

    public String getBeginvalue() {
	return this.beginvalue;
    }

    public String getEndvalue() {
	return this.endvalue;
    }

    public String getFilterfield() {
	return this.filterfield;
    }

    public void setAction(String pAction) {
	this.action = pAction;
    }

    public void setBeginvalue(String pBeginvalue) {
	this.beginvalue = pBeginvalue;
    }

    public void setEndvalue(String pEndValue) {
	this.endvalue = pEndValue;
    }

    public void setFilterfield(String pFilterField) {
	this.filterfield = pFilterField;
    }

    @Override
    public String toString() {
	StringBuilder rta = new StringBuilder();
	rta.append("[action=").append(this.action);
	rta.append("]");
	return rta.toString();
    }

    public FormFile getArticlesToImport() {
	return this.articlesToImport;
    }

    public void setArticlesToImport(FormFile pArticlesToImport) {
	this.articlesToImport = pArticlesToImport;
    }

    /**
     * @return the cart
     */
    public List<Articulo> getCart() {
	return this.cart;
    }

    /**
     * @param cart
     *            the cart to set
     */
    public void setCart(List<Articulo> pCart) {
	this.cart = pCart;
    }

    /**
     * @return the search
     */
    public List<Articulo> getSearch() {
	return this.search;
    }

    /**
     * @param search
     *            the search to set
     */
    public void setSearch(List<Articulo> pSearch) {
	this.search = pSearch;
    }

}
