package com.invel.promo.struts.form;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.invel.promo.dao.DepartamentoSelected;

public class FindDepartmentForm extends ActionForm {

    private static final long serialVersionUID = 1L;

    private String action;

    transient private Collection<DepartamentoSelected> allDepartments;

    transient private Collection<DepartamentoSelected> departments;

    public void myReset(ActionMapping arg0, HttpServletRequest arg1) {
	// super.reset(arg0, arg1);
	this.action = "";
	this.departments = null;
    }

    public String getAction() {
	return this.action;
    }

    public Collection<DepartamentoSelected> getAllDepartments() {
	return this.allDepartments;
    }

    public Collection<DepartamentoSelected> getDepartments() {
	return this.departments;
    }

    public void setAction(String pAction) {
	this.action = pAction;
    }

    public void setAllDepartments(Collection<DepartamentoSelected> deptos) {
	this.allDepartments = deptos;
    }

    public void setDepartments(Collection<DepartamentoSelected> deptos) {
	this.departments = deptos;
    }

    @Override
    public String toString() {
	StringBuilder rta = new StringBuilder();
	rta.append("[action=").append(this.action);
	rta.append("]");
	return rta.toString();
    }

}
