/*
 * ShowXmlForm.java
 *
 * Created on 20 de septiembre de 2005, 15:55
 *
 * To change this template, choose Tools | Options and locate the template
 * under the Source Creation and Management node. Right-click the template
 * and choose Open.
 * You can then make changes to the template in the Source Editor.
 */

package com.invel.promo.struts.form;

import invel.framework.xform.xelements.XForm;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class ShowXmlForm extends ActionForm {

    private static final long serialVersionUID = 1L;

    private String description;

    private String title;

    private int type_promo;

    transient private XForm xform;

    private String xml;

    public ShowXmlForm() {
	// Default constructor
    }

    public ShowXmlForm(int pTypePromo, String pDescription, XForm pXForm) {
	this.type_promo = pTypePromo;
	this.description = pDescription;
	this.xform = pXForm;
    }

    public String getDescription() {
	return this.description;
    }

    public String getTitle() {
	return this.title;
    }

    public int getTypepromo() {
	return this.type_promo;
    }

    public XForm getXform() {
	return this.xform;
    }

    public String getXml() {
	return this.xml;
    }

    @Override
    public void reset(ActionMapping arg0, HttpServletRequest arg1) {
	// super.reset(arg0, arg1);
	this.description = "";
    }

    public void setDescription(String pDescription) {
	this.description = pDescription;
    }

    public void setTitle(String pTitle) {
	this.title = pTitle;
    }

    public void setTypepromo(int pTypePromo) {
	this.type_promo = pTypePromo;
    }

    public void setXform(XForm pXForm) {
	this.xform = pXForm;
    }

    public void setXml(String pXml) {
	this.xml = pXml;
    }
}
