/*
 * PromoForm.java
 *
 * Created on 19 de septiembre de 2005, 15:57
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.invel.promo.struts.form;

import invel.framework.xform.DomUtils;
import invel.framework.xform.FormException;
import invel.framework.xform.xelements.XForm;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.w3c.dom.Document;

import com.invel.promo.struts.action.PrepareFormAction;

public class PromoForm extends ActionForm {

    private static final long serialVersionUID = 1L;

    /** El código del tipo de promoción */
    private int type_promo;

    /** El nombre del tipo de promoción */
    private String stringTypePromo;

    private String errormessage;

    /** El nombre de esta promoción */
    private String description;

    private String whereToGo;

    private String html;

    private int code;

    /** Si la promo esté o no habilitada en BD. */
    private boolean enabled;

    transient private XForm xform;

    transient private Document dataxml;

    List listAgrup = new ArrayList();

    Integer agrupId;
    static Log localLog = LogFactory.getLog(PromoForm.class);
    public void myReset() {
	localLog.debug("PromoForm. myReset called");
	this.html = "";
	this.dataxml = null;
	this.description = "";
	this.stringTypePromo = "";
	this.xform = null;
	this.errormessage = "";
    }

    public void updateData(HttpServletRequest request) throws FormException {
	if(this.getXform()!=null){
	    setData(getXform().getDocument(request, getData()));
	}else{
	    localLog.debug("XForm nulo, no se puede actualizar datos");
	}
    }

    public Document getData() {
	return this.dataxml;
    }

    public String getDescription() {
	return this.description;
    }

    public String getErrormessage() {
	return this.errormessage;
    }

    public int getTypePromo() {
	return this.type_promo;
    }

    public String getWhereToGo() {
	return this.whereToGo + getTypePromo();
    }

    public XForm getXform() {
	return this.xform;
    }

    public String getHtml() {
	return this.html;
    }

    public void setData(Document doc) {
	this.dataxml = doc;
    }

    public void setDescription(String pDescription) {
	this.description = pDescription;
    }

    public void setErrormessage(String pErrorMessage) {
	this.errormessage = pErrorMessage;
    }

    public void setTypePromo(int pTypePromo) {
	this.type_promo = pTypePromo;
    }

    public void setWhereToGo(String pWhereMustGo) {
	this.whereToGo = pWhereMustGo + "?typepromo=";
    }

    public List getListAgrup() {
	return this.listAgrup;
    }

    public void setListAgrup(List pListAgrup) {
	this.listAgrup = pListAgrup;
    }

    public Integer getAgrupId() {
	return this.agrupId;
    }

    public void setAgrupId(Integer pAgrupId) {
	this.agrupId = pAgrupId;
    }

    /**
     * Si ya tengo el XForm parseado, utilizo este método. Seteo el
     * ResourceBundle ac.
     * 
     * @param pXForm
     * @param locale
     */
    public void setXform(XForm pXForm, Locale locale) {
	localLog.debug("setXform called");
	if(pXForm==null){
	    localLog.debug("XForm es nulo!");
	    
	}
	this.xform = pXForm;
	ResourceBundle rb = ResourceBundle
		.getBundle("MessageResources", locale);
	this.xform.setResourceBundle(rb);
    }

    public void setHtml(String pHtml) {
	this.html = pHtml;
    }

    /*
     * public void reset(ActionMapping arg0, HttpServletRequest arg1) {
     * super.reset(arg0, arg1); this.description = ""; }
     */
    @Override
    public String toString() {
	StringBuilder astring = new StringBuilder();
	astring.append("[code=").append(this.code);
	astring.append("|typePromo=").append(this.type_promo);
	astring.append("|description=").append(this.description);
	if (this.dataxml == null) {
	    astring.append("|xml=(none)");
	} else {
	    astring.append("|xml=").append(
		    DomUtils.domToString(this.dataxml, null, null, false));
	}/*
	  * astring.append("|xform=").append(
	  * DomUtils.domToString(this.xform..getSourceDoc(), null,
	  * PromoCommons.htmlencoding, false)); astring.append("]");
	  */
	return astring.toString();
    }

    public int getCode() {
	return this.code;
    }

    public void setCode(int pCode) {
	this.code = pCode;
    }

    public String getStringTypePromo() {
	return this.stringTypePromo;
    }

    public void setStringTypePromo(String pStringTypePromo) {
	this.stringTypePromo = pStringTypePromo;
    }

    public boolean isEnabled() {
	return this.enabled;
    }

    /**
     * @param enabled
     *            the enabled to set
     */
    public void setEnabled(boolean pEnabled) {
	this.enabled = pEnabled;
    }

}
