package com.invel.promo.struts.form;

import org.apache.struts.action.ActionForm;

public class CompilerForm extends ActionForm {

    private static final long serialVersionUID = 1L;

    private String result;

    public String getResult() {
	return this.result;
    }

    public void setResult(String pResult) {
	this.result = pResult;
    }

}
