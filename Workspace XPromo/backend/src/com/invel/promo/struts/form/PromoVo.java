/*
 * PromoVo.java
 *
 * Created on 16 de septiembre de 2005, 13:56
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.invel.promo.struts.form;

import invel.framework.xform.xelements.XForm;

import org.apache.struts.action.ActionForm;

public class PromoVo extends ActionForm {

    private static final long serialVersionUID = 1L;

    private String description;

    private int id;

    transient private XForm xform;

    private String xml;

    /** Creates a new instance of PromoVo */
    public PromoVo() {
	// Default constructor
    }

    public PromoVo(int pId, String pDescription, XForm pXForm) {
	this.id = pId;
	this.description = pDescription;
	this.xform = pXForm;
    }

    public String getDescription() {
	return this.description;
    }

    public int getId() {
	return this.id;
    }

    public XForm getXform() {
	return this.xform;
    }

    public String getXml() {
	return this.xml;
    }

    public void setDescription(String pDescription) {
	this.description = pDescription;
    }

    public void setId(int pId) {
	this.id = pId;
    }

    public void setXform(XForm pXForm) {
	this.xform = pXForm;
    }

    public void setXml(String pXml) {
	this.xml = pXml;
    }

}
