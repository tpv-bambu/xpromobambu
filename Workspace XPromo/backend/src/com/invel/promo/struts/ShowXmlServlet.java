/*
 * ShowXmlServlet.java
 *
 * Created on 18 de septiembre de 2005, 11:26
 * Servlet encargado de imprimir en el response el XForm. Este
 * debe estar ya creado. El flujo normal es ir por el action de
 * struts que carga el xform en el request y termina redirigiendo
 * a este servlet.
 *
 */

package com.invel.promo.struts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.invel.promo.struts.form.PromoForm;

public class ShowXmlServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * Returns a short description of the servlet.
     * 
     * @return String
     */
    @Override
    public String getServletInfo() {
	return "Short description";
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     * 
     * @param request
     *            servlet request
     * @param response
     *            servlet response
     * @exception ServletException
     * @exception IOException
     */
    @Override
    protected void doGet(HttpServletRequest request,
	    HttpServletResponse response) throws ServletException, IOException {
	processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * 
     * @param request
     *            servlet request
     * @param response
     *            servlet response
     */
    @Override
    protected void doPost(HttpServletRequest request,
	    HttpServletResponse response) throws ServletException, IOException {
	processRequest(request, response);
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     * 
     * @param request
     *            servlet request
     * @param response
     *            servlet response
     * @exception java.io.IOException
     */
    protected void processRequest(HttpServletRequest request,
	    HttpServletResponse response) throws IOException {
	response.setContentType("text/html;charset=UTF-8");
	PrintWriter out = response.getWriter();
	PromoForm xmld = (PromoForm) request.getAttribute("promoform");

	out.println("<table width=100% cellpadding=0 cellspacing=0 align=left>");
	out.println("<tr><td class=fondomodulos valign=top><blockquote>");

	out.println("<div class=subtitulo align=left>");
	out.println("<br><span class=subtitulo>");
	out.println("Tipo de Promoción: " + xmld.getStringTypePromo());
	out.println("</span>");
	out.println("</div>");
	out.println("<hr class=\"linea\" color=\"#006600\" noshade=\"noshade\" size=\"1\">");
	out.println("</blockquote>");

	String errors = xmld.getErrormessage();
	if ((errors != null) && (errors.length() > 0)) {
	    out.println("<tr><td><blockquote><div class=\"textowarning\" align=left><span class=\"textowarning\"><pre>");
	    out.println(errors);
	    out.println("</pre><hr class=\"linea\" color=\"#006600\" noshade=\"noshade\" size=\"1\">");
	    out.println("</span></div></blockquote></td></tr>");
	}
	out.println("</td></tr>");
	out.println("<tr><td>");
	out.println("<ul>");
	out.println(xmld.getHtml());
	out.println("</ul>");
	out.println("</td></tr>");
	out.println("</table>");
    }
}
