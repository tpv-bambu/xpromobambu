/*
 * PromoSelected.java
 *
 * Created on 16 de octubre de 2005, 11:18
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.invel.promo.dao;

import java.io.Serializable;

public class PromoSelected extends SimplePromo implements Serializable {

    private static final long serialVersionUID = 1L;

    private boolean selected;

    public PromoSelected(Promo promo) {
	this.selected = false;
	super.setInternalId(promo.getInternalId());
	super.setIntTypePromo(promo.getIntTypePromo());
	super.setDescription(promo.getDescription());
	super.setStringXML(promo.getStringXML());
	super.setEnabled(promo.isEnabled());
	super.setDescTypePromo(promo.getDescTypePromo());
	super.setDesde(promo.getDesde());
	super.setHasta(promo.getHasta());
    }

    public boolean isSelected() {
	return this.selected;
    }

    public void setSelected(boolean pSelected) {
	this.selected = pSelected;
    }

    @Override
    public String toString() {
	StringBuilder art = new StringBuilder();
	art.append("[id=").append(super.getInternalId());
	art.append("|selected=").append(this.selected);
	art.append("|description=").append(super.getDescription());
	art.append("|typepromo=").append(super.getIntTypePromo());
	art.append("|xml=").append(super.getStringXML());
	art.append("]");
	return art.toString();
    }
}
