package com.invel.promo.dao;

import java.io.*;

import com.invel.common.PromoCommons;

public class SimpleSucursal implements Sucursal, Serializable {

    private static final long serialVersionUID = 1L;

    private int cod_sucursal;

    private String descripcion;

    private String ipFtp;

    private String passwordFtp;

    private String userFtp;

    private boolean selected;

    public SimpleSucursal() {
	// Default constructor
    }

    public SimpleSucursal(int pCodSucursal, String pDescripcion,
	    String pUserFtp, String pPasswordFtp, String pIpFtp) {
	this.cod_sucursal = pCodSucursal;
	if (pDescripcion != null) {
	    this.descripcion = pDescripcion.trim();
	} else {
	    this.descripcion = pDescripcion;
	}
	this.userFtp = pUserFtp;
	this.passwordFtp = pPasswordFtp;
	this.ipFtp = pIpFtp;
    }

    public int getCod_sucursal() {
	return this.cod_sucursal;
    }

    public String getDescripcion() {
	return this.descripcion;
    }

    /**
     * @return Returns the ipFtp.
     */
    public String getIpFtp() {
	return this.ipFtp;
    }

    /**
     * @return Returns the passwordFtp.
     */
    public String getPasswordFtp() {
	return this.passwordFtp;
    }

    /**
     * @return Returns the usuarioFtp.
     */
    public String getUserFtp() {
	return this.userFtp;
    }

    public void setCod_sucursal(int pCodSucursal) {
	this.cod_sucursal = pCodSucursal;
    }

    public void setDescripcion(String pDescripcion) {
	this.descripcion = pDescripcion;
    }

    /**
     * @param pIpFtp
     *            The ipFtp to set.
     */
    public void setIpFtp(String pIpFtp) {
	this.ipFtp = pIpFtp;
    }

    /**
     * @param pPasswordFtp
     *            The passwordFtp to set.
     */
    public void setPasswordFtp(String pPasswordFtp) {
	this.passwordFtp = pPasswordFtp;
    }

    /**
     * @param pUserFtp
     *            The ftp user to set.
     */
    public void setUserFtp(String pUserFtp) {
	this.userFtp = pUserFtp;
    }

    @Override
    public String toString() {
	StringBuilder pro_suc = new StringBuilder();
	pro_suc.append("[cod_sucursal=").append(this.cod_sucursal);
	pro_suc.append("|desripcion=").append(this.descripcion);
	pro_suc.append("|userFtp=").append(this.userFtp);
	pro_suc.append("|passwordFtp=").append(this.passwordFtp);
	pro_suc.append("|ipFtp=").append(this.ipFtp);
	pro_suc.append("]");
	return pro_suc.toString();
    }

    public boolean isSelected() {
	return this.selected;
    }

    public void setSelected(boolean pSelected) {
	this.selected = pSelected;
    }

    public String getPath() throws IOException {
	StringBuilder result = new StringBuilder();
	result.append(PromoCommons.tempPath);
	result.append(this.cod_sucursal);
	result.append('-');
	result.append(this.descripcion);
	result.append('/');
	File temp = new File(result.toString());
	if (!temp.exists()) {
	    temp.mkdirs();
	}
	return result.toString();
    }

}
