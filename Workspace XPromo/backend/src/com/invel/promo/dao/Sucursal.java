package com.invel.promo.dao;

import java.io.IOException;

public interface Sucursal {
    /**
     * ID de la sucursal
     * 
     * @return int
     */
    int getCod_sucursal();

    /**
     * C�rdoba, Villa Ocampo, etc. El nombre de la sucursal
     * 
     * @return String
     */
    String getDescripcion();

    /**
     * El usuario para acceder al servidor FTP de la sucursal
     * 
     * @return String
     */
    String getUserFtp();

    /**
     * El password para acceder al servidor FTP de la sucursal
     * 
     * @return String
     */
    String getPasswordFtp();

    /**
     * El IP del servidor FTP de la sucursal.
     * 
     * @return String
     */
    String getIpFtp();

    /**
     * Carpeta donde se crearan los archivos cod, pro y xml de la sucursal. Se
     * asegura que la carpeta existe.
     * 
     * @return String
     */
    String getPath() throws IOException;
}
