package com.invel.promo.dao;

import java.net.InetAddress;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.invel.XValidate.Permiso;
import com.invel.XValidate.Client.XValidateClientImpl;
import com.invel.XValidate.Client.XValidateClientInterface;
import com.invel.common.PromoCommons;
import com.invel.promo.dao.User.InvalidUserException;
import com.invel.promo.struts.action.InitCreatePromoAction;

/**
 * 
 * UserStore encargado de validar usuarios mediante el XValidate.
 * 
 * @author Fernando Rueda
 * 
 */
public class SimpleUserValidateStore implements UserStore {

    /** Groupid es '1' para toda el area de InStore */
    public static final int GROUP_ID = 1;

    /** Gestor de Reglas de Negocio tiene asignado el id de aplicaci�n '31' */
    public static final int APP_ID = 31;

    /**
     * El ID de accesopadre que se corresponde con el id de aplicacion 31. XPromo y Gestor de impuestos
     * tienen como ID padre al gestor de reglas de negocio.
     */
    public static final int PARENT_ID = 10310000;

    static Log localLog = LogFactory.getLog(InitCreatePromoAction.class);

    /**
     * Implementaci�n m�todo interfaz.
     * 
     * @author frueda
     * @param username
     * @param password
     * @return User
     */
    public User getUser(String username, String pPassword) throws Exception {
	SimpleUserValidate result = null;
	try {
	    //
	    XValidateClientInterface client = new XValidateClientImpl(username,
		    PromoCommons.getUrlwebservice());

	    // Ejecuto el logueo con el pass y la pc desde la cual lo ejecuto(en
	    // este caso... mi pc)
	    InetAddress addr = InetAddress.getLocalHost(); // obtener direcci�n
	    // IP
	    client.login(pPassword, addr.getHostAddress()); // pc desde donde se
	    // llama
	    // Controlamos si dio error
	    if (client.getLastResponseCode() == XValidateClientInterface.OK) {
		result = createUser(username, pPassword, client);
	    } else if (client.getLastResponseCode() == XValidateClientInterface.ACTUALIZAR_PWD) {
		throw new InvalidUserException("Actualizar password.");
	    } else if (client.getLastResponseCode() == XValidateClientInterface.CTA_BLOQUEADA) {
		throw new InvalidUserException("Cuenta bloqueada.");
	    } else if (client.getLastResponseCode() == XValidateClientInterface.PWD_REJECTED) {
		throw new InvalidUserException("Password desconocido");
	    } else if (client.getLastResponseCode() == XValidateClientInterface.SERVER_ERROR) {
		throw new InvalidUserException("Error de servidor.");
	    } else if (client.getLastResponseCode() == XValidateClientInterface.NEW_PWD_INVALIDO) {
		throw new InvalidUserException("Inv�lido password nuevo.");
	    } else if (client.getLastResponseCode() == XValidateClientInterface.NOT_ALLOWED_YET) {
		throw new InvalidUserException(
			"En estos momentos no se encuentra disponible.");
	    } else if (client.getLastResponseCode() == XValidateClientInterface.UNKNOWN_USER) {
		throw new InvalidUserException("Usuario desconocido");
	    } else if (client.getLastResponseCode() == XValidateClientInterface.LOST_PERFIL) {
		throw new InvalidUserException("Perfil perdido.");
	    } else if (client.getLastResponseCode() == XValidateClientInterface.PADRE_NO_EXISTE) {
		throw new InvalidUserException("No existe la clase padre.");
	    } else if (client.getLastResponseCode() == XValidateClientInterface.ACCESO_NO_EXISTE) {
		throw new InvalidUserException("No existe el acceso.");
	    } else if (client.getLastResponseCode() == XValidateClientInterface.CLIENT_ERROR) {
		throw new InvalidUserException("Error en el cliente.");
	    } else if (client.getLastResponseCode() == XValidateClientInterface.LOGIN_VENCIDO) {
		throw new InvalidUserException("Login vencido.");
	    }

	    // Deslogueo
	    // client.logout();
	} catch (Exception ex) {
	    localLog.error("Erroraso!!");
	    localLog.error(ex.getMessage());
	    ex.printStackTrace();
	    throw new InvalidUserException(ex.getMessage());
	}

	return result;
    }

    /**
     * Crea una instancia de un XValidateUser. Precondiciones: username y
     * password correctos y client!=null
     * 
     * @param username
     * @param password
     * @param client
     * @return User
     * @throws Exception
     */
    private SimpleUserValidate createUser(String username, String password,
	    XValidateClientInterface client) throws Exception {
	SimpleUserValidate user = new SimpleUserValidate(username, password,
		client);

	Permiso p = client.getAccess(GROUP_ID, APP_ID,
		    EResource.GESTOR_REGLAS_DE_NEGOCIO.getID(),
		    EResource.GESTOR_REGLAS_DE_NEGOCIO.name(),
		    EResource.GESTOR_REGLAS_DE_NEGOCIO.getParent());
	p = client.getAccess(GROUP_ID, APP_ID,
		    EResource.XPROMO.getID(),
		    EResource.XPROMO.name(),
		    EResource.XPROMO.getParent());
	p = client.getAccess(GROUP_ID, APP_ID,
		    EResource.GESTOR_IMPUESTOS.getID(),
		    EResource.GESTOR_IMPUESTOS.name(),
		    EResource.GESTOR_IMPUESTOS.getParent());	
	
	user.setCanassociatepromo(this.canassociatepromo(client));
	user.setCancompilepromo(this.cancompilepromo(client));
	user.setCancreatepromo(this.cancreatepromo(client));
	user.setCandeletepromo(this.candeletepromo(client));
	user.setCanexportpromo(this.canexportpromo(client));
	user.setCanreadpromo(this.canreadpromo(client));
	user.setCanupdatepromo(this.canupdatepromo(client));
	user.setCanConfigureXPromo(this.canConfigureXPromo(client));
	
	user.setAbleToAssociateTax(this.canassociatetax(client));
	user.setAbleToCompileTax(this.cancompiletax(client));
	user.setAbleToCreateTax(this.cancreatetax(client));
	user.setAbleToDeleteTax(this.candeletetax(client));
	user.setAbleToExportTax(this.canexporttax(client));
	user.setAbleToReadTax(this.canreadtax(client));
	user.setAbleToUpdateTax(this.canupdatetax(client));
	user.setAbleToConfigureGestorImpuesto(this.canConfigureGestorDeImpuestos(client));
	return user;
    }

    /**
     * Valida si el usuario tiene permiso para crear una nueva impuesto.
     * 
     * @param client
     *            Origen de la información.
     * @return boolean
     * @throws Exception
     */
    public boolean cancreatetax(XValidateClientInterface client)
	    throws Exception {
	boolean ret = false;

	try {
	    Permiso p = client.getAccess(GROUP_ID, APP_ID,
		    EResource.IMPUESTOSABM.getID(),
		    EResource.IMPUESTOSABM.getName(),
		    EResource.IMPUESTOSABM.getParent());
	    if (!p.isNone() && !p.hasDenied()) {
		if (p.hasAdd()) {
		    ret = true;
		}
	    }
	} catch (Exception ex) {
	    throw new InvalidUserException(
		    "Se produjo un error al validar permiso de crear promoci�n.");
	}
	return ret;
    }

    /**
     * Valida si tiene permiso de borrar un impuesto
     * 
     * @param client
     * @return boolean
     * @throws Exception
     */

    public boolean candeletetax(XValidateClientInterface client)
	    throws Exception {
	boolean ret = false;
	try {
	    Permiso p = client.getAccess(GROUP_ID, APP_ID,
		    EResource.IMPUESTOSABM.getID(),
		    EResource.IMPUESTOSABM.getName(),
		    EResource.IMPUESTOSABM.getParent());
	    if (!p.isNone() && !p.hasDenied()) {
		if (p.hasDelete()) {
		    ret = true;
		}
	    }
	} catch (Exception ex) {
	    throw new InvalidUserException(
		    "Se produjo un error al asignar permiso de eliminat promoci�n.");
	}
	return ret;
    }

    /**
     * canreadtax:it checks if a tax can be read
     * 
     * @param client
     * @return boolean
     * @throws Exception
     */
    public boolean canreadtax(XValidateClientInterface client)
	    throws Exception {
	boolean ret = false;
	try {
	    Permiso p = client.getAccess(GROUP_ID, APP_ID,
		    EResource.IMPUESTOSABM.getID(),
		    EResource.IMPUESTOSABM.getName(),
		    EResource.IMPUESTOSABM.getParent());
	    if (!p.isNone() && !p.hasDenied()) {
		if (p.hasList()) {
		    ret = true;
		}
	    }
	} catch (Exception ex) {
	    throw new InvalidUserException(
		    "Se produjo un error al asignar permiso de leer promoci�n.");
	}
	return ret;
    }

    /**
     * canupdatepromo:it checks if a tax can be updated.
     * 
     * @param client
     * @return boolean
     * @throws Exception
     */

    public boolean canupdatetax(XValidateClientInterface client)
	    throws Exception {
	boolean ret = false;
	try {
	    Permiso p = client.getAccess(GROUP_ID, APP_ID,
		    EResource.IMPUESTOSABM.getID(),
		    EResource.IMPUESTOSABM.getName(),
		    EResource.IMPUESTOSABM.getParent());
	    if (!p.isNone() && !p.hasDenied()) {
		if (p.hasChange()) {
		    ret = true;
		}
	    }
	} catch (Exception ex) {
	    throw new InvalidUserException(
		    "Se produjo un error al asignar permiso de actualizar promoci�n.");
	}
	return ret;
    }

    /**
     * canassociatepromo:it checks if a tax can be associated.
     * 
     * @param client
     * @return boolean
     * @throws Exception
     */
    public boolean canassociatetax(XValidateClientInterface client)
	    throws Exception {
	boolean ret = false;
	try {
	    // solo para crear el nodo del xpromo.
	    // @todo repensar todo esto...por ahora este debe ser el 1 metodo a
	    // llamar
	    Permiso p = client.getAccess(GROUP_ID, APP_ID, 0, "XPromo", 0);
	    p = client.getAccess(GROUP_ID, APP_ID,
		    EResource.ASOCIAR_IMPUESTOS.getID(),
		    EResource.ASOCIAR_IMPUESTOS.getName(),
		    EResource.ASOCIAR_IMPUESTOS.getParent());
	    if (!p.isNone() && !p.hasDenied()) {
		if (p.hasAdd()) {
		    ret = true;
		}
	    }
	} catch (Exception ex) {
	    localLog.error(ex.getMessage(), ex);
	    throw new InvalidUserException(
		    "Se produjo un error al validar permiso de asociar promoci�n."
			    + ex.getMessage());
	}
	return ret;
    }

    /**
     * cancompilepromo:it checks if a tax can be compiled.
     * 
     * @param client
     * @return boolean
     * @throws Exception
     */
    public boolean cancompiletax(XValidateClientInterface client)
	    throws Exception {
	boolean ret = false;
	try {
	    Permiso p = client.getAccess(GROUP_ID, APP_ID,
		    EResource.COMPILAR_IMPUESTOS.getID(),
		    EResource.COMPILAR_IMPUESTOS.getName(),
		    EResource.COMPILAR_IMPUESTOS.getParent());
	    if (!p.isNone() && !p.hasDenied()) {
		if (p.hasAdd()) {
		    ret = true;
		}
	    }
	} catch (Exception ex) {
	    throw new InvalidUserException(
		    "Se produjo un error al validar permiso de asociar promoci�n.");
	}
	return ret;
    }

    /**
     * canexportpromo:it checks if a tax can be exported.
     * 
     * @param client
     * @return boolean
     * @throws Exception
     */
    public boolean canexporttax(XValidateClientInterface client)
	    throws Exception {
	boolean ret = false;
	try {
	    Permiso p = client.getAccess(GROUP_ID, APP_ID,
		    EResource.IMPUESTOSABM.getID(),
		    EResource.IMPUESTOSABM.getName(),
		    EResource.IMPUESTOSABM.getParent());
	    if (!p.isNone() && !p.hasDenied()) {
		if (p.hasExport()) {

		    ret = true;
		}
	    }
	} catch (Exception ex) {
	    throw new InvalidUserException(
		    "Se produjo un error al validar permiso de exportar promoci�n.");
	}
	return ret;
    }

    /**
     * Asociado al permiso de configuracion del xpromo , por el momento solo
     * cambiamos el tipo de promoci�n. Si surge otro acceso deberian ser
     * hermanos entre si, e hijos de un "configurar" gen�rico.
     * 
     * @param client
     * @return boolean
     * @throws Exception
     */
    public boolean canConfigureGestorDeImpuestos(XValidateClientInterface client)
	    throws Exception {
	boolean ret = false;
	try {
	    Permiso p = client.getAccess(GROUP_ID, APP_ID,
		    EResource.CONFIGURAR_IMPUESTOS.getID(),
		    EResource.CONFIGURAR_IMPUESTOS.getName(),
		    EResource.CONFIGURAR_IMPUESTOS.getParent());
	    if (!p.isNone() && !p.hasDenied()) {
		if (p.hasChange()) {
		    ret = true;
		}
	    }
	} catch (Exception ex) {
	    throw new InvalidUserException(
		    "Se produjo un error al validar permiso de exportar promoci�n.");
	}
	return ret;
    }

    /**
     * Valida si el usuario tiene permiso para crear una nueva promoci�n.
     * 
     * @param client
     *            Origen de la informaci�n.
     * @return boolean
     * @throws Exception
     */
    public boolean cancreatepromo(XValidateClientInterface client)
	    throws Exception {
	boolean ret = false;
	try {
	    Permiso p = client.getAccess(GROUP_ID, APP_ID,
		    EResource.PROMOCIONES.getID(),
		    EResource.PROMOCIONES.getName(),
		    EResource.PROMOCIONES.getParent());
	    if (!p.isNone() && !p.hasDenied()) {
		if (p.hasAdd()) {
		    ret = true;
		}
	    }
	} catch (Exception ex) {
	    throw new InvalidUserException(
		    "Se produjo un error al validar permiso de crear promoci�n.");
	}
	return ret;
    }

    /**
     * Valida si
     * 
     * @param client
     * @return boolean
     * @throws Exception
     */

    public boolean candeletepromo(XValidateClientInterface client)
	    throws Exception {
	boolean ret = false;
	try {
	    Permiso p = client.getAccess(GROUP_ID, APP_ID,
		    EResource.PROMOCIONES.getID(),
		    EResource.PROMOCIONES.getName(),
		    EResource.PROMOCIONES.getParent());
	    if (!p.isNone() && !p.hasDenied()) {
		if (p.hasDelete()) {
		    ret = true;
		}
	    }
	} catch (Exception ex) {
	    throw new InvalidUserException(
		    "Se produjo un error al asignar permiso de eliminat promoci�n.");
	}
	return ret;
    }

    /**
     * canreadpromo:it checks if a promotion can be read
     * 
     * @param client
     * @return boolean
     * @throws Exception
     */
    public boolean canreadpromo(XValidateClientInterface client)
	    throws Exception {
	boolean ret = false;
	try {
	    Permiso p = client.getAccess(GROUP_ID, APP_ID,
		    EResource.PROMOCIONES.getID(),
		    EResource.PROMOCIONES.getName(),
		    EResource.PROMOCIONES.getParent());
	    if (!p.isNone() && !p.hasDenied()) {
		if (p.hasList()) {
		    ret = true;
		}
	    }
	} catch (Exception ex) {
	    throw new InvalidUserException(
		    "Se produjo un error al asignar permiso de leer promoci�n.");
	}
	return ret;
    }

    /**
     * canupdatepromo:it checks if a promotion can be update.
     * 
     * @param client
     * @return boolean
     * @throws Exception
     */

    public boolean canupdatepromo(XValidateClientInterface client)
	    throws Exception {
	boolean ret = false;
	try {
	    Permiso p = client.getAccess(GROUP_ID, APP_ID,
		    EResource.PROMOCIONES.getID(),
		    EResource.PROMOCIONES.getName(),
		    EResource.PROMOCIONES.getParent());
	    if (!p.isNone() && !p.hasDenied()) {
		if (p.hasChange()) {
		    ret = true;
		}
	    }
	} catch (Exception ex) {
	    throw new InvalidUserException(
		    "Se produjo un error al asignar permiso de actualizar promoci�n.");
	}
	return ret;
    }

    /**
     * canassociatepromo:it checks if a promotion can be associated.
     * 
     * @param client
     * @return boolean
     * @throws Exception
     */
    public boolean canassociatepromo(XValidateClientInterface client)
	    throws Exception {
	boolean ret = false;
	try {
	    // solo para crear el nodo del xpromo.
	    // @todo repensar todo esto...por ahora este debe ser el 1 metodo a
	    // llamar
	    Permiso p = client.getAccess(GROUP_ID, APP_ID, 0, "XPromo", 0);
	    p = client.getAccess(GROUP_ID, APP_ID,
		    EResource.ASOCIAR_PROMOCIONES.getID(),
		    EResource.ASOCIAR_PROMOCIONES.getName(),
		    EResource.ASOCIAR_PROMOCIONES.getParent());
	    if (!p.isNone() && !p.hasDenied()) {
		if (p.hasAdd()) {
		    ret = true;
		}
	    }
	} catch (Exception ex) {
	    localLog.error(ex.getMessage(), ex);
	    throw new InvalidUserException(
		    "Se produjo un error al validar permiso de asociar promoci�n."
			    + ex.getMessage());
	}
	return ret;
    }

    /**
     * cancompilepromo:it checks if a promotion can be compiled.
     * 
     * @param client
     * @return boolean
     * @throws Exception
     */
    public boolean cancompilepromo(XValidateClientInterface client)
	    throws Exception {
	boolean ret = false;
	try {
	    Permiso p = client.getAccess(GROUP_ID, APP_ID,
		    EResource.COMPILAR_PROMOCIONES.getID(),
		    EResource.COMPILAR_PROMOCIONES.getName(),
		    EResource.COMPILAR_PROMOCIONES.getParent());
	    if (!p.isNone() && !p.hasDenied()) {
		if (p.hasAdd()) {
		    ret = true;
		}
	    }
	} catch (Exception ex) {
	    throw new InvalidUserException(
		    "Se produjo un error al validar permiso de asociar promoci�n.");
	}
	return ret;
    }

    /**
     * canexportpromo:it checks if a promotion can be exported.
     * 
     * @param client
     * @return boolean
     * @throws Exception
     */
    public boolean canexportpromo(XValidateClientInterface client)
	    throws Exception {
	boolean ret = false;
	try {
	    Permiso p = client.getAccess(GROUP_ID, APP_ID,
		    EResource.PROMOCIONES.getID(),
		    EResource.PROMOCIONES.getName(),
		    EResource.PROMOCIONES.getParent());
	    if (!p.isNone() && !p.hasDenied()) {
		if (p.hasExport()) {

		    ret = true;
		}
	    }
	} catch (Exception ex) {
	    throw new InvalidUserException(
		    "Se produjo un error al validar permiso de exportar promoci�n.");
	}
	return ret;
    }

    /**
     * Asociado al permiso de configuracion del xpromo , por el momento solo
     * cambiamos el tipo de promoci�n. Si surge otro acceso deberian ser
     * hermanos entre si, e hijos de un "configurar" gen�rico.
     * 
     * @param client
     * @return boolean
     * @throws Exception
     */
    public boolean canConfigureXPromo(XValidateClientInterface client)
	    throws Exception {
	boolean ret = false;
	try {
	    Permiso p = client.getAccess(GROUP_ID, APP_ID,
		    EResource.CONFIGURAR.getID(),
		    EResource.CONFIGURAR.getName(),
		    EResource.CONFIGURAR.getParent());
	    if (!p.isNone() && !p.hasDenied()) {
		if (p.hasChange()) {
		    ret = true;
		}
	    }
	} catch (Exception ex) {
	    throw new InvalidUserException(
		    "Se produjo un error al validar permiso de exportar promoci�n.");
	}
	return ret;
    }

    /**
     * No implemented metod of interface.
     * 
     */
    public User create(String username, String password) throws Exception {
	// TODO Auto-generated method stub
	return null;
    }

    /**
     * No implemented metod of interface.
     * 
     */
    public boolean logout(String username) throws Exception {
	// TODO Auto-generated method stub
	return false;
    }

}

/**
 * Enumeraci�n con los recursos asociados al XPromo y Gestor de Impuesots.
 * 
 * @author szeballos
 * 
 */
enum EResource {
    
    
    /** Recurso PROMOCIONES, su id y su nombre */
    GESTOR_REGLAS_DE_NEGOCIO(0,"Gestor Reglas de Negocio",0),
    
    XPROMO(1, "XPromo", SimpleUserValidateStore.PARENT_ID), 
    GESTOR_IMPUESTOS(2, "Gestor de Impuestos", SimpleUserValidateStore.PARENT_ID),

    PROMOCIONES(3, "ABM Promociones", SimpleUserValidateStore.PARENT_ID+XPROMO.id),
    /** Recurso ASOCIAR_PROMOCIONES, su id y su nombre */
    ASOCIAR_PROMOCIONES(5, "Asociar Promociones a Sucursales", SimpleUserValidateStore.PARENT_ID+XPROMO.id),
    /** Recurso COMPILAR_PROMOCIONES, su id y su nombre */
    COMPILAR_PROMOCIONES(6, "Compilar Promociones", SimpleUserValidateStore.PARENT_ID+XPROMO.id),
    /** Recurso Configuracion* */
    CONFIGURAR(7, "Configuración",SimpleUserValidateStore.PARENT_ID+ XPROMO.id),

    IMPUESTOSABM(8, "ABM Impuestos", SimpleUserValidateStore.PARENT_ID+GESTOR_IMPUESTOS.id), 
    ASOCIAR_IMPUESTOS(9,"Asociar Impuestos a Sucursales", SimpleUserValidateStore.PARENT_ID+GESTOR_IMPUESTOS.id), 
    COMPILAR_IMPUESTOS(10, "Compilar Impuestos", SimpleUserValidateStore.PARENT_ID+GESTOR_IMPUESTOS.id), 
    CONFIGURAR_IMPUESTOS(11, "Configuración", SimpleUserValidateStore.PARENT_ID+GESTOR_IMPUESTOS.id);

    private final int id;

    private final String name;

    private final int parent;

    private static final int PARENT_ID = 10310000;
    /**
     * Constructor del tipo
     * 
     * @param pID
     * @param pName
     */
    EResource(int pID, String pName, int pParent) {
	this.id = pID;
	this.name = pName;
	this.parent = pParent;
    }

    /**
     * Retorna el nombre asociado al recurso
     * 
     * @return String
     */
    public String getName() {
	return this.name;
    }

    /**
     * Igual resultado que llamar a <code>getName()</code>
     */
    @Override
    public String toString() {
	return this.getName();
    }

    /**
     * Retorna el ID asociado al recurso
     * 
     * @return int
     */
    public int getID() {
	return this.id;
    }

    /**
     * @return the parent
     */
    public int getParent() {
	return this.parent;
    }

}
