/*
 * ArticuloStore.java
 *
 * Created on 29 de septiembre de 2005, 15:24
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.invel.promo.dao;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface ArticuloStore {

    String ORDER_BARRA = " order by codigo_barra";

    String ORDER_CODE = " order by cod_interno";

    String ORDER_ALFA = " order by cod_interno2";

    String ORDER_NAME = " order by nombre";

    String ORDER_MARCA = " order by marca";

    void create(Articulo articulo) throws Exception;

    Articulo getArticulo(String cod_bar, long cod_int, String cod_int_alfa)
	    throws Exception;

    List<Articulo> getAllArticulo(String order) throws Exception;

    List<Articulo> getAllArticulo(String order, String field, String valueFrom,
	    String valueTo) throws Exception;

    List<Articulo> getAllArticulo(String order, String field, long valueFrom,
	    long valueTo) throws Exception;

    /**
     * Toma un conjunto de c�digos de barras y devuelve una colecci�n de
     * instancias de art�culos. Utilizado al importar listas de art�culos en una
     * promoci�n.
     * 
     * @param articulos
     * @return Collection<Articulo>
     * @throws SQLException
     */
    List<Articulo> importArticulo(Collection<String> articulos)
	    throws SQLException;

    /**
     * Informa todos los art�culos que se intent� importar, pero que no fueron
     * encontrados en base de datos. M�todo informativo ha llamar una vez que se
     * ha llamado a <code>imporArticulo</code>
     * 
     * @return Collection<String>
     * @throws SQLException
     */
    public List<String> getImportMissing() throws SQLException;
}
