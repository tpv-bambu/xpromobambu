package com.invel.promo.dao;

public class SimpleDepartamento implements Departamento {

    private long cod_depto;

    private String desc_depto;

    public SimpleDepartamento(long pCodDepto, String pDescDepto) {
	this.cod_depto = pCodDepto;
	this.desc_depto = pDescDepto;
    }

    public long getCod_depto() {
	return this.cod_depto;
    }

    public String getDesc_depto() {
	return this.desc_depto;
    }

    public void setCod_depto(long pCodDepto) {
	this.cod_depto = pCodDepto;
    }

    public void setDesc_depto(String pDescDepto) {
	this.desc_depto = pDescDepto;
    }
}
