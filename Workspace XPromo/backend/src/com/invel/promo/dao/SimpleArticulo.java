/*
 * SimpleArticulo.java
 *
 * Created on 29 de septiembre de 2005, 15:29
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.invel.promo.dao;

import java.io.Serializable;

public class SimpleArticulo implements Articulo, Serializable {

    private static final long serialVersionUID = 1L;

    private long codInterno;

    private String codBarra;

    private String codInternoAlfa;

    private String description;

    private String marca;

    private double price;

    // Por default queremos que los articulos se instancien con cant. en 1.
    private int cantidad = 1;

    @SuppressWarnings("unused")
    private double discount;

    private boolean selected;

    /** Precio con impuestos incluidos del articulo */
    private double precioGravado;

    /**
     * Creates a new instance of SimpleArticulo
     * 
     * @param pCodBarra
     * @param pDescription
     * @param pPrice
     * @param pMarca
     * @param pCodInternoAlfa
     * @param pCodInterno
     */
    public SimpleArticulo(String pCodBarra, String pDescription, double pPrice,
	    String pMarca, String pCodInternoAlfa, long pCodInterno) {
	this.codBarra = pCodBarra;
	this.description = pDescription;
	this.price = pPrice;
	this.marca = pMarca;
	this.codInternoAlfa = pCodInternoAlfa;
	this.codInterno = pCodInterno;
    }

    public String getCodBarra() {
	return this.codBarra;
    }

    public long getCodInterno() {
	return this.codInterno;
    }

    public String getCodInternoAlfa() {
	return this.codInternoAlfa;
    }

    public String getDescription() {
	return this.description;
    }

    public String getMarca() {
	return this.marca;
    }

    public double getPrice() {
	return this.price;
    }

    public void setCodBarra(String pCodBarra) {
	this.codBarra = pCodBarra;
    }

    public void setCodInterno(long pCodInterno) {
	this.codInterno = pCodInterno;
    }

    public void setCodInternoAlfa(String pCodInternoAlfa) {
	this.codInternoAlfa = pCodInternoAlfa;
    }

    public void setDescription(String pDescription) {
	this.description = pDescription;
    }

    public void setMarca(String pMarca) {
	this.marca = pMarca;
    }

    public void setPrice(double pPrice) {
	this.price = pPrice;
    }

    @Override
    public boolean equals(Object articulo) {
	if (this == articulo) {
	    return true;
	}
	if (articulo instanceof Articulo) {
	    Articulo art = (Articulo) articulo;
	    if (this.codBarra.equals(art.getCodBarra())
		    && (this.codInterno == art.getCodInterno())
		    && (this.codInternoAlfa.equals(art.getCodInternoAlfa()))) {
		return true;
	    }
	}
	return false;
    }

    @Override
    public String toString() {
	StringBuilder art = new StringBuilder();
	art.append("[codBarra=").append(this.codBarra);
	art.append("|description=").append(this.description);
	art.append("|price=").append(this.price);
	art.append("]");
	return art.toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.invel.promo.dao.Articulo#getCantidad()
     */
    public int getCantidad() {
	// TODO Auto-generated method stub
	return this.cantidad;
    }

    public void setCantidad(int pCant) {
	this.cantidad = pCant;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.invel.promo.dao.Articulo#getSelected()
     */
    public boolean getSelected() {
	// TODO Auto-generated method stub
	return this.selected;
    }

    /**
     * Getter, implementacion metodo interfaz.
     */
    public double getPrecioGravado() {
	// TODO Auto-generated method stub
	return this.precioGravado;
    }

    /**
     * Setter del precio gravado.
     * 
     * @param precio
     */
    public void setPrecioGravado(double precio) {
	this.precioGravado = precio;
    }
}
