package com.invel.promo.dao;

import java.util.Collection;

public interface ProSucStore {
    public ProSuc getProSuc(int cod_sucursal, int cod_promo) throws Exception;

    public void create(ProSuc prosuc) throws Exception;

    /**
     * Actualiza la referencia a <code>promo</code> a todas las sucursales que
     * tengan habilitada dicha promo.
     * 
     * @param promo
     * @throws Exception
     * @return int cantidad de filas afectadas
     */
    public int updatePromo(Promo promo) throws Exception;

    public void deleteCodSuc(int cod_sucursal, boolean impuesto) throws Exception;

    public void deleteCodPromo(int cod_promo) throws Exception;

    public Collection<ProSuc> getAllPromo(int cod_sucursal, boolean impuesto) throws Exception;

    public Collection<ProSuc> getAllSucursal(int cod_promo) throws Exception;
}
