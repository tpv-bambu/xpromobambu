/**
 * 
 */
package com.invel.promo.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Store encargado de acceder a la informaci�n de estructura comercial.
 * 
 * @author szeballos
 * 
 */
public interface EstructuraComercialStore {

    /**
     * Devuelve todos los niveles cargados en bd, dado que no son muchos se
     * podrian fetchear una �nica vez desde BD.
     */
    public Collection<EstructuraComercialNivel> getAllNiveles() throws SQLException;

    /**
     * Dado un c�digo de nivel, devuelve el nivel de estructura correspondiente.
     * 
     * @param code
     * @return EstructuraComercialNivel
     */
    public EstructuraComercialNivel getNivel(int code) throws SQLException;

    /**
     * Dado un nivel, obtener todos los nodos que pertenecen a dicho nivel.
     * 
     * @param nivel
     * @return Collection<EstructuraComercialNodo>
     */
    public ArrayList<EstructuraComercialNodo> getNodos(
	    EstructuraComercialNivel nivel) throws Exception;

    /**
     * Obtiene el nodo asociado al nivel y c�digo de nodo recibido por
     * par�metro.
     * 
     * @param codeN1
     * @param codeN2
     * @return EstructuraComercialNodo
     * @throws SQLException
     */
    EstructuraComercialNodo getNodo(int codeN1, String codeN2)
	    throws SQLException;
    
    
    
    /**
     * Indica si estamos usando el codigo de clasificacion extendido o el regular
     * en los nodos de la clasif2.
     * @return
     */
    boolean usaClasificacionExtendida();

}
