package com.invel.promo.dao;

public class SimpleMunicipio implements Municipios {

    private int codMunicipio;
    // para evitar que sea null
    private String descripcion = "";

    @Override
    public int getCodMunicipio() {
	// TODO Auto-generated method stub
	return codMunicipio;
    }

    @Override
    public String getDescripcion() {
	// TODO Auto-generated method stub
	return descripcion;
    }

    @Override
    public void setCodMunicipio(int codMunicipio) {
	this.codMunicipio = codMunicipio;
    }

    @Override
    public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
    }

    public boolean equals(Object another) {
	boolean result = false;
	if (!(another instanceof SimpleMunicipio)) {
	    return result;
	}
	SimpleMunicipio anotherMunicipio = (SimpleMunicipio) another;
	if (this.codMunicipio == anotherMunicipio.getCodMunicipio()) {
	    result = true;
	}
	return result;
    }

    @Override
    public int hashCode() {
	int result = 0;
	result = descripcion.hashCode() + 37 * this.codMunicipio;
	return result;
    }

}
