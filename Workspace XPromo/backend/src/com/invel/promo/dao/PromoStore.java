/*
 * PromoStore.java
 *
 * Created on 19 de septiembre de 2005, 12:15
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.invel.promo.dao;

import java.util.Collection;

/**
 * Interfaz DAO para el acceso a base de datos de objetos "promos".
 * 
 * @author marcelo sotero
 * 
 */
public interface PromoStore {
    /**
     * Recupera una promo desde la bd.
     * 
     * @param id
     *            Identificaci�n de la promo a recuperar
     * @param xml
     *            Si true se carga el xml de la promo, c.c. solo se cargan
     *            codigo y t�tulo, pero no el xml en si.
     * 
     * @return Promoci�n
     * @throws Exception
     */
    Promo getPromo(int id, boolean xml) throws Exception;

    /**
     * Obtiene todas las promociones desde la BD
     * 
     * @param xml
     *            Si true, busca desde bd todas las promociones incluyendo el
     *            xml asociado. Caso contrario solo se obtiene una lista de
     *            promociones.
     * @param tax 
     * 		  Indica si busco promociones (false) o impuestos (true)
     * 
     * @return Collection<Promo>
     * @throws Exception
     */
    Collection<Promo> getAllPromo(boolean xml, boolean tax) throws Exception;

    /**
     * Obtiene todas las promociones desde la BD ordenadas segun seleccion del
     * usuario.
     * 
     * @param xml
     *            si true, busca desde bd todas las promociones incluyendo el
     *            xml asociado. Caso contrario solo se obtiene una lista de
     *            promociones.
     *            
     * @param tax 
     * 		  Indica si busco promociones (false) o impuestos (true)
     *            
     * @param orderType
     *            Identificador del criuterio de ordenamiento a aplicar.
     * @return Collection<Promo>
     * @throws Exception
     */
    Collection<Promo> getAllPromo(boolean xml, boolean tax, int orderType) throws Exception;

    /**
     * Obtiene todas las promociones desde la BD.
     * 
     * @param xml
     *            si true, busca desde bd todas las promociones incluyendo el
     *            xml asociado. Caso contrario solo se obtiene el esquema de la
     *            promo sin su xml.
     * @param codigos
     *            C�digos de las promociones a recuperar.
     * @return Collection<Promo>
     */
    Collection<Promo> getAllPromo(boolean xml, String[] codigos)
	    throws Exception;

    /**
     * Obtiene todas las promociones desde la BD, filtrada por tipo.
     * 
     * @param type
     * @return Collection<Promo>
     * @throws Exception
     */
    Collection<Promo> getAllTypePromo(int type) throws Exception;

    /**
     * Obtiene todas las promociones desde la BD, filtrada por tipo y ordenandas
     * seg{un la opci�n seleccionada por el usuario.
     * 
     * @param type
     *            Tipo de promoci�n
     * @param orderType
     *            Identificador de criterio de ordenamiento
     * @return Collection<Promo>
     * @throws Exception
     */
    Collection<Promo> getAllTypePromo(int type, int orderType) throws Exception;

    /**
     * Obtiene un listado de todas las promociones desde la BD que sean del tipo
     * <code>type</code>. No obtiene el XML de la promo en s�, por lo que es
     * �til si solo se necesita el listado de las promociones (con tipo y
     * t�tulo), pero no la promo en si.
     * 
     * @param type
     * @return Collection <Promo>
     */
    Collection<Promo> getAllListTypePromo(int type) throws Exception;

    /**
     * Guarda una promo en BD
     * 
     * @param promo
     * @throws Exception
     */
    void create(Promo promo) throws Exception;

    /**
     * Actualiza una promo en BD
     * 
     * @param promo
     * @throws Exception
     */
    void update(Promo promo) throws Exception;

    /**
     * Borra una promo de la BD
     * 
     * @param id
     *            Identificaci�n de la promo a eliminar
     * @throws Exception
     */
    void delete(int id) throws Exception;

    /**
     * Habilita una promo en la BD
     * 
     * @param promo
     * @throws Exception
     */
    void enable(Promo promo) throws Exception;

    /**
     * Deshabilita una promo en BD
     * 
     * @param promo
     * @throws Exception
     */
    void disable(Promo promo) throws Exception;

    /**
     * Obtiene todas las promociones habilitadas.
     * 
     * @param tax 
     * 		  Indica si busco promociones (false) o impuestos (true)
     *
     * 
     * @return Collection<Promo>
     */
    Collection<PromoSelected> getAllPromoEnabled(boolean tax) throws Exception;

    /**
     * Excepcion a lanzar en caso de no encontrar la promoci�n en la base de
     * datos.
     * 
     * @author szeballos
     * 
     */
    public static class PromoNotFoundException extends Exception {
	static final long serialVersionUID = -3387516993124229948L;

	public PromoNotFoundException() {
	    super();
	    // TODO Auto-generated constructor stub
	}

	public PromoNotFoundException(String message, Throwable cause) {
	    super(message, cause);
	    // TODO Auto-generated constructor stub
	}

	public PromoNotFoundException(String message) {
	    super(message);
	    // TODO Auto-generated constructor stub
	}

	public PromoNotFoundException(Throwable cause) {
	    super(cause);
	    // TODO Auto-generated constructor stub
	}

    }

}
