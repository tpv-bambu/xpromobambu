package com.invel.promo.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SimpleProSucStore implements ProSucStore {

    private String url;

    private String username;

    private String password;

    public SimpleProSucStore(Properties properties) {
	this.url = properties.getProperty("url");
	this.username = properties.getProperty("username");
	this.password = properties.getProperty("password");
    }

    protected static final String FIELDS_INSERT = "cod_sucursal, cod_promo";

    protected static final String INSERT_SQL = "insert into XPRO_SUC ( "
	    + FIELDS_INSERT + " ) values ( ?, ?)";

    protected static final String DELETE_SQL = "delete from XPRO_SUC where cod_sucursal = ? and cod_promo in (select xp.cod_promo from xpromo xp join type_promo tp on xp.type_promo = tp.type_promo where tp.es_impuesto=?)";

    protected static final String DELETE_COD_PROMO = "delete from XPRO_SUC where cod_promo = ?";

    protected static final String UPDATE_PROMO = "update XPRO_SUC set COD_PROMO=? where COD_PROMO=?";

    static Log log = LogFactory.getLog(SimpleProSucStore.class);

    public ProSuc getProSuc(int cod_sucursal, int cod_promo) throws Exception {
	ProSuc aprosuc = null;
	Connection conn = null;
	try {
	    conn = createConnection();
	    PreparedStatement s = conn
		    .prepareStatement("select * from XPRO_SUC where cod_sucursal = ? and cod_promo = ?");
	    s.setInt(1, cod_sucursal);
	    s.setInt(2, cod_promo);
	   
	    ResultSet results = s.executeQuery();
	    if (results.next()) {
		aprosuc = createProSuc(results);
	    } else {
		throw new Exception("XPROSUC with cod_sucursal=" + cod_sucursal
			+ " and cod_promo=" + cod_promo + " not found.");
	    }
	} finally {
	    closeConnection(conn);
	}
	return aprosuc;
    }

    public void create(ProSuc prosuc) throws Exception {
	Connection conn = null;
	try {
	    conn = createConnection();
	    PreparedStatement s = conn.prepareStatement(INSERT_SQL);
	    s.setInt(1, prosuc.getCod_sucursal());
	    s.setInt(2, prosuc.getCod_promo());
	    s.execute();
	} finally {
	    closeConnection(conn);
	}

    }

    public void deleteCodSuc(int cod_sucursal,boolean impuesto) throws Exception {
	Connection conn = null;
	try {
	    conn = createConnection();
	    PreparedStatement s = conn.prepareStatement(DELETE_SQL);
	    s.setInt(1, cod_sucursal);
	    s.setBoolean(2, impuesto);
	    s.execute();
	} finally {
	    closeConnection(conn);
	}
    }

    public void deleteCodPromo(int cod_promo) throws Exception {
	Connection conn = null;
	try {
	    conn = createConnection();
	    PreparedStatement s = conn.prepareStatement(DELETE_COD_PROMO);
	    s.setInt(1, cod_promo);
	    s.execute();
	} finally {
	    closeConnection(conn);
	}
    }

    public Collection<ProSuc> getAllPromo(int cod_sucursal,boolean impuesto) throws Exception {
	ArrayList<ProSuc> prosuclist = new ArrayList<ProSuc>();
	Connection conn = null;
	try {
	    conn = createConnection();
	    PreparedStatement s = conn
		    .prepareStatement("select xp_suc.* from XPRO_SUC xp_suc join XPROMO xp on xp.cod_promo=xp_suc.cod_promo join TYPE_PROMO tp  on xp.type_promo=tp.type_promo where cod_sucursal = ? and tp.es_impuesto=?");
	    s.setInt(1, cod_sucursal);
	    s.setBoolean(2, impuesto);
	    ResultSet results = s.executeQuery();
	    while (results.next()) {
		ProSuc prosuc = createProSuc(results);
		prosuclist.add(prosuc);
	    }
	} finally {
	    closeConnection(conn);
	}
	return prosuclist;
    }

    public Collection<ProSuc> getAllSucursal(int cod_promo) throws Exception {
	ArrayList<ProSuc> prosuclist = new ArrayList<ProSuc>();
	Connection conn = null;
	try {
	    conn = createConnection();
	    PreparedStatement s = conn
		    .prepareStatement("select * from XPRO_SUC where cod_promo = ?");
	    s.setInt(1, cod_promo);
	    ResultSet results = s.executeQuery();
	    while (results.next()) {
		ProSuc prosuc = createProSuc(results);
		prosuclist.add(prosuc);
	    }
	} finally {
	    closeConnection(conn);
	}
	return prosuclist;
    }

    private ProSuc createProSuc(ResultSet results) throws Exception {
	ProSuc aprosuc = new SimpleProSuc(results.getInt("cod_sucursal"),
		results.getInt("cod_promo"));
	return aprosuc;
    }

    private Connection createConnection() throws Exception {
	// log.debug("Connecting to db [url=" + url + ",username=" + username
	// + ",password=" + password + "]");
	return DriverManager.getConnection(this.url, this.username,
		this.password);
    }

    private void closeConnection(Connection conn) {
	if (conn != null) {
	    try {
		conn.close();
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}
    }

    public int updatePromo(Promo promo) throws Exception {
	int result = -1;
	Connection conn = null;
	try {
	    conn = this.createConnection();
	    PreparedStatement stmt = conn.prepareStatement(UPDATE_PROMO);
	    stmt.setInt(1, promo.getInternalId());
	    stmt.setInt(2, promo.getOldInternalId());
	    result = stmt.executeUpdate();
	} finally {
	    closeConnection(conn);
	}
	return result;
    }
}
