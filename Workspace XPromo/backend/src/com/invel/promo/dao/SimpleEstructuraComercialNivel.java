/**
 * 
 */
package com.invel.promo.dao;

/**
 * @author szeballos
 * 
 */
public class SimpleEstructuraComercialNivel implements EstructuraComercialNivel {

    private int codigo;
    private String descripcion;
    private int digitoFin;
    private int digitoInicio;
    private int status;

    /*
     * (non-Javadoc)
     * 
     * @see com.invel.promo.dao.EstructuraComercialNivel#getCodigo()
     */
    public int getCodigo() {
	// TODO Auto-generated method stub
	return this.codigo;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.invel.promo.dao.EstructuraComercialNivel#getDescripcion()
     */
    public String getDescripcion() {
	// TODO Auto-generated method stub
	return this.descripcion;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.invel.promo.dao.EstructuraComercialNivel#getDigitoFin()
     */
    public int getDigitoFin() {
	// TODO Auto-generated method stub
	return this.digitoFin;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.invel.promo.dao.EstructuraComercialNivel#getDigitoInicio()
     */
    public int getDigitoInicio() {
	// TODO Auto-generated method stub
	return this.digitoInicio;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.invel.promo.dao.EstructuraComercialNivel#getStatus()
     */
    public int getStatus() {
	// TODO Auto-generated method stub
	return this.status;
    }

    /**
     * @param codigo
     *            the codigo to set
     */
    public void setCodigo(int pCodigo) {
	this.codigo = pCodigo;
    }

    /**
     * @param descripcion
     *            the descripcion to set
     */
    public void setDescripcion(String pDescripcion) {
	this.descripcion = pDescripcion;
    }

    /**
     * @param digitoFin
     *            the digitoFin to set
     */
    public void setDigitoFin(int pDigitoFin) {
	this.digitoFin = pDigitoFin;
    }

    /**
     * @param digitoInicio
     *            the digitoInicio to set
     */
    public void setDigitoInicio(int pDigitoInicio) {
	this.digitoInicio = pDigitoInicio;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(int pStatus) {
	this.status = pStatus;
    }

}
