/*
 * Articulo.java
 *
 * Created on 29 de septiembre de 2005, 15:26
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.invel.promo.dao;

public interface Articulo {

    public long getCodInterno();

    public String getCodBarra();

    public String getCodInternoAlfa();

    public String getDescription();

    public String getMarca();

    public double getPrice();

    public boolean getSelected();

    public int getCantidad();

    public void setCantidad(int cantidad);

    public void setPrecioGravado(double pPrice);

    /**
     * Devuelve el <b>monto</b> del impuesto interno.
     * 
     * @return
     */
    public double getPrecioGravado();

}
