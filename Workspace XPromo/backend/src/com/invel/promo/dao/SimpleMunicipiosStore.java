package com.invel.promo.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;

import java.util.Properties;

public class SimpleMunicipiosStore implements MunicipiosStore {

    private static final String SELECT = "select * from municipios order by cod_municipio";

    private String password;
    private String url;
    private String username;

    public SimpleMunicipiosStore(Properties properties) {
	this.url = properties.getProperty("url");
	this.username = properties.getProperty("username");
	this.password = properties.getProperty("password");
    }

    @Override
    public ArrayList<Municipios> getAllMunicipios() throws Exception {
	// TODO Auto-generated method stub
	ArrayList<Municipios> municipios = new ArrayList<Municipios>();

	Connection conn = this.createConnection();
	java.sql.Statement stmt = conn.createStatement();
	ResultSet rs = stmt.executeQuery(SELECT);
	while (rs.next()) {
	    municipios.add(createMunicipio(rs));
	}
	rs.close();
	stmt.close();
	return municipios;
    }

    private Municipios createMunicipio(ResultSet rs) throws Exception {
	// TODO Auto-generated method stub
	SimpleMunicipio result = new SimpleMunicipio();
	result.setDescripcion(rs.getString("descripcion"));
	result.setCodMunicipio(rs.getInt("cod_municipio"));
	return result;
    }

    private Connection createConnection() throws Exception {
	// log.debug("Connecting to db [url=" + url + ",username=" + username
	// + ",password=" + password + "]");
	return DriverManager.getConnection(this.url, this.username,
		this.password);
    }

}
