/*
 * SimpleTypePromo.java
 *
 * Created on 17 de septiembre de 2005, 15:20
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.invel.promo.dao;

import invel.framework.xform.xelements.XForm;

import java.io.Serializable;

public class SimpleTypePromo implements TypePromo, Serializable {

    private static final long serialVersionUID = 1L;

    private String description;

    private String documentation;

    private int id;

    private transient XForm xform;

    private int suggestedCode;

    // nuevo campo de requerimiento
    private String descriptionInvel;

    public SimpleTypePromo(int pId, String pDescripcion, String pDocumentation,
	    XForm pXForm) {
	this.id = pId;
	this.description = pDescripcion;
	if (pDocumentation.equalsIgnoreCase("")) {
	    this.documentation = "<i>No posee documentaci�n.</i>";
	} else {
	    this.documentation = pDocumentation;
	}
	this.xform = pXForm;
    }

    public String getDescription() {
	return this.description;
    }

    public String getDocumentation() {
	return this.documentation;
    }

    public int getId() {
	return this.id;
    }

    public XForm getXForm() {
	return this.xform;
    }

    public void setDescription(String pDescription) {
	this.description = pDescription;
    }

    public void setDocumentation(String pDocumentation) {
	this.description = pDocumentation;
    }

    public void setId(int pId) {
	this.id = pId;
    }

    public void setXForm(XForm pXForm) {
	this.xform = pXForm;
    }

    public int getSuggestedCode() {
	return this.suggestedCode;
    }

    public void setSuggestedCode(int pSuggestedCode) {
	this.suggestedCode = pSuggestedCode;
    }

    public String getDescriptionInvel() {
	return this.descriptionInvel;
    }

    public void setDescriptionInvel(String pDescriptionInvel) {
	this.descriptionInvel = pDescriptionInvel;
    }

    /**
     * Implementacion m�todo interfaz CompareTo
     */
    public int compareTo(TypePromo o) {
	int result = 0;
	if (this.getDescription() == o.getDescription()) {
	    result = 0;
	} else if (this.getDescription() == null) {
	    result = -1;
	} else {
	    result = this.getDescription().compareTo(o.getDescription());
	}
	return result;
    }
}
