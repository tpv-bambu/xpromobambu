package com.invel.promo.dao;

/**
 * Municipios con los que trabaja.
 * 
 * @author sebastian
 * 
 */
public interface Municipios {

    public int getCodMunicipio();

    public void setCodMunicipio(int codMunicipio);

    public String getDescripcion();

    public void setDescripcion(String descripcion);

}
