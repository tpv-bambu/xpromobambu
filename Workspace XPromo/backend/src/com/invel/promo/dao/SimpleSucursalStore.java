package com.invel.promo.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.invel.common.PromoCommons;
import com.invel.common.PromoSystem;

public class SimpleSucursalStore implements SucursalStore {

    protected static String FIELDS_SELEC = "COD_SUCURSAL, DESC_SUC, USUARIO_FTP, PASSWORD_FTP, DIRECCION_IP_FTP";

    protected static String SELEC_SQL = "select " + FIELDS_SELEC
	    + " from SUCURSALES order by COD_SUCURSAL";

    protected static String SELEC_BY_COD_SUC = "select " + FIELDS_SELEC
	    + " from SUCURSALES where COD_SUCURSAL = ?";

    // selecciona las sucursales por promocion
    protected static String SELECT_SUCURSAL_BY_PROMO = "select s.cod_sucursal, s.desc_suc, s.usuario_ftp, "
	    + "s.password_ftp, s.direccion_ip_ftp "
	    + "from sucursales s, xpromo xp, xpro_suc xps "
	    + "where xps.cod_sucursal = s.cod_sucursal "
	    + "and xps.cod_promo = xp.cod_promo "
	    + "and xp.cod_promo = ? "
	    + "order by s.desc_suc ";

    // selcciona sucursales q tengan al menos un registro en la tabla
    // 'XPRO_SUC'
    protected static String SELECT_SUCURSAL_INCLUDE_ENABLED_PROMO = "select distinct s.cod_sucursal, "
	    + "s.desc_suc, s.usuario_ftp, s.password_ftp, s.direccion_ip_ftp "
	    + "from sucursales s, xpro_suc xp, xpromo x "
	    + "where xp.cod_sucursal = s.cod_sucursal "
	    + "and xp.cod_promo = x.cod_promo " + "and x.habilitado = 1 ";

    private String url;

    private String username;

    private String password;

    private static Log LOG = LogFactory.getLog(SimpleSucursalStore.class);

    private static final String DB_SUCURSALES = "SELECT nombre_servidor, "
	    + "nombre_base, nombre_usuario, pass FROM sucursales WHERE cod_sucursal = ? ";

    /** Store Procedure */
    private static final String SP_SUC = "{call SCRIPT_ALL_CONDICIONAL (26,?)}";

    public SimpleSucursalStore(Properties properties) {
	this.url = properties.getProperty("url");
	this.username = properties.getProperty("username");
	this.password = properties.getProperty("password");
    }

    public Collection<SimpleSucursal> getAllSucursal() throws Exception {
	ArrayList<SimpleSucursal> suclist = new ArrayList<SimpleSucursal>();
	Connection conn = null;
	try {
	    conn = createConnectionCentral();
	    PreparedStatement s = conn.prepareStatement(SELEC_SQL);
	    ResultSet results = s.executeQuery();
	    while (results.next()) {
		SimpleSucursal asucursal = createSucursal(results);
		suclist.add(asucursal);
	    }
	} finally {
	    closeConnection(conn);
	}
	return suclist;
    }

    public Collection<SimpleSucursal> getAllSucursalByPromo(Integer cod_promo)
	    throws Exception {

	ArrayList<SimpleSucursal> suclist = new ArrayList<SimpleSucursal>();
	Connection conn = null;

	try {
	    conn = createConnectionCentral();
	    PreparedStatement s = conn
		    .prepareStatement(SELECT_SUCURSAL_BY_PROMO);
	    s.setInt(1, cod_promo);
	    ResultSet results = s.executeQuery();

	    while (results.next()) {
		SimpleSucursal asucursal = createSucursal(results);
		suclist.add(asucursal);
	    }

	} finally {
	    closeConnection(conn);
	}

	return suclist;
    }

    public Sucursal getSucursal(int cod_sucursal) throws Exception {
	Sucursal sucursal = null;
	Connection conn = null;
	try {
	    conn = createConnectionCentral();
	    PreparedStatement s = conn.prepareStatement(SELEC_BY_COD_SUC);
	    s.setInt(1, cod_sucursal);
	    ResultSet results = s.executeQuery();
	    if (results.next()) {
		sucursal = createSucursal(results);
	    } else {
		throw new Exception("Sucursal with cod_sucursal "
			+ cod_sucursal + " not found.");
	    }
	} finally {
	    closeConnection(conn);
	}
	return sucursal;
    }

    public void sendScript(Sucursal to) throws Exception {

	Connection conn = null;
	try {
	    String host = PromoSystem.getHost();
	    String db = PromoSystem.getCentralDB();
	    conn = createConnectionCentral();

	    PreparedStatement ps = conn.prepareStatement(DB_SUCURSALES);
	    ps.setInt(1, to.getCod_sucursal());
	    ResultSet rs = ps.executeQuery();
	    if (rs.next()) {
		String sucHost, sucUser, sucPass, sucURL, sucDB;
		Connection sucConn;
		CallableStatement sucStmt;

		sucHost = rs.getString(1);
		sucDB = rs.getString(2);
		sucUser = rs.getString(3);
		sucPass = rs.getString(4);

		sucURL = this.url.replace(db, sucDB);
		sucURL = sucURL.replace(host, sucHost);
		LOG.debug(sucURL);

		sucConn = DriverManager.getConnection(sucURL, sucUser, sucPass);
		LOG.info("Establecida conexion JDBC con: " + sucConn);
		sucStmt = sucConn.prepareCall(SP_SUC);
		sucStmt.setString(1, PromoCommons.getOrigenFileScriptFtp()
			+ " " + PromoCommons.getDestinoFileScriptFtp());

		sucStmt.executeUpdate();
		if(PromoCommons.enviarSQL){
		    sucStmt.setString(1, PromoCommons.getOrigenFileSQLFtp()
			+ " " + PromoCommons.getDestinoFileSQLFtp());
		    sucStmt.executeUpdate();
		}

		sucStmt.close();
		sucConn.close();
	    }
	} finally {
	    closeConnection(conn);
	}
    }

    public Collection<SimpleSucursal> getAllSucIncludeEnabledPromo()
	    throws Exception {

	ArrayList<SimpleSucursal> suclist = new ArrayList<SimpleSucursal>();
	Connection conn = null;

	try {
	    conn = createConnectionCentral();
	    PreparedStatement s = conn
		    .prepareStatement(SELECT_SUCURSAL_INCLUDE_ENABLED_PROMO);
	    ResultSet results = s.executeQuery();

	    while (results.next()) {
		SimpleSucursal asucursal = createSucursal(results);
		suclist.add(asucursal);
	    }

	} finally {
	    closeConnection(conn);
	}

	return suclist;
    }

    // M�todos Privados

    private SimpleSucursal createSucursal(ResultSet results) throws Exception {
	SimpleSucursal sucursal = new SimpleSucursal(
		results.getInt("cod_sucursal"), results.getString("desc_suc"),
		results.getString("usuario_ftp"),
		results.getString("password_ftp"),
		results.getString("direccion_ip_ftp"));
	return sucursal;
    }

    private Connection createConnectionCentral() throws Exception {
	// log.debug("Connecting to db [url=" + url + ",username=" + username
	// + ",password=" + password + "]");
	return DriverManager.getConnection(this.url, this.username,
		this.password);
    }

    private void closeConnection(Connection conn) {
	if (conn != null) {
	    try {
		conn.close();
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}
    }
}
