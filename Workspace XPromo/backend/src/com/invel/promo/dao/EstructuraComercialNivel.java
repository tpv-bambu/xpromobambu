/**
 * 
 */
package com.invel.promo.dao;

/**
 * La estructura comercial esta asociado a departamentos y secciones del
 * comercio, por ejemplo secci�n limpieza dependiente del departamento hogar. La
 * estructura comercial es un �rbol n-ario, este objeto no est� asociado a los
 * nodos en s� sino a la profundidad de los mismos en el �rbol.
 * 
 * @author szeballos
 * 
 */
public interface EstructuraComercialNivel {

    /**
     * Asociado al campo status en base de datos...
     * 
     * @return int
     */
    int getStatus();

    /**
     * El c�digo asociado a esta estructura, �nico.
     * 
     * @return int
     */
    int getCodigo();

    /**
     * Descripci�n de este nivel de estructura.
     * 
     * @return int
     */
    String getDescripcion();

    /**
     * Todos los nodos que pertenecen a este nivel guardan su c�digo a partir de
     * este d�gito en el c�digo del art�culo.
     * 
     * @return int
     */
    int getDigitoInicio();

    /**
     * Todos los nodos que pertenecen a este nivel guardan su c�digo hasta este
     * d�gito en el c�digo del art�culo.
     * 
     * @return int
     */
    int getDigitoFin();
}
