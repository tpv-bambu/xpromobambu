package com.invel.promo.dao;

import java.io.Serializable;

public class SimpleProSuc implements ProSuc, Serializable {

    private static final long serialVersionUID = 1L;

    private int cod_promo;

    private int cod_sucursal;

    public SimpleProSuc() {
	// Default constructor
    }

    public SimpleProSuc(int pCodSucursal, int pCodPromo) {
	this.cod_sucursal = pCodSucursal;
	this.cod_promo = pCodPromo;
    }

    public int getCod_promo() {
	return this.cod_promo;
    }

    public int getCod_sucursal() {
	return this.cod_sucursal;
    }

    public void setCod_promo(int pCodPromo) {
	this.cod_promo = pCodPromo;
    }

    public void setCod_sucursal(int pCodSucursal) {
	this.cod_sucursal = pCodSucursal;
    }

    @Override
    public String toString() {
	StringBuilder pro_suc = new StringBuilder();
	pro_suc.append("[cod_promo=").append(this.cod_promo);
	pro_suc.append("|cod_sucursal=").append(this.cod_sucursal);
	pro_suc.append("]");
	return pro_suc.toString();
    }
}
