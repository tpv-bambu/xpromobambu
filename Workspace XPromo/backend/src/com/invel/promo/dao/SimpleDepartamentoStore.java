package com.invel.promo.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SimpleDepartamentoStore implements DepartamentoStore {

    protected static String FIELDS = "cod_depto, desc_depto";

    protected static String SELEC_ALL = "select " + FIELDS
	    + " from DEPARTAMENTOS";

    protected static String SELEC_SQL = "select " + FIELDS
	    + " from DEPARTAMENTOS where cod_depto = ?";

    static Log log = LogFactory.getLog(SimpleDepartamentoStore.class);

    private String password;

    private String url;

    private String username;

    public SimpleDepartamentoStore(Properties properties) {
	this.url = properties.getProperty("url");
	this.username = properties.getProperty("username");
	this.password = properties.getProperty("password");
    }

    public Collection<Departamento> getAllDepartamentos(String order)
	    throws Exception {
	ArrayList<Departamento> deptos = new ArrayList<Departamento>();
	Connection conn = null;
	String sqlCommand = SELEC_ALL;
	if (order != null) {
	    sqlCommand = sqlCommand + order;
	}
	try {
	    conn = createConnection();
	    PreparedStatement s = conn.prepareStatement(sqlCommand);
	    ResultSet results = s.executeQuery();
	    while (results.next()) {
		Departamento departamento = createDepartamento(results);
		deptos.add(departamento);
		// art.put(articulo.getId(), articulo);
	    }
	} finally {
	    closeConnection(conn);
	}
	return deptos;
    }

    public Departamento getDepartamento(long id) throws Exception {
	Departamento departamento = null;
	Connection conn = null;
	try {
	    conn = createConnection();
	    PreparedStatement s = conn.prepareStatement(SELEC_SQL);
	    s.setLong(1, id);
	    ResultSet results = s.executeQuery();
	    if (results.next()) {
		departamento = createDepartamento(results);
	    } else {
		throw new Exception("Departamento con id " + id
			+ " no encontrado.");
	    }
	} finally {
	    closeConnection(conn);
	}

	return departamento;
    }

    private void closeConnection(Connection conn) {
	if (conn != null) {
	    try {
		conn.close();
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}
    }

    private Connection createConnection() throws Exception {
	// log.debug("Connecting to db [url=" + url + ",username=" + username
	// + ",password=" + password + "]");
	return DriverManager.getConnection(this.url, this.username,
		this.password);
    }

    private Departamento createDepartamento(ResultSet results) throws Exception {
	Departamento depto = new SimpleDepartamento(
		results.getLong("cod_depto"), results.getString("desc_depto"));
	return depto;
    }

}
