/*
 * SimpleArticuloStore.java
 *
 * Created on 29 de septiembre de 2005, 15:40
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.invel.promo.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public final class SimpleArticuloStore implements ArticuloStore {

    protected static final String FIELDS = "codigo_barra, nombre, precio_sin_iva, marca, impuesto_interno,cod_impuesto1,cod_impuesto2,cod_impuesto3,cod_impuesto4,cod_iva,cod_interno2, cod_interno";

    protected static String INSERT_SQL = "insert into ARTICULO ( " + FIELDS
	    + " ) values ( ?, ?, ?, ?, ?, ? )";

    protected static String SELEC_SQL = "select "
	    + FIELDS
	    + " from ARTICULO where codigo_barra = ? and cod_interno = ? and cod_interno2 = ?";

    protected static String SELEC_ALL = "select " + FIELDS + " from ARTICULO";

    // Sentencias asociadas a la importacion
    protected static final String INSERT_IMPORT = "insert into imp_promos values (?,null)";

    /**
     * Obtengo todos los articulos procesados por el SP que existen en tabla
     * articulo
     */
    protected static final String SELECT_IMPORT = "select "
	    + FIELDS
	    + " from articulo, imp_promos "
	    + " where RTRIM(LTRIM(articulo.CODIGO_BARRA))=imp_promos.UPC"
	    + " union "
	    + " select "
	    + FIELDS
	    + " from articulo, imp_promos_a_temp"
	    + " where RTRIM(LTRIM(articulo.CODIGO_BARRA))=imp_promos_a_temp.UPC";

    /* Calling the Store Procedure */
    protected static final String SP_IMPORT = "{call GENERAR_DIGITOV_PROMO}";

    protected static final String FUN_GRAVADO = "{?=call GRAVAR_PRECIO(?,?,?,?,?,?,?)}";

    /* Limpiar la tabla de importaciones una vez que termino la importacion */
    protected static final String DELETE_IMPORT = "delete from imp_promos";

    private String url;

    private String username;

    private String password;

    static Log log = LogFactory.getLog(SimpleArticuloStore.class);

    public SimpleArticuloStore(Properties properties) {
	this.url = properties.getProperty("url");
	this.username = properties.getProperty("username");
	this.password = properties.getProperty("password");
    }

    public Articulo getArticulo(String cod_bar, long cod_int,
	    String cod_int_alfa) throws Exception {
	Articulo article = null;
	Connection conn = null;
	try {
	    conn = createConnection();
	    PreparedStatement s = conn.prepareStatement(SELEC_SQL);
	    s.setString(1, cod_bar);
	    s.setLong(2, cod_int);
	    s.setString(3, cod_int_alfa);
	    ResultSet results = s.executeQuery();
	    if (results.next()) {
		article = createArticulo(results, conn);
	    } else {
		log.error("Art�culo no encontrado: " + cod_int);
	    }
	} finally {

	    closeConnection(conn);
	}

	return article;
    }

    /**
     * Implementaci�n interfaz.
     */
    public List<Articulo> importArticulo(Collection<String> articulos)
	    throws SQLException {
	ArrayList<Articulo> result = new ArrayList<Articulo>();
	Connection conn = createConnection();
	PreparedStatement insert_pstmt = conn.prepareStatement(INSERT_IMPORT);
	Statement stmt = conn.createStatement();
	CallableStatement cstmt = conn.prepareCall(SP_IMPORT);
	ResultSet rs = null;

	// limpio la imp_promos
	stmt.executeUpdate(DELETE_IMPORT);

	// inserto en imp_promos
	for (String art : articulos) {
	    if (art != null) {
		insert_pstmt.setString(1, art.trim());
		insert_pstmt.executeUpdate();
	    }
	}
	// ejecuto SP
	cstmt.executeUpdate();
	// y leo los articulos que existen en tabla articulos
	rs = stmt.executeQuery(SELECT_IMPORT);
	while (rs.next()) {
	    Articulo art = createArticulo(rs, conn);
	    result.add(art);
	}

	// y libero recursos
	cstmt.close();
	stmt.close();
	insert_pstmt.close();
	rs.close();
	this.closeConnection(conn);
	return result;
    }

    public List<String> getImportMissing() throws SQLException {
	ArrayList<String> result = new ArrayList<String>();
	Connection conn = createConnection();
	Statement stmt = conn.createStatement();
	ResultSet rs = stmt.executeQuery("Select upc from imp_promos_C_temp");
	while (rs.next()) {
	    result.add(rs.getString(1));
	}
	rs.close();
	this.closeConnection(conn);
	return result;
    }

    public List<Articulo> getAllArticulo(String order) throws Exception {
	ArrayList<Articulo> articles = new ArrayList<Articulo>();
	Connection conn = null;
	String sqlCommand = SELEC_ALL;
	if (order != null) {
	    sqlCommand = sqlCommand + order;
	}
	try {
	    conn = createConnection();
	    PreparedStatement s = conn.prepareStatement(sqlCommand);
	    ResultSet results = s.executeQuery();
	    while (results.next()) {
		Articulo articulo = createArticulo(results, conn);
		articles.add(articulo);
	    }
	} finally {
	    closeConnection(conn);
	}
	return articles;
    }

    public List<Articulo> getAllArticulo(String order, String field,
	    String valueFrom, String valueTo) throws Exception {

	ArrayList<Articulo> articles = new ArrayList<Articulo>();
	Connection conn = null;
	String sqlCommand = "select " + FIELDS + " from articulo where "
		+ field + " >= ? and " + field + " <= ?";
	if (order != null) {
	    sqlCommand = sqlCommand + order;
	}
	try {
	    PreparedStatement s;
	    conn = createConnection();
	    s = conn.prepareStatement(sqlCommand);
	    s.setString(1, valueFrom);
	    s.setString(2, valueTo);

	    ResultSet results = s.executeQuery();
	    while (results.next()) {
		Articulo articulo = createArticulo(results, conn);
		articles.add(articulo);
	    }
	} finally {
	    closeConnection(conn);
	}
	return articles;
    }

    public List<Articulo> getAllArticulo(String order, String field,
	    long valueFrom, long valueTo) throws Exception {

	ArrayList<Articulo> articles = new ArrayList<Articulo>();
	Connection conn = null;
	String sqlCommand = "select " + FIELDS + " from articulo where "
		+ field + " >= ? and " + field + " <= ?";
	if (order != null) {
	    sqlCommand = sqlCommand + order;
	}
	try {
	    PreparedStatement s;
	    conn = createConnection();
	    s = conn.prepareStatement(sqlCommand);
	    s.setLong(1, valueFrom);
	    s.setLong(2, valueTo);

	    ResultSet results = s.executeQuery();
	    while (results.next()) {
		Articulo articulo = createArticulo(results, conn);
		articles.add(articulo);
	    }
	} finally {
	    closeConnection(conn);
	}
	return articles;
    }

    public void create(Articulo article) throws Exception {
	Connection conn = null;
	try {
	    conn = createConnection();
	    PreparedStatement stmt = conn.prepareStatement(INSERT_SQL);
	    stmt.setString(1, article.getCodBarra());
	    stmt.setString(2, article.getDescription());
	    stmt.setDouble(3, article.getPrice());
	    stmt.setString(4, article.getMarca());
	    stmt.setString(5, article.getCodInternoAlfa());
	    stmt.setLong(6, article.getCodInterno());
	    stmt.executeUpdate();
	} finally {
	    closeConnection(conn);
	}
    }

    private Articulo createArticulo(ResultSet results, Connection conn)
	    throws SQLException {
	String codInterno2=results.getString("cod_interno2");
	if(codInterno2==null){
	    codInterno2="";
	}else{
	    codInterno2=codInterno2.trim();
	}
	SimpleArticulo article = new SimpleArticulo(results.getString(
		"codigo_barra").trim(), results.getString("nombre").trim(),
		results.getDouble("precio_sin_iva"), results.getString("marca")
			.trim(), codInterno2,results.getLong("cod_interno"));
	// cargar precio gravado

	CallableStatement cstmt = conn.prepareCall(FUN_GRAVADO);
	cstmt.registerOutParameter(1, java.sql.Types.DECIMAL);
	cstmt.setDouble(2, results.getDouble("precio_sin_iva"));
	cstmt.setInt(3, results.getInt("cod_iva"));
	cstmt.setInt(4, results.getInt("cod_impuesto1"));
	cstmt.setInt(5, results.getInt("cod_impuesto2"));
	cstmt.setInt(6, results.getInt("cod_impuesto3"));
	cstmt.setInt(7, results.getInt("cod_impuesto4"));
	cstmt.setDouble(8, results.getDouble("impuesto_interno"));
	cstmt.executeUpdate();

	article.setPrecioGravado(cstmt.getDouble(1));
	cstmt.close();
	return article;
    }

    private Connection createConnection() throws SQLException {
	// log.debug("Connecting to db [url=" + url + ",username=" + username
	// + ",password=" + password + "]");
	return DriverManager.getConnection(this.url, this.username,
		this.password);
    }

    private void closeConnection(Connection conn) {
	if (conn != null) {
	    try {
		conn.close();
	    } catch (Exception ex) {
		ex.printStackTrace();
	    }
	}
    }
}
