package com.invel.promo.dao;

import java.util.Collection;

public interface SucursalStore {
    public Collection<SimpleSucursal> getAllSucursal() throws Exception;

    public Sucursal getSucursal(int cod_sucursal) throws Exception;

    public void sendScript(Sucursal to) throws Exception;

    /**
     * Metodo q retorna una coleccion de objetos sucursales por codigo de promo.
     * 
     * @param cod_promo
     *            Codigo de promocion
     * @return Collection<SimpleSucursal>
     * @throws Exception
     */
    public Collection<SimpleSucursal> getAllSucursalByPromo(Integer cod_promo)
	    throws Exception;

    /**
     * Meotod q retorna las sucursales q tienen asignadas promociones
     * habilitadas.
     * 
     * @return Collection<SimpleSucursal>
     * @throws Exception
     */
    public Collection<SimpleSucursal> getAllSucIncludeEnabledPromo()
	    throws Exception;

}
