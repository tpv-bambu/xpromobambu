/**
 * 
 */
package com.invel.promo.dao;

import java.sql.*;
import java.util.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.invel.promo.struts.form.ECForm;

/**
 * Obtiene la estructura comercial a partir de las tablas clasif1 y clasif2 de
 * la dbinvel.
 * 
 * @author szeballos
 * 
 */
public class SimpleEstructuraComercialStore implements EstructuraComercialStore {

    private String password;
    private String url;
    private String username;

    private boolean clasifExt=false;
   
    private Map<Integer, EstructuraComercialNivel> niveles = new HashMap<Integer, EstructuraComercialNivel>();
    private static final String SL_ALL_LEVELS = "SELECT * FROM clasif1 order by CLA1_COD_CLASIF1";
    private static final String SL_NODES = "SELECT * FROM clasif2 WHERE CLA2_COD_CLASIF1= ? order by descripcion";
    private static final String SL_NODE_EXT = "SELECT * FROM clasif2 WHERE CLA2_COD_CLASIF1= ? AND COD_CLASIF2_EXT=?";
    private static final String SL_NODE = "SELECT * FROM clasif2 WHERE CLA2_COD_CLASIF1= ? AND CLA2_COD_CLASIF2=?";
    private static final String SL_CONFIG_EXT="SELECT * FROM Config WHERE NOMBRE_VARIABLE='ClasificacionExtendida'";
    
    private static Log log = LogFactory.getLog(SimpleEstructuraComercialStore.class);

    public SimpleEstructuraComercialStore(Properties props) throws SQLException  {

	this(props.getProperty("password"), props.getProperty("url"), props
		.getProperty("username"));

    }
    
    /**
     * @param password
     * @param url
     * @param username
     */
    public SimpleEstructuraComercialStore(String pPassword, String pURL,
	    String pUsername) throws SQLException {
	super();
	this.password = pPassword;
	this.url = pURL;
	this.username = pUsername;
	this.clasifExt=this.usaCodigoClasificacionExtendidaPrivate();

    }
    
    private boolean usaCodigoClasificacionExtendidaPrivate() throws SQLException{
	boolean result=false;
	Connection conn = this.createConnection();
	Statement stmt = conn.createStatement();
	ResultSet rs = stmt.executeQuery(SL_CONFIG_EXT);
	while (rs.next()) {
	    String valor = rs.getString("VALOR_VARIABLE");
	    log.debug("Valor de ClasificacionExtendida "+valor);
	    if(valor!=null && !valor.trim().equals("0")){
		//ok, usamos codigo extendido
		log.debug("Se usa codigo extendido");
		result=true;
	    }else{
		log.debug("No se usa codigo extendido");
	    }
	    
	}
	closeConnection(conn, stmt, rs);
	return result;	
    }
    
    
    public boolean usaClasificacionExtendida(){
	return this.clasifExt;
    }



    /*
     * (non-Javadoc)
     * 
     * @see com.invel.promo.dao.EstructuraComercialStore#getAllNiveles()
     */
    public Collection<EstructuraComercialNivel> getAllNiveles() throws SQLException{
	
	
	// TODO Auto-generated method stub
	Connection conn = this.createConnection();
	Statement stmt = conn.createStatement();
	ResultSet rs = stmt.executeQuery(SL_ALL_LEVELS);
	while (rs.next()) {
	    SimpleEstructuraComercialNivel result = new SimpleEstructuraComercialNivel();
	    result.setStatus(rs.getInt("status"));
	    result.setCodigo(rs.getInt("CLA1_COD_CLASIF1"));
	    result.setDescripcion(rs.getString("DESCRIPCION"));
	    result.setDigitoInicio(rs.getInt("DIGITO_INI"));
	    result.setDigitoFin(rs.getInt("DIGITO_FIN"));
	    this.niveles.put(result.getCodigo(), result);
	}
	closeConnection(conn, stmt, rs);
	return this.niveles.values();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.invel.promo.dao.EstructuraComercialStore#getNivel(int)
     */
    public EstructuraComercialNivel getNivel(int code) throws SQLException{
	// TODO Auto-generated method stub
	this.getAllNiveles();
	return this.niveles.get(code);
    }

    public EstructuraComercialNodo getNodo(int codeN1, String codeN2)
	    throws SQLException {
	Connection conn = this.createConnection();
	SimpleEstructuraComercialNodo nodo = new SimpleEstructuraComercialNodo();
	nodo.setEstructuraComercialNivel(this.niveles.get(codeN1));
	PreparedStatement stmt;
	if(this.clasifExt){
	    stmt = conn.prepareStatement(SL_NODE_EXT);
	}else{
	    stmt = conn.prepareStatement(SL_NODE);
	}
	stmt.setInt(1, codeN1);
	stmt.setString(2, codeN2.trim());
	ResultSet rs = stmt.executeQuery();
	if (rs.next()) {
	    
	    
	    nodo.setCode2(rs.getInt("CLA2_COD_CLASIF2"));
	    if(this.clasifExt){
		nodo.setCode(rs.getString("COD_CLASIF2_EXT"));
	    }else{
		nodo.setCode(String.valueOf(nodo.getCode2()));
	    }
	   // nodo.setCode(rs.getString("COD_CLASIF2_EXT"));
	    log.debug("Un nodo de clasif 2 seria: ");
	    log.debug(nodo);
	    nodo.setDescripcion(rs.getString("DESCRIPCION"));
	    nodo.setStatus(rs.getInt("STATUS"));
	    
	}
	return nodo;

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.invel.promo.dao.EstructuraComercialStore#getNodos(com.invel.promo
     * .dao.EstructuraComercialNivel)
     */
    public ArrayList<EstructuraComercialNodo> getNodos(
	    EstructuraComercialNivel nivel) throws Exception {
	ArrayList<EstructuraComercialNodo> result = new ArrayList<EstructuraComercialNodo>();
	Connection conn = this.createConnection();
	PreparedStatement stmt = conn.prepareStatement(SL_NODES);
	stmt.setInt(1, nivel.getCodigo());
	ResultSet rs = stmt.executeQuery();

	while (rs.next()) {
	    SimpleEstructuraComercialNodo nodo = new SimpleEstructuraComercialNodo();
	    nodo.setEstructuraComercialNivel(nivel);
	    if(this.clasifExt){
		nodo.setCode(rs.getString("COD_CLASIF2_EXT"));
	    }else{
		nodo.setCode(rs.getString("CLA2_COD_CLASIF2"));
	    }
	    nodo.setCode2(rs.getInt("CLA2_COD_CLASIF2"));
	    nodo.setDescripcion(rs.getString("DESCRIPCION"));
	    nodo.setStatus(rs.getInt("STATUS"));
	    result.add(nodo);
	}
	closeConnection(conn, stmt, rs);

	// TODO Auto-generated method stub
	return result;
    }

    private void closeConnection(Connection conn, Statement stmt, ResultSet rs) {
	if (rs != null) {
	    try {
		rs.close();
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}
	if (stmt != null) {
	    try {
		stmt.close();
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}
	if (conn != null) {
	    try {
		conn.close();
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}
    }

    private Connection createConnection() throws SQLException {
	// log.debug("Connecting to db [url=" + url + ",username=" + username
	// + ",password=" + password + "]");
	return DriverManager.getConnection(this.url, this.username,
		this.password);
    }

}
