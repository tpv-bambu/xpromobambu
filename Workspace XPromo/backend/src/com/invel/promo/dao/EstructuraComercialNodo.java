/**
 * 
 */
package com.invel.promo.dao;

/**
 * La estructura comercial esta asociado a departamentos y secciones del
 * comercio, por ejemplo secci�n limpieza dependiente del departamento hogar. La
 * estructura comercial es un �rbol n-ario, este objeto est� asociado a los
 * nodos en s� del �rbol.
 * 
 * Notar que aqu� conozco el c�digo del nodo y el nivel en el que est�, pero (me
 * parece) no tengo forma de armar el �rbol en si con esta informaci�n.
 * 
 * @author szeballos
 * 
 */
public interface EstructuraComercialNodo {

    /**
     * Devuelve el nivel al que pertenece este nodo.
     * 
     */
    EstructuraComercialNivel getEstructuraComercialNivel();

    /**
     * Asociado al campo status de base de datos.
     * 
     * @return int
     */
    int getStatus();

    /**
     * Devuelve la descripci�n de este nodo
     */
    String getDescripcion();

    /**
     * Devuelve el c�digo de este nodo, el cual debe ser �nico para el nivel,
     * pero se puede repetir entre distintos niveles.
     * 
     * @return int
     */
    String getCode();

    /**
     * Asociado al campo CLA2_COD_CLASIF1 de la base, ni idea que ser� ya que
     * est�n todos cereados.
     * 
     * @return int
     */
    int getCode2();

    /**
     * La descripci�n del nivel al que pertenece este nodo.
     * 
     * @return String
     */
    String getDescripcionNivel();
}
