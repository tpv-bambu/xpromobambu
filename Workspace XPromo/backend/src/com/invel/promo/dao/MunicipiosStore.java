package com.invel.promo.dao;

import java.util.List;

public interface MunicipiosStore {

    public List<Municipios> getAllMunicipios() throws Exception;

}
