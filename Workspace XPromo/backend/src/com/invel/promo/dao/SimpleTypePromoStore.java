/*
 * SimpleTypePromoStore.java
 *
 * Created on 17 de septiembre de 2005, 15:12
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.invel.promo.dao;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.invel.common.PromoCommons;
import invel.framework.xform.FormParser;
import invel.framework.xform.SimpleFormReporter;
import invel.framework.xform.xelements.XForm;

public class SimpleTypePromoStore implements TypePromoStore, Serializable {

    private static final long serialVersionUID = 1L;

    // Select en tabla typepromos, para obtener los id y descripcion de los
    // tipos
    // de promos.
    private static final String SELECT = "select TYPE_PROMO,DESCRIPTION,DOCUMENTATION,DESCRIPTION_INVEL  from type_promo where ES_IMPUESTO=? ORDER BY description";

    // Select en tabla typepromos, para obtener los id y descripcion de los
    // tipos
    // de promos segun .
    private static final String SELECT_GROUP = "select TYPE_PROMO,DESCRIPTION,DOCUMENTATION,DESCRIPTION_INVEL from type_promo where TYPE_PROMO in ( ";

    // Select para obtener el ID recomendado al crear una nueva promo.
    private static final String SUGGESTED_ID = "select max(cod_promo)+1 from xpromo";

    // Modifica la descripcion del cliente en los tipos de promociones
    protected static final String UPDATE_DESCRIPTION = "update TYPE_PROMO set DESCRIPTION = ? where TYPE_PROMO = ?";

    private String password;

    private String url;

    private String username;

    public SimpleTypePromoStore(Properties properties) {
	this.url = properties.getProperty("url");
	this.username = properties.getProperty("username");
	this.password = properties.getProperty("password");
    }

    public void create() throws Exception {
	// Default implementation of abstract method
    }

    public void update(TypePromo typePromo) throws Exception {
	Connection conn = null;
	try {
	    conn = createConnection();
	    PreparedStatement stmt = conn.prepareStatement(UPDATE_DESCRIPTION);
	    stmt.setString(1, typePromo.getDescription());
	    stmt.setInt(2, typePromo.getId());
	    stmt.executeUpdate();
	} finally {
	    closeConnection(conn);
	}
    }

    /**
     * Para obtener listados de todos los tipos de promociones, notar que si
     * bien se devuelven colecciones de TypePromo no se fetchea el String del
     * objeto XForm.
     * 
     * @return Collection<TypePromo>
     * @throws Exception
     */
    public Collection<TypePromo> getAllTypePromo(boolean tax) throws Exception {
	ArrayList<TypePromo> alltypepromo = new ArrayList<TypePromo>();
	Connection conn = null;
	try {
	    conn = createConnection();
	    PreparedStatement s = conn.prepareStatement(SELECT);
	    s.setBoolean(1, tax);
	    ResultSet results = s.executeQuery();
	    while (results.next()) {
		TypePromo atypePromo = createTypePromo(results, false);
		alltypepromo.add(atypePromo);
	    }
	} finally {
	    closeConnection(conn);
	}

	return alltypepromo;
    }

    /**
     * Para obtener listados de todos los tipos de promociones q pertenezcan al
     * grupo seleccionado, notar que si bien se devuelven colecciones de
     * TypePromo no se fetchea el String del objeto XForm.
     * 
     * @return Collection<TypePromo>
     * @throws Exception
     */
    public Collection<TypePromo> getTypePromoGroup(ArrayList<String> ids)
	    throws Exception {
	ArrayList<TypePromo> alltypepromo = new ArrayList<TypePromo>();
	Connection conn = null;
	try {
	    conn = createConnection();
	    String select_group_dinamic = SELECT_GROUP;
	    // el for va hasta el ids.length -1 para cerrar terminar fuera del
	    // for la query.
	    for (int i = 0; i < ids.size() - 1; i++) {
		select_group_dinamic += "?, ";
	    }
	    select_group_dinamic += "?) ORDER BY description";
	    PreparedStatement s = conn.prepareStatement(select_group_dinamic);

	    for (int i = 0; i < ids.size(); i++) {
		// en la posicion i+1 seteo el valor [i] del arreglo
		s.setInt(i + 1, Integer.parseInt(ids.get(i)));
	    }

	    ResultSet results = s.executeQuery();
	    while (results.next()) {
		TypePromo atypePromo = createTypePromo(results, false);
		alltypepromo.add(atypePromo);
	    }
	} finally {
	    closeConnection(conn);
	}

	return alltypepromo;
    }

    public TypePromo getTypePromo(int id) throws Exception {
	TypePromo atypePromo = null;
	Connection conn = null;
	try {
	    conn = createConnection();
	    PreparedStatement s = conn
		    .prepareStatement("select * from type_promo where type_promo=?");
	    s.setInt(1, id);
	    ResultSet results = s.executeQuery();
	    if (results.next()) {
		atypePromo = createTypePromo(results, true);
	    } else {
		throw new Exception("Type Promocion with id " + id
			+ " not found.");
	    }
	} finally {
	    closeConnection(conn);
	}
	return atypePromo;
    }

    private void closeConnection(Connection conn) {
	if (conn != null) {
	    try {
		conn.close();
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}
    }

    private Connection createConnection() throws Exception {
	// log.debug("Connecting to db [url=" + url + ",username=" + username
	// + ",password=" + password + "]");
	return DriverManager.getConnection(this.url, this.username,
		this.password);
    }

    /**
     * Para crear instancias de TypePromo. En caso de crearse la instancia del
     * XForm, se obtiene de la base un n�mero de ID sin usar y se lo carga en el
     * XForm (como recomendaci�n que el usuario podr� cambiar).
     * 
     * @param results
     *            El result set para acceder a la base.
     * @param xform
     *            carga en el TypePromo el String del XForm si true, cc no.
     * @return TypePromo
     * @throws Exception
     */
    private TypePromo createTypePromo(ResultSet results, boolean xform)
	    throws Exception {

	TypePromo aTypePromo = null;
	int suggestedID = suggestedID();

	if (xform) {
	    int type = results.getInt("type_promo");
	    String pathFile = findXForm(type);
	    FormParser parser = new FormParser();
	    parser.setFormReporter(new SimpleFormReporter());
	    // parser.

	    XForm tempForm = parser.parseFile(pathFile,
		    new File(PromoCommons.getFormspath()).toURI().toString());

	    aTypePromo = new SimpleTypePromo(type,
		    results.getString("description"),
		    results.getString("documentation"), tempForm);
	    aTypePromo.setSuggestedCode(suggestedID);
	} else {
	    aTypePromo = new SimpleTypePromo(results.getInt("type_promo"),
		    results.getString("description"),
		    results.getString("documentation"), null);

	}
	return aTypePromo;
    }

    /**
     * Obtiene de la base el ID de promo sugerido.
     * 
     * @return int
     * @throws Exception
     */
    private int suggestedID() throws Exception {
	Connection conn = this.createConnection();
	ResultSet set = conn.createStatement().executeQuery(SUGGESTED_ID);
	set.next();
	return set.getInt(1);
    }

    /**
     * Obtiene el path completo al archivo XForm asociado a <code> type</code>
     * 
     * @param type
     *            ID de promo a cargar
     * @return Representaci�n en String del XForm.
     * @throws IOException
     */
    private static String findXForm(int type) throws IOException {
	File dir = new File(com.invel.common.PromoCommons.getFormspath());
	String result = null, pattern = String.format("%03d_", type);
	File[] files = dir.listFiles();
	for (File file : files) {
	    if (file.getName().startsWith(pattern)) {
		result = file.getAbsolutePath();
		break;// Encontre el archivo, termino el for.
	    }
	}
	if (result == null) {
	    throw new IOException("No se encontro el archivo XForm de promoID:"
		    + type);
	}
	return result;
    }
}
