/*
 * SimplePromoStore.java
 *
 * Created on 19 de septiembre de 2005, 12:22
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.invel.promo.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.invel.common.PromoCommons;

public class SimplePromoStore implements PromoStore {
    // TODO ESTO EST� SOLO PARA SQL SERVER

    protected static final String FIELDS_INSERT = "cod_promo,type_promo, descript, xmld, habilitado, cod_externo, desde, hasta";

    protected static final String INSERT_SQL = "insert into XPROMO ( "
	    + FIELDS_INSERT + " ) values ( ?, ?, ?, ?, ?, ?, ?, ?)";

    // protected static final String SELEC_ALL = "select * from XPROMO";
    protected static final String SELEC_ALL = "select xp.cod_promo, xp.type_promo, xp.descript, xp.xmld, xp.habilitado, xp.cod_externo, xp.cod_enlace, tp.description, xp.desde, xp.hasta from XPROMO xp inner join TYPE_PROMO tp on xp.type_promo = tp.type_promo where tp.es_impuesto= ? ";

    protected static final String SELEC_SQL = "select xp.cod_promo, xp.type_promo, xp.descript, xp.xmld, xp.habilitado, xp.cod_externo, xp.cod_enlace, tp.description, xp.desde, xp.hasta from XPROMO xp inner join TYPE_PROMO tp on xp.type_promo = tp.type_promo where cod_promo = ?";

    protected static final String SELECT_NO_XML = "select xp.cod_promo, xp.type_promo, xp.descript, xp.habilitado, xp.cod_externo, xp.cod_enlace, tp.description, xp.desde, xp.hasta from XPROMO xp inner join TYPE_PROMO tp on xp.type_promo = tp.type_promo where cod_promo=?";

    // selecciona todas las promociones habilitadas
    protected static final String SELECT_ALL_PROMO_ENABLED = "select xp.cod_promo, xp.type_promo, xp.descript, xp.xmld, xp.habilitado, xp.cod_externo, xp.cod_enlace, tp.description, xp.desde, xp.hasta from xpromo xp inner join TYPE_PROMO tp on xp.type_promo = tp.type_promo where habilitado = 1 and tp.es_impuesto=? order by xp.cod_promo";

    protected static final String SELEC_TYPE_SQL = "select xp.cod_promo, xp.type_promo, xp.descript, xp.xmld, xp.habilitado, xp.cod_externo, xp.cod_enlace, tp.description, xp.desde, xp.hasta from XPROMO xp inner join TYPE_PROMO tp on xp.type_promo = tp.type_promo where xp.type_promo = ? order by xp.cod_promo";

    protected static final String UPDATE_ENABLED = "update XPROMO set habilitado = ? where cod_promo = ?";

    protected static final String DELETE_SQL = "delete from XPROMO where cod_promo = ?";

    /** Selecciona solo un listado de todas las promociones, no los xml */
    protected static final String SELECT_LIST = "select XP.COD_PROMO, XP.TYPE_PROMO, XP.DESCRIPT, XP.HABILITADO, XP.COD_EXTERNO, TP.DESCRIPTION, XP.DESDE, XP.HASTA from XPROMO XP inner join TYPE_PROMO TP on XP.TYPE_PROMO = TP.TYPE_PROMO where TP.ES_IMPUESTO= ? ";

    protected static final String SELECT_LIST_TYPE = "select COD_PROMO, TYPE_PROMO, DESCRIPT, HABILITADO, COD_EXTERNO from XPROMO where TYPE_PROMO=?";

    protected static final String SELECT_LIST_CODE = "select COD_PROMO, TYPE_PROMO, DESCRIPT, HABILITADO, COD_EXTERNO from XPROMO where COD_PROMO=?";

    /** Parametros de la conexion */
    private String url;

    private String username;

    private String password;

    static Log log = LogFactory.getLog(SimplePromoStore.class);

    public SimplePromoStore(Properties properties) {
	this.url = properties.getProperty("url");
	this.username = properties.getProperty("username");
	this.password = properties.getProperty("password");
    }

    public Promo getPromo(int id, boolean xml) throws PromoNotFoundException,
	    Exception {
	Promo apromo = null;
	Connection conn = null;
	try {
	    conn = createConnection();
	    PreparedStatement s = null;
	    if (xml) {
		s = conn.prepareStatement(SELEC_SQL);
	    } else {
		s = conn.prepareStatement(SELECT_NO_XML);
	    }
	    s.setInt(1, id);
	    ResultSet results = s.executeQuery();
	    if (results.next()) {
		apromo = createPromocion(results, xml);
	    } else {
		throw new PromoNotFoundException("Promocion with cod_promo "
			+ id + " not found.");
	    }
	    results.close();
	    s.close();
	} finally {
	    closeConnection(conn);
	}

	return apromo;
    }

    public void delete(int id) throws Exception {
	Connection conn = null;
	try {
	    conn = createConnection();
	    PreparedStatement s = conn.prepareStatement(DELETE_SQL);
	    s.setInt(1, id);
	    s.executeUpdate();
	    s.close();
	} finally {
	    closeConnection(conn);
	}
    }

    /**
     * Implementaci�n m�todo interfaz.
     */
    public Collection<Promo> getAllPromo(boolean xml, boolean tax) throws Exception {
	ArrayList<Promo> promociones = new ArrayList<Promo>();
	Connection conn = null;
	try {
	    conn = createConnection();
	    PreparedStatement s = null;
	    if (xml) {
		s = conn.prepareStatement(SELEC_ALL + " " + getOrderPromo(0));
	    } else {
		s = conn.prepareStatement(SELECT_LIST + " " + getOrderPromo(0));
	    }
	    s.setBoolean(1, tax);
	    ResultSet results = s.executeQuery();
	    while (results.next()) {
		promociones.add(createPromocion(results, xml));
	    }
	    results.close();
	    s.close();
	} finally {
	    closeConnection(conn);
	}
	return promociones;
    }

    public Collection<Promo> getAllTypePromo(int type) throws Exception {
	ArrayList<Promo> promociones = new ArrayList<Promo>();
	Connection conn = null;
	try {
	    conn = createConnection();
	    PreparedStatement s = conn.prepareStatement(SELEC_TYPE_SQL);
	    s.setInt(1, type);
	    ResultSet results = s.executeQuery();
	    while (results.next()) {

		promociones.add(createPromocion(results, true));
	    }
	    results.close();
	    s.close();
	} finally {
	    closeConnection(conn);
	}
	return promociones;
    }

    public void create(Promo promo) throws Exception {
	Connection conn = null;
	try {

	    int i = 1;
	    conn = createConnection();
	    // Statement identity = conn.createStatement();
	    // identity.executeUpdate("SET IDENTITY_INSERT xpromo ON");
	    PreparedStatement stmt = conn.prepareStatement(INSERT_SQL);
	    stmt.setInt(i++, promo.getInternalId());
	    stmt.setInt(i++, promo.getIntTypePromo());
	    stmt.setString(i++, promo.getDescription());
	    stmt.setString(i++, promo.getStringXML());
	    if (promo.isEnabled()) {
		stmt.setInt(i++, PromoCommons.PROMO_ENABLED);
	    } else {
		stmt.setInt(i++, PromoCommons.PROMO_DISABLED);
	    }
	    stmt.setString(i++, promo.getExternalId());
	    stmt.setString(i++, promo.getDesde());
	    stmt.setString(i++, promo.getHasta());
	    stmt.executeUpdate();

	    // identity.executeUpdate("SET IDENTITY_INSERT xpromo OFF");
	    // identity.close();
	    stmt.close();
	} finally {
	    closeConnection(conn);
	}
    }

    public void enable(Promo promo) throws Exception {
	Connection conn = null;
	try {
	    int i = 1;
	    conn = createConnection();
	    PreparedStatement stmt = conn.prepareStatement(UPDATE_ENABLED);
	    stmt.setInt(i++, (PromoCommons.PROMO_ENABLED));
	    stmt.setInt(i++, promo.getInternalId());
	    stmt.executeUpdate();
	    stmt.close();
	} finally {
	    closeConnection(conn);
	}
    }

    public void disable(Promo promo) throws Exception {
	Connection conn = null;
	try {
	    int i = 1;
	    conn = createConnection();
	    PreparedStatement stmt = conn.prepareStatement(UPDATE_ENABLED);
	    stmt.setInt(i++, PromoCommons.PROMO_DISABLED);
	    stmt.setInt(i++, promo.getInternalId());
	    stmt.executeUpdate();
	    stmt.close();
	} finally {
	    closeConnection(conn);
	}
    }

    public void update(Promo promo) throws Exception {
	Connection conn = null;
	try {
	    conn = createConnection();
	    Statement stmt = conn.createStatement();
	    ResultSet rs = stmt
		    .executeQuery("SELECT cod_promo FROM xpromo where cod_promo = "
			    + promo.getInternalId());
	    if (promo.getInternalId() != promo.getOldInternalId() && rs.next()) {
		// se intent� cambiar el c�digo de promo por una ya existente.
		stmt.close();
		throw new Exception("Error. El c�digo \""
			+ promo.getInternalId() + "\" est� presente en la base");
	    }
	    stmt.executeUpdate("DELETE from xpromo where cod_promo="
		    + promo.getOldInternalId());
	    stmt.close();
	    this.create(promo);
	} finally {
	    closeConnection(conn);
	}
    }

    /**
     * 
     * @param results
     * @param xml
     *            si true no carga el xml en el objeto promo.
     * @param pDescripcion
     *            si true el result trae el campo descripcion.
     * @return Promo
     * @throws Exception
     */
    private Promo createPromocion(ResultSet results, boolean xml)
	    throws Exception {
	String sXml = null;
	int resultenabled = 0;
	if (xml) {
	    sXml = results.getString("xmld");
	}
	SimplePromo promo;
	resultenabled = results.getInt("habilitado");

	promo = new SimplePromo(results.getInt("cod_promo"),
		results.getInt("type_promo"), results.getString("descript"),
		sXml, results.getInt("habilitado"),
		results.getString("description"), results.getString("desde"),
		results.getString("hasta"));

	promo.setExternalId(results.getString("cod_externo"));
	// (+) Deshabilita una promoci�n de acuerdo a las condiciones
	if (resultenabled == PromoCommons.PROMO_ENABLED
		&& promo.isEnabled() == false) {
	    disable(promo);
	}
	return promo;
    }

    private Connection createConnection() throws Exception {
	return DriverManager.getConnection(this.url, this.username,
		this.password);
    }

    private void closeConnection(Connection conn) {
	if (conn != null) {
	    try {
		conn.close();
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}
    }

    /**
     * Implementaci�n m�todo interfaz
     */
    public Collection<Promo> getAllListTypePromo(int type) throws Exception {

	ArrayList<Promo> result = new ArrayList<Promo>();
	Connection conn = null;
	try {
	    conn = createConnection();
	    PreparedStatement s = conn.prepareStatement(SELECT_LIST_TYPE);
	    s.setInt(0, type);
	    ResultSet results = s.executeQuery();
	    while (results.next()) {
		result.add(createPromocion(results, true));
	    }
	    results.close();
	    s.close();
	} finally {
	    closeConnection(conn);
	}
	return result;
    }

    public Collection<Promo> getAllPromo(boolean xml, String[] codigos)
	    throws PromoNotFoundException, Exception {
	ArrayList<Promo> result = new ArrayList<Promo>();
	Connection conn = null;
	try {
	    conn = createConnection();

	    PreparedStatement s = null;
	    if (xml) {
		s = conn.prepareStatement(SELEC_SQL);
	    } else {
		s = conn.prepareStatement(SELECT_LIST_CODE);
	    }
	    for (String cod : codigos) {
		s.setString(1, cod);
		ResultSet results = s.executeQuery();
		if (results.next()) {
		    result.add(createPromocion(results, xml));
		} else {
		    throw new PromoNotFoundException(
			    "Promocion with cod_promo " + cod + " not found.");
		}
		results.close();
	    }

	    s.close();
	} finally {
	    closeConnection(conn);
	}

	return result;
    }

    public Collection<PromoSelected> getAllPromoEnabled(boolean tax) throws Exception {

	ArrayList<PromoSelected> promociones = new ArrayList<PromoSelected>();
	Connection conn = null;

	try {
	    conn = createConnection();
	    PreparedStatement s = conn
		    .prepareStatement(SELECT_ALL_PROMO_ENABLED);
	    s.setBoolean(1, tax);
	    ResultSet results = s.executeQuery();
	    Promo promo;
	    PromoSelected promoSelected;
	    while (results.next()) {
		promo = createPromocion(results, false);
		promoSelected = new PromoSelected(promo);
		promociones.add(promoSelected);
	    }
	} finally {
	    closeConnection(conn);
	}
	return promociones;
    }

    public Collection<Promo> getAllPromo(boolean xml, boolean tax, int orderType)
	    throws Exception {

	ArrayList<Promo> promociones = new ArrayList<Promo>();
	Connection conn = null;
	try {
	    conn = createConnection();
	    PreparedStatement s = null;
	    if (xml) {
		s = conn.prepareStatement(SELEC_ALL + " "
			+ getOrderPromo(orderType));
	    } else {
		s = conn.prepareStatement(SELECT_LIST + " "
			+ getOrderPromo(orderType));
	    }
	    s.setBoolean(1, tax);
	    ResultSet results = s.executeQuery();
	    while (results.next()) {
		promociones.add(createPromocion(results, xml));
	    }
	    results.close();
	    s.close();
	} finally {
	    closeConnection(conn);
	}
	return promociones;

    }

    public Collection<Promo> getAllTypePromo(int type, int orderType)
	    throws Exception {

	ArrayList<Promo> promociones = new ArrayList<Promo>();
	Connection conn = null;
	try {
	    conn = createConnection();
	    PreparedStatement s = conn.prepareStatement(SELEC_TYPE_SQL + " "
		    + getOrderPromo(orderType));
	    s.setInt(1, type);
	    ResultSet results = s.executeQuery();
	    while (results.next()) {

		promociones.add(createPromocion(results, true));
	    }
	    results.close();
	    s.close();
	} finally {
	    closeConnection(conn);
	}
	return promociones;

    }

    /**
     * Metodo q retorna la sentencia sql order by seg�n el criterio seleccionado
     * por el usuario.
     * 
     * @param orderType
     *            Identificador del tipo de campo a ordenar
     * @return String
     */
    private String getOrderPromo(int orderType) {

	String sqlOrder = "order by ";

	switch (orderType) {
	case 0: {
	    sqlOrder = sqlOrder + "xp.cod_promo";
	    break;
	}
	case 1: {
	    sqlOrder = sqlOrder + "tp.description";
	    break;
	}
	case 2: {
	    sqlOrder = sqlOrder + "xp.desde";
	    break;
	}
	case 3: {
	    sqlOrder = sqlOrder + "xp.hasta";
	    break;
	}
	case 4: {
	    sqlOrder = sqlOrder + "xp.descript";
	    break;
	}
	default: {
	    sqlOrder = sqlOrder + "xp.cod_promo";
	    break;
	}
	}

	return sqlOrder;

    }

}
