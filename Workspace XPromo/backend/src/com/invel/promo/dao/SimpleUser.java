/*
 * SimpleUser.java
 *
 * Created on 7 de octubre de 2005, 10:43
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.invel.promo.dao;

public class SimpleUser implements User {

    private SimpleUserStore dataStore;

    private String email;

    private String firstName;

    private String lastName;

    private String password;

    private String username;
    
    private boolean impuesto;

    /**
     * Creates a new instance of SimpleUser
     * 
     * @param pDataStore
     * @param pUserName
     * @param pPassword
     */
    SimpleUser(SimpleUserStore pDataStore, String pUserName, String pPassword) {
	this.dataStore = pDataStore;
	this.username = pUserName;
	this.password = pPassword;
    }

    public String getEmail() {
	return this.email;
    }

    public String getFirstname() {
	return this.firstName;
    }

    public String getLastname() {
	return this.lastName;
    }

    public String getPassword() {
	return this.password;
    }

    public String getUsername() {
	return this.username;
    }

    public void save() throws Exception {
	this.dataStore.save(this);
    }

    public void setEmail(String pEmail) {
	this.email = pEmail;
    }

    public void setFirstname(String pFirstName) {
	this.firstName = pFirstName;
    }

    public void setLastname(String pLastName) {
	this.lastName = pLastName;
    }

    public void setPassword(String pPassword) {
	this.password = pPassword;
    }

    public void setUsername(String pUserName) {
	this.username = pUserName;
    }

    /**
     * En validacion por BD siempre tendr� permiso de crear promociones.
     */
    public boolean getCancreatepromo() {
	return true;
    }

    /**
     * En validaci�n por BD siempre tendr� permiso para asociar promociones
     */
    public boolean getCanassociatepromo() {
	return true;
    }

    /**
     * En validaci�n por BD siempre tendr� permiso para compilar promociones
     */
    public boolean getCancompilepromo() {
	return true;
    }

    /**
     * En validaci�n por BD siempre tendr� permiso para borrar promociones.
     */
    public boolean getCandeletepromo() {
	return true;
    }

    /**
     * En validaci�n por BD siempre tendr� permiso para leer promociones.
     */
    public boolean getCanreadpromo() {
	return true;
    }

    /**
     * En validaci�n por BD siempre tendr� permiso para modificar promociones.
     */
    public boolean getCanupdatepromo() {
	return true;
    }

    /**
     * En validaci�n por BD siempre tendr� permiso para exportar promociones.
     */
    public boolean getCanexportpromo() {
	return true;
    }

    /**
     * En validaci�n por BD siempre nunca expirar� la sesi�n.
     */
    public boolean isExpired() {
	return false;
    }

    /**
     * Implementaci�n m�todo interfaz.
     */
    public void logout() {
	// No hay recursos que liberar en validaci�n por BD.

    }

    /*
     * En esta implementacion el usuario tiene siempre permiso de configuracion.
     * @return true
     */
    public boolean getCanConfigureXPromo() {
	// TODO Auto-generated method stub
	return true;
    }

    
    

    public void setImpuesto(boolean impuesto) {
	// TODO Auto-generated method stub
	this.impuesto=impuesto;
    }
    
    @Override
    public boolean isImpuesto() {
	// TODO Auto-generated method stub
	return this.impuesto;
    }

    @Override
    public boolean isAbleToCreateTax() {
	// TODO Auto-generated method stub
	return false;
    }

    @Override
    public boolean isAbleToDeleteTax() {
	// TODO Auto-generated method stub
	return false;
    }

    @Override
    public boolean isAbleToUpdateTax() {
	// TODO Auto-generated method stub
	return false;
    }

    @Override
    public boolean isAbleToReadTax() {
	// TODO Auto-generated method stub
	return false;
    }

    @Override
    public boolean isAbleToCompileTax() {
	// TODO Auto-generated method stub
	return false;
    }

    @Override
    public boolean isAbleToAssociateTax() {
	// TODO Auto-generated method stub
	return false;
    }

    @Override
    public boolean isAbleToExportTax() {
	// TODO Auto-generated method stub
	return false;
    }

    @Override
    public boolean isAbleToConfigureGestorImpuesto() {
	// TODO Auto-generated method stub
	return false;
    }    
}
