/*
 * TypePromoStore.java
 *
 * Created on 17 de septiembre de 2005, 15:02
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.invel.promo.dao;

import java.util.ArrayList;
import java.util.Collection;

public interface TypePromoStore {

    public TypePromo getTypePromo(int id) throws Exception;

    public Collection<TypePromo> getTypePromoGroup(ArrayList<String> ids)
	    throws Exception;

    public Collection<TypePromo> getAllTypePromo(boolean tax) throws Exception;

    public void create() throws Exception;

    public void update(TypePromo typePromo) throws Exception;

}
