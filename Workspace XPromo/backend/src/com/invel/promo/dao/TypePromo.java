/*
 * TypePromo.java
 *
 * Created on 17 de septiembre de 2005, 15:17
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.invel.promo.dao;

import invel.framework.xform.xelements.XForm;

public interface TypePromo extends Comparable<TypePromo> {
    /**
     * ID del TIPO de promoci�n (muchas promociones pueden tener un mismo ID de
     * promoci�n).
     * 
     * @return int
     */
    int getId();

    /**
     * Descripci�n de la promoci�n, se termina guardando una especie de t�tulo
     * 
     * @return String
     */
    String getDescription();

    /**
     * Documentaci�n de la promoci�n, se guarda la descripcion del
     * funcionamiento de cada promoci�n.
     * 
     * @return String
     */
    String getDocumentation();

    /**
     * Objeto XForm asociado a este tipo de promoci�n
     * 
     * @return XForm
     */
    XForm getXForm();

    /**
     * Al crear una nueva promoci�n se obtiene desde base de datos un C�digo de
     * promoci�n sin uso (y as� evitar violaciones de llaves).
     * 
     * El c�digo de promoci�n es �nico para cada promoci�n.
     * 
     * @return int
     */
    int getSuggestedCode();

    /**
     * Descripci�n interna de Invel de la promoci�n.
     * 
     * @return String
     */
    String getDescriptionInvel();

    /**
     * Setter del suggestedCode
     * 
     * @param pSuggestedCode
     */
    void setSuggestedCode(int pSuggestedCode);

    /**
     * Setter de Description
     * 
     * @param pDescription
     */
    void setDescription(String pDescription);
}
