package com.invel.promo.dao;

import java.util.Collection;

public interface DepartamentoStore {

    String ORDER_CODE = " order by cod_depto";

    String ORDER_DESC = " order by desc_depto";

    Collection<Departamento> getAllDepartamentos(String order) throws Exception;

    Departamento getDepartamento(long id) throws Exception;
}
