/*
 * SimplePromo.java
 *
 * Created on 19 de septiembre de 2005, 12:41
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.invel.promo.dao;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.xml.xpath.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.*;

import com.invel.common.PromoCommons;

public class SimplePromo implements Promo, Serializable {

    // variables del request asociados a las fechas de vigencia
    private final static String RANGO_FECHA = "promo_preconditions_rangofecha";

    private final static String FROM_YEAR = "promo_preconditions_rango_fecha_v0_year";

    private final static String FROM_MONTH = "promo_preconditions_rango_fecha_v0_month";

    private final static String FROM_DAY = "promo_preconditions_rango_fecha_v0_day";

    private final static String UNTIL_YEAR = "promo_preconditions_rango_fecha_v1_year";

    private final static String UNTIL_MONTH = "promo_preconditions_rango_fecha_v1_month";

    private final static String UNTIL_DAY = "promo_preconditions_rango_fecha_v1_day";

    private final static Log LOG = LogFactory.getLog(PromoCommons.class);

    private static final long serialVersionUID = 1L;

    private String description;

    private boolean enabled;

    private int id;

    private int oldID;

    private String externalId;

    private int type_promo;

    private String xml;

    private String descTypePromo; // 29/08/2007

    private String desde;

    private String hasta;

    /** Patr�n (regex) utilizado al exportar */
    private static final Pattern QUOTES = Pattern.compile(("'"));

    /** Creates a new instance of SimplePromo */
    public SimplePromo() {
	// Default constructor
    }

    public SimplePromo(int pId, int pTypePromo, String pDescription,
	    String pXml, int pEnabled, String pDescTypePromo, String pDesde,
	    String pHasta) {
	this.id = pId;
	this.type_promo = pTypePromo;
	this.description = pDescription;
	this.descTypePromo = pDescTypePromo; // 29/08/2007
	this.xml = pXml;
	if (pHasta == null) {
	    this.hasta = "";
	    this.desde = "";
	}
	int intHoy = 0;
	int intHasta = 0;
	if (pEnabled == PromoCommons.PROMO_ENABLED) {
	    this.enabled = true;
	} else {
	    this.enabled = false;
	}

	if (pHasta != null && pHasta.length() > 0) {
	    Date date = new Date();
	    DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	    String fecha = dateFormat.format(date);
	    intHoy = Integer.parseInt(fecha);
	    this.hasta = pHasta;
	    this.desde = pDesde;
	    intHasta = Integer.parseInt(this.hasta);
	    if (intHoy > intHasta) {
		this.enabled = false;
	    }
	}
	this.xml = parcheID(this.xml);
    }

    @Override
    public boolean equals(Object promo) {
	if (this == promo) {
	    return true;
	}
	if (promo instanceof Promo) {
	    Promo pr = (Promo) promo;
	    if (this.id == pr.getInternalId()) {
		return true;
	    }
	}
	return false;
    }

    public String getDescription() {
	return this.description;
    }

    public int getInternalId() {
	return this.id;
    }

    // Se crean los get y los set de descTypePromo por req. 59 29/08/2007
    public String getDescTypePromo() {
	return this.descTypePromo;
    }

    public void setDescTypePromo(String pDescTypePromo) {
	this.descTypePromo = pDescTypePromo;
    }

    public void setDesde(String pDesde) {
	this.desde = pDesde;
    }

    public void setHasta(String pHasta) {
	this.hasta = pHasta;
    }

    public int getIntTypePromo() {
	return this.type_promo;
    }

    public void setIntTypePromo(int pTypePromo) {
	this.type_promo = pTypePromo;
    }

    public String getStringXML() {
	return this.xml;
    }

    public void setStringXML(String pXml) {
	this.xml = pXml;
	// solo hace falta llamarlo con promos viejas en BD, las que se editan
	// al menos una vez ya tienen que venir con el ID como atributo.
	// parcheID(xml);
    }

    public boolean isEnabled() {
	return this.enabled;
    }

    public void setDescription(String pDescription) {
	this.description = pDescription;
    }

    public void setEnabled(boolean pEnabled) {
	this.enabled = pEnabled;
    }

    public void setInternalId(int pId) {
	this.id = pId;
    }

    @Override
    public String toString() {
	StringBuilder art = new StringBuilder();
	art.append("[id=").append(this.id);
	art.append("|description=").append(this.description);
	art.append("|xml=").append(this.xml);
	art.append("]");
	return art.toString();
    }

    public void setOldInternalId(int pOldID) {
	this.oldID = pOldID;
    }

    public int getOldInternalId() {
	return this.oldID;
    }

    /**
     * Implementaci�n m�todo interfaz. E
     */
    public String export(EExportFormats format) {
	String result = null;
	switch (format) {
	case SQLSERVER:
	    result = exportSQLServer();
	    break;
	case ORACLE:
	    result = exportOracle();
	    break;
	default:
	    // nada
	}
	return result;
    }

    private String exportSQLServer() {
	StringBuilder result = new StringBuilder(512);
	result.append(" EXEC INSERT_XPROMO ");
	result.append(this.id);
	result.append(',');
	result.append(this.type_promo);
	result.append(", '");
	result.append(replaceQuotes(this.description));
	result.append("', '");
	result.append(replaceQuotes(this.xml));
	result.append("', '");
	if (this.enabled) {
	    result.append(PromoCommons.PROMO_ENABLED);
	} else {
	    result.append(PromoCommons.PROMO_DISABLED);
	}
	result.append("', '");
	result.append(this.desde);
	result.append("', '");
	result.append(this.hasta);
	result.append("', '");
	result.append(this.externalId);
	result.append("'\n\n");
	return result.toString();
    }

    /**
     * M�todo especializado en generar el script para oracle.
     * 
     * @return String
     */
    private String exportOracle() {
	StringWriter result = new StringWriter(1024);
	PrintWriter printWriter = new PrintWriter(result);
	BufferedReader promo = new BufferedReader(new StringReader(this.xml));

	printWriter.println("DECLARE VAR CLOB; ");
	printWriter.println("BEGIN");
	printWriter.println("\tVAR:=");
	try {
	    for (String line = promo.readLine(); line != null; line = promo
		    .readLine()) {
		printWriter.print("\t\t'");
		printWriter.print(replaceQuotes(line));
		printWriter.println("'||CHR(13)||");
	    }
	} catch (IOException ex) {
	    LOG.error(
		    "Error al generar script oracle, el mismo no sera valido..",
		    ex);
	}
	printWriter.println("\t\t'';");// bad programmer!... slap!
	printWriter.print("\tINSERT_XPROMO (");
	printWriter.print(this.id + ", ");
	printWriter.print(this.type_promo + ", '");
	printWriter.print(replaceQuotes(this.description) + "', ");
	printWriter.print("VAR, '");
	if (this.enabled) {
	    printWriter.print(PromoCommons.PROMO_ENABLED + "', '");
	} else {
	    printWriter.print(PromoCommons.PROMO_DISABLED + "', '");
	}
	printWriter.print(this.desde + "', '");
	printWriter.print(this.hasta + "', '");
	printWriter.println(this.externalId + "');");
	printWriter.println("END;");
	printWriter.println("/");
	printWriter.println();
	printWriter.flush();
	return result.toString();

    }

    /**
     * Reemplaza todas las ocurrencias aisladas de comillas simples {'} por
     * doble comillas simples {''}, para que no falle el script sql.
     * 
     * @return String
     */
    private String replaceQuotes(String source) {
	String result = null;
	if (source != null) {
	    Matcher matcher = QUOTES.matcher(source);
	    result = matcher.replaceAll("''");
	}
	return result;
    }

    /**
     * Se encarga de ver si la promo tiene un tag <promo_id> y, si no tiene el
     * atributo 'id', toma el valor del tag y lo carga en el nuevo atributo id.
     * 
     * Este metodo permite la compatibilidad hacia atras.
     */
    private static final String parcheID(String xml) {
	String result = xml;
	if (xml == null) {
	    return null;// nada que hacer si no hay xml con el que trabajar.
	}
	try {
	    XPath xpathAtributo = XPathFactory.newInstance().newXPath();
	    XPath xpathTag = XPathFactory.newInstance().newXPath();

	    Document doc = invel.framework.xform.DomUtils.parse(xml,
		    PromoCommons.dbencoding, null, null);

	    String idAtributo = xpathAtributo.evaluate(
		    PromoCommons.getCodeXPath(), doc);
	    if (idAtributo == null || idAtributo.trim().length() == 0) {
		Element idTag = (Element) xpathTag.evaluate("/promo/promo_id",
			doc, XPathConstants.NODE);
		Attr atributo = doc.createAttribute("id");
		atributo.setTextContent(idTag.getTextContent());
		doc.getDocumentElement().setAttribute("id",
			idTag.getTextContent());
		result = invel.framework.xform.DomUtils.domToString(doc, null,
			PromoCommons.dbencoding, false);
	    }

	} catch (Exception ex) {
	    LOG.error("Error al parsear XML (parcheID)", ex);
	}
	return result;
    }

    public void setXMLid(String idnuevo) {
	try {
	    XPath xpathAtributo = XPathFactory.newInstance().newXPath();
	    Document doc = invel.framework.xform.DomUtils.parse(this.xml,
		    PromoCommons.dbencoding, null, null);
	    Attr idNode = (Attr) xpathAtributo.evaluate(
		    PromoCommons.getCodeXPath(), doc, XPathConstants.NODE);
	    idNode.setNodeValue(idnuevo);
	    this.xml = invel.framework.xform.DomUtils.domToString(doc, null,
		    PromoCommons.dbencoding, false);
	}

	catch (Exception ex) {
	    LOG.error("Error al parsear XML (parcheID)", ex);
	}
    }

    public String getExternalId() {
	return this.externalId;
    }

    public void setExternalId(String pExternalId) {
	this.externalId = pExternalId;
    }

    public String getDesde() {
	return this.desde;
    }

    public String getHasta() {
	return this.hasta;
    }

    /**
     * Implementaci�n m�todo interfaz.
     */
    public void setVigencia(HttpServletRequest request) {
	String rangofecha = request.getParameter(RANGO_FECHA);

	if ("true".equals(rangofecha)) {
	    String fromyear = request.getParameter(FROM_YEAR);
	    String frommonth = request.getParameter(FROM_MONTH);
	    String fromday = request.getParameter(FROM_DAY);

	    String upyear = request.getParameter(UNTIL_YEAR);
	    String upmonth = request.getParameter(UNTIL_MONTH);

	    String upday = request.getParameter(UNTIL_DAY);
	    this.desde = fromyear + frommonth + fromday;
	    this.hasta = upyear + upmonth + upday;
	} else {
	    this.desde = null;
	    this.hasta = null;
	}
    }

}
