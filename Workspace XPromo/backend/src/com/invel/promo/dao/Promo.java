/*
 * Promo.java
 *
 * Created on 19 de septiembre de 2005, 12:17
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.invel.promo.dao;

import javax.servlet.http.HttpServletRequest;

public interface Promo {

    /**
     * Asociado al c�digo interno de la promoci�n. El c�digo interno es un valor
     * muy asociado al motor de promociones.
     * 
     * @return int C�digo de la promoci�n
     */
    int getInternalId();

    /**
     * Asociado a la descripcion del tipo de promocion. Es de caracter
     * informativo.
     * 
     * @return String Descripcion del tipo de promocion
     */
    String getDescTypePromo();

    /**
     * Asociado al c�digo externo de la promoci�n. El motor se encarga de
     * publicar este c�digo externo, pero no afecta la l�gica del motor.
     * 
     * @return String
     */
    String getExternalId();

    /**
     * Si est� o no habilitada la promo
     * 
     * @return boolean
     */
    boolean isEnabled();

    /**
     * Devuelve la descripci�n de la promoci�n (el nombre con el que se la
     * guarda), i.e. "Promo BANCO F�CIL Primera Compra".
     * 
     * @return String
     */
    String getDescription();

    /**
     * Devuelve el entero asociado al tipo de promoci�n de la cual esta promo es
     * instancia. Por ejemplo, los bloques son tipo '0'.
     * 
     * @return int tipo de la promoci�n
     */
    int getIntTypePromo();

    /**
     * Devuelve el XML asociado a la promoci�n, en forma de String.
     * 
     * @return String
     */
    String getStringXML();

    /**
     * Para las ediciones, aqu� se guarda el c�digo original de la promo. En
     * caso de editar una promo aqu� se guarda el valor leido de base, y getID
     * devuelve el valor leido del request. En futuras versiones este metodo
     * puede no hacer falta, pero por ahora lo necesito porque editar una promo
     * implica borrar y crear un campo en base.
     * 
     * @return int
     */
    int getOldInternalId();

    /**
     * Devuelve la promoci�n exportada, en alg�n formato definido por
     * <code>format</code>. Al momento de documentar esta funci�n, la
     * exportaci�n toma la forma de una llamada a un procedimiento en la base de
     * datos.
     * 
     * @return String
     */
    String export(EExportFormats format);

    /**
     * Setea un nuevo valor de 'id' en el XML asociado a la promoci�n. Solo
     * usado al crear una nueva promoci�n a partir de una existente (copiar
     * promo).
     * 
     * No se recomienda usar mucho este metodo dado que cambia la estructura
     * interna de la promoci�n.
     * 
     * @param idnuevo
     */
    void setXMLid(String idnuevo);

    /**
     * Devuelve la fecha que se inici� la promoci�n.
     * 
     * @return String
     */
    String getDesde();

    /**
     * Devuelve la fecha de vencimiento de la promoci�n.
     * 
     * @return String
     */
    String getHasta();

    /**
     * Setea la fecha de inicio y vencimiento de la promo.
     */
    void setVigencia(HttpServletRequest request);

    /*
     * String getStrvigencia();
     */

    /**
     * Los formatos en los que se debe exportar la promocion.
     */
    static enum EExportFormats {
	SQLSERVER, ORACLE;

    }

}
