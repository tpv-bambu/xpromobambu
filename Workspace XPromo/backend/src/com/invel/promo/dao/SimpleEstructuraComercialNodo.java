/**
 * 
 */
package com.invel.promo.dao;

/**
 * @author szeballos
 * 
 */
public class SimpleEstructuraComercialNodo implements EstructuraComercialNodo {

    private String code = "";
    private int code2;
    private String descripcion = "";
    /**
     * nivel asociado a este nodo, notar que muchos nodos comparten el mismo
     * nivel
     */
    private EstructuraComercialNivel nivel = null;

    private int status;

    /**
     * @param code
     *            the code to set
     */
    public void setCode(String pCode) {
	this.code = pCode;
	if (this.code != null) {
	    this.code.trim();
	}
    }

    /**
     * @param code2
     *            the code2 to set
     */
    public void setCode2(int pCode2) {
	this.code2 = pCode2;
    }

    /**
     * @param descripcion
     *            the descripcion to set
     */
    public void setDescripcion(String pDescripcion) {
	this.descripcion = pDescripcion;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(int pStatus) {
	this.status = pStatus;
    }

    public void setEstructuraComercialNivel(EstructuraComercialNivel pNivel) {
	this.nivel = pNivel;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.invel.promo.dao.EstructuraComercialNodo#getCode()
     */
    public String getCode() {
	// TODO Auto-generated method stub
	return this.code;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.invel.promo.dao.EstructuraComercialNodo#getCode2()
     */
    public int getCode2() {
	// TODO Auto-generated method stub
	return this.code2;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.invel.promo.dao.EstructuraComercialNodo#getDescripcion()
     */
    public String getDescripcion() {
	// TODO Auto-generated method stub
	return this.descripcion;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.invel.promo.dao.EstructuraComercialNodo#getEstructuraComercialNivel()
     */
    public EstructuraComercialNivel getEstructuraComercialNivel() {
	// TODO Auto-generated method stub
	return this.nivel;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.invel.promo.dao.EstructuraComercialNodo#getStatus()
     */
    public int getStatus() {
	// TODO Auto-generated method stub
	return this.status;
    }

    @Override
    public boolean equals(Object other) {
	boolean result = false;
	SimpleEstructuraComercialNodo nodo = (SimpleEstructuraComercialNodo) other;
	if (other == this) {
	    result = true;
	}
	if (!(other instanceof SimpleEstructuraComercialNodo)
		|| this.nivel == null) {
	    return false;
	}

	else if (this.code.equals(nodo.code)
		&& this.nivel.getCodigo() == nodo.getEstructuraComercialNivel()
			.getCodigo()) {
	    result = true;
	}
	return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.invel.promo.dao.EstructuraComercialNodo#getDescripcionNivel()
     */
    public String getDescripcionNivel() {
	// TODO Auto-generated method stub
	return this.nivel.getDescripcion();
    }
    
    @Override
    public String toString(){
	StringBuilder b = new StringBuilder();
	b.append("Codigo: ");
	b.append(this.code);
	b.append("\n Codigo 2: ");
	b.append(this.code2);
	b.append("\n Descripcion: ");
	b.append(this.descripcion);
	return b.toString();
    }

}
