package com.invel.promo.dao;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.invel.XValidate.XValidateException;
import com.invel.XValidate.Client.XValidateClientInterface;
import com.invel.promo.struts.action.PrepareFormAction;

public class SimpleUserValidate implements User {

    private String password;

    private String username;

    private boolean cancreatepromo;

    private boolean candeletepromo;

    private boolean canupdatepromo;

    private boolean canreadpromo;

    private boolean cancompilepromo;

    private boolean canassociatepromo;

    private boolean canexportpromo;

    private boolean canConfigureXPromo;

    private boolean ableToCreateTax;

    private boolean ableToDeleteTax;
    
    private boolean ableToUpdateTax;

    private boolean ableToReadTax;

    private boolean ableToCompileTax;

    private boolean ableToAssociateTax;

    private boolean ableToExportTax;

    private boolean ableToConfigureGestorImpuesto;

    private transient XValidateClientInterface client;

    
    /**indica si se logueo por la pantalla de impuestos o promos*/
    private boolean impuesto;

    static transient Log localLog = LogFactory.getLog(PrepareFormAction.class);

    /**
     * Constructor del tipo.
     * 
     * @param pUserName
     * @param pPassword
     * @param pClient
     */
    SimpleUserValidate(String pUserName, String pPassword,
	    XValidateClientInterface pClient) {
	this.username = pUserName;
	this.password = pPassword;
	this.client = pClient;
    }

    /**
     * Get password.
     */
    public String getPassword() {
	return this.password;
    }

    /**
     * Get user name.
     */
    public String getUsername() {
	return this.username;
    }

    /**
     * Set password.
     * 
     * @param pPassword
     */
    public void setPassword(String pPassword) {
	this.password = pPassword;
    }

    /**
     * Set user name.
     * 
     * @param pUserName
     */
    public void setUsername(String pUserName) {
	this.username = pUserName;
    }

    /**
     * Set can create promo.
     * 
     * @param ret
     */
    public void setCancreatepromo(boolean ret) {

	this.cancreatepromo = ret;
    }

    /**
     * Implementaci�n interfaz.
     */
    public boolean getCancreatepromo() {
	return this.cancreatepromo;
    }

    /**
     * Set can delete promo
     * 
     * @param ret
     */
    public void setCandeletepromo(boolean ret) {

	this.candeletepromo = ret;
    }

    /**
     * Implementaci�n interfaz.
     */
    public boolean getCandeletepromo() {
	return this.candeletepromo;
    }

    /**
     * Set can update promo
     * 
     * @param ret
     */
    public void setCanupdatepromo(boolean ret) {
	this.canupdatepromo = ret;
    }

    /**
     * Get can update promo
     */
    public boolean getCanupdatepromo() {
	return this.canupdatepromo;
    }

    /**
     * Set can read promo
     * 
     * @param ret
     */
    public void setCanreadpromo(boolean ret) {
	this.canreadpromo = ret;
    }

    /**
     * Get can read promo
     */
    public boolean getCanreadpromo() {
	return this.canreadpromo;
    }

    /**
     * Set can associate promo
     * 
     * @param ret
     */
    public void setCanassociatepromo(boolean ret) {
	this.canassociatepromo = ret;
    }

    /**
     * Implementaci�n interfaz
     */
    public boolean getCanassociatepromo() {
	return this.canassociatepromo;
    }

    /**
     * Set can compile promo
     * 
     * @param ret
     */
    public void setCancompilepromo(boolean ret) {
	this.cancompilepromo = ret;
    }

    /**
     * Get can compile promo
     */
    public boolean getCancompilepromo() {
	return this.cancompilepromo;
    }

    /**
     * Set can export promo
     * 
     * @param ret
     */
    public void setCanexportpromo(boolean ret) {
	this.canexportpromo = ret;
    }

    /**
     * Get can export promo
     */
    public boolean getCanexportpromo() {
	return this.canexportpromo;
    }

    /**
     * Seter
     * 
     * @param param
     */
    public void setCanConfigureXPromo(boolean param) {
	this.canConfigureXPromo = param;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.invel.promo.dao.User#getCanConfigureXPromo()
     */
    public boolean getCanConfigureXPromo() {
	// TODO Auto-generated method stub
	return this.canConfigureXPromo;
    }

    /**
     * Implementaci�n interfaz.
     */
    public boolean isExpired() {
	boolean result = false;
	try {
	    result = !this.client.isActiveControl();
	} catch (XValidateException e) {
	    localLog.error(
		    "Error al controlar estado de sesi�n, se fuerza deslogueo",
		    e);
	}

	return result;
    }

    /**
     * No implement metod of interface.
     */
    public String getEmail() {
	// TODO Auto-generated method stub
	return null;
    }

    /**
     * Implementaci�n m�todo interfaz.
     * 
     */
    public void logout() {
	try {
	    this.client.logout();
	} catch (XValidateException e) {
	    localLog.error(
		    "Error al controlar estado de sesi�n, se fuerza deslogueo",
		    e);
	}
    }

    /**
     * No implement metod of interface.
     */

    public String getFirstname() {
	// TODO Auto-generated method stub
	return null;
    }

    /**
     * No implement metod of interface.
     */

    public String getLastname() {
	// TODO Auto-generated method stub
	return null;
    }

    /**
     * No implement metod of interface.
     */

    public void save() throws Exception {
	// TODO Auto-generated method stub

    }

    /**
     * No implement metod of interface.
     */

    public void setEmail(String email) {
	// TODO Auto-generated method stub

    }

    /**
     * No implement metod of interface.
     */

    public void setFirstname(String firstname) {
	// TODO Auto-generated method stub

    }

    /**
     * No implement metod of interface.
     */

    public void setLastname(String lastname) {
	// TODO Auto-generated method stub

    }



    public void setImpuesto(boolean impuesto) {
	// TODO Auto-generated method stub
	this.impuesto=impuesto;
    }
    
    @Override
    public boolean isImpuesto() {
	// TODO Auto-generated method stub
	return this.impuesto;
    }

    public void setAbleToCreateTax(boolean ableToCreateTax) {
        this.ableToCreateTax = ableToCreateTax;
    }

    public void setAbleToDeleteTax(boolean ableToDeleteTax) {
        this.ableToDeleteTax = ableToDeleteTax;
    }

    public void setAbleToUpdateTax(boolean ableToUpdateTax) {
        this.ableToUpdateTax = ableToUpdateTax;
    }

    public void setAbleToReadTax(boolean ableToReadTax) {
        this.ableToReadTax = ableToReadTax;
    }

    public void setAbleToCompileTax(boolean ableToCompileTax) {
        this.ableToCompileTax = ableToCompileTax;
    }

    public void setAbleToAssociateTax(boolean ableToAssociateTax) {
        this.ableToAssociateTax = ableToAssociateTax;
    }

    public void setAbleToExportTax(boolean ableToExportTax) {
        this.ableToExportTax = ableToExportTax;
    }

    public void setAbleToConfigureGestorImpuesto(
    	boolean ableToConfigureGestorImpuesto) {
        this.ableToConfigureGestorImpuesto = ableToConfigureGestorImpuesto;
    }

    public boolean isAbleToCreateTax() {
        return ableToCreateTax;
    }

    public boolean isAbleToDeleteTax() {
        return ableToDeleteTax;
    }

    public boolean isAbleToUpdateTax() {
        return ableToUpdateTax;
    }

    public boolean isAbleToReadTax() {
        return ableToReadTax;
    }

    public boolean isAbleToCompileTax() {
        return ableToCompileTax;
    }

    public boolean isAbleToAssociateTax() {
        return ableToAssociateTax;
    }

    public boolean isAbleToExportTax() {
        return ableToExportTax;
    }

    public boolean isAbleToConfigureGestorImpuesto() {
        return ableToConfigureGestorImpuesto;
    }   
    
    
    

}
