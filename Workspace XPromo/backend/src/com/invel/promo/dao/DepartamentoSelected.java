package com.invel.promo.dao;

import java.io.Serializable;

public class DepartamentoSelected implements Departamento, Serializable {

    private static final long serialVersionUID = 1L;

    private long cod_depto;

    private String desc_depto;

    private Articulo descarga;

    private boolean selected;

    public DepartamentoSelected(Departamento depto) {
	this.cod_depto = depto.getCod_depto();
	this.desc_depto = depto.getDesc_depto();
    }

    @Override
    public boolean equals(Object departamento) {
	if (this == departamento) {
	    return true;
	} else if (departamento instanceof Departamento) {
	    Departamento depto = (Departamento) departamento;
	    if (this.cod_depto == depto.getCod_depto()) {
		return true;
	    }
	}
	return false;
    }

    public long getCod_depto() {
	return this.cod_depto;
    }

    public String getDesc_depto() {
	return this.desc_depto;
    }

    public boolean isSelected() {
	return this.selected;
    }

    public void setCod_depto(long pCodDepto) {
	this.cod_depto = pCodDepto;
    }

    public void setDesc_depto(String pDescDepto) {
	this.desc_depto = pDescDepto;
    }

    public void setSelected(boolean select) {
	this.selected = select;
    }

    @Override
    public String toString() {
	StringBuilder art = new StringBuilder();
	art.append("[cod_depto=").append(this.cod_depto);
	art.append("|description=").append(this.desc_depto);
	art.append("]");
	return art.toString();
    }

    public Articulo getDescarga() {
	return this.descarga;
    }

    public void setDescarga(Articulo itemdescarga) {
	this.descarga = itemdescarga;
    }
}
