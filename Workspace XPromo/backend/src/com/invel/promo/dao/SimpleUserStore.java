/*
 * SimpleUserStore.java
 *
 * Created on 7 de octubre de 2005, 11:04
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.invel.promo.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;

import com.invel.promo.dao.User.InvalidUserException;

public class SimpleUserStore implements UserStore {

    private String url;

    private String username;

    private String password;

    public SimpleUserStore(Properties properties) {
	this.url = properties.getProperty("url");
	this.username = properties.getProperty("username");
	this.password = properties.getProperty("password");
    }

    public User getUser(String pUsername, String pPassword) throws Exception {
	User result = null;
	Connection conn = null;
	try {
	    conn = createConnection();
	    PreparedStatement st = conn
		    .prepareStatement("select * from promo_users where username = ?");
	    st.setString(1, pUsername);
	    ResultSet results = st.executeQuery();
	    if (results.next()) {
		result = createUser(results);
		if (!result.getPassword().equals(pPassword)) {
		    throw new InvalidUserException("Password Invalido: "
			    + this.password);
		}
	    } else {
		throw new InvalidUserException("Usuario Invalido: "
			+ this.username);
	    }
	} finally {
	    closeConnection(conn);
	}
	return result;
    }

    public User create(String pUsername, String pPassword) throws Exception {
	Connection conn = null;
	try {
	    conn = createConnection();
	    PreparedStatement st = conn
		    .prepareStatement("insert into promo_users (username, password) values (?, ?)");
	    st.setString(1, pUsername);
	    st.setString(2, pPassword);
	    // int rowCount =
	    st.executeUpdate();
	    return getUser(pUsername, pPassword);
	} finally {
	    closeConnection(conn);
	}
    }

    public void save(User user) throws Exception {
	Connection conn = null;
	try {
	    conn = createConnection();
	    PreparedStatement st = conn
		    .prepareStatement("update promo_users set email = ?, firstname = ?, lastname = ?, password = ? where username = ?");
	    st.setString(1, user.getEmail());
	    st.setString(2, user.getFirstname());
	    st.setString(3, user.getLastname());
	    st.setString(4, user.getPassword());
	    st.setString(5, user.getUsername());
	    // int rowCount =
	    st.executeUpdate();
	} finally {
	    closeConnection(conn);
	}
    }

    private Connection createConnection() throws Exception {
	return DriverManager.getConnection(this.url, this.username,
		this.password);
    }

    private void closeConnection(Connection conn) {
	if (conn != null) {
	    try {
		conn.close();
	    } catch (Exception ex) {
		ex.printStackTrace();
	    }
	}
    }

    private User createUser(ResultSet results) throws Exception {
	User user = new SimpleUser(this, results.getString("username"),
		results.getString("password"));
	user.setEmail(results.getString("email"));
	user.setFirstname(results.getString("firstname"));
	user.setLastname(results.getString("lastname"));
	return user;
    }

}
