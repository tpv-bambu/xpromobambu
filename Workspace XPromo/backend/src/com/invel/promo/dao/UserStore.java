/*
 * UserStore.java
 *
 * Created on 7 de octubre de 2005, 10:54
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.invel.promo.dao;

public interface UserStore {

    /**
     * Obtiene el usuario a partir de su username y password.
     * 
     * @param username
     * @param pPassword
     * @return User asociado al username y password recibidos por par�metro.
     * @throws Exception
     */
    public User getUser(String username, String pPassword) throws Exception;

    /**
     * Crea un nuevo usuario, con su respectivo username y password
     * 
     * @param username
     * @param password
     * @return User el nuevo usuario creado
     * @throws Exception
     */
    public User create(String username, String password) throws Exception;

}
