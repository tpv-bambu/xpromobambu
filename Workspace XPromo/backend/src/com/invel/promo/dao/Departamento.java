package com.invel.promo.dao;

public interface Departamento {

    public long getCod_depto();

    public String getDesc_depto();
}
