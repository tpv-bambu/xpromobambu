/*
 * User.java
 *
 * Created on 7 de octubre de 2005, 10:30
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.invel.promo.dao;

public interface User {
    String getUsername();

    void setUsername(String username);

    String getPassword();

    void setPassword(String password);

    String getFirstname();

    void setFirstname(String firstname);

    String getLastname();

    void setLastname(String lastname);

    String getEmail();

    void setEmail(String email);

    /**
     * Valida si el usuario tiene o no permiso para crear una nueva promoci�n.
     * 
     * @return boolean
     */
    boolean getCancreatepromo();

    /**
     * Valida si el usuario tiene o no permiso para borrar una promoci�n
     * 
     * @return boolean
     */
    boolean getCandeletepromo();

    /**
     * Valida si el usuario tiene o no permiso para modificar una promoci�n.
     * 
     * @return boolean
     */
    boolean getCanupdatepromo();

    /**
     * Valida si el usuario tiene o no permiso para leer una promoci�n.
     * 
     * @return boolean
     */
    boolean getCanreadpromo();

    /**
     * Valida si el usuario tiene o no permiso para compilar una promoci�n.
     * 
     * @return boolean
     */
    boolean getCancompilepromo();

    /**
     * Valida si el usuario tiene o no permiso para asociar promociones a
     * sucursales.
     * 
     * @return boolean
     */
    boolean getCanassociatepromo();

    /**
     * Valida si el usuario tiene o no permiso para exportar promociones.
     * 
     * @return boolean
     */
    boolean getCanexportpromo();

    /**
     * Valida si expir� o no la sesi�n del usuario.
     * 
     * @return boolean
     */
    boolean isExpired();

    /**
     * Valida si el usuario tiene o no permiso para configurar el XPromo
     * 
     * @return boolean;
     */
    boolean getCanConfigureXPromo();

    /**
     * Libera recursos asociados a este usuario.
     * 
     */
    void logout();

    void save() throws Exception;

    /**
     * Exception a lanzar en caso de errores de validaci�n de usuario.
     * 
     * @author szeballos
     * 
     */
    static class InvalidUserException extends Exception {
	public static final long serialVersionUID = 1;

	InvalidUserException() {
	    super();
	    // TODO Auto-generated constructor stub
	}

	public InvalidUserException(String message, Throwable cause) {
	    super(message, cause);
	    // TODO Auto-generated constructor stub
	}

	public InvalidUserException(String message) {
	    super(message);
	    // TODO Auto-generated constructor stub
	}

	public InvalidUserException(Throwable cause) {
	    super(cause);
	    // TODO Auto-generated constructor stub
	}

    }

    /**
     * Nuevos permisos para el motor de impuestos, basicamente duplican los
     * permisos de promociones.
     * 
     * @return
     */

    public boolean isAbleToCreateTax();

    public boolean isAbleToDeleteTax();

    public boolean isAbleToUpdateTax();

    public boolean isAbleToReadTax() ;

    public boolean isAbleToCompileTax();

    public boolean isAbleToAssociateTax();

    public boolean isAbleToExportTax();

    public boolean isAbleToConfigureGestorImpuesto();
    
    //me dice si el usuario se logueo via login de impuesto o promos.
    boolean isImpuesto();
    
    void setImpuesto(boolean impuesto);

}
