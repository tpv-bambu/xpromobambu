////////////////////////////////////////////////////////////////////////
//
// agrupacionType.java
//
// This file was generated by XMLSpy 2007r3sp1 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////

package com.invel.promo.agrupaciones;

public class AgrupacionType extends com.altova.xml.TypeBase {

    public AgrupacionType(org.w3c.dom.Node init) {
	super(init);
	instantiateMembers();
    }

    private void instantiateMembers() {

	this.typepromo = new MemberElement_typepromo(
		this,
		com.invel.promo.agrupaciones.Agrupaciones_TypeInfo.binder
			.getMembers()[com.invel.promo.agrupaciones.Agrupaciones_TypeInfo._altova_mi_altova_agrupacionType_altova_typepromo]);
	this.name = new MemberElement_name(
		this,
		com.invel.promo.agrupaciones.Agrupaciones_TypeInfo.binder
			.getMembers()[com.invel.promo.agrupaciones.Agrupaciones_TypeInfo._altova_mi_altova_agrupacionType_altova_name]);
    }

    // Attributes

    // Elements

    public MemberElement_typepromo typepromo;

    public static class MemberElement_typepromo {
	public static class MemberElement_typepromo_Iterator implements
		java.util.Iterator {
	    private org.w3c.dom.Node nextNode;

	    private MemberElement_typepromo member;

	    public MemberElement_typepromo_Iterator(
		    MemberElement_typepromo pMember) {
		this.member = pMember;
		this.nextNode = pMember.owner.getElementFirst(pMember.info);
	    }

	    public boolean hasNext() {
		while (this.nextNode != null) {
		    if (com.altova.xml.TypeBase.memberEqualsNode(
			    this.member.info, this.nextNode))
			return true;
		    this.nextNode = this.nextNode.getNextSibling();
		}
		return false;
	    }

	    public Object next() {
		TypepromoType nx = new TypepromoType(this.nextNode);
		this.nextNode = this.nextNode.getNextSibling();
		return nx;
	    }

	    public void remove() {
		//
	    }
	}

	protected com.altova.xml.TypeBase owner;

	protected com.altova.typeinfo.MemberInfo info;

	public MemberElement_typepromo(com.altova.xml.TypeBase pOwner,
		com.altova.typeinfo.MemberInfo pInfo) {
	    this.owner = pOwner;
	    this.info = pInfo;
	}

	public TypepromoType at(int index) {
	    return new TypepromoType(this.owner.getElementAt(this.info, index));
	}

	public TypepromoType first() {
	    return new TypepromoType(this.owner.getElementFirst(this.info));
	}

	public TypepromoType last() {
	    return new TypepromoType(this.owner.getElementLast(this.info));
	}

	public TypepromoType append() {
	    return new TypepromoType(this.owner.createElement(this.info));
	}

	public boolean exists() {
	    return count() > 0;
	}

	public int count() {
	    return this.owner.countElement(this.info);
	}

	public void remove() {
	    this.owner.removeElement(this.info);
	}

	public java.util.Iterator iterator() {
	    return new MemberElement_typepromo_Iterator(this);
	}
    }

    public MemberElement_name name;

    public static class MemberElement_name {
	public static class MemberElement_name_Iterator implements
		java.util.Iterator {
	    private org.w3c.dom.Node nextNode;

	    private MemberElement_name member;

	    public MemberElement_name_Iterator(MemberElement_name pMember) {
		this.member = pMember;
		this.nextNode = pMember.owner.getElementFirst(pMember.info);
	    }

	    public boolean hasNext() {
		while (this.nextNode != null) {
		    if (com.altova.xml.TypeBase.memberEqualsNode(
			    this.member.info, this.nextNode))
			return true;
		    this.nextNode = this.nextNode.getNextSibling();
		}
		return false;
	    }

	    public Object next() {
		NameType nx = new NameType(this.nextNode);
		this.nextNode = this.nextNode.getNextSibling();
		return nx;
	    }

	    public void remove() {
		//
	    }
	}

	protected com.altova.xml.TypeBase owner;

	protected com.altova.typeinfo.MemberInfo info;

	public MemberElement_name(com.altova.xml.TypeBase pOwner,
		com.altova.typeinfo.MemberInfo pInfo) {
	    this.owner = pOwner;
	    this.info = pInfo;
	}

	public NameType at(int index) {
	    return new NameType(this.owner.getElementAt(this.info, index));
	}

	public NameType first() {
	    return new NameType(this.owner.getElementFirst(this.info));
	}

	public NameType last() {
	    return new NameType(this.owner.getElementLast(this.info));
	}

	public NameType append() {
	    return new NameType(this.owner.createElement(this.info));
	}

	public boolean exists() {
	    return count() > 0;
	}

	public int count() {
	    return this.owner.countElement(this.info);
	}

	public void remove() {
	    this.owner.removeElement(this.info);
	}

	public java.util.Iterator iterator() {
	    return new MemberElement_name_Iterator(this);
	}
    }
}
