package com.invel.promo.datahandler;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class DataHandlerAdapter implements IFormDataHandler {

    protected final static Element getLastElement(Element node) {
	Element result = null;
	Node current = node.getFirstChild();
	while (current != null) {
	    if (current.getNodeType() == Node.ELEMENT_NODE) {
		result = (Element) current;
	    }
	    current = current.getNextSibling();
	}
	return result;
    }

    protected final static void clearItems(Element node) {
	Node current = node.getFirstChild();
	while (current != null) {
	    node.removeChild(current);
	    current = node.getFirstChild();
	}
    }

    public final static void addNew(Document doc, Element node, String childName) {
	Element lastElement = getLastElement(node);
	Element child = doc.createElement(childName);

	int value;
	if (lastElement == null) {
	    value = 1;
	} else {
	    value = Integer.parseInt(lastElement.getAttribute(ITERATION_MARK));
	    value++;
	}
	child.setAttribute(ITERATION_MARK, String.valueOf(value));
	node.appendChild(child);
    }

    public final static void delLast(Element node) {
	// Default implementation
	Element lastElement = getLastElement(node);
	if (lastElement != null) {
	    node.removeChild(lastElement);
	}
    }

    public Object getValue(Element node) {
	// Null adapter implementation
	return null;
    }

    public void setValue(Element node, Object item) {
	// Null adapter implementation
    }

    public Object exportData(Element node) {
	// Null adapter implementation
	return null;
    }

    public void importData(Document data, Element node, Object items) {
	// Null adapter implementation
    }

}
