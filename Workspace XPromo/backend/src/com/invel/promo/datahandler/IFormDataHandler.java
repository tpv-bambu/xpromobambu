package com.invel.promo.datahandler;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public interface IFormDataHandler {

    String ITERATION_MARK = "iteration";

    // void addNew(Document doc, Element node);
    // void delLast(Element node);

    /**
     * Importa los datos desde un origen externo hacia el xml de la promo node:
     * donde colocar los datos.
     */
    void importData(Document data, Element node, Object items);

    /**
     * Obtiene un objeto asociado a
     * 
     * @param node
     * @return Object
     */
    Object exportData(Element node);

    Object getValue(Element node);

    void setValue(Element node, Object item);
}
