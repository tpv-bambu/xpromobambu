package com.invel.promo.datahandler;

import java.util.ArrayList;
import java.util.Collection;

import org.w3c.dom.*;

import com.invel.promo.dao.Articulo;

public class AmsetPrecio extends DataHandlerAdapter {

    /*
     * <item> <element datatype="article">...</element> <count
     * datatype="num">1</count> <precio_fijo_unitario>1</precio_fijo_unitario>
     * </item>
     */

    private static final String TAG_ITEM = "item";

    private static final String TAG_ELEMENT = "element";

    private static final String TAG_COUNT = "count";

    private static final String TAG_PRICE = "precio_fijo_unitario";

    // static private Log log = LogFactory.getLog(AmsetSimple.class);
    @Override
    public Object exportData(Element node) {
	ArrayList<Articulo> items = new ArrayList<Articulo>();
	NodeList nodes = node.getChildNodes();
	for (int i = 0; i < nodes.getLength(); i++) {
	    Node current = nodes.item(i);
	    if (current.getNodeType() == Node.ELEMENT_NODE) {
		Element element = (Element) current;
		if (TAG_ITEM.equals(element.getNodeName())) {
		    exportItem(items, element);
		}
	    }
	}

	return items;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void importData(Document data, Element node, Object items) {
	Collection<Articulo> newitems = (Collection<Articulo>) items;
	clearItems(node);
	int iteration = 0;
	if (newitems != null) {
	    for (Articulo selected : newitems) {
		Element item = importItem(data, selected);
		iteration++;
		item.setAttribute(ITERATION_MARK, String.valueOf(iteration));
		node.appendChild(item);
	    }
	}
    }

    /**
     * exporta desde el xml hacia nuevos objetos entidad Articulo.
     * 
     * @param items
     * @param node
     */
    private void exportItem(Collection<Articulo> items, Element node) {

	Articulo articulo = null;
	int count = -1;
	double price = 0;

	NodeList nodes = node.getChildNodes();
	for (int i = 0; i < nodes.getLength(); i++) {
	    Node current = nodes.item(i);
	    if (current.getNodeType() == Node.ELEMENT_NODE) {
		Element element = (Element) current;

		if (TAG_ELEMENT.equals(element.getNodeName())) {
		    articulo = SingleElement.exportArticle(element);
		}

		if (TAG_COUNT.equals(element.getNodeName())) {
		    try {
			count = Integer.parseInt(element.getTextContent());
		    } catch (Exception ex) {
			// Ignore
		    }
		}

		if (TAG_PRICE.equals(element.getNodeName())) {
		    try {

			price = Double.parseDouble(element.getTextContent());

		    } catch (Exception ex) {
			// Ignore
		    }
		}

	    }
	}
	if (articulo != null) {
	    articulo.setCantidad(count);
	    articulo.setPrecioGravado(price);
	    items.add(articulo);
	}
    }

    private Element importItem(Document doc, Articulo artic) {
	Element item = doc.createElement(TAG_ITEM);
	Element count = doc.createElement(TAG_COUNT);
	Element precio = doc.createElement(TAG_PRICE);

	Element element = SingleElement.importArticle(doc, artic, TAG_ELEMENT);

	count.setAttribute("datatype", "num");
	if (artic.getCantidad() > 0) {
	    count.setTextContent(String.valueOf(artic.getCantidad()));
	}

	precio.setTextContent(String.valueOf(artic.getPrecioGravado()));

	item.appendChild(element);
	item.appendChild(count);
	item.appendChild(precio);

	return item;
    }

}
