package com.invel.promo.datahandler;

import invel.framework.xform.DomUtils;
import invel.framework.xform.xelements.XForm;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public abstract class FormDataHandler {

    // static private Log log = LogFactory.getLog(FormDataHandler.class);

    private static final IFormDataHandler getDataHandler(XForm xform) {
	IFormDataHandler dataHandler = null;
	try {
	    Class managerClass = Class.forName("com.invel.promo.datahandler."
		    + xform.srcDataManager);
	    dataHandler = (IFormDataHandler) managerClass.newInstance();
	} catch (ClassNotFoundException ex) {
	    // TODO: handle exception
	    ex.printStackTrace();
	} catch (IllegalAccessException ex) {
	    // TODO: handle exception
	    ex.printStackTrace();
	} catch (InstantiationException ex) {
	    // TODO: handle exception
	    ex.printStackTrace();
	}
	return dataHandler;
    }

    private static final Element getDataNode(Document data, XForm xform) {
	Element item = null;
	try {
	    XPath xpath = DomUtils.getXPath();
	    item = (Element) xpath.evaluate(xform.srcXPath, data,
		    XPathConstants.NODE);
	} catch (XPathExpressionException ex) {
	    // TODO: handle exception
	    ex.printStackTrace();
	}
	return item;
    }

    public static final void addNew(Document data, XForm xform) {
	// IFormDataHandler dataHandler = getDataHandler(xform);
	Element node = getDataNode(data, xform);
	// dataHandler.addNew(data, node);
	// TODO cambiar si alguno llega a ser distinto
	DataHandlerAdapter.addNew(data, node, xform.srcDataManager);
    }

    public static final void delLast(Document data, XForm xform) {
	// IFormDataHandler dataHandler = getDataHandler(xform);
	Element node = getDataNode(data, xform);
	// dataHandler.delLast(node);
	// TODO cambiar si alguno llega a ser distinto
	DataHandlerAdapter.delLast(node);
    }

    public static final void importData(Document data, XForm xform, Object items) {
	IFormDataHandler dataHandler = getDataHandler(xform);
	Element node = getDataNode(data, xform);
	dataHandler.importData(data, node, items);
    }

    public static final Object exportData(Document data, XForm xform) {
	IFormDataHandler dataHandler = getDataHandler(xform);
	Element node = getDataNode(data, xform);
	return dataHandler.exportData(node);
    }

    public static final Object getValue(Document data, XForm xform) {
	IFormDataHandler dataHandler = getDataHandler(xform);
	Element node = getDataNode(data, xform);
	return dataHandler.getValue(node);
    }

    public static final void setValue(Document data, XForm xform, Object item) {
	IFormDataHandler dataHandler = getDataHandler(xform);
	Element node = getDataNode(data, xform);
	dataHandler.setValue(node, item);
    }
    /*
     * public static final void debug(Document data, XForm xform) { Element node
     * = getDataNode(data, xform);
     * log.debug("-------------------------------------------");
     * log.debug(DomUtils.domToString(node, null, null, true)); log.debug(" ");
     * }
     */
}
