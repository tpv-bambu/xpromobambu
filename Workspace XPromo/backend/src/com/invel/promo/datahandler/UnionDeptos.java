package com.invel.promo.datahandler;

import java.util.ArrayList;
import java.util.Collection;

import org.w3c.dom.*;

import com.invel.common.PromoSystem;
import com.invel.promo.dao.Articulo;
import com.invel.promo.dao.Departamento;
import com.invel.promo.dao.DepartamentoSelected;

public class UnionDeptos extends DataHandlerAdapter {

    /*
     * <d> <depto datatype="department">1</depto> <descarga
     * datatype="article">...</descarga> </d>
     */

    // static private Log log = LogFactory.getLog(UnionDeptos.class);
    @Override
    public Object exportData(Element node) {
	ArrayList<DepartamentoSelected> items = new ArrayList<DepartamentoSelected>();
	NodeList nodes = node.getChildNodes();
	for (int i = 0; i < nodes.getLength(); i++) {
	    Node current = nodes.item(i);
	    if (current.getNodeType() == Node.ELEMENT_NODE) {
		Element element = (Element) current;
		if ("d".equals(element.getNodeName())) {
		    exportItem(items, element);
		}
	    }
	}

	return items;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void importData(Document data, Element node, Object items) {
	Collection<DepartamentoSelected> newitems = (Collection<DepartamentoSelected>) items;
	clearItems(node);
	int iteration = 0;
	for (DepartamentoSelected selected : newitems) {
	    Element item = importItem(data, selected);
	    iteration++;
	    item.setAttribute(ITERATION_MARK, String.valueOf(iteration));
	    node.appendChild(item);
	}
    }

    private void exportItem(Collection<DepartamentoSelected> items, Element node) {

	long intcode = -1;
	Articulo descarga = null;

	NodeList nodes = node.getChildNodes();
	for (int i = 0; i < nodes.getLength(); i++) {
	    Node current = nodes.item(i);
	    if (current.getNodeType() == Node.ELEMENT_NODE) {
		Element element = (Element) current;
		if ("depto".equals(element.getNodeName())) {
		    try {
			intcode = Long.parseLong(element.getTextContent());
		    } catch (Exception ex) {
			// Ignore
			ex.printStackTrace();
		    }
		}
		if ("descarga".equals(element.getNodeName())) {
		    descarga = SingleElement.exportArticle(element);
		}
	    }
	}

	if (intcode != -1) {
	    try {
		Departamento department = PromoSystem.getDepartamentoStore()
			.getDepartamento(intcode);
		DepartamentoSelected depto = new DepartamentoSelected(
			department);
		if (descarga != null) {
		    depto.setDescarga(descarga);
		}
		depto.setSelected(true);
		items.add(depto);
	    } catch (Exception ex) {
		// TODO Auto-generated catch block
		ex.printStackTrace();
	    }
	}
    }

    private Element importItem(Document doc, DepartamentoSelected selected) {
	Element item = doc.createElement("d");
	Element depto = doc.createElement("depto");
	Element description = doc.createElement("description");
	Articulo itemDescarga = selected.getDescarga();

	depto.setTextContent(String.valueOf(selected.getCod_depto()));
	depto.setAttribute("datatype", "department");
	description.setTextContent(selected.getDesc_depto());

	item.appendChild(depto);
	item.appendChild(description);

	if (itemDescarga != null) {
	    Element descarga = SingleElement.importArticle(doc, itemDescarga,
		    "descarga");
	    item.appendChild(descarga);
	}

	return item;
    }

}
