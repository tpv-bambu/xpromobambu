package com.invel.promo.datahandler;

import java.util.ArrayList;
import java.util.Collection;

import org.w3c.dom.*;

import com.invel.promo.dao.Articulo;

public class AmsetSimple extends DataHandlerAdapter {

    /*
     * <item> <element datatype="article">...</element> <count
     * datatype="num">1</count> </item>
     */

    // static private Log log = LogFactory.getLog(AmsetSimple.class);
    @Override
    public Object exportData(Element node) {
	Collection<Articulo> items = null;
	NodeList nodes = node.getChildNodes();
	ArrayList<Element> elementXML = new ArrayList<Element>();
	for (int i = 0; i < nodes.getLength(); i++) {
	    Node current = nodes.item(i);
	    if (current.getNodeType() == Node.ELEMENT_NODE) {
		Element item = (Element) current;
		if ("item".equals(item.getNodeName())) {
		    for (int j = 0; j < current.getChildNodes().getLength(); j++) {
			Node element = item.getChildNodes().item(j);
			if ("element".equals(element.getNodeName())) {
			    elementXML.add((Element) element);
			}
		    }

		    // exportItem(items, element);
		}
	    }
	}
	items = SingleElement.exportArticle(elementXML);
	return items;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void importData(Document data, Element node, Object items) {
	Collection<Articulo> newitems = (Collection<Articulo>) items;
	clearItems(node);
	int iteration = 0;
	if (newitems != null) {
	    for (Articulo selected : newitems) {
		Element item = importItem(data, selected);
		iteration++;
		item.setAttribute(ITERATION_MARK, String.valueOf(iteration));
		node.appendChild(item);
	    }
	}
    }

    /*
     * private void exportItem(Collection<ArticuloSelected> items, Element node)
     * {
     * 
     * Articulo articulo = null; int count = -1;
     * 
     * NodeList nodes = node.getChildNodes(); for (int i = 0; i <
     * nodes.getLength(); i++) { Node current = nodes.item(i); if
     * (current.getNodeType() == Node.ELEMENT_NODE) { Element element =
     * (Element) current; if ("element".equals(element.getNodeName())) {
     * articulo = SingleElement.exportArticle(element); } if
     * ("count".equals(element.getNodeName())) { try { count =
     * Integer.parseInt(element.getTextContent()); } catch (Exception ex) { //
     * Ignore } } } } if (articulo != null) { ArticuloSelected artsel = new
     * ArticuloSelected(articulo); if (count > 0) { artsel.setCantidad(count); }
     * items.add(artsel); } }
     */

    private Element importItem(Document doc, Articulo artic) {
	Element item = doc.createElement("item");
	Element count = doc.createElement("count");
	Element element = SingleElement.importArticle(doc, artic, "element");

	count.setAttribute("datatype", "num");
	if (artic.getCantidad() > 0) {
	    count.setTextContent(String.valueOf(artic.getCantidad()));
	}

	item.appendChild(element);
	item.appendChild(count);
	return item;
    }

}
