package com.invel.promo.datahandler;

import java.util.*;

import org.w3c.dom.*;

import com.invel.common.PromoSystem;
import com.invel.promo.dao.Articulo;

public class SingleElement extends DataHandlerAdapter {

    /*
     * <? datatype="article"> <cod_int>22</cod_int> <cod_bar>33</cod_bar>
     * <cod_ext>44</cod_bar> <description>articulo</description> </?>
     */

    @Override
    public Object getValue(Element node) {
	return exportArticle(node);
    }

    @Override
    public void setValue(Element node, Object item) {
	// TODO a lo mejor se puede optimizar buscando los subnodos
	// ya existentes y modificandolos.
	clearItems(node);
	importArticle(node.getOwnerDocument(), (Articulo) item, node);
    }

    public static Collection<Articulo> exportArticle(Collection<Element> nodes) {
	List<Articulo> result = null;
	ArrayList<String> barCodes = new ArrayList<String>(nodes.size());
	for (Element root : nodes) {
	    NodeList childs = root.getChildNodes();
	    for (int i = 0; i < childs.getLength(); i++) {
		Node current = childs.item(i);
		if (current.getNodeType() == Node.ELEMENT_NODE) {
		    Element element = (Element) current;
		    if ("cod_bar".equals(current.getNodeName())) {
			barCodes.add(element.getTextContent());
		    }
		}
	    }
	}
	try {
	    result = PromoSystem.getArticuloStore().importArticulo(barCodes);
	} catch (Exception ex) {
	    ex.printStackTrace();
	}
	return result;
    }

    public static Articulo exportArticle(Element node) {
	Articulo result = null;
	String barcode = null;
	String intcode2 = null;
	long intcode = -1;

	NodeList nodes = node.getChildNodes();
	for (int i = 0; i < nodes.getLength(); i++) {
	    Node current = nodes.item(i);
	    if (current.getNodeType() == Node.ELEMENT_NODE) {
		Element element = (Element) current;
		if ("cod_int".equals(element.getNodeName())) {
		    try {
			intcode = Long.parseLong(element.getTextContent());
		    } catch (Exception ex) {
			// Ignore
		    }
		}
		if ("cod_bar".equals(element.getNodeName())) {
		    barcode = element.getTextContent();
		}
		if ("cod_ext".equals(element.getNodeName())) {
		    intcode2 = element.getTextContent();
		}
	    }
	}
	if ((intcode != -1) && (intcode2 != null) && (barcode != null)) {
	    try {
		result = PromoSystem.getArticuloStore().getArticulo(barcode,
			intcode, intcode2);
	    } catch (Exception ex) {
		// TODO Auto-generated catch block
		ex.printStackTrace();
	    }
	}
	return result;
    }

    public static Element importArticle(Document doc, Articulo artic,
	    String elementName) {
	Element result = doc.createElement(elementName);
	importArticle(doc, artic, result);
	return result;
    }

    /**
     * Importar es crear el nodo xml a partir del objeto entidad
     * 
     * @param doc
     * @param artic
     * @param element
     */
    private static void importArticle(Document doc, Articulo artic,
	    Element element) {
	Element cod_int = doc.createElement("cod_int");
	Element cod_bar = doc.createElement("cod_bar");
	Element cod_ext = doc.createElement("cod_ext");
	Element description = doc.createElement("description");

	element.setAttribute("datatype", "article");

	cod_int.setTextContent(String.valueOf(artic.getCodInterno()));
	cod_bar.setTextContent(artic.getCodBarra());
	cod_ext.setTextContent(artic.getCodInternoAlfa());
	description.setTextContent(artic.getDescription());

	element.appendChild(cod_int);
	element.appendChild(cod_bar);
	element.appendChild(cod_ext);
	element.appendChild(description);
    }
}
