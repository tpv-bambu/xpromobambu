/**
 * 
 */
package com.invel.promo.datahandler;

import java.util.ArrayList;
import java.util.Collection;

import org.w3c.dom.*;

import com.invel.common.PromoSystem;
import com.invel.promo.dao.*;

/**
 * Esta implementaci�n est� asociada
 * 
 * <clasificaciones datatype="clasification"> <nivel>XX</nivel>
 * <concepto>YY</concepto> </clasificaciones>
 * 
 * @author szeballos
 * 
 */
public class SingleEC extends DataHandlerAdapter {

    @Override
    public Object getValue(Element node) {
	// Null adapter implementation
	return SingleEC.exportEC(node);
    }

    @Override
    public void setValue(Element node, Object item) {
	clearItems(node);
	SingleEC.importEC(node.getOwnerDocument(),
		(EstructuraComercialNodo) item, node);
    }

    /**
     * Devuelve una lista de SimpleEstructuraComercial segun lo leido desde el
     * nodo.
     * 
     * @todo hacer mas eficiente esto, esto abre muchas bd conexions.
     */
    @Override
    public Object exportData(Element node) {
	// Null adapter implementation
	ArrayList<EstructuraComercialNodo> result = new ArrayList<EstructuraComercialNodo>();
	NodeList nodes = node.getChildNodes();
	for (int i = 0; i < nodes.getLength(); i++) {
	    Node current = nodes.item(i);
	    if (current.getNodeType() == Node.ELEMENT_NODE) {
		Element element = (Element) current;
		if ("clasificacion".equals(element.getNodeName())) {
		    result.add(SingleEC.exportEC(element));
		}
	    }
	}
	return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void importData(Document data, Element node, Object items) {
	// Null adapter implementation
	int iteration = 0;
	clearItems(node);
	Collection<EstructuraComercialNodo> nodos = (Collection<EstructuraComercialNodo>) items;
	for (EstructuraComercialNodo nodoEC : nodos) {
	    iteration++;
	    Element clasificacion = data.createElement("clasificacion");
	    node.appendChild(clasificacion);
	    clasificacion.setAttribute(ITERATION_MARK,
		    String.valueOf(iteration));
	    SingleEC.importEC(data, nodoEC, clasificacion);
	}
    }

    /**
     * coloca en el xml (element) la informaci�n contenida en <code>nodo</code>
     * 
     * @param doc
     * @param nodo
     * @param element
     */
    private static void importEC(Document doc, EstructuraComercialNodo nodo,
	    Element element) {
	Element nivel = doc.createElement("nivel");
	Element concepto = doc.createElement("concept");
	Element description = doc.createElement("description");
	nivel.setTextContent(String.valueOf(nodo.getEstructuraComercialNivel()
		.getCodigo()));
	concepto.setTextContent(nodo.getCode().trim());
	description.setTextContent(nodo.getDescripcion().trim());
	element.appendChild(nivel);
	element.appendChild(concepto);
	element.appendChild(description);
	// element.setAttribute("datatype", "clasification");//probar si hace
	// falta esto o XForm lo pone.
    }

    /**
     * 
     * Genera una EstructuraComercialNodo a partir del elemento XML.
     * 
     * No me interesa el tag description, busco lo que dice la base de datos.
     * 
     * @param node
     *            asociado al XPath del datahandler (clasificacion).
     * @return Articulo
     */
    private static EstructuraComercialNodo exportEC(Element node) {
	EstructuraComercialNodo result = null;
	SimpleEstructuraComercialNivel nivelEC = new SimpleEstructuraComercialNivel();

	String nivel = null;
	String concept = null;

	NodeList nodes = node.getChildNodes();
	for (int i = 0; i < nodes.getLength(); i++) {
	    Node current = nodes.item(i);
	    if (current.getNodeType() == Node.ELEMENT_NODE) {
		Element element = (Element) current;
		if ("nivel".equals(element.getNodeName())) {
		    try {
			nivel = element.getTextContent();

		    } catch (Exception ex) {
			// Ignore
		    }
		}
		if ("concept".equals(element.getNodeName())) {
		    concept = element.getTextContent();
		}
	    }
	}
	if ((nivel != null) && (concept != null)) {
	    nivelEC.setCodigo(Integer.parseInt(nivel));
	    try {
		result = PromoSystem.getECStore().getNodo(
			Integer.parseInt(nivel), concept);

	    } catch (Exception ex) {
		// TODO Auto-generated catch block
		ex.printStackTrace();
	    }
	}
	return result;
    }

}
