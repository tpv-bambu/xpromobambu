package com.invel.promo.datahandler;

import org.w3c.dom.Element;

public class SimpleValue extends DataHandlerAdapter {

    @Override
    public Object getValue(Element node) {
	return node.getTextContent();
    }

    @Override
    public void setValue(Element node, Object item) {
	node.setTextContent(item.toString());
    }

}
