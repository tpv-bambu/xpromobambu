package com.invel.promo.datahandler;

import java.util.ArrayList;
import java.util.Collection;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.invel.common.PromoSystem;
import com.invel.promo.dao.EstructuraComercialNodo;
import com.invel.promo.dao.Municipios;
import com.invel.promo.dao.SimpleEstructuraComercialNivel;
import com.invel.promo.dao.SimpleMunicipio;

/**
 * Esta implementaci�n est� asociada
 * 
 * <municipio datatype="municipio"> <codigo>XX</codigo>
 * <descripcion>YY</descripcion> </municipio>
 * 
 * @author szeballos
 * 
 */
public class SingleMunicipio extends DataHandlerAdapter {

    @Override
    public Object getValue(Element node) {
	// Null adapter implementation
	return SingleMunicipio.exportMuni(node);
    }

    @Override
    public void setValue(Element node, Object item) {
	clearItems(node);
	SingleMunicipio.importMuni(node.getOwnerDocument(), (Municipios) item,
		node);
    }

    /**
     * Devuelve una lista de SimpleEstructuraComercial segun lo leido desde el
     * nodo.
     * 
     * @todo hacer mas eficiente esto, esto abre muchas bd conexions.
     */
    @Override
    public Object exportData(Element node) {
	// Null adapter implementation
	ArrayList<Municipios> result = new ArrayList<Municipios>();
	NodeList nodes = node.getChildNodes();
	for (int i = 0; i < nodes.getLength(); i++) {
	    Node current = nodes.item(i);
	    if (current.getNodeType() == Node.ELEMENT_NODE) {
		Element element = (Element) current;
		if ("municipio".equals(element.getNodeName())) {
		    result.add(SingleMunicipio.exportMuni(element));
		}
	    }
	}
	return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void importData(Document data, Element node, Object items) {
	// Null adapter implementation
	int iteration = 0;
	clearItems(node);
	Collection<Municipios> nodos = (Collection<Municipios>) items;
	for (Municipios muni : nodos) {
	    iteration++;
	    Element clasificacion = data.createElement("municipio");
	    node.appendChild(clasificacion);
	    clasificacion.setAttribute(ITERATION_MARK,
		    String.valueOf(iteration));
	    SingleMunicipio.importMuni(data, muni, clasificacion);
	}
    }

    /**
     * coloca en el xml (element) la informaci�n contenida en <code>nodo</code>
     * 
     * @param doc
     * @param nodo
     * @param element
     */
    private static void importMuni(Document doc, Municipios nodo,
	    Element element) {

	Element codigo = doc.createElement("codigo");
	Element description = doc.createElement("descripcion");

	codigo.setTextContent(String.valueOf(nodo.getCodMunicipio()).trim());
	description.setTextContent(nodo.getDescripcion().trim());

	element.appendChild(codigo);
	element.appendChild(description);
	// element.setAttribute("datatype", "clasification");//probar si hace
	// falta esto o XForm lo pone.
    }

    /**
     * 
     * Genera un Municipio a partir del elemento XML.
     * 
     * No me interesa el tag description, busco lo que dice la base de datos.
     * 
     * @param node
     *            asociado al XPath del datahandler (clasificacion).
     * @return Articulo
     */
    private static Municipios exportMuni(Element node) {

	SimpleMunicipio muni = new SimpleMunicipio();

	String codigo = null;
	String descripcion = null;

	NodeList nodes = node.getChildNodes();
	for (int i = 0; i < nodes.getLength(); i++) {
	    Node current = nodes.item(i);
	    if (current.getNodeType() == Node.ELEMENT_NODE) {
		Element element = (Element) current;
		if ("codigo".equals(element.getNodeName())) {
		    try {
			codigo = element.getTextContent();

		    } catch (Exception ex) {
			// Ignore
		    }
		}
		if ("descripcion".equals(element.getNodeName())) {
		    descripcion = element.getTextContent();
		}
	    }
	}
	if (codigo != null && descripcion != null) {
	    muni.setCodMunicipio(Integer.parseInt(codigo));
	    muni.setDescripcion(descripcion);
	}

	/*
	 * if ((nivel != null) && (concept != null)) {
	 * nivelEC.setCodigo(Integer.parseInt(nivel)); try { result =
	 * PromoSystem.getECStore().getNodo(Integer.parseInt(nivel),concept);
	 * 
	 * } catch (Exception ex) { // TODO Auto-generated catch block
	 * ex.printStackTrace(); } }
	 */
	return muni;
    }
}
