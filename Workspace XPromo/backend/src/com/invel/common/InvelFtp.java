package com.invel.common;

import java.io.*;
import java.net.SocketException;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.ftp.*;

public class InvelFtp {

    public static int ALL_RIGHT = 0;

    public static int MISSING_PARAMETER = 1;

    public static int REFUSED_CONNECTION = 2;

    public static int COULD_NOT_CONNECT = 3;

    public static int ANOTHER_ERROR = 4;

    static Log log = LogFactory.getLog(InvelFtp.class);

    private String server, username, password;

    private Map<String, String> files;

    private boolean storeFile = false, binaryTransfer = false;

    /**
     * @param pStoreFile
     *            si el valor es true indica que se har� un put, false indica un
     *            get
     * @param pBinaryTransfer
     *            si es true, indica que se transfiere el archivo como binario,
     *            false indica que es un archivo ascci
     * @param pServer
     *            nro de ip o url del servidor
     * @param pUsername
     *            usuario
     * @param pPassword
     *            password
     * 
     * @param pFilesHash
     *            mapea nombres locales con nombres remotos.
     */
    public InvelFtp(boolean pStoreFile, boolean pBinaryTransfer,
	    String pServer, String pUsername, String pPassword,
	    Map<String, String> pFiles) {
	super();
	// TODO Auto-generated constructor stub
	this.storeFile = pStoreFile;
	this.binaryTransfer = pBinaryTransfer;
	this.server = pServer;
	this.username = pUsername;
	this.password = pPassword;
	this.files = pFiles;
    }

    public int execute() {
	FTPClient ftp;
	boolean error = false;
	String st ="";

	log.debug("Begin FTP connection");
	log.debug("\tServer: " + this.server);
	log.debug("\tUser:" + this.username);
	for (Map.Entry entry : this.files.entrySet()) {
	    log.debug("\tFile, local: " + entry.getKey() + ", remote:"
		    + entry.getValue());
	}

	if (this.server == null || this.username == null
		|| this.password == null || this.files == null) {
	    // TODO Agregar mensaje para mostrar en pagina
	    // System.err.println(USAGE);
	    // // System.exit(1);
	    return InvelFtp.MISSING_PARAMETER;
	}

	ftp = new FTPClient();
	ftp.addProtocolCommandListener(new PrintCommandListener(log));
	System.setProperty("java.net.preferIPv4Stack" , "true");

	try {
	    int reply;
	    log.info("usando java.net.preferIPv4Stack , true"); 
	    
		if( PromoCommons.getIpFijaWin10().length()< 1 ){
		    log.info("usando ftp ip de tabla sucursales en dbinvel");
		    //ftp.connect(this.server);
		    st = this.server.replaceAll("\\s+","");
		    ftp.connect(st,21);
		    log.info("Connected to " + this.server + ".");
		}else {
		    log.info("usando parametro IpWin10Ftp para el ftp");
		    ftp.connect(PromoCommons.getIpFijaWin10(), 21);
		    log.info("Connected to " + PromoCommons.getIpFijaWin10() + ".");
		}
	    
	    
	    //ftp.connect(this.server);
	    //ftp.connect(this.server.toString(),21);
	    
	    

	    // After connection attempt, you should check the reply code to
	    // verify
	    // success.
	    reply = ftp.getReplyCode();

	    if (!FTPReply.isPositiveCompletion(reply)) {
		ftp.disconnect();
		log.error("FTP server refused connection.");
		// TODO Agregar mensaje a pagina System.exit(1);
		return REFUSED_CONNECTION;

	    }
	} catch (IOException ex) {
	    ex.printStackTrace();
	    log.error("Excepcion de conexion recorcholis.Esta conectado?");
	    
	    if (ftp.isConnected()) {
		log.error("SIP conectado");
		try {
		    ftp.disconnect();
		    log.error("Desconecto");
		} catch (IOException iox) {
		    System.out.println("Oops! Something wrong happened");
	            iox.printStackTrace();
		    // do nothing
		}
	    } else { 
		log.error("NOP conectado");
	    }
	    
	    // System.err.println("Could not connect to server.");
	    log.error("Could not connect to server.");
	    ex.printStackTrace();
	    return COULD_NOT_CONNECT;
	    // TODO agregar mensaje a pagina System.exit(1);
	}
	

	__main: try {
	    if (!ftp.login(this.username, this.password)) {
		ftp.logout();
		error = true;
		break __main;
	    }

	    // System.out.println("Remote system is " + ftp.getSystemName());
	    log.info("Remote system is " + ftp.getSystemName());

	    if (this.binaryTransfer) {
		ftp.setFileType(FTP.BINARY_FILE_TYPE);
	    }

	    // Use passive mode as default because most of us are
	    // behind firewalls these days.
	    ftp.enterLocalPassiveMode();

	    if (this.storeFile) {
		InputStream input;
		for (Map.Entry<String, String> entry : this.files.entrySet()) {
		    input = new FileInputStream(entry.getKey());
		    ftp.storeFile(entry.getValue(), input);
		    input.close();
		}

	    } else {
		OutputStream output;
		for (Map.Entry<String, String> entry : this.files.entrySet()) {
		    output = new FileOutputStream(entry.getKey());
		    ftp.retrieveFile(entry.getValue(), output);
		    output.close();
		}
	    }
	    // ftp.logout();
	} catch (FTPConnectionClosedException e) {
	    error = true;
	    log.error("Server closed connection.");
	    e.printStackTrace();
	} catch (SocketException e) {
	    error = true;
	    log.error(e);
	    e.printStackTrace();
	} catch (IOException e) {
	    error = true;
	    e.printStackTrace();
	    // log.error(e);
	} finally {
	    if (ftp.isConnected()) {
		try {
		    ftp.disconnect();
		} catch (IOException iox) {
		    // do nothing
		}
	    }
	}
	// TODO mostrar mensaje System.exit(error ? 1 : 0);
	return (error ? ANOTHER_ERROR : ALL_RIGHT);
    }

}
