/*
 * InvelServlet.java
 *
 * Created on 17 de septiembre de 2005, 11:55
 */

package com.invel.common;

import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class InvelServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    static Log log = LogFactory.getLog(InvelServlet.class);

    @Override
    public void init() throws ServletException {
	try {
	    ServletConfig servletConfig = getServletConfig();
	    ServletContext servletContext = servletConfig.getServletContext();

	    String propertyName = "invel.promociones.properties.path";
	    String path = servletConfig.getInitParameter(propertyName);
	    if (path == null) {
		log.error("Init parameter " + propertyName + " not specified.");
		path = "/WEB-INF/promociones.properties";
	    }
	    log.info("Using " + path + " to load Promociones properties.");

	    Properties p = new Properties();
	    p.load(servletContext.getResourceAsStream(path));

	    log.info("Using properties " + p);
	    PromoCommons.init(p);
	    PromoSystem.init(p);

	} catch (Exception e) {
	    throw new ServletException("Error initializing servlet", e);
	}
    }
}
