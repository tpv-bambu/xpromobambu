/*
 * PromoSystem.java
 *
 * Created on 17 de septiembre de 2005, 12:19
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.invel.common;

import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.invel.promo.dao.*;

public class PromoSystem {
    private static ArticuloStore articuloStore;

    private static DepartamentoStore departamentoStore;

    private static PromoStore promoStore;

    private static ProSucStore prosucStore;

    private static SucursalStore sucursalStore;

    private static TypePromoStore typePromoStore;

    private static UserStore userStore;

    private static EstructuraComercialStore ec_store;

    private static MunicipiosStore mun_store;

    private static ArrayList<String> errors = new ArrayList<String>();

    /** Servidor remoto donde se encuentra la base de datos central */
    private static String host;

    /** Nombre de la Base de Datos */
    private static String db;

    /** URL de conexi�n a la base de datos */
    private static String url;

    static Log log = LogFactory.getLog(PromoSystem.class);

    public static ArticuloStore getArticuloStore() {
	return articuloStore;
    }

    public static DepartamentoStore getDepartamentoStore() {
	return departamentoStore;
    }

    public static PromoStore getPromoStore() {
	return promoStore;
    }

    public static ProSucStore getProSucStore() {
	return prosucStore;
    }

    public static SucursalStore getSucursalStore() {
	return sucursalStore;
    }

    public static MunicipiosStore getMunicipiosStore() {
	return mun_store;
    }

    public static TypePromoStore getTypePromoStore() {
	return typePromoStore;
    }

    public static UserStore getUserStore() {
	return userStore;
    }

    /**
     * En caso de haber existido errores que invaliden al XPromo este m�todo
     * devuelve un listado de ellos. Caso contrario tira null.
     * 
     * @return String
     */
    public static ArrayList<String> getErrors() {
	return errors;
    }

    /**
     * Devuelve el host a la base de datos central
     * 
     * @return String
     */
    public static String getHost() {
	return host;
    }

    /**
     * Devuelve el URL de conexión a la base de datos central
     * 
     * @return String
     */
    public static String getURL() {
	return url;
    }

    /**
     * Devuelve el nombre de la base de datos central
     * 
     * @return String
     */
    public static String getCentralDB() {
	return db;
    }

    /**
     * Valida e inicializa la conexión a base de datos y los objetos DAO.
     * 
     * @param properties
     */
    public static void init(Properties properties) {
	errors.clear();
	if (properties == null) {
	    errors.add("Error al leer archivo de configuracion.");
	    return;//
	}
	url = properties.getProperty("url");
	host = properties.getProperty("host");
	db = properties.getProperty("db");
	url = url.replace("{host}", host);
	url = url.replace("{db}", db);
	properties.setProperty("url", url);

	String driverName = properties.getProperty("driver");

	if (driverName == null || host == null || db == null || url == null) {
	    errors.add("Par�metros 'driver', 'host', 'url' � 'db' no cargado en"
		    + " .properties");
	    return;//
	}

	if (!url.contains(host) || !url.contains(db)) {
	    errors.add("Parámetro 'url' inconsitente con parámetro 'host' ó 'db'");
	    return;
	}

	try {
	    Class.forName(driverName);
	    DriverManager.getConnection(url, properties);
	} catch (ClassNotFoundException ex) {
	    errors.add("Error en driver JDBC: " + ex.getMessage());

	} catch (Exception ex) {
	    errors.add("Error al crear conexión a base de datos: "
		    + ex.getMessage());
	    log.error(ex);
	}

	typePromoStore = new SimpleTypePromoStore(properties);
	promoStore = new SimplePromoStore(properties);
	departamentoStore = new SimpleDepartamentoStore(properties);
	articuloStore = new SimpleArticuloStore(properties);
	// userStore = new SimpleUserStore(properties); -- viejo anterior a
	// Xvalidate
	userStore = new SimpleUserValidateStore();
	prosucStore = new SimpleProSucStore(properties);
	sucursalStore = new SimpleSucursalStore(properties);
	mun_store = new SimpleMunicipiosStore(properties);
	try {
	    ec_store = new SimpleEstructuraComercialStore(properties);
	} catch (Exception ex) {
	    errors.add(ex.getMessage());
	    log.error(ex);
	}
	log.info("Stores Created");
    }

    /**
     * @return the ec_store
     */
    public static EstructuraComercialStore getECStore() {
	return ec_store;
    }
}
