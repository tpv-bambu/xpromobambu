/**
 * 
 */
package com.invel.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Clase con funciones para archivos.
 * 
 * @author Administrador
 * @version 1.0
 */
public class FileHelper {

    /**
     * Metodo q elimina archivos de una carpeta con una determinada extension.
     * 
     * @param folder
     *            Carpeta
     * @param extension
     *            Extension
     */
    public void deleteFiles(String folder, String extension) {

	ExtensionFilter filter = new ExtensionFilter(extension);
	File dir = new File(folder);

	String[] list = dir.list(filter);
	File file;
	if (list.length == 0)
	    return;

	for (int i = 0; i < list.length; i++) {
	    file = new File(folder + list[i]);
	    file.delete();
	}

    }

    /**
     * Metodo q copia una archivo de una carpeta a otra.
     * 
     * @param source
     *            Archivo origen
     * @param dest
     *            Archivo destino
     * @throws IOException
     */
    public static void copy(File source, File dest) throws IOException {

	FileChannel in = null, out = null;

	try {

	    in = new FileInputStream(source).getChannel();
	    out = new FileOutputStream(dest).getChannel();

	    long size = in.size();
	    MappedByteBuffer buf = in.map(FileChannel.MapMode.READ_ONLY, 0,
		    size);

	    out.write(buf);

	} finally {
	    if (in != null)
		in.close();
	    if (out != null)
		out.close();
	}
    }

    /**
     * Clase interna filtro de las extensiones.
     * 
     * @author Administrador
     * @version 1.0
     */
    class ExtensionFilter implements FilenameFilter {
	private String extension;

	public ExtensionFilter(String pExtension) {
	    this.extension = pExtension;
	}

	public boolean accept(File dir, String name) {
	    return (name.endsWith(this.extension));
	}
    }

    public static void main(String args[]) {
	FileHelper file = new FileHelper();
	file.deleteFiles("C:/temp/", ".txt");
    }

}
