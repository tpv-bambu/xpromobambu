package com.invel.common;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.invel.promo.agrupaciones.Agrupaciones2;

/**
 * Define opciones de configuración globales para toda la aplicaciï¿½n.
 * 
 * @author szeballos
 * 
 */
public class PromoCommons {
    public static final int DEFAULT_PROMO_SELECTED = -1;

    public static final int DEFAULT_TYPE_SELECTED = -1;

    public static final int PROMO_DISABLED = 2;

    public static final int PROMO_ENABLED = 1;

    public static final int ALL_PROMOS = 0;

    public static String compiler;

    public static int lengthBarCode = 13;

    public static int lengthInnerCode = 10;

    // referencias a archivos
    /** Path al xml a generar* */
    public static String progXml;

    /** Path a la plantilla de transformaciï¿½n principal */
    public static String progXsl;
    /** Path a la plantilla de transformaciï¿½n de anï¿½lisis de competencias */
    public static String listadoXsl;

    /**
     * Path al cï¿½digo fuente de las promo, se obtiene aplicando al progxml la
     * transformaciï¿½n del progXsl
     */
    public static String promoPro;

    /** Referencia a tree.xml */
    public static String treeXml;

    /** Archivo compilado de las promociones, a enviar a las cajas */
    public static String xpromoCod;

    /** Salida stdout del compilador de promos */
    public static String xpromoTxt;

    /** Ubicación del archivo versión.xml */
    private static String versionXML;

    /** Path del xml de Agrupacion de Promociones */
    private static String xmlGrupPromoPath;

    /** Referencia al objeto de Altova */
    private static Agrupaciones2 agrupaciones;

    /**
     * Todos estos parametros se necesitan al compilar la promocion, generan una
     * promociï¿½n ficticia que se utiliza cuando una promocion que se supone
     * debe cargar en el monedero, por algï¿½n motivo no puede y se termina
     * emitiendo este cupï¿½n de error.
     */
    public static int cuponerror;
    public static int cuponerrorvigencia;
    public static String cuponerrorlimite;

    public static String validar_afinidad;
    public static String validar_vigencia;
    public static String validar_rangofecha;
    public static String validar_rangohora;
    public static String validar_rangocaja;
    public static String validar_perfil;
    public static String validar_rangotarjeta;

    public static String dbencoding;

    public static String htmlencoding;

    private static String destinoFileScriptFtp = "promo.cod";

    private static String origenFileScriptFtp = "promo.cod";
    
    private static String origenFileSQLFtp ="promo.sql";
    private static String destinoFileSQLFtp = "promo.sql";

    private static String nameXPath;

    private static String codeXPath;

    private static String externCodeXPath = "/promo/cod_externo";
    private static String ipFijaWin10 = "";

    // parametros de directorios
    private static String workpath;

    private static String compiladorPath;

    private static String commonPath;

    private static String promosPath;

    private static String formspath;

    /** URL del servidor XValidate */
    private static String urlwebservice;

    // Los errores de configuraciï¿½n se van apilando aquï¿½.
    private static ArrayList<String> errors = new ArrayList<String>();

    // Indica si se renderizarï¿½n los mensajes de error cargados en los
    // xforms.
    private static boolean xformsErrorMessages = false;

    // path de la carpeta temp adonde se van a copiar los archivos
    // compilados por cada sucursal
    public static String tempPath;

    // nombre de los archivos q se generan por sucursal en el procesdo de
    // compilacion
    private static String fileNamePro;

    private static String fileNameCod;

    private static String fileNameTxt;

    private static String fileNameXml;

    static Log log = LogFactory.getLog(PromoCommons.class);

    public static String xpromoSQL = "promo.sql";

    public static String sqlXsl = "";

    public static boolean enviarSQL=true;
    

    public static String getDestinoFileScriptFtp() {
	return destinoFileScriptFtp;
    }
    
    public static String getDestinoFileSQLFtp() {
	return destinoFileSQLFtp;
    }
    
    public static String getOrigenFileSQLFtp() {
	return origenFileSQLFtp;
    }

    public static int getLengthBarCode() {
	return lengthBarCode;
    }

    public static int getLengthInnerCode() {
	return lengthInnerCode;
    }

    public static String getOrigenFileScriptFtp() {
	return origenFileScriptFtp;
    }

    public static String getFileNamePro() {
	return fileNamePro;
    }

    public static String getFileNameCod() {
	return fileNameCod;
    }

    public static String getFileNameTxt() {
	return fileNameTxt;
    }
    
    public static String getIpFijaWin10() {
	return ipFijaWin10;
    }
 

    public static String getFileNameXml() {
	return fileNameXml;
    }

    /**
     * Obtiene la lista de errores, en caso de estar vacia es que no ocurriï¿½
     * ningï¿½n error.
     * 
     * @return String
     */
    public static Collection<String> getErrors() {
	return errors;
    }

    /**
     * Obtiene el nombre del tag en el xml de la promo correspondiente al nombre
     * de la promociï¿½n. En el archivo de configuraciï¿½n se guarda en sintaxis
     * de XPath, al leer el properties (en init) se adecua para leerlo
     * directamente del request.
     * 
     * @return String
     */
    public static String getNameXPath() {
	return nameXPath;
    }

    /**
     * Obtiene el nombre del tag en el xml de la promo correspondiente al "id"
     * de la promociï¿½n. En el archivo de configuraciï¿½n se guarda en sintaxis
     * de XPath, al leer el properties (en init) se adecua para leerlo
     * directamente del request.
     * 
     * @return String
     */
    public static String getCodeXPath() {
	return codeXPath;
    }

    /**
     * Obtiene el xpath asociado al cï¿½digo externo de la promo, es decir como
     * encontrar en el xml el campo asociado al cï¿½digo externo.
     * 
     * @return String
     * @TODO tener objetos XPath ya compilados.
     */
    public static String getExternCodeXPath() {
	return externCodeXPath;
    }

    public static String getExternCodeRequestParameter() {
	String result = externCodeXPath.replaceAll("/@", "_");
	result = result.replaceAll("/", "_");
	result = result.substring(1);
	return result;
    }

    /**
     * Devuelve el nombre que tiene en el request el id de la promo.
     * 
     * @return String
     */
    public static String getCodeRequestParameter() {
	String result = codeXPath.replaceAll("/@", "_");
	result = result.replaceAll("/", "_");
	result = result.substring(1);
	return result;
    }

    /**
     * Devuelve el nombre que tiene en el request el nombre de la promo.
     * 
     * @return String
     */
    public static String getNameRequestParameter() {
	String result = null;
	result = nameXPath.replaceAll("/@", "_");
	result = result.replaceAll("/", "_");
	result = result.substring(1);
	return result;
    }

    /**
     * Si true se configura al motor de formularios para que imprima los
     * mensajes de error en fallos de validaciones, c.c. no
     * 
     * @return boolean
     */
    public static boolean isXFormsErrorMessages() {
	return xformsErrorMessages;
    }

    /**
     * Inicializa los parï¿½metros de configuraciï¿½n de un properties.
     * InvelServlet llama a este mï¿½todo al cargarse.
     * 
     * @param properties
     * @throws Exception
     */
    public static void init(Properties properties) {
	errors.clear();
	nameXPath = properties.getProperty("xpath.nombre.promo");
	if (nameXPath == null) {
	    errors.add("xpath.nombre.promo no configurado en promo properties");
	}

	codeXPath = properties.getProperty("xpath.codigo.promo");
	if (codeXPath == null) {
	    errors.add("xpath.codigo.promo no configurado en promo properties");
	}

	dbencoding = properties.getProperty("dbencoding");
	if (dbencoding == null) {
	    errors.add("dbencoding no configurado en promo properties");
	}

	htmlencoding = properties.getProperty("htmlencoding");
	if (htmlencoding == null) {
	    errors.add("htmlencoding no configurado en promo properties");
	}

	workpath = properties.getProperty("workpath");
	if (workpath == null) {
	    errors.add("Workspace must be specified in promo properties");
	}

	urlwebservice = properties.getProperty("urlwebservice");
	if (urlwebservice == null) {
	    errors.add("UrlWebService must be specified in promo properties");
	}

	// incializo los nombres de los archivos de compilacion
	fileNameCod = properties.getProperty("xpromo.file.compiler.name.cod");
	if (fileNameCod == null)
	    errors.add("FileNameCod must be specified in promo properties");

	fileNamePro = properties.getProperty("xpromo.file.compiler.name.pro");
	if (fileNamePro == null)
	    errors.add("FileNamePro must be specified in promo properties");

	fileNameTxt = properties.getProperty("xpromo.file.compiler.name.txt");
	if (fileNameTxt == null)
	    errors.add("FileNameTxt must be specified in promo properties");

	fileNameXml = properties.getProperty("xpromo.file.compiler.name.xml");
	if (fileNameXml == null)
	    errors.add("FileNameXml must be specified in promo properties");

	formspath = properties.getProperty("formspath");
	compiladorPath = properties.getProperty("compilador.path");
	commonPath = properties.getProperty("plantillas.common");
	promosPath = properties.getProperty("plantillas.promos");
	xformsErrorMessages = Boolean.parseBoolean(properties.getProperty(
		"xform.error.messages", "false"));
	enviarSQL = Boolean.parseBoolean(properties.getProperty(
		"mandar.promo.sql", "true"));	
	if (formspath == null) {
	    formspath = workpath + "xforms/";
	}
	if (compiladorPath == null) {
	    compiladorPath = workpath + "pc/";
	}

	if (commonPath == null) {
	    commonPath = workpath + "common/";
	}

	if (promosPath == null) {
	    promosPath = workpath + "promos/";
	}
	initPath(workpath);

	// Obtengo elpath y luego el xml
	xmlGrupPromoPath = commonPath + "agrupacionPromocion.xml";
	try {
	    agrupaciones = Agrupaciones2.loadFromFile(xmlGrupPromoPath);
	} catch (Exception ex) {
	    errors.add("No se pudo levantar el xml de Agrupacion de promociones: "
		    + ex.getMessage());
	}

	try {
	    cuponerror = Integer
		    .parseInt(properties.getProperty("cupon.error"));
	} catch (Exception ex) {
	    errors.add("Cupon de error mal configurado: " + ex.getMessage());
	}

	try {
	    cuponerrorvigencia = Integer.parseInt(properties
		    .getProperty("cupon.error.vigencia"));
	} catch (Exception ex) {
	    errors.add("Vigencia del cupon de error mal configurado: "
		    + ex.getMessage());
	}
	cuponerrorlimite = properties.getProperty("cupon.error.limite");
	if (cuponerrorlimite == null) {
	    errors.add("Límite del cupon de error mal configurado");
	}
	validar_afinidad = properties.getProperty("validar_afinidad");
	validar_vigencia = properties.getProperty("validar_vigencia");
	validar_rangofecha = properties.getProperty("validar_rangofecha");
	validar_rangocaja = properties.getProperty("validar_rangocaja");
	validar_rangohora = properties.getProperty("validar_rangohora");
	validar_rangotarjeta = properties.getProperty("validar_rangotarjeta");
	validar_perfil = properties.getProperty("validar_perfil");
	if (validar_afinidad == null || validar_perfil == null
		|| validar_rangocaja == null || validar_rangofecha == null
		|| validar_rangohora == null || validar_rangotarjeta == null
		|| validar_vigencia == null) {
	    errors.add("Revisar promociones.properties, faltan cargar parametros de validar consultas");
	}

	String slongBarCode = properties.getProperty("longCodBarra");
	if (slongBarCode != null) {
	    try {
		lengthBarCode = Integer.parseInt(slongBarCode);
	    } catch (Exception ex) {
		errors.add("lengthBarCode mal configurado en PromoCommons "
			+ ex);
	    }
	}

	String slongInnerCode = properties.getProperty("longCodIntAlfa");
	if (slongBarCode != null) {
	    try {
		lengthInnerCode = Integer.parseInt(slongInnerCode);
	    } catch (Exception ex) {
		errors.add("Exception in PromoCommons " + ex);
	    }
	}

	String origenScript = properties.getProperty("OrigenFileScript");
	if (origenScript != null) {
	    origenFileScriptFtp = origenScript;
	}

	String destinoScript = properties.getProperty("DestinoFileScript");
	if (destinoScript != null) {
	    destinoFileScriptFtp = destinoScript;
	}
	
	String destinoSQL = properties.getProperty("DestinoFileSQL");
	if (destinoScript != null) {
	    destinoFileSQLFtp = destinoSQL;
	}
	
	String origenSQL = properties.getProperty("OrigenFileSQL");
	if (origenSQL != null) {
	    origenFileSQLFtp = origenSQL;
	}
	
	String ipfijaWin10 = properties.getProperty("IpWin10Ftp");
	if (ipfijaWin10 != null) {
	    ipFijaWin10 = ipfijaWin10;
	}
	 
	
	
	if (errors.size() > 0) {
	    log.error("Configuración Incompleta:");
	    for (String s : errors) {
		log.error(s);
	    }
	} else {
	    // valido la configuracion.
	    try {
		validate();
	    } catch (IOException ex) {
		// Ignorar
	    }
	}
    }

    /**
     * Inicializa variables que guardan ubicaciones de archivos y carpetas
     * usadas por todo el programa.
     * 
     * @param path
     */
    public static void initPath(String path) {
	treeXml = commonPath + "tree.xml";
	versionXML = commonPath + "version.xml";
	progXsl = commonPath + "prog.xsl";
	listadoXsl = commonPath + "listado.xsl";
	compiler = compiladorPath + "PC.EXE";
	sqlXsl = commonPath + "consultas.xsl";
	/*
	 * xpromoCod = path + "promo.cod"; xpromoTxt = path + "xpromo.txt";
	 * promoPro = path + "promo.pro"; progXml = path + "prog.xml";
	 */
	xpromoSQL = "promo.sql";
	xpromoCod = "promo.cod";
	xpromoTxt = "xpromo.txt";
	promoPro = "promo.pro";
	progXml = "prog.xml";
	tempPath = path + "temp/";
    }

    /**
     * El nombre del archivo XML al que aplicaremos la transformación para
     * obtener un html con las versiones de las plantillas usadas.
     * 
     * @return String
     */
    public static String getVersionXML() {
	return versionXML;
    }

    public static String getFormspath() {
	return formspath;
    }

    public static String getWorkpath() {
	return workpath;
    }

    // (+) Se agrega por req. Xvalidator
    public static String getUrlwebservice() {
	return urlwebservice;
    }

    // Se agrega metodo por Req 73 - Inc 27033 DS 05/09/2007
    public static String getXmlGrupPromoPath() {
	return xmlGrupPromoPath;
    }

    public static Agrupaciones2 getXmlAgrupaciones() {
	return agrupaciones;
    }

    /**
     * Valida que la configuraciÃ³n sea correcta, como ser que la carpeta de
     * XForms tenga al menos un XForm, etc.
     * 
     */
    private static void validate() throws IOException {

	/**
	 * Clase privada para filtrar archivos de tipo xform
	 * 
	 * @author szeballos
	 * 
	 */
	class XFormFilter implements FilenameFilter {

	    public boolean accept(File dir, String name) {

		Pattern p = Pattern.compile(".*\\.xform",
			Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(name);
		return m.matches();
	    }
	}
	/**
	 * Clase privada para filtrar archivos de tipo xsl.
	 * 
	 * @author szeballos
	 * 
	 */
	class XSLFilter implements FilenameFilter {
	    public boolean accept(File dir, String name) {
		Pattern p = Pattern.compile(".*\\.xsl",
			Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(name);
		return m.matches();
	    }
	}

	/**
	 * Clase privada para filtrar archivos de tipo xinc.
	 * 
	 * @author szeballos
	 * 
	 */
	class XIncFilter implements FilenameFilter {
	    public boolean accept(File dir, String name) {
		Pattern p = Pattern.compile(".*\\.xinc",
			Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(name);
		return m.matches();
	    }
	}

	java.io.File file = new File(treeXml);

	if (!file.canRead()) {
	    errors.add("tree.xml inexistente: " + treeXml);
	}
	file = new File(workpath);
	if (!file.canRead()) {
	    errors.add("Carpeta de trabajo inexistente: " + workpath);
	    throw new IOException();
	}
	file = new File(versionXML);
	if (!file.canRead()) {
	    errors.add("Version.xml inexistente: " + versionXML);
	}
	file = new File(progXsl);
	if (!file.canRead()) {
	    errors.add("prog.xsl inexistente: " + progXsl);
	}

	file = new File(listadoXsl);
	if (!file.canRead()) {
	    errors.add("listado.xsl inexistente: " + listadoXsl);
	}

	file = new File(compiler);
	if (!file.canRead()) {
	    errors.add("Path al compilador mal configurado: " + compiler);
	    throw new IOException();
	}

	file = new File(formspath);
	if (file == null || !file.canRead()
		|| file.list(new XFormFilter()).length == 0) {
	    errors.add("Carpeta de XForms no contiene ningun xform: "
		    + formspath);
	    throw new IOException();
	}

	file = new File(promosPath);
	if (file == null || !file.canRead()
		|| file.list(new XSLFilter()).length == 0) {
	    errors.add("Carpeta de plantillas no contiene ningun archivo xsl: "
		    + promosPath);
	    throw new IOException();
	}

	file = new File(formspath);
	if (file == null || !file.canRead()
		|| file.list(new XIncFilter()).length == 0) {
	    errors.add("Carpeta de XForms no contiene ningun xinc: "
		    + formspath);
	    throw new IOException();
	}
    }

}
