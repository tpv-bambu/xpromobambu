<%@ page language="java" %>

<%@ taglib prefix="bean"  uri="/WEB-INF/struts-bean.tld" %>
<%@ taglib prefix="html"  uri="/WEB-INF/struts-html.tld" %>
<%@ taglib prefix="logic" uri="/WEB-INF/struts-logic.tld" %>
<%@ taglib prefix="tiles" uri="/WEB-INF/struts-tiles.tld" %>

<tiles:useAttribute id="title" name="title" classname="java.lang.String" />

<div align=center>
<html:html locale="true">
<head>

<title><bean:write name="title" /></title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link href="common/estilos_6.css" rel="stylesheet" type="text/css">


</head>

<body>

<table class="marco" border="0" cellpadding="0" cellspacing="0" width="760">
<tr>
<th colspan="5">
	<tiles:insert attribute="header"/>
</th>
</tr>

<tr>

<td align="center" valign="top" width="560">
	<tiles:insert attribute="body"/>
</td>
</tr>

<tr>

<th colspan="5">
	<tiles:insert attribute="footer" ignore="true"/>
</th>
</tr>

</table>

<DIV ID="testdiv1" STYLE="position:absolute;visibility:hidden;background-color:white;layer-background-color:white;"></DIV>
</body>
</html:html>
</div>