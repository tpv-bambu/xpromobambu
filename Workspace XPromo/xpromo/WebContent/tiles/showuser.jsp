<%@ page language="java" %>

<%@ taglib prefix="bean"  uri="/WEB-INF/struts-bean.tld" %>
<%@ taglib prefix="html"  uri="/WEB-INF/struts-html.tld" %>
<%@ taglib prefix="logic" uri="/WEB-INF/struts-logic.tld" %>
<%@ taglib prefix="tiles" uri="/WEB-INF/struts-tiles.tld" %>

<tiles:useAttribute id="title" name="title" classname="java.lang.String" />

<html:html locale="true">
<head>
<title><bean:write name="title" /> : <bean:write name="showuser" property="username"/></title>
</head>

<body>

<table>

<tr>
<td width="150">
&nbsp;
</td>
<td align="center">
	<tiles:insert attribute="header"/>
</td>
</tr>

<tr>
<td width="150" valign="top">
	<tiles:insert attribute="menu"/>
</td>

<td>
	<tiles:insert attribute="body"/>
</td>
</tr>

<tr>
<td></td>
<td>
	<tiles:insert attribute="footer" ignore="true"/>
</td>
</tr>

</table>

</body>
</html:html>
