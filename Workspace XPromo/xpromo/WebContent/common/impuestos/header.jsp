<%@ taglib prefix="logic" uri="/WEB-INF/struts-logic.tld" %>
<%@ taglib prefix="html"  uri="/WEB-INF/struts-html.tld" %>
<%@ taglib prefix="bean"  uri="/WEB-INF/struts-bean.tld" %>
<%@ taglib prefix="jstl"  uri="http://java.sun.com/jsp/jstl/core" %>

<div align="center"><html:img page="/images/cabecera_1_impuesto.jpg" alt="Invel Impuestos" width="100%" height="84"/><br></div>
<table class="marco" border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
    <tr>
      <td colspan="3" background="images/menu.jpg" height="25" valign="middle"><div class="titulo" align="center">InStore - Gestor de Impuestos </div></td>
      <td colspan="2" class="menusuperior" background="images/menu.jpg" valign="middle" align="right">
        <jstl:if test='${user!=null && user.impuesto }' >
        <html:link forward="logout"><bean:message key="app.logoutuser"/>  </html:link>
        |
        </jstl:if>
        <html:link forward="initmainimpuesto"><bean:message key="title.inicio"/></html:link>
        |
        <html:link forward="showabout"><bean:message key="impuestos.title.about"/></html:link>
      </td>
    </tr>
        <jstl:if test='${user!=null && user.impuesto }' >
		    <tr>
		        <td colspan="2">
		          <jsp:useBean id="user" scope="session" type="com.invel.promo.dao.User"/>
		          <div class="textoform" align="left"> Bienvenido <jsp:getProperty name="user" property="username"/></div>
		        </td>
		    </tr>
    	</jstl:if>

    <jstl:if test='${user==null || !user.impuesto }' >
	  	<!-- tr-->
	  	<td colspan="5">
		<html:form action="doLogin" method="POST" focus="username">
	
		  <input type="hidden" name="redirect" value="/InitMainPromo.do">
		  <input type="hidden" name="command" value="login" />
		   <input type="hidden" name="impuesto" value="true"/>
		  <table cellpadding="2">
		   <tr>
		      <td align="left" class="textomodulos1"><bean:message key="login.label.username" /></td>
		      <td><input type="text" name="username" size="9" maxlength="30"></td>
	
		      <td align="left" class="textomodulos1"><bean:message key="login.label.password" /></td>
		      <td><input type="password" name="password" size="12" maxlength="30"></td>
	
		      <td align="left"><html:submit styleClass="login" value="Login" /></td>
		   </tr>
		  </table>
	
	
			</html:form>
	      </td>
      </jstl:if>
</table>
