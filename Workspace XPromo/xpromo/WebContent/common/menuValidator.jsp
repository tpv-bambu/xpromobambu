<%@ taglib prefix="bean" uri="/WEB-INF/struts-bean.tld"%>
<%@ taglib prefix="html" uri="/WEB-INF/struts-html.tld"%>
<%@ taglib prefix="logic" uri="/WEB-INF/struts-logic.tld"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<table border="0" cellpadding="0" cellspacing="0" width="188">
  <c:if test="${user!=null && !user.impuesto}">
    <c:if test="${user.cancreatepromo}">
      <tr>
        <td class="fondo_iconos" align="center" height="39" valign="middle"><img src="./images/1nueva_p.gif" height="34" width="35">
        </td>
        <td class="fondobotonera" valign="middle"><a href="InitPromo.do?" >Nueva Promoción </a>
        </td>
      </tr>
    </c:if>
    <c:if test="${user.canreadpromo}">
      <tr>
        <td class="fondo_iconos" align="center" height="39" valign="middle"><img src="./images/2ver_pro.gif" height="34" width="35">
        </td>
        <td class="fondobotonera" valign="middle"><a href="ListPromoPage.do?" >Ver Promociones </a>
        </td>
      </tr>
    </c:if>
    <c:if test="${user.canassociatepromo}">
      <tr>
        <td class="fondo_iconos" align="center" height="39" valign="middle"><img src="./images/3asociar.gif" height="34" width="35">
        </td>
        <td class="fondobotonera" valign="middle"><a href="ListPromoXSucursal.do?command=searchPromoByBranch" >Asociar a Sucursales</a></td>
      </tr>
    </c:if>
    <c:if test="${user.cancompilepromo}">
      <tr>
        <td class="fondo_iconos" align="center" height="42" valign="middle"><img src="./images/5compila.gif" height="34" width="35">
        </td>
        <td class="fondobotonera" valign="middle"><a href="ShowSendPromo.do?" >Compilar Promociones </a></td>
      </tr>
    </c:if>
    <c:if test="${user.canConfigureXPromo}">
      <tr>
        <td class="fondo_iconos" align="center" height="42" valign="middle"><img src="./images/4perfile.gif" height="34" width="35">
        </td>
        <td class="fondobotonera" valign="middle"><a href="TypePromo.do?command=prepareForm" >Configuración </a></td>
      </tr>
    </c:if>
    <!--  
    <tr>
      <td class="fondo_iconos" align="center" height="42" valign="middle"><img src="./images/6exportar.gif" height="34" width="35"></td>
      <td class="fondobotonera" valign="middle"><a href="ExportPromo.do?"/>Exportar Promociones </a></td>
    </tr>
    -->
    <tr>
      <td colspan="2" class="fondobotonera" bgcolor="#e4e9ef" cellpadding="0" cellspacing="0" valign="top">&nbsp;</td>
    </tr>

  </c:if>
  <!-- Usuario presente y trabajando en modo impuesto -->

  <c:if test='${user!=null && user.impuesto }'>
    <c:if test="${user.ableToCreateTax}">
      <tr>
        <td class="fondo_iconos"  align="center" height="39" valign="middle"><img src="./images/1nueva_p.gif" height="34" width="35">
        </td>
        <td class="fondobotonera" valign="middle"><a href="InitPromo.do?" >Nuevo Impuesto </a>
        </td>
      </tr>
    </c:if>
    <c:if test="${user.ableToReadTax}">
      <tr>
        <td class="fondo_iconos" align="center" height="39" valign="middle"><img src="./images/2ver_pro.gif" height="34" width="35">
        </td>
        
        <td class="fondobotonera" valign="middle"><a href="ListPromoPage.do?" >Ver Impuestos </a>
        </td>
        </a>
        </td>
      </tr>
  </c:if>
   <c:if test="${user.ableToAssociateTax}">
      <tr>
        <td class="fondo_iconos" align="center" height="39" valign="middle"><img src="./images/3asociar.gif" height="34" width="35">
        </td>
        <td class="fondobotonera" valign="middle">
          <a href="ListPromoXSucursal.do?command=searchPromoByBranch" >Asociar a Sucursales</a>
        </td>
      </tr>
    </c:if>
    <c:if test="${user.ableToCompileTax}">
      <tr>
        <td class="fondo_iconos" align="center" height="42" valign="middle"><img src="./images/5compila.gif" height="34" width="35">
        </td>
        <td class="fondobotonera" valign="middle"><a href="ShowSendPromo.do?" > Compilar Impuestos </a>
        </td> 
       </td>
      </tr>
    </c:if>
    <c:if test="${user.ableToConfigureGestorImpuesto}">
      <tr>
        <td class="fondo_iconos" align="center" height="42" valign="middle"><img src="./images/4perfile.gif" height="34" width="35">
        </td>
        <td class="fondobotonera" valign="middle">
          <a href="TypePromo.do?command=prepareForm" >Configuración </a>
        </td>
      </tr>
    </c:if>
    <!--  
    <tr>
      <td class="fondo_iconos" align="center" height="42" valign="middle"><img src="./images/6exportar.gif" height="34" width="35"></td>
      <td class="fondobotonera" valign="middle"><a href="ExportPromo.do?"/>Exportar Promociones </a></td>
    </tr>
    -->
    
</c:if>  
  
  
  
  <logic:notPresent name="user">
            <tr>
      <td class="fondobotonera" align="center" height="39" valign="middle"><img src="./images/1nueva_p.gif" height="34" width="35">
              </td>
      <td class="fondobotonera" valign="middle">Nueva Promoción</td>
    </tr>
    <tr>
      <td class="fondobotonera" align="center" height="39" valign="middle"><img src="./images/2ver_pro.gif" height="34" width="35">
              </td>
      <td class="fondobotonera" valign="middle">Ver Promociones</td>
    </tr>
    <tr>
      <td class="fondobotonera" align="center" height="39" valign="middle"><img src="./images/3asociar.gif" height="34" width="35">
              </td>
      <td class="fondobotonera" valign="middle">Asociar a Sucursales</td>
    </tr>
    <tr>
      <td class="fondobotonera" align="center" height="42" valign="middle"><img src="./images/5compila.gif" height="34" width="35">
              </td>
      <td class="fondobotonera" valign="middle">Compilar Promociones</td>
    </tr>
    <tr>
      <td class="fondobotonera" align="center" height="42" valign="middle"><img src="./images/4perfile.gif" height="34" width="35">
              </td>
      <td class="fondobotonera" valign="middle">Configuración</td>
    </tr>
    <!--  
    <tr>
      <td class="fondo_iconos" align="center" height="42" valign="middle"><img src="./images/6exportar.gif" height="34" width="35"></td>
      <td class="fondobotonera" valign="middle"><a href="ExportPromo.do?"/>Exportar Promociones </a></td>
    </tr>
    -->
  </logic:notPresent>

        </table>