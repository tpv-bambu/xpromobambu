<%@ page language="java" %>
<%@ taglib prefix="bean"  uri="/WEB-INF/struts-bean.tld" %>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
  <td height="20" valign="middle" class="menusuperior" background="./images/menu.jpg">
  	<div align="right">
      <a href="http://www.invel.com.ar"><bean:message key="footer.label.site.invel" /></a>&nbsp;&nbsp;&nbsp;
      <a href="mailto:mesadeayuda@invel.com.ar"><bean:message key="footer.label.contact.us" /></a>&nbsp;&nbsp;
    </div>
  </td>
</tr>
</table>