<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib prefix="bean" uri="/WEB-INF/struts-bean.tld"%>
<%@ taglib prefix="html" uri="/WEB-INF/struts-html.tld"%>
<%@ taglib prefix="logic" uri="/WEB-INF/struts-logic.tld"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<table width="100%">
  <logic:present scope="session" name="user">
    <tr>
      <td colspan="3" rowspan="8" class="fondomodulos" valign="top">
        <blockquote>
          <br> <span class="subtitulo">Gestor de Impuestos </span>
          <hr class="linea" color="#006600" noshade="noshade" size="1">
          <ul>
            <c:if test="${user.ableToCreateTax}">
              <li class="textomodulos1" type="square">Seleccione <span class="vinculostextos"><a
                  href="InitPromo.do?" />Nuevo Impuesto</a> </span> para crear un nuevo régimen de impuesto desde cero.</li>
            </c:if>
            <c:if test="${user.ableToReadTax}">
              <li class="textomodulos1" type="square"><span class="vinculostextos"><a href="ListPromoPage.do?" />Ver
                  Impuestos</a> </span> le permite navegar entre los distintos régimenes de impuestos creados y seleccionarlas para
                editarlos, borrarlos o editarlos.</li>
            </c:if>
            <c:if test="${user.ableToAssociateTax}">
              <li class="textomodulos1" type="square">Con <span class="vinculostextos"><a
                  href="ListPromoXSucursal.do?" />Asociar a Sucursales</a> </span> administra qué régimen impositivo pertenecen a cada
                sucursal.</li>

            </c:if>
            <c:if test="${user.ableToCompileTax}">
              <li class="textomodulos1" type="square"><span class="vinculostextos"><a href="ShowSendPromo.do?" />Compilar
                  Impuestos</a> </span> permite preparar el paquete completo que debe ser enviado a las cajas.</li>
            </c:if>
            <c:if test="${user.ableToConfigureGestorImpuesto}">
              <li class="textomodulos1" type="square"><span class="vinculostextos"><a
                  href="TypePromo.do?command=prepareForm" />Configuración</a> </span> permite modificar la descripción de un tipo de
                r&eacute;gimen impositivo.</li>
            </c:if>
          </ul>
        </blockquote>
      </td>
    </tr>

  </logic:present>
  <logic:notPresent name="user">
    <tr>
      <td align="center" class="textowarning">
        <ul>
          <html:messages message="true" id="msg">
            <li><bean:write name="msg" /></li>
          </html:messages>
        </ul>
      </td>
    </tr>
    <tr>
      <td colspan="3" rowspan="8" class="fondomodulos" valign="top">
        <blockquote>
          <br> <span class="subtitulo">Gestor de Impuestos </span>
          <hr class="linea" color="#006600" noshade="noshade" size="1">
          <ul>
            <li class="textomodulos1" type="square">Seleccione <b> Nuevo Impuesto </b> para crear un nuevo régimen impositivo desde cero.</li>
            <li class="textomodulos1" type="square"><span class=""><b>Ver impuestos</b> </span> le permite navegar entre
              ellas y seleccionarlas para edici&oacute;n.</li>
            <li class="textomodulos1" type="square">Con <span class=""><b>asociar a sucursales</b> </span> administra qu&eacute; regimen impositivo pertenece a cada sucursal.</li>
            <li class="textomodulos1" type="square"><span class=""><b>Compilar Impuestos</b> </span> permite preparar el
              paquete completo que debe ser enviado a las cajas.</li>
            <li class="textomodulos1" type="square"><span class=""><b>Configuración</b> </span> permite modificar la
              descripción de un tipo de r&eacute;gimen impositivo.</li>
          </ul>
          <span class="subtitulo"><bean:message key="message.login" /> </span> <br />
        </blockquote>
      </td>
  </logic:notPresent>
</table>
