<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html" prefix="html"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<table cellpadding="0" cellspacing="0" border="0" align="center" width="570">
  <tr>
    <td class="fondomodulos" valign="top">
    	<blockquote>
    		<div align="left" class="subtitulo"><br>
    			<span class="subtitulo">
    				<bean:message key="subtitle.associate.sucursal.promo"/>
    			</span>
    			<hr class="linea" color="#006600" noshade="noshade" size="1">
    		</div>
    	</blockquote>
    </td>
  </tr>
  <tr>
    <td align="center" valign="middle">
    	<div class="textowarning">
    	<ul>
				<html:messages id="msg" property="errors" message="true">
          <li>
	          <bean:write name="msg"/>
					</li>
	      </html:messages>		
			</ul>
    	</div>
    	<div class="textoconfirm">
    	<ul>
      		<html:messages id="msg" property="confirms" message="true">
        		<li>
        			<bean:write name="msg"/>
        		</li>
      		</html:messages>
    	</ul>    	
    	</div>
    </td>
  </tr>
</table>
<html:form action="ListPromoXSucursal.do" method="POST">
	<html:hidden property="command" value=""/>
  	<div align="center">
	  	<table align="center" width="500" cellpadding="3" cellspacing="0">
	    	<tr>
	    		<td align="left" colspan="2">
			   		<span class="textomodulos1">
	    				<bean:message key="label.associate.filter"/>
	    			</span>
	    			<!-- radio button filtrado por sucursales -->
	    			<html:radio property="filter" value="0" onclick="getPromosByBranch();">
		    			<span class="form1">
		    				<bean:message key="label.associate.filter.branch"/>
	    				</span>
	    			</html:radio>
	    			<!-- radio button filtrado por promociones -->
	    			<html:radio property="filter" value="1" onclick="getBranchByPromo();">
		    			<span class="form1">
		    				<bean:message key="label.associate.filter.promotion"/>
	    				</span>
	    			</html:radio>
	    		</td>
	    	</tr>
	    	<tr>
	      		<td align="left">
	      			<span class="textomodulos1">
		      			<c:if test="${prosucForm.filter == 0}">
	      					<bean:message key="label.associate.filter.branch" />:
	      				</c:if>
		      			<c:if test="${prosucForm.filter == 1}">
	      					<bean:message key="label.associate.filter.promotion" />:
	      				</c:if>
	      			</span>
	    			<!-- select sucursales -->
	      			<c:if test="${prosucForm.filter == 0}">
		      			<html:select name="prosucForm" property="sucursal" styleClass="form" onchange="getPromosByBranch();" style="width:200px;">
	    	    			<html:options collection="sucursales" property="cod_sucursal" labelProperty="descripcion" />
	      				</html:select>
	      			</c:if>
	    			<!-- select promociones -->
		   			<c:if test="${prosucForm.filter == 1}">
		      			<html:select name="prosucForm" property="promotion" styleClass="form" onchange="getBranchByPromo();" style="width:250px;">
	    	    			<html:options collection="promosEnabled" property="internalId" labelProperty="description" />
	      				</html:select>
	    			</c:if>
	      		</td>
	      		<td align="right" class="textomodulos1">
		      		<!-- check select/deselect -->
							<input id="idCheckall" type="checkbox" name="checkall" onclick="checkUncheckAll(this);"
								value="<bean:message key="button.selectall"/>"/>
							<bean:message key="button.selectall"/>
	      		</td>
	      		<td align="right">
	      		</td>
	    	</tr>
	  	</table>
	   	<table>
			<tr>
				<td></td>
				<td align="left">
					<!-- tabla columnas de la grilla -->
					<table class="marco"  cellpadding="3" cellspacing="0" width="508">
						<tr class="cabeceratabla" align="center">
							<TD width="5%"></TD>
							<TD align="center" width="10%">
								<bean:message key="label.column.cod"/>
							</TD>
							<TD align="center" width="85%">
								<bean:message key="label.column.descripcion"/>
							</TD>
						</tr>
					</table>
					<table class="marco" width="500">
						<tr>
							<td>
								<div align="left" style="width: 500; height: 300; overflow: auto;">
									<table class="marco" cellpadding="3" cellspacing="0" width="500">
										<!-- muestro la grilla de promociones -->
						      			<c:if test="${prosucForm.filter == 0}">
											<!-- verificamos sino esta vacia la collecciòn del form de la acciòn -->
											<logic:notEmpty name="prosucForm" property="promociones">
												<!-- iteramos la collecciòn para generar los datos de la grilla -->
								      			<logic:iterate id="promo" name="prosucForm" property="promociones">
								        			<TR>
								          				<td class="enabled" width="5%">
								       	   					<input id="promoSelected" type="checkbox" name="promo_selected"
																value="<bean:write name="promo" property="internalId"/>"
								            					<logic:equal name="promo" property="selected" value="true">
								               						checked="checked"
								            					</logic:equal>
															/>
								          				</td>						        			
								          				<TD valign="top" class="fondotabla" align="right" width="10%">
								          					<bean:write name="promo" property="internalId"/>
								          				</TD>
								          				<TD valign="top" class="fondobotonera" width="85%">
								          					<bean:write name="promo" property="description"/>
								          				</TD>
								        			</TR>
								      			</logic:iterate>
								    		</logic:notEmpty>
						      			</c:if>
										<!-- mostramos las girlla de sucursales -->
										<c:if test="${prosucForm.filter == 1}">
											<!-- verificamos sino esta vacia la collecciòn del form de la acciòn -->
											<logic:notEmpty name="prosucForm" property="sucursales">
												<!-- iteramos la collecciòn para generar los datos de la grilla -->
								      			<logic:iterate id="suc" name="prosucForm" property="sucursales">
								        			<TR>
								          				<td class="enabled" width="5%">
								       	   					<input id="sucSelected" type="checkbox" name="suc_selected"
																value="<bean:write name="suc" property="cod_sucursal"/>"
																<logic:equal name="suc" property="selected" value="true">
								               						checked="checked"
								            					</logic:equal>
															/>
								          				</td>						        			
								          				<TD valign="top" class="fondotabla" align="right" width="10%">
								          					<bean:write name="suc" property="cod_sucursal"/>
								          				</TD>
								          				<TD valign="top" class="fondobotonera" width="85%">
								          					<bean:write name="suc" property="descripcion"/>
								          				</TD>
								        			</TR>
								      			</logic:iterate>
								    		</logic:notEmpty>
										</c:if>									
									</table>
								</div>
							</td>
						</tr>
					</table>	
				</td>
			</tr>
		</table>
	  	<br>
	  	<table align="center" cellpadding="3" cellspacing="0" width="500">
	    	<TR>
	    		<td>
					<!-- boton acpetar -->
					<html:button property="methodToCall" styleClass="form" style="cursor:pointer;"
						onclick="javascript:asociatePromoBranch();">
						<bean:message key="button.accept" />
					</html:button>     		
	    		</td>
	      		<TD>
	      			<div align="right">
	      				<span class="vinculostextos"><a href="InitMainPromo.do?" />
	      					<bean:message key="label.back" /></a>
	      				</span>
	      			</div>
	      		</TD>
	    	</TR>
	  	</table>
  	</div>
</html:form>

