<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html" prefix="html"%>

<table width="570" align="center">
  <tr>
    <td class="fondomodulos">
    <blockquote>
    <div align="left" class="subtitle"><logic:present name="showxmlform">
      <span class="subtitulo"><br>
      <bean:message key="label.type.promo" />: <bean:write name="showxmlform" property="stringTypePromo" /></span>
    </logic:present>
    <hr class="linea" color="#006600" noshade="noshade" size="1">
    </div>
    </blockquote>
    </td>
  </tr>
  <tr>
    <td align="center" valign="middle">
    <ul>
      <html:messages message="true" id="msg">
        <li><bean:write name="msg" /></li>
      </html:messages>
    </ul>
    </td>
  </tr>
</table>

<html:form action="SaveCopyPromo.do" method="POST">

  <table align="center">
  	<tr>
  	  <td>
      <html:submit property="command" styleClass="form">
        <bean:message key="button.return" />
      </html:submit>
      </td>
    </tr>
  </table>

</html:form>

