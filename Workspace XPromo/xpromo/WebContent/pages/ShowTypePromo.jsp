<%@ taglib uri="/WEB-INF/struts-bean" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html" prefix="html"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div align="center">
  <table cellpadding="0" cellspacing="0" border="0" align="center" width="500">
    <tr>

      <td align="center" class="textowarning">
        <ul>
          <html:messages message="true" id="msg">
            <li><bean:write name="msg" /></li>
          </html:messages>
        </ul>
      </td>
    </tr>
    <tr>
      <td class="fondomodulos" valign="top">
        <blockquote>
          <html:form action="/InitPromo.do">
            <c:if test="${!user.impuesto}">
              <div class="subtitulo" align="left">
                <br> <span class="subtitulo"> <bean:message key="label.group" /> </span>
                <hr class="linea" color="#006600" noshade="noshade" size="1">
                <html:select name="initCreatePromoForm" property="description" styleClass="form">
                  <html:options collection="xformtree" property="value" labelProperty="label" />
                </html:select>
                <html:submit property="command" styleClass="form">
                  <bean:message key="button.filter" />
                </html:submit>
              </div>
            </c:if>
          </html:form>

          <div class="subtitulo" align="left">
            <br> <span class="subtitulo"><bean:message key="app.promociones" /> </span>
            <hr class="linea" color="#006600" noshade="noshade" size="1">
          </div>
        </blockquote>
        <blockquote>
          <div align="left" style="width: 500; height: 300; overflow: auto;">
            <table cellpadding="0" cellspacing="0" width="100%">
              <logic:iterate id="typepromovo" name="xformlist">
                <tr>
                  <td align="left">
                    <li valign=top class="vinculostextos" onmouseover="Tip('<bean:write name="typepromovo" property="documentation"/>',TITLE,'<bean:write name="typepromovo" property="description" />')"><a
                      href="ShowFormPromo.do?typepromo=<bean:write name="typepromovo" property="id"/>&command=init"> <bean:write name="typepromovo" property="description" /> </a>
                  </li>
                  </td>
                </tr>
              </logic:iterate>
            </table>
          </div>
        </blockquote>
      </td>
    </tr>
  </table>
  <br>
  <!-- tabla link volver -->
  <table align="center" cellpadding="3" cellspacing="0" width="500">
    <TR>
      <TD>
        <div align="right">
          <span class="vinculostextos"><a href="InitMainPromo.do?" /> <bean:message key="label.back" /></a> </span>
        </div>
      </TD>
    </TR>
  </table>
  <br>
</div>
