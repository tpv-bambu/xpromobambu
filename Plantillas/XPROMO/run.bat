@echo off

set localpath=common
set pcpath=pc

IF EXIST promo.pro del promo.pro
IF EXIST promo.cod del promo.cod
xsltproc --stringparam pathProgXml ../prog.xml -o promo.pro %localpath%\prog.xsl %localpath%\tree.xml
IF EXIST promo.pro %pcpath%\pc promo.pro -o promo.cod

IF EXIST promo.sql del promo.sql
xsltproc -o promo.sql %localpath%\consultas.xsl prog.xml

pause
