Promo 67 clasificacion descuentocompleto ticket
Esta promo permite realizar descuentos a grupos de art�culos que cumplan con la estructura mercadologica definida en la promo.

Cuando nos referimos a la clasificaci�n en la estructura M nos nos referimos a los distintos niveles donde se puede clasificar el articulo en la estructura de productos.
Para poner un ejemplo el articulo  Yerba CBS x 500gr esta definida en el Departamento Almacen ( 1) , dentro del departamento puede ser el rubro Yerbas ( 147) y dentro del rubro puede ser la familia CBS ( 101) y ademas ... etc etc.
Ese datos de la estructura del producto viene en el campo cod_clasificacion de la tabla articulos que es un string
DEPARTAMENTO =001
RUBRO = 147
FAMILIA =101
Entonces si vamos a manejar 3 niveles el codigo de clasificacion seria 001147101.
La promo clasificaci�n comun es decir la plantilla 66 (Clasificacion Descuento Ticket)  permite definir varias condiciones de los 3 niveles pero no son agrupadas, es decir si ponemos estos 3 datos en la plantilla 66 se va a promocionar cualquier producto del depto 101, cualquier producto del rubro 147 o cualquier producto de la familia 101.. Si se quiere que se cumplan las 3 condiciones ahi hay que usar la plantilla 67 nueva.( Clasificacion Completa Descuento ticket) que si engloba las 3 clasificaciones simultaneas.



Promo 77 bambu un medio descuento
No usable o poco usable

Promo 78 bambu varios medios descuento ( promo postpago estandard)
Es una promo que permite generar un descuento en el ticket si se paga con el medio,submedio y plan configurado. SOlo se puede usar esos medios para activar la promo. Si por ejemplo se pone efectivo y se paga con efectivo y tarjeta no va a dejar terminar el ticket

Promo 79 promo descuento escalonada
Esta promoci�n permite generar descuentos distintos segun la cantidad a llevar de articulos. De esa forma quizas al llevar mas articulos se obtiene un descuento mayor
En este ejemplo llevando entre 3 a 5 cocas se obtiene un descuento en pesos de $5 sobre el total. Es decir si las cocas salen $18 individualmente, el ticket saldria 18x3 = 54 las cocas - $5 (descuento)
Si se llevaran 4 seria 18 x4 - $5
Ahora si se llevan 6 el descuento total seria de $10. Y llevando 9 o mas seria de $15
La unica contra que veo es que conviene en vez de llevar 9, conviene llevar 3 tandas de 3. Asi que eso tiene que tenerse cuidado


Promo 80 grupo parcial descuento
Esta promo permite generar un descuento porcentual a una cantidad minima de productos que se hayan tickeado dentro un grupo mas grande definido.
De esta manera se puede cargar una lista de productos y definir a partir de que cantidad de los articulos tickeados se realizar� el descuento % de esos mismos
Ejemplo de la lista de 5 articulos , si se tickean 4 como minimo se har� el 30% de descuento a los articulos tickeado, hasta un limite de 8 grupos. ( 1 grupo son 4 articulos, asi que 8 grupos seran 32 articulos).


Promo 81 mxn grupo parcial 
Esta promoci�n es una tipica MXN pero a diferencia de la promo estandard que la cantidad M y N eran sobre el mismo articulo, aqui se permite tomar la cantidad de la lista de articulos para considerar el M.
Es decir por ejemplo hay una promo 3x2 es decir llevando 3 paga 2, esos 3 pueden ser 3 articulos distintos de la lista desplegada

Promo 82 depto plan pago descuento
Esta plantilla se hizo para ser una promo postpago con medio de pago que afecte solo a los departamentos configurados.

Se debe configurar en bloque =1 es decir post-pago. cargar el o los departamentos de la listas. Luego el porcentaje de descuento y finalmente los planes de pago a los que se le aplicar�


Documentacion
https://dbambu.atlassian.net/wiki/spaces/DDP/pages/96823478/Nuevas+Plantillas