
// PROMOCIONES GENERADAS CON VERSION DEL SISTEMA DE PLANTILLAS Nro: 1.23.002 - (28/02/13)

		// ************************ Inicializacion Promos PRE-PAGO ************************ //
Promotion prepago prepago_init
Parameters
	global wpurch = 0;
	global excluidos = {};
	global excluir = false;
	global beneficios = 0;
	global beneficiados = {};
	global tarjeta = "";
	global per = 0;
//	global dinero_ahorrado = 0;
  global primera_vez = true;
	global anteriores = {};

  
	global monto_benef_promo65 = 0;
  

Benefits
	wpurch = $wholepurchase;
	excluidos = excluded;
	tarjeta = stringRNV(ram_cuenta_cliente);
	per = numRNV(ram_perfil);

		

  // ******  INICIO BLOQUE **************************************************************** //
  // ******  BLOQUE NAME = Bloque PREPAGO
  // ******  BLOQUE ID = 0
  // ******  CASH2BENEF =  1


//************** BLOQUE DE PROMOCIONES ********************
// Seccion Inicial

Promotion prepago bloque_seccion_inicial_0

// Description:

Parameters

	extern excluir;
		    
	extern excluidos;
	extern beneficios;
	extern beneficiados;
	global excluir_0 = false;


	  
  global benefits_otorgados_0_0 = 0;

  global benefits_otorgados_0_1 = 0;

  global benefits_otorgados_0_2 = 0;

  global benefits_otorgados_0_3 = 0;

  global benefits_otorgados_0_4 = 0;

  global benefits_otorgados_0_5 = 0;

  global benefits_otorgados_0_6 = 0;

  global benefits_otorgados_0_7 = 0;

  global benefits_otorgados_0_8 = 0;

  global benefits_otorgados_0_9 = 0;

  global benefits_otorgados_0_10 = 0;

  global benefits_otorgados_0_11 = 0;


Conditions


Benefits

	  

  skip;


  // ********  PROMOCIONES DEL BLOQUE 0 **************************************************** //


	  
		// ************************ CONFIGURACIONES MOTOR DE PROMOCIONES ************************ //
Promotion prepago global_config
Benefits
	numRNV(cupon_error) = 11;
	numRNV(cupon_vigencia) = 5;
	stringRNV(cupon_limite) = "10 de Octubre de 2010";


// FIN DE PROMO 0 del BLOQUE 0

Promotion prepago undo_0_0
Parameters
//	extern excluidos_0;
	extern excluidos;
Conditions
Benefits
//	excluded = excluidos_0;
	excluded = excluidos;
	skip;

// --- *** ---


	    
		

//********** Promoción Invel: Grupo Parcial precio fijo en ticket *************

Promotion prepago grupo_parcial_precio_fijo_ticket_0_1





PreConditions


	date in [2016-01-28,2016-02-03];

log(file, "Promo "++ 579 ++ ": VIGENTE");
	

	!excluir_0;
		    	
	requireAll {(article x111269_7790080065312,2), (article x111627_7790080065299,2), (article x110967_7790080065305,2), (article x111184_7790080065282,2)};



Parameters

	extern excluir;
		    

	extern excluir_0;
//	extern beneficiados;
				    


	maximo_grupos = 2;
	grupo = {(article x111269_7790080065312,2), (article x111627_7790080065299,2), (article x110967_7790080065305,2), (article x111184_7790080065282,2)};

	precio_fijo = 24.98; // precio fijo por grupo
	precio_real = $grupo;


	comprados = (purchase ) meet grupo;



	global publicar_0_1 = {};
	global publicar_arts_0_1 = {};

	extern excluidos;
	extern benefits_otorgados_0_1;
	extern beneficiados;


Conditions




	|comprados| == |grupo|;
	precio_fijo >= 999999;


Benefits

	c = maximo_grupos;
	for (a,k) in comprados do
		c = c min &(k / grupo.a);
	od;

	descuento = c * (precio_real - precio_fijo);

	for (a,k) in grupo do
		publicar_0_1 = publicar_0_1 + {(a, descuento * (${(a,k)} / precio_real))};
		publicar_arts_0_1 = publicar_arts_0_1 + {(a,k)};
	od;

	benefits_otorgados_0_1 = \publicar_0_1\ *  1;

	excluidos = excluidos + publicar_arts_0_1;
			// se excluyen los artículos bonificados para que no participen
			// de nuevas promociones.
	

	blog(file, "Promo "++ 579 ++ ": LATENTE");



// FIN DE PROMO 1 del BLOQUE 0

Promotion prepago undo_0_1
Parameters
//	extern excluidos_0;
	extern excluidos;
Conditions
Benefits
//	excluded = excluidos_0;
	excluded = excluidos;
	skip;

// --- *** ---


	    
		

//********** Promoción Invel: Grupo Parcial precio fijo en ticket *************

Promotion prepago grupo_parcial_precio_fijo_ticket_0_2





PreConditions


	date in [2016-02-04,2016-02-10];

log(file, "Promo "++ 585 ++ ": VIGENTE");
	

	!excluir_0;
		    	
	requireAll {(article x109372_7790080068405,1), (article x16660_7790080068412,1), (article x109125_7790080068429,1)};



Parameters

	extern excluir;
		    

	extern excluir_0;
//	extern beneficiados;
				    


	maximo_grupos = 2;
	grupo = {(article x109372_7790080068405,1), (article x16660_7790080068412,1), (article x109125_7790080068429,1)};

	precio_fijo = 21.58; // precio fijo por grupo
	precio_real = $grupo;


	comprados = (purchase ) meet grupo;



	global publicar_0_2 = {};
	global publicar_arts_0_2 = {};

	extern excluidos;
	extern benefits_otorgados_0_2;
	extern beneficiados;


Conditions




	|comprados| == |grupo|;
	precio_fijo >= 999999;


Benefits

	c = maximo_grupos;
	for (a,k) in comprados do
		c = c min &(k / grupo.a);
	od;

	descuento = c * (precio_real - precio_fijo);

	for (a,k) in grupo do
		publicar_0_2 = publicar_0_2 + {(a, descuento * (${(a,k)} / precio_real))};
		publicar_arts_0_2 = publicar_arts_0_2 + {(a,k)};
	od;

	benefits_otorgados_0_2 = \publicar_0_2\ *  1;

	excluidos = excluidos + publicar_arts_0_2;
			// se excluyen los artículos bonificados para que no participen
			// de nuevas promociones.
	

	blog(file, "Promo "++ 585 ++ ": LATENTE");



// FIN DE PROMO 2 del BLOQUE 0

Promotion prepago undo_0_2
Parameters
//	extern excluidos_0;
	extern excluidos;
Conditions
Benefits
//	excluded = excluidos_0;
	excluded = excluidos;
	skip;

// --- *** ---


	    
		

//********** Promoción Invel: Grupo completo precio fijo en ticket *************

Promotion prepago grupo_completo_precio_fijo_ticket_0_3





PreConditions


	date in [2016-01-28,2016-02-03];

log(file, "Promo "++ 587 ++ ": VIGENTE");
	

	!excluir_0;
		    	
	requireAll {(article x102429_7790080037401,2)};



Parameters

	extern excluir;
		    

	extern excluir_0;
//	extern beneficiados;
				    


	maximo_grupos = 1000;
	grupo = {(article x102429_7790080037401,2)};

	precio_fijo = 19.98; // precio fijo por grupo
	precio_real = $grupo;


	comprados = (purchase ) meet grupo;



	global publicar_0_3 = {};
	global publicar_arts_0_3 = {};

	extern wpurch;

	extern excluidos;
	extern benefits_otorgados_0_3;
	extern beneficiados;


Conditions




	|comprados| == |grupo|;
	precio_real >= precio_fijo;


Benefits

	c = maximo_grupos;
	for (a,k) in comprados do
		c = c min &(k / grupo.a);
	od;

	descuento = c * (precio_real - precio_fijo);

	for (a,k) in grupo do
		publicar_0_3 = publicar_0_3 + {(a, descuento * (${(a,k)} / precio_real))};
		publicar_arts_0_3 = publicar_arts_0_3 + {(a,k)};
	od;

	benefits_otorgados_0_3 = \publicar_0_3\ *  1;

	excluidos = excluidos + publicar_arts_0_3;
			// se excluyen los artículos bonificados para que no participen
			// de nuevas promociones.
	

	blog(file, "Promo "++ 587 ++ ": LATENTE");



// FIN DE PROMO 3 del BLOQUE 0

Promotion prepago undo_0_3
Parameters
//	extern excluidos_0;
	extern excluidos;
Conditions
Benefits
//	excluded = excluidos_0;
	excluded = excluidos;
	skip;

// --- *** ---


	    
		

//********** Promoción Invel: Grupo Parcial precio fijo en ticket *************

Promotion prepago grupo_parcial_precio_fijo_ticket_0_4





PreConditions


	date in [2016-01-28,2016-02-03];

log(file, "Promo "++ 588 ++ ": VIGENTE");
	

	!excluir_0;
		    	
	requireAll {(article x109389_7790080068443,2), (article x109390_7790080068436,2), (article x109391_7790080068467,2), (article x109392_7790080067835,2), (article x109393_7790080068450,2)};



Parameters

	extern excluir;
		    

	extern excluir_0;
//	extern beneficiados;
				    


	maximo_grupos = 2;
	grupo = {(article x109389_7790080068443,2), (article x109390_7790080068436,2), (article x109391_7790080068467,2), (article x109392_7790080067835,2), (article x109393_7790080068450,2)};

	precio_fijo = 18.58; // precio fijo por grupo
	precio_real = $grupo;


	comprados = (purchase ) meet grupo;



	global publicar_0_4 = {};
	global publicar_arts_0_4 = {};

	extern excluidos;
	extern benefits_otorgados_0_4;
	extern beneficiados;


Conditions




	|comprados| == |grupo|;
	precio_fijo >= 999999;


Benefits

	c = maximo_grupos;
	for (a,k) in comprados do
		c = c min &(k / grupo.a);
	od;

	descuento = c * (precio_real - precio_fijo);

	for (a,k) in grupo do
		publicar_0_4 = publicar_0_4 + {(a, descuento * (${(a,k)} / precio_real))};
		publicar_arts_0_4 = publicar_arts_0_4 + {(a,k)};
	od;

	benefits_otorgados_0_4 = \publicar_0_4\ *  1;

	excluidos = excluidos + publicar_arts_0_4;
			// se excluyen los artículos bonificados para que no participen
			// de nuevas promociones.
	

	blog(file, "Promo "++ 588 ++ ": LATENTE");



// FIN DE PROMO 4 del BLOQUE 0

Promotion prepago undo_0_4
Parameters
//	extern excluidos_0;
	extern excluidos;
Conditions
Benefits
//	excluded = excluidos_0;
	excluded = excluidos;
	skip;

// --- *** ---


	    
		

//********** Promoción Invel: Grupo Parcial precio fijo en ticket *************

Promotion prepago grupo_parcial_precio_fijo_ticket_0_5





PreConditions


	date in [2016-01-28,2016-02-03];

log(file, "Promo "++ 592 ++ ": VIGENTE");
	

	!excluir_0;
		    	
	requireAll {(article x109379_7790080067873,2), (article x109378_7790080067958,2), (article x109380_7790080067965,2)};



Parameters

	extern excluir;
		    

	extern excluir_0;
//	extern beneficiados;
				    


	maximo_grupos = 2;
	grupo = {(article x109379_7790080067873,2), (article x109378_7790080067958,2), (article x109380_7790080067965,2)};

	precio_fijo = 18.58; // precio fijo por grupo
	precio_real = $grupo;


	comprados = (purchase ) meet grupo;



	global publicar_0_5 = {};
	global publicar_arts_0_5 = {};

	extern excluidos;
	extern benefits_otorgados_0_5;
	extern beneficiados;


Conditions




	|comprados| == |grupo|;
	precio_fijo >= 999999;


Benefits

	c = maximo_grupos;
	for (a,k) in comprados do
		c = c min &(k / grupo.a);
	od;

	descuento = c * (precio_real - precio_fijo);

	for (a,k) in grupo do
		publicar_0_5 = publicar_0_5 + {(a, descuento * (${(a,k)} / precio_real))};
		publicar_arts_0_5 = publicar_arts_0_5 + {(a,k)};
	od;

	benefits_otorgados_0_5 = \publicar_0_5\ *  1;

	excluidos = excluidos + publicar_arts_0_5;
			// se excluyen los artículos bonificados para que no participen
			// de nuevas promociones.
	

	blog(file, "Promo "++ 592 ++ ": LATENTE");



// FIN DE PROMO 5 del BLOQUE 0

Promotion prepago undo_0_5
Parameters
//	extern excluidos_0;
	extern excluidos;
Conditions
Benefits
//	excluded = excluidos_0;
	excluded = excluidos;
	skip;

// --- *** ---


	    
		

//********** Promoción Invel: Grupo completo precio fijo en ticket *************

Promotion prepago grupo_completo_precio_fijo_ticket_0_6





PreConditions


	date in [2016-01-28,2016-02-03];

log(file, "Promo "++ 593 ++ ": VIGENTE");
	

	!excluir_0;
		    	
	requireAll {(article x108644_7790080067903,2)};



Parameters

	extern excluir;
		    

	extern excluir_0;
//	extern beneficiados;
				    


	maximo_grupos = 1000;
	grupo = {(article x108644_7790080067903,2)};

	precio_fijo = 21.18; // precio fijo por grupo
	precio_real = $grupo;


	comprados = (purchase ) meet grupo;



	global publicar_0_6 = {};
	global publicar_arts_0_6 = {};

	extern wpurch;

	extern excluidos;
	extern benefits_otorgados_0_6;
	extern beneficiados;


Conditions




	|comprados| == |grupo|;
	precio_real >= precio_fijo;


Benefits

	c = maximo_grupos;
	for (a,k) in comprados do
		c = c min &(k / grupo.a);
	od;

	descuento = c * (precio_real - precio_fijo);

	for (a,k) in grupo do
		publicar_0_6 = publicar_0_6 + {(a, descuento * (${(a,k)} / precio_real))};
		publicar_arts_0_6 = publicar_arts_0_6 + {(a,k)};
	od;

	benefits_otorgados_0_6 = \publicar_0_6\ *  1;

	excluidos = excluidos + publicar_arts_0_6;
			// se excluyen los artículos bonificados para que no participen
			// de nuevas promociones.
	

	blog(file, "Promo "++ 593 ++ ": LATENTE");



// FIN DE PROMO 6 del BLOQUE 0

Promotion prepago undo_0_6
Parameters
//	extern excluidos_0;
	extern excluidos;
Conditions
Benefits
//	excluded = excluidos_0;
	excluded = excluidos;
	skip;

// --- *** ---


	    
		

//********** Promoción Invel: Grupo Parcial precio fijo en ticket *************

Promotion prepago grupo_parcial_precio_fijo_ticket_0_7





PreConditions


	date in [2016-02-01,2016-02-03];

log(file, "Promo "++ 594 ++ ": VIGENTE");
	

	!excluir_0;
		    	
	requireAll {(article x16885_7793940651021,2), (article x16889_7793940651106,2), (article x17183_7793940651113,2)};



Parameters

	extern excluir;
		    

	extern excluir_0;
//	extern beneficiados;
				    


	maximo_grupos = 2;
	grupo = {(article x16885_7793940651021,2), (article x16889_7793940651106,2), (article x17183_7793940651113,2)};

	precio_fijo = 15.88; // precio fijo por grupo
	precio_real = $grupo;


	comprados = (purchase ) meet grupo;



	global publicar_0_7 = {};
	global publicar_arts_0_7 = {};

	extern excluidos;
	extern benefits_otorgados_0_7;
	extern beneficiados;


Conditions




	|comprados| == |grupo|;
	precio_fijo >= 999999;


Benefits

	c = maximo_grupos;
	for (a,k) in comprados do
		c = c min &(k / grupo.a);
	od;

	descuento = c * (precio_real - precio_fijo);

	for (a,k) in grupo do
		publicar_0_7 = publicar_0_7 + {(a, descuento * (${(a,k)} / precio_real))};
		publicar_arts_0_7 = publicar_arts_0_7 + {(a,k)};
	od;

	benefits_otorgados_0_7 = \publicar_0_7\ *  1;

	excluidos = excluidos + publicar_arts_0_7;
			// se excluyen los artículos bonificados para que no participen
			// de nuevas promociones.
	

	blog(file, "Promo "++ 594 ++ ": LATENTE");



// FIN DE PROMO 7 del BLOQUE 0

Promotion prepago undo_0_7
Parameters
//	extern excluidos_0;
	extern excluidos;
Conditions
Benefits
//	excluded = excluidos_0;
	excluded = excluidos;
	skip;

// --- *** ---


	    
		

//********** Promoción Invel: Grupo Parcial precio fijo en ticket *************

Promotion prepago grupo_parcial_precio_fijo_ticket_0_8





PreConditions


	date in [2016-02-04,2016-02-10];

log(file, "Promo "++ 595 ++ ": VIGENTE");
	

	!excluir_0;
		    	
	requireAll {(article x101161_7790315000880,2), (article x97159_7798062540284,2), (article x97155_7798062540291,2), (article x107653_7798062540734,2), (article x101162_7798062541014,2), (article x104720_7798062541700,2), (article x104721_7798062541731,2), (article x104182_7798062541762,2), (article x107447_7798062541878,2), (article x109740_7798062542356,2), (article x109744_7798062542370,2)};



Parameters

	extern excluir;
		    

	extern excluir_0;
//	extern beneficiados;
				    


	maximo_grupos = 2;
	grupo = {(article x101161_7790315000880,2), (article x97159_7798062540284,2), (article x97155_7798062540291,2), (article x107653_7798062540734,2), (article x101162_7798062541014,2), (article x104720_7798062541700,2), (article x104721_7798062541731,2), (article x104182_7798062541762,2), (article x107447_7798062541878,2), (article x109740_7798062542356,2), (article x109744_7798062542370,2)};

	precio_fijo = 39.98; // precio fijo por grupo
	precio_real = $grupo;


	comprados = (purchase ) meet grupo;



	global publicar_0_8 = {};
	global publicar_arts_0_8 = {};

	extern excluidos;
	extern benefits_otorgados_0_8;
	extern beneficiados;


Conditions




	|comprados| == |grupo|;
	precio_fijo >= 999999;


Benefits

	c = maximo_grupos;
	for (a,k) in comprados do
		c = c min &(k / grupo.a);
	od;

	descuento = c * (precio_real - precio_fijo);

	for (a,k) in grupo do
		publicar_0_8 = publicar_0_8 + {(a, descuento * (${(a,k)} / precio_real))};
		publicar_arts_0_8 = publicar_arts_0_8 + {(a,k)};
	od;

	benefits_otorgados_0_8 = \publicar_0_8\ *  1;

	excluidos = excluidos + publicar_arts_0_8;
			// se excluyen los artículos bonificados para que no participen
			// de nuevas promociones.
	

	blog(file, "Promo "++ 595 ++ ": LATENTE");



// FIN DE PROMO 8 del BLOQUE 0

Promotion prepago undo_0_8
Parameters
//	extern excluidos_0;
	extern excluidos;
Conditions
Benefits
//	excluded = excluidos_0;
	excluded = excluidos;
	skip;

// --- *** ---


	    
		

//********** Promoción Invel: Grupo Parcial precio fijo en ticket *************

Promotion prepago grupo_parcial_precio_fijo_ticket_0_9





PreConditions


	date in [2016-02-04,2016-02-10];

log(file, "Promo "++ 597 ++ ": VIGENTE");
	

	!excluir_0;
		    	
	requireAll {(article x102059_7790080065152,1), (article x102087_7790080065169,1)};



Parameters

	extern excluir;
		    

	extern excluir_0;
//	extern beneficiados;
				    


	maximo_grupos = 2;
	grupo = {(article x102059_7790080065152,1), (article x102087_7790080065169,1)};

	precio_fijo = 15.38; // precio fijo por grupo
	precio_real = $grupo;


	comprados = (purchase ) meet grupo;



	global publicar_0_9 = {};
	global publicar_arts_0_9 = {};

	global descarga_0_9 = article x_;

	extern excluidos;
	extern benefits_otorgados_0_9;
	extern beneficiados;


Conditions




	|comprados| == |grupo|;
	precio_fijo >= 999999;


Benefits

	c = maximo_grupos;
	for (a,k) in comprados do
		c = c min &(k / grupo.a);
	od;

	descuento = c * (precio_real - precio_fijo);

	for (a,k) in grupo do
		publicar_0_9 = publicar_0_9 + {(a, descuento * (${(a,k)} / precio_real))};
		publicar_arts_0_9 = publicar_arts_0_9 + {(a,k)};
	od;

	benefits_otorgados_0_9 = \publicar_0_9\ *  1;

	excluidos = excluidos + publicar_arts_0_9;
			// se excluyen los artículos bonificados para que no participen
			// de nuevas promociones.
	

	blog(file, "Promo "++ 597 ++ ": LATENTE");



// FIN DE PROMO 9 del BLOQUE 0

Promotion prepago undo_0_9
Parameters
//	extern excluidos_0;
	extern excluidos;
Conditions
Benefits
//	excluded = excluidos_0;
	excluded = excluidos;
	skip;

// --- *** ---


	    
		

//********** Promoción Invel: Grupo Parcial precio fijo en ticket *************

Promotion prepago grupo_parcial_precio_fijo_ticket_0_10





PreConditions


	date in [2016-02-04,2016-02-10];

log(file, "Promo "++ 598 ++ ": VIGENTE");
	

	!excluir_0;
		    	
	requireAll {(article x109395_7790080068511,1), (article x109394_7790080068528,1)};



Parameters

	extern excluir;
		    

	extern excluir_0;
//	extern beneficiados;
				    


	maximo_grupos = 2;
	grupo = {(article x109395_7790080068511,1), (article x109394_7790080068528,1)};

	precio_fijo = 22.58; // precio fijo por grupo
	precio_real = $grupo;


	comprados = (purchase ) meet grupo;



	global publicar_0_10 = {};
	global publicar_arts_0_10 = {};

	extern excluidos;
	extern benefits_otorgados_0_10;
	extern beneficiados;


Conditions




	|comprados| == |grupo|;
	precio_fijo >= 999999;


Benefits

	c = maximo_grupos;
	for (a,k) in comprados do
		c = c min &(k / grupo.a);
	od;

	descuento = c * (precio_real - precio_fijo);

	for (a,k) in grupo do
		publicar_0_10 = publicar_0_10 + {(a, descuento * (${(a,k)} / precio_real))};
		publicar_arts_0_10 = publicar_arts_0_10 + {(a,k)};
	od;

	benefits_otorgados_0_10 = \publicar_0_10\ *  1;

	excluidos = excluidos + publicar_arts_0_10;
			// se excluyen los artículos bonificados para que no participen
			// de nuevas promociones.
	

	blog(file, "Promo "++ 598 ++ ": LATENTE");



// FIN DE PROMO 10 del BLOQUE 0

Promotion prepago undo_0_10
Parameters
//	extern excluidos_0;
	extern excluidos;
Conditions
Benefits
//	excluded = excluidos_0;
	excluded = excluidos;
	skip;

// --- *** ---


	    
		

//********** Promoción Invel: Grupo Parcial precio fijo en ticket *************

Promotion prepago grupo_parcial_precio_fijo_ticket_0_11





PreConditions


	date in [2016-02-04,2016-02-10];

log(file, "Promo "++ 599 ++ ": VIGENTE");
	

	!excluir_0;
		    	
	requireAll {(article x14696_7501001133276,1), (article x15500_7501001133290,1), (article x100548_7501001244323,1), (article x95905_7506195120347,1), (article x101562_7506195143353,1), (article x101889_7506295355076,1), (article x104424_7506309805566,1), (article x105172_7506309805610,1), (article x104387_7506309810362,1), (article x106855_7506309894492,1), (article x105121_7506309895222,1), (article x105124_7506309895246,1), (article x105125_7506309895284,1), (article x106857_7506309895307,1), (article x105153_7506309895338,1), (article x105160_7506309895369,1), (article x105164_7506309895383,1), (article x105151_7506309895437,1), (article x106853_7506309897516,1), (article x105156_7506309897547,1), (article x105162_7506309897578,1), (article x105154_7506309897592,1), (article x109922_7506339346039,1), (article x109834_7506339346091,1), (article x108320_7506339393064,1), (article x108330_7506339393088,1)};



Parameters

	extern excluir;
		    

	extern excluir_0;
//	extern beneficiados;
				    


	maximo_grupos = 2;
	grupo = {(article x14696_7501001133276,1), (article x15500_7501001133290,1), (article x100548_7501001244323,1), (article x95905_7506195120347,1), (article x101562_7506195143353,1), (article x101889_7506295355076,1), (article x104424_7506309805566,1), (article x105172_7506309805610,1), (article x104387_7506309810362,1), (article x106855_7506309894492,1), (article x105121_7506309895222,1), (article x105124_7506309895246,1), (article x105125_7506309895284,1), (article x106857_7506309895307,1), (article x105153_7506309895338,1), (article x105160_7506309895369,1), (article x105164_7506309895383,1), (article x105151_7506309895437,1), (article x106853_7506309897516,1), (article x105156_7506309897547,1), (article x105162_7506309897578,1), (article x105154_7506309897592,1), (article x109922_7506339346039,1), (article x109834_7506339346091,1), (article x108320_7506339393064,1), (article x108330_7506339393088,1)};

	precio_fijo = 99.98; // precio fijo por grupo
	precio_real = $grupo;


	comprados = (purchase ) meet grupo;



	global publicar_0_11 = {};
	global publicar_arts_0_11 = {};

	extern excluidos;
	extern benefits_otorgados_0_11;
	extern beneficiados;


Conditions




	|comprados| == |grupo|;
	precio_fijo >= 999999;


Benefits

	c = maximo_grupos;
	for (a,k) in comprados do
		c = c min &(k / grupo.a);
	od;

	descuento = c * (precio_real - precio_fijo);

	for (a,k) in grupo do
		publicar_0_11 = publicar_0_11 + {(a, descuento * (${(a,k)} / precio_real))};
		publicar_arts_0_11 = publicar_arts_0_11 + {(a,k)};
	od;

	benefits_otorgados_0_11 = \publicar_0_11\ *  1;

	excluidos = excluidos + publicar_arts_0_11;
			// se excluyen los artículos bonificados para que no participen
			// de nuevas promociones.
	

	blog(file, "Promo "++ 599 ++ ": LATENTE");



// FIN DE PROMO 11 del BLOQUE 0

Promotion prepago undo_0_11
Parameters
//	extern excluidos_0;
	extern excluidos;
Conditions
Benefits
//	excluded = excluidos_0;
	excluded = excluidos;
	skip;

// --- *** ---


	    




  // ********  FIN BLOQUE 0 *************************************************************** //
//************** BLOOQUE DE PROMOCIONES ********************
// Seccion Final

Promotion prepago bloque_seccion_final_0

// Description: COMPITEN = false

Parameters

	extern excluir;
		    

	extern excluidos;
	extern beneficiados;
	extern beneficios;
//	extern per;
//	extern tarjeta;

	  
  global promo_activa_0_0 = false;
  extern benefits_otorgados_0_0;
	
					
  global promo_activa_0_1 = false;
  extern benefits_otorgados_0_1;
	
					

	// Parametros correspondientes a la promo grupo_parcial_precio_fijo_ticket_0_1
	extern publicar_0_1;
	extern publicar_arts_0_1;

	// -------------------------------- promo grupo_parcial_precio_fijo_ticket_0_1
  global promo_activa_0_2 = false;
  extern benefits_otorgados_0_2;
	
					

	// Parametros correspondientes a la promo grupo_parcial_precio_fijo_ticket_0_2
	extern publicar_0_2;
	extern publicar_arts_0_2;

	// -------------------------------- promo grupo_parcial_precio_fijo_ticket_0_2
  global promo_activa_0_3 = false;
  extern benefits_otorgados_0_3;
	
					

	// Parametros correspondientes a la promo grupo_completo_precio_fijo_ticket_0_3
	extern publicar_0_3;
	extern publicar_arts_0_3;

	// -------------------------------- promo grupo_completo_precio_fijo_ticket_0_3
  global promo_activa_0_4 = false;
  extern benefits_otorgados_0_4;
	
					

	// Parametros correspondientes a la promo grupo_parcial_precio_fijo_ticket_0_4
	extern publicar_0_4;
	extern publicar_arts_0_4;

	// -------------------------------- promo grupo_parcial_precio_fijo_ticket_0_4
  global promo_activa_0_5 = false;
  extern benefits_otorgados_0_5;
	
					

	// Parametros correspondientes a la promo grupo_parcial_precio_fijo_ticket_0_5
	extern publicar_0_5;
	extern publicar_arts_0_5;

	// -------------------------------- promo grupo_parcial_precio_fijo_ticket_0_5
  global promo_activa_0_6 = false;
  extern benefits_otorgados_0_6;
	
					

	// Parametros correspondientes a la promo grupo_completo_precio_fijo_ticket_0_6
	extern publicar_0_6;
	extern publicar_arts_0_6;

	// -------------------------------- promo grupo_completo_precio_fijo_ticket_0_6
  global promo_activa_0_7 = false;
  extern benefits_otorgados_0_7;
	
					

	// Parametros correspondientes a la promo grupo_parcial_precio_fijo_ticket_0_7
	extern publicar_0_7;
	extern publicar_arts_0_7;

	// -------------------------------- promo grupo_parcial_precio_fijo_ticket_0_7
  global promo_activa_0_8 = false;
  extern benefits_otorgados_0_8;
	
					

	// Parametros correspondientes a la promo grupo_parcial_precio_fijo_ticket_0_8
	extern publicar_0_8;
	extern publicar_arts_0_8;

	// -------------------------------- promo grupo_parcial_precio_fijo_ticket_0_8
  global promo_activa_0_9 = false;
  extern benefits_otorgados_0_9;
	
					

	// Parametros correspondientes a la promo grupo_parcial_precio_fijo_ticket_0_9
	extern publicar_0_9;
	extern publicar_arts_0_9;

	extern descarga_0_9;

	// -------------------------------- promo grupo_parcial_precio_fijo_ticket_0_9
  global promo_activa_0_10 = false;
  extern benefits_otorgados_0_10;
	
					

	// Parametros correspondientes a la promo grupo_parcial_precio_fijo_ticket_0_10
	extern publicar_0_10;
	extern publicar_arts_0_10;

	// -------------------------------- promo grupo_parcial_precio_fijo_ticket_0_10
  global promo_activa_0_11 = false;
  extern benefits_otorgados_0_11;
	
					

	// Parametros correspondientes a la promo grupo_parcial_precio_fijo_ticket_0_11
	extern publicar_0_11;
	extern publicar_arts_0_11;

	// -------------------------------- promo grupo_parcial_precio_fijo_ticket_0_11

Conditions
// -----------------------------------------------

// -----------------------------------------------

Benefits


	// Promos NO Compiten - SUMANDO LOS BENEFICIOS DE TODAS

	benef = 0;
	  

	if benefits_otorgados_0_0 > 0 then
		promo_activa_0_0 = true;
		benef = benef + benefits_otorgados_0_0;
	else skip; fi;
		    

	if benefits_otorgados_0_1 > 0 then
		promo_activa_0_1 = true;
		benef = benef + benefits_otorgados_0_1;
	else skip; fi;
		    

	if benefits_otorgados_0_2 > 0 then
		promo_activa_0_2 = true;
		benef = benef + benefits_otorgados_0_2;
	else skip; fi;
		    

	if benefits_otorgados_0_3 > 0 then
		promo_activa_0_3 = true;
		benef = benef + benefits_otorgados_0_3;
	else skip; fi;
		    

	if benefits_otorgados_0_4 > 0 then
		promo_activa_0_4 = true;
		benef = benef + benefits_otorgados_0_4;
	else skip; fi;
		    

	if benefits_otorgados_0_5 > 0 then
		promo_activa_0_5 = true;
		benef = benef + benefits_otorgados_0_5;
	else skip; fi;
		    

	if benefits_otorgados_0_6 > 0 then
		promo_activa_0_6 = true;
		benef = benef + benefits_otorgados_0_6;
	else skip; fi;
		    

	if benefits_otorgados_0_7 > 0 then
		promo_activa_0_7 = true;
		benef = benef + benefits_otorgados_0_7;
	else skip; fi;
		    

	if benefits_otorgados_0_8 > 0 then
		promo_activa_0_8 = true;
		benef = benef + benefits_otorgados_0_8;
	else skip; fi;
		    

	if benefits_otorgados_0_9 > 0 then
		promo_activa_0_9 = true;
		benef = benef + benefits_otorgados_0_9;
	else skip; fi;
		    

	if benefits_otorgados_0_10 > 0 then
		promo_activa_0_10 = true;
		benef = benef + benefits_otorgados_0_10;
	else skip; fi;
		    

	if benefits_otorgados_0_11 > 0 then
		promo_activa_0_11 = true;
		benef = benef + benefits_otorgados_0_11;
	else skip; fi;
		    
	skip;
	// ----------------------------------------------------
	beneficios = benef;
	// -------------------------------------------------




// Aplicando los Beneficios que vienen por parametro

	if beneficios > 0 then
		excluir = true;
	else
		skip;
	fi;
				  		
// -------------------------------------------------




Promotion prepago fin_bloque_raiz_0
Parameters
			
	// Parametros correspondientes al bloque_0
	extern promo_activa_0_0;
	
					
	extern promo_activa_0_1;
	
					

	// Parametros correspondientes a la promo grupo_parcial_precio_fijo_ticket_0_1
	extern publicar_0_1;
	extern publicar_arts_0_1;

	// -------------------------------- promo grupo_parcial_precio_fijo_ticket_0_1
	extern promo_activa_0_2;
	
					

	// Parametros correspondientes a la promo grupo_parcial_precio_fijo_ticket_0_2
	extern publicar_0_2;
	extern publicar_arts_0_2;

	// -------------------------------- promo grupo_parcial_precio_fijo_ticket_0_2
	extern promo_activa_0_3;
	
					

	// Parametros correspondientes a la promo grupo_completo_precio_fijo_ticket_0_3
	extern publicar_0_3;
	extern publicar_arts_0_3;

	// -------------------------------- promo grupo_completo_precio_fijo_ticket_0_3
	extern promo_activa_0_4;
	
					

	// Parametros correspondientes a la promo grupo_parcial_precio_fijo_ticket_0_4
	extern publicar_0_4;
	extern publicar_arts_0_4;

	// -------------------------------- promo grupo_parcial_precio_fijo_ticket_0_4
	extern promo_activa_0_5;
	
					

	// Parametros correspondientes a la promo grupo_parcial_precio_fijo_ticket_0_5
	extern publicar_0_5;
	extern publicar_arts_0_5;

	// -------------------------------- promo grupo_parcial_precio_fijo_ticket_0_5
	extern promo_activa_0_6;
	
					

	// Parametros correspondientes a la promo grupo_completo_precio_fijo_ticket_0_6
	extern publicar_0_6;
	extern publicar_arts_0_6;

	// -------------------------------- promo grupo_completo_precio_fijo_ticket_0_6
	extern promo_activa_0_7;
	
					

	// Parametros correspondientes a la promo grupo_parcial_precio_fijo_ticket_0_7
	extern publicar_0_7;
	extern publicar_arts_0_7;

	// -------------------------------- promo grupo_parcial_precio_fijo_ticket_0_7
	extern promo_activa_0_8;
	
					

	// Parametros correspondientes a la promo grupo_parcial_precio_fijo_ticket_0_8
	extern publicar_0_8;
	extern publicar_arts_0_8;

	// -------------------------------- promo grupo_parcial_precio_fijo_ticket_0_8
	extern promo_activa_0_9;
	
					

	// Parametros correspondientes a la promo grupo_parcial_precio_fijo_ticket_0_9
	extern publicar_0_9;
	extern publicar_arts_0_9;

	extern descarga_0_9;

	// -------------------------------- promo grupo_parcial_precio_fijo_ticket_0_9
	extern promo_activa_0_10;
	
					

	// Parametros correspondientes a la promo grupo_parcial_precio_fijo_ticket_0_10
	extern publicar_0_10;
	extern publicar_arts_0_10;

	// -------------------------------- promo grupo_parcial_precio_fijo_ticket_0_10
	extern promo_activa_0_11;
	
					

	// Parametros correspondientes a la promo grupo_parcial_precio_fijo_ticket_0_11
	extern publicar_0_11;
	extern publicar_arts_0_11;

	// -------------------------------- promo grupo_parcial_precio_fijo_ticket_0_11

	  

	// ---------- ---------------- -- bloque_0
	extern per;
	extern tarjeta;
//	extern dinero_ahorrado;

Benefits
			
	// Aplicando Beneficios correspondientes al bloque_0

// BENEFICIO -> COMPETENCIA = false
// Aplicando TODOS los Beneficios

		  
	if promo_activa_0_0 then
					
					
		skip;
	else skip; fi;

	    
	if promo_activa_0_1 then
					
					

	// Aplicando Beneficios correspondientes a la grupo_parcial_precio_fijo_ticket_0_1

	// Informando eventos

	rec(promoId, 579, "200");


	for (a,k) in publicar_0_1 do
		mc = {(a, publicar_arts_0_1.a)};
		credit(a, k, mc);
		rec(artsBeneficiary, mc);
		rec(creditBenefit, k);
	od;



	blog(file, "Promo "++ 579 ++ ": ACTIVA");




	// ------------------------------------------ promo grupo_parcial_precio_fijo_ticket_0_1
		skip;
	else skip; fi;

	    
	if promo_activa_0_2 then
					
					

	// Aplicando Beneficios correspondientes a la grupo_parcial_precio_fijo_ticket_0_2

	// Informando eventos

	rec(promoId, 585, "200");


	for (a,k) in publicar_0_2 do
		mc = {(a, publicar_arts_0_2.a)};
		credit(a, k, mc);
		rec(artsBeneficiary, mc);
		rec(creditBenefit, k);
	od;



	blog(file, "Promo "++ 585 ++ ": ACTIVA");




	// ------------------------------------------ promo grupo_parcial_precio_fijo_ticket_0_2
		skip;
	else skip; fi;

	    
	if promo_activa_0_3 then
					
					

	// Aplicando Beneficios correspondientes a la grupo_completo_precio_fijo_ticket_0_3

	// Informando eventos

	rec(promoId, 587, "200");


	for (a,k) in publicar_0_3 do
		mc = {(a, publicar_arts_0_3.a)};
		credit(a, k, mc);
		rec(artsBeneficiary, mc);
		rec(creditBenefit, k);
	od;



	blog(file, "Promo "++ 587 ++ ": ACTIVA");


	print("&Promo Sancor Chocolatada");



	// ------------------------------------------ promo grupo_completo_precio_fijo_ticket_0_3
		skip;
	else skip; fi;

	    
	if promo_activa_0_4 then
					
					

	// Aplicando Beneficios correspondientes a la grupo_parcial_precio_fijo_ticket_0_4

	// Informando eventos

	rec(promoId, 588, "200");


	for (a,k) in publicar_0_4 do
		mc = {(a, publicar_arts_0_4.a)};
		credit(a, k, mc);
		rec(artsBeneficiary, mc);
		rec(creditBenefit, k);
	od;



	blog(file, "Promo "++ 588 ++ ": ACTIVA");




	// ------------------------------------------ promo grupo_parcial_precio_fijo_ticket_0_4
		skip;
	else skip; fi;

	    
	if promo_activa_0_5 then
					
					

	// Aplicando Beneficios correspondientes a la grupo_parcial_precio_fijo_ticket_0_5

	// Informando eventos

	rec(promoId, 592, "200");


	for (a,k) in publicar_0_5 do
		mc = {(a, publicar_arts_0_5.a)};
		credit(a, k, mc);
		rec(artsBeneficiary, mc);
		rec(creditBenefit, k);
	od;



	blog(file, "Promo "++ 592 ++ ": ACTIVA");




	// ------------------------------------------ promo grupo_parcial_precio_fijo_ticket_0_5
		skip;
	else skip; fi;

	    
	if promo_activa_0_6 then
					
					

	// Aplicando Beneficios correspondientes a la grupo_completo_precio_fijo_ticket_0_6

	// Informando eventos

	rec(promoId, 593, "200");


	for (a,k) in publicar_0_6 do
		mc = {(a, publicar_arts_0_6.a)};
		credit(a, k, mc);
		rec(artsBeneficiary, mc);
		rec(creditBenefit, k);
	od;



	blog(file, "Promo "++ 593 ++ ": ACTIVA");


	print("&Promo YOGS CEREAL");



	// ------------------------------------------ promo grupo_completo_precio_fijo_ticket_0_6
		skip;
	else skip; fi;

	    
	if promo_activa_0_7 then
					
					

	// Aplicando Beneficios correspondientes a la grupo_parcial_precio_fijo_ticket_0_7

	// Informando eventos

	rec(promoId, 594, "200");


	for (a,k) in publicar_0_7 do
		mc = {(a, publicar_arts_0_7.a)};
		credit(a, k, mc);
		rec(artsBeneficiary, mc);
		rec(creditBenefit, k);
	od;



	blog(file, "Promo "++ 594 ++ ": ACTIVA");




	// ------------------------------------------ promo grupo_parcial_precio_fijo_ticket_0_7
		skip;
	else skip; fi;

	    
	if promo_activa_0_8 then
					
					

	// Aplicando Beneficios correspondientes a la grupo_parcial_precio_fijo_ticket_0_8

	// Informando eventos

	rec(promoId, 595, "200");


	for (a,k) in publicar_0_8 do
		mc = {(a, publicar_arts_0_8.a)};
		credit(a, k, mc);
		rec(artsBeneficiary, mc);
		rec(creditBenefit, k);
	od;



	blog(file, "Promo "++ 595 ++ ": ACTIVA");




	// ------------------------------------------ promo grupo_parcial_precio_fijo_ticket_0_8
		skip;
	else skip; fi;

	    
	if promo_activa_0_9 then
					
					

	// Aplicando Beneficios correspondientes a la grupo_parcial_precio_fijo_ticket_0_9

	// Informando eventos

	rec(promoId, 597, "200");


	credit(descarga_0_9,\publicar_0_9\,publicar_arts_0_9);
	for (a,k) in publicar_0_9 do
		mc = {(a, publicar_arts_0_9.a)};
		rec(artsBeneficiary, mc);
		rec(creditBenefit, k);
	od;



	blog(file, "Promo "++ 597 ++ ": ACTIVA");




	// ------------------------------------------ promo grupo_parcial_precio_fijo_ticket_0_9
		skip;
	else skip; fi;

	    
	if promo_activa_0_10 then
					
					

	// Aplicando Beneficios correspondientes a la grupo_parcial_precio_fijo_ticket_0_10

	// Informando eventos

	rec(promoId, 598, "200");


	for (a,k) in publicar_0_10 do
		mc = {(a, publicar_arts_0_10.a)};
		credit(a, k, mc);
		rec(artsBeneficiary, mc);
		rec(creditBenefit, k);
	od;



	blog(file, "Promo "++ 598 ++ ": ACTIVA");




	// ------------------------------------------ promo grupo_parcial_precio_fijo_ticket_0_10
		skip;
	else skip; fi;

	    
	if promo_activa_0_11 then
					
					

	// Aplicando Beneficios correspondientes a la grupo_parcial_precio_fijo_ticket_0_11

	// Informando eventos

	rec(promoId, 599, "200");


	for (a,k) in publicar_0_11 do
		mc = {(a, publicar_arts_0_11.a)};
		credit(a, k, mc);
		rec(artsBeneficiary, mc);
		rec(creditBenefit, k);
	od;



	blog(file, "Promo "++ 599 ++ ": ACTIVA");




	// ------------------------------------------ promo grupo_parcial_precio_fijo_ticket_0_11
		skip;
	else skip; fi;

	    
// -----------------------------


	// --------- ---------- ---------------- -- bloque_0
//	Si se acumularon descuentos, se imprime el mensaje apropiado.
//	if dinero_ahorrado > 0 then
//		if log(total, "REBAJADO UD. AHORRO $"++dinero_ahorrado) then skip; else skip; fi;
//	else skip; fi;
	

	skip;

		

  // ******  INICIO BLOQUE **************************************************************** //
  // ******  BLOQUE NAME = Bloque POSTPAGO
  // ******  BLOQUE ID = 1
  // ******  CASH2BENEF =  1


//************** BLOQUE DE PROMOCIONES ********************
// Seccion Inicial

Promotion postpago bloque_seccion_inicial_1

// Description:

Parameters

	extern excluir;
		    
	extern excluidos;
	extern beneficios;
	extern beneficiados;
	global excluir_1 = false;


	  

Conditions


Benefits

	  

  skip;


  // ********  PROMOCIONES DEL BLOQUE 1 **************************************************** //


	  




  // ********  FIN BLOQUE 1 *************************************************************** //
//************** BLOOQUE DE PROMOCIONES ********************
// Seccion Final

Promotion postpago bloque_seccion_final_1

// Description: COMPITEN = false

Parameters

	extern excluir;
		    

	extern excluidos;
	extern beneficiados;
	extern beneficios;
//	extern per;
//	extern tarjeta;

	  

Conditions
// -----------------------------------------------

// -----------------------------------------------

Benefits


	// Promos NO Compiten - SUMANDO LOS BENEFICIOS DE TODAS

	benef = 0;
	  
	skip;
	// ----------------------------------------------------
	beneficios = benef;
	// -------------------------------------------------




// Aplicando los Beneficios que vienen por parametro

	if beneficios > 0 then
		excluir = true;
	else
		skip;
	fi;
				  		
// -------------------------------------------------




Promotion postpago fin_bloque_raiz_1
Parameters
			
	// Parametros correspondientes al bloque_1

	  

	// ---------- ---------------- -- bloque_1
	extern per;
	extern tarjeta;
//	extern dinero_ahorrado;

Benefits
			
	// Aplicando Beneficios correspondientes al bloque_1

// BENEFICIO -> COMPETENCIA = false
// Aplicando TODOS los Beneficios

		  
// -----------------------------


	// --------- ---------- ---------------- -- bloque_1

	skip;

		