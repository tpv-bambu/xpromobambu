
###### false

#
# Script de Consulta de Promociones generado con Versión de Plantillas Nro: 1.2.009 - (22/02/2011)
#

# Vaciado de Tablas
TRUNCATE TABLE promos_participantes;
TRUNCATE TABLE promos_vigencia;

	
# Promoción  - global_config
# Condiciones de Vigencia.
		
		
# Articulos Participantes.
		
# Articulos Beneficiados.		
		
# Departamentos Participantes.
		

# Clasificaciones Participantes.
		
# Promoción 579 - grupo_parcial_precio_fijo_ticket
# Condiciones de Vigencia.
		
INSERT INTO promos_vigencia (cod_promo, nombre, descripcion, rango_hora, hora_desde, hora_hasta, rango_fecha, fecha_desde, fecha_hasta, tipo, dias, domingo, lunes, martes, miercoles, jueves, viernes, sabado, perfil, rango_caja, caja_desde, caja_hasta, rango_tarjeta, tarjeta_desde, tarjeta_hasta, afinidad, detalle, info) VALUES("579", "grupo_parcial_precio_fijo_ticket", "YOGS LIGHT FRUTADO", "0", "00:00:00", "00:00:00", "1", "2016/01/28", "2016/02/03", "1", "0", "", "", "", "", "", "", "", "0", "0", "0", "0", "0", "0", "0", "0", "2;12.49;PROMOCION YOGS LIGHT FRUTADO\n", "Desde el 28/01/2016 hasta el 03/02/2016.\n");

INSERT INTO promos_participantes (cod_promo, cod_perfil) VALUES("579", "0");
		
		
# Articulos Participantes.
		
INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("579", "111269", "7790080065312");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("579", "111627", "7790080065299");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("579", "110967", "7790080065305");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("579", "111184", "7790080065282");

# Articulos Beneficiados.		
		
# Departamentos Participantes.
		

# Clasificaciones Participantes.
		
# Promoción 585 - grupo_parcial_precio_fijo_ticket
# Condiciones de Vigencia.
		
INSERT INTO promos_vigencia (cod_promo, nombre, descripcion, rango_hora, hora_desde, hora_hasta, rango_fecha, fecha_desde, fecha_hasta, tipo, dias, domingo, lunes, martes, miercoles, jueves, viernes, sabado, perfil, rango_caja, caja_desde, caja_hasta, rango_tarjeta, tarjeta_desde, tarjeta_hasta, afinidad, detalle, info) VALUES("585", "grupo_parcial_precio_fijo_ticket", "SANCOR BIO 185GR", "0", "00:00:00", "00:00:00", "1", "2016/02/04", "2016/02/10", "1", "0", "", "", "", "", "", "", "", "0", "0", "0", "0", "0", "0", "0", "0", "2;10.79;PROMOCION SANCOR BIO\n", "Desde el 04/02/2016 hasta el 10/02/2016.\n");

INSERT INTO promos_participantes (cod_promo, cod_perfil) VALUES("585", "0");
		
		
# Articulos Participantes.
		
INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("585", "109372", "7790080068405");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("585", "16660", "7790080068412");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("585", "109125", "7790080068429");

# Articulos Beneficiados.		
		
# Departamentos Participantes.
		

# Clasificaciones Participantes.
		
# Promoción 587 - grupo_completo_precio_fijo_ticket
# Condiciones de Vigencia.
		
INSERT INTO promos_vigencia (cod_promo, nombre, descripcion, rango_hora, hora_desde, hora_hasta, rango_fecha, fecha_desde, fecha_hasta, tipo, dias, domingo, lunes, martes, miercoles, jueves, viernes, sabado, perfil, rango_caja, caja_desde, caja_hasta, rango_tarjeta, tarjeta_desde, tarjeta_hasta, afinidad, detalle, info) VALUES("587", "grupo_completo_precio_fijo_ticket", "Chocolatada Sancor 1L", "0", "00:00:00", "00:00:00", "1", "2016/01/28", "2016/02/03", "1", "0", "", "", "", "", "", "", "", "0", "0", "0", "0", "0", "0", "0", "0", "", "Desde el 28/01/2016 hasta el 03/02/2016.\n");

INSERT INTO promos_participantes (cod_promo, cod_perfil) VALUES("587", "0");
		
		
# Articulos Participantes.
		
INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("587", "102429", "7790080037401");

# Articulos Beneficiados.		
		
# Departamentos Participantes.
		

# Clasificaciones Participantes.
		
# Promoción 588 - grupo_parcial_precio_fijo_ticket
# Condiciones de Vigencia.
		
INSERT INTO promos_vigencia (cod_promo, nombre, descripcion, rango_hora, hora_desde, hora_hasta, rango_fecha, fecha_desde, fecha_hasta, tipo, dias, domingo, lunes, martes, miercoles, jueves, viernes, sabado, perfil, rango_caja, caja_desde, caja_hasta, rango_tarjeta, tarjeta_desde, tarjeta_hasta, afinidad, detalle, info) VALUES("588", "grupo_parcial_precio_fijo_ticket", "Sancor yogs cremoso entero Pack X2", "0", "00:00:00", "00:00:00", "1", "2016/01/28", "2016/02/03", "1", "0", "", "", "", "", "", "", "", "0", "0", "0", "0", "0", "0", "0", "0", "2;9.29;PROMOCION YOGS CREMOSO\n", "Desde el 28/01/2016 hasta el 03/02/2016.\n");

INSERT INTO promos_participantes (cod_promo, cod_perfil) VALUES("588", "0");
		
		
# Articulos Participantes.
		
INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("588", "109389", "7790080068443");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("588", "109390", "7790080068436");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("588", "109391", "7790080068467");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("588", "109392", "7790080067835");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("588", "109393", "7790080068450");

# Articulos Beneficiados.		
		
# Departamentos Participantes.
		

# Clasificaciones Participantes.
		
# Promoción 592 - grupo_parcial_precio_fijo_ticket
# Condiciones de Vigencia.
		
INSERT INTO promos_vigencia (cod_promo, nombre, descripcion, rango_hora, hora_desde, hora_hasta, rango_fecha, fecha_desde, fecha_hasta, tipo, dias, domingo, lunes, martes, miercoles, jueves, viernes, sabado, perfil, rango_caja, caja_desde, caja_hasta, rango_tarjeta, tarjeta_desde, tarjeta_hasta, afinidad, detalle, info) VALUES("592", "grupo_parcial_precio_fijo_ticket", "YOGS LIGHT BEBIBLE 185GR", "0", "00:00:00", "00:00:00", "1", "2016/01/28", "2016/02/03", "1", "0", "", "", "", "", "", "", "", "0", "0", "0", "0", "0", "0", "0", "0", "2;9.29;PROMOCION YOGS BEBIBLE\n", "Desde el 28/01/2016 hasta el 03/02/2016.\n");

INSERT INTO promos_participantes (cod_promo, cod_perfil) VALUES("592", "0");
		
		
# Articulos Participantes.
		
INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("592", "109379", "7790080067873");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("592", "109378", "7790080067958");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("592", "109380", "7790080067965");

# Articulos Beneficiados.		
		
# Departamentos Participantes.
		

# Clasificaciones Participantes.
		
# Promoción 593 - grupo_completo_precio_fijo_ticket
# Condiciones de Vigencia.
		
INSERT INTO promos_vigencia (cod_promo, nombre, descripcion, rango_hora, hora_desde, hora_hasta, rango_fecha, fecha_desde, fecha_hasta, tipo, dias, domingo, lunes, martes, miercoles, jueves, viernes, sabado, perfil, rango_caja, caja_desde, caja_hasta, rango_tarjeta, tarjeta_desde, tarjeta_hasta, afinidad, detalle, info) VALUES("593", "grupo_completo_precio_fijo_ticket", "YOGS CEREARL ENTERO 165Gr", "0", "00:00:00", "00:00:00", "1", "2016/01/28", "2016/02/03", "1", "0", "", "", "", "", "", "", "", "0", "0", "0", "0", "0", "0", "0", "0", "", "Desde el 28/01/2016 hasta el 03/02/2016.\n");

INSERT INTO promos_participantes (cod_promo, cod_perfil) VALUES("593", "0");
		
		
# Articulos Participantes.
		
INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("593", "108644", "7790080067903");

# Articulos Beneficiados.		
		
# Departamentos Participantes.
		

# Clasificaciones Participantes.
		
# Promoción 594 - grupo_parcial_precio_fijo_ticket
# Condiciones de Vigencia.
		
INSERT INTO promos_vigencia (cod_promo, nombre, descripcion, rango_hora, hora_desde, hora_hasta, rango_fecha, fecha_desde, fecha_hasta, tipo, dias, domingo, lunes, martes, miercoles, jueves, viernes, sabado, perfil, rango_caja, caja_desde, caja_hasta, rango_tarjeta, tarjeta_desde, tarjeta_hasta, afinidad, detalle, info) VALUES("594", "grupo_parcial_precio_fijo_ticket", "ALIMENTO JUNIOR 1 LTS", "0", "00:00:00", "00:00:00", "1", "2016/02/01", "2016/02/03", "1", "0", "", "", "", "", "", "", "", "0", "0", "0", "0", "0", "0", "0", "0", "2;7.99;PROMOCION JUNIOR BEBIBLE\n", "Desde el 01/02/2016 hasta el 03/02/2016.\n");

INSERT INTO promos_participantes (cod_promo, cod_perfil) VALUES("594", "0");
		
		
# Articulos Participantes.
		
INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("594", "16885", "7793940651021");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("594", "16889", "7793940651106");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("594", "17183", "7793940651113");

# Articulos Beneficiados.		
		
# Departamentos Participantes.
		

# Clasificaciones Participantes.
		
# Promoción 595 - grupo_parcial_precio_fijo_ticket
# Condiciones de Vigencia.
		
INSERT INTO promos_vigencia (cod_promo, nombre, descripcion, rango_hora, hora_desde, hora_hasta, rango_fecha, fecha_desde, fecha_hasta, tipo, dias, domingo, lunes, martes, miercoles, jueves, viernes, sabado, perfil, rango_caja, caja_desde, caja_hasta, rango_tarjeta, tarjeta_desde, tarjeta_hasta, afinidad, detalle, info) VALUES("595", "grupo_parcial_precio_fijo_ticket", "LEVITE S/GAS 2250 CCa", "0", "00:00:00", "00:00:00", "1", "2016/02/04", "2016/02/10", "1", "0", "", "", "", "", "", "", "", "0", "0", "0", "0", "0", "0", "0", "0", "2;19.99;PROMOCION LEVITE 2250CC\n", "Desde el 04/02/2016 hasta el 10/02/2016.\n");

INSERT INTO promos_participantes (cod_promo, cod_perfil) VALUES("595", "0");
		
		
# Articulos Participantes.
		
INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("595", "101161", "7790315000880");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("595", "97159", "7798062540284");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("595", "97155", "7798062540291");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("595", "107653", "7798062540734");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("595", "101162", "7798062541014");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("595", "104720", "7798062541700");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("595", "104721", "7798062541731");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("595", "104182", "7798062541762");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("595", "107447", "7798062541878");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("595", "109740", "7798062542356");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("595", "109744", "7798062542370");

# Articulos Beneficiados.		
		
# Departamentos Participantes.
		

# Clasificaciones Participantes.
		
# Promoción 597 - grupo_parcial_precio_fijo_ticket
# Condiciones de Vigencia.
		
INSERT INTO promos_vigencia (cod_promo, nombre, descripcion, rango_hora, hora_desde, hora_hasta, rango_fecha, fecha_desde, fecha_hasta, tipo, dias, domingo, lunes, martes, miercoles, jueves, viernes, sabado, perfil, rango_caja, caja_desde, caja_hasta, rango_tarjeta, tarjeta_desde, tarjeta_hasta, afinidad, detalle, info) VALUES("597", "grupo_parcial_precio_fijo_ticket", "YOGUR SANCOR FIRME X190", "0", "00:00:00", "00:00:00", "1", "2016/02/04", "2016/02/10", "1", "0", "", "", "", "", "", "", "", "0", "0", "0", "0", "0", "0", "0", "0", "2;7.69;PROMOCION SANCOR YOG FIRME\n", "Desde el 04/02/2016 hasta el 10/02/2016.\n");

INSERT INTO promos_participantes (cod_promo, cod_perfil) VALUES("597", "0");
		
		
# Articulos Participantes.
		
INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("597", "102059", "7790080065152");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("597", "102087", "7790080065169");

# Articulos Beneficiados.		
		
# Departamentos Participantes.
		

# Clasificaciones Participantes.
		
# Promoción 598 - grupo_parcial_precio_fijo_ticket
# Condiciones de Vigencia.
		
INSERT INTO promos_vigencia (cod_promo, nombre, descripcion, rango_hora, hora_desde, hora_hasta, rango_fecha, fecha_desde, fecha_hasta, tipo, dias, domingo, lunes, martes, miercoles, jueves, viernes, sabado, perfil, rango_caja, caja_desde, caja_hasta, rango_tarjeta, tarjeta_desde, tarjeta_hasta, afinidad, detalle, info) VALUES("598", "grupo_parcial_precio_fijo_ticket", "SANCOR PRIMEROS SABORES", "0", "00:00:00", "00:00:00", "1", "2016/02/04", "2016/02/10", "1", "0", "", "", "", "", "", "", "", "0", "0", "0", "0", "0", "0", "0", "0", "2;11.29;PROMOCION SANCOR PRIMEROS SABORE\n", "Desde el 04/02/2016 hasta el 10/02/2016.\n");

INSERT INTO promos_participantes (cod_promo, cod_perfil) VALUES("598", "0");
		
		
# Articulos Participantes.
		
INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("598", "109395", "7790080068511");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("598", "109394", "7790080068528");

# Articulos Beneficiados.		
		
# Departamentos Participantes.
		

# Clasificaciones Participantes.
		
# Promoción 599 - grupo_parcial_precio_fijo_ticket
# Condiciones de Vigencia.
		
INSERT INTO promos_vigencia (cod_promo, nombre, descripcion, rango_hora, hora_desde, hora_hasta, rango_fecha, fecha_desde, fecha_hasta, tipo, dias, domingo, lunes, martes, miercoles, jueves, viernes, sabado, perfil, rango_caja, caja_desde, caja_hasta, rango_tarjeta, tarjeta_desde, tarjeta_hasta, afinidad, detalle, info) VALUES("599", "grupo_parcial_precio_fijo_ticket", "PROMO HEAD AND SHOULDERS", "0", "00:00:00", "00:00:00", "1", "2016/02/04", "2016/02/10", "1", "0", "", "", "", "", "", "", "", "0", "0", "0", "0", "0", "0", "0", "0", "2;49.99;PROMOCION HYS\n", "Desde el 04/02/2016 hasta el 10/02/2016.\n");

INSERT INTO promos_participantes (cod_promo, cod_perfil) VALUES("599", "0");
		
		
# Articulos Participantes.
		
INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("599", "14696", "7501001133276");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("599", "15500", "7501001133290");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("599", "100548", "7501001244323");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("599", "95905", "7506195120347");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("599", "101562", "7506195143353");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("599", "101889", "7506295355076");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("599", "104424", "7506309805566");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("599", "105172", "7506309805610");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("599", "104387", "7506309810362");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("599", "106855", "7506309894492");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("599", "105121", "7506309895222");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("599", "105124", "7506309895246");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("599", "105125", "7506309895284");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("599", "106857", "7506309895307");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("599", "105153", "7506309895338");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("599", "105160", "7506309895369");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("599", "105164", "7506309895383");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("599", "105151", "7506309895437");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("599", "106853", "7506309897516");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("599", "105156", "7506309897547");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("599", "105162", "7506309897578");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("599", "105154", "7506309897592");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("599", "109922", "7506339346039");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("599", "109834", "7506339346091");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("599", "108320", "7506339393064");

INSERT INTO promos_participantes (cod_promo, cod_art_int, cod_art_bar) VALUES("599", "108330", "7506339393088");

# Articulos Beneficiados.		
		
# Departamentos Participantes.
		

# Clasificaciones Participantes.
		