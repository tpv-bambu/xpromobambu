#!/bin/bash
# run.sh
# Aplicar transformaciones XLST a los datos XML para generar el programa de promociones PRO
# y luego compilarlo para generar el binario COD.

localpath=common
pcpath=pc

# Eliminar el viejo promo.pro 
if [ -e promo.pro ] 
then
    rm -f promo.pro
fi

# Eliminar el viejo promo.cod
if [ -e promo.cod ] 
then
    rm -f promo.cod
fi

# Aplicar las transformaciones XSLT para generar el nuevo promo.pro
xsltproc --stringparam pathProgXml ../prog.xml -o promo.pro $localpath/prog.xsl $localpath/tree.xml

# Compilar el nuevo promo.pro para generar el nuevo promo.cod
if [ -e promo.pro ] 
then
    $pcpath/pc promo.pro -o promo.cod
fi

#sleep 2s

exit 0
